# Organisation de courses et d'événements

## 1. Code QR pour installer l'application et lancer une course

> #### Astuce
>
> Le jour de l'événement, un code QR de la course permet aux concurrents d'installer l'application sur leur téléphone (Android ou iOS). Une fois l'application chargée, le même code QR permet de positionner automatiquement l'application sur la course.

[Vidéo de démonstration](https://youtu.be/E9kXqeCuYS8)

Après la création d'un parcours, la page ["Mes parcours"](https://vikazimut.vikazim.fr/web/#/planner/courses/-1) affiche
un bouton "Codes QR" pour chaque parcours.
Le code QR de la course permet d'installer l'application à partir de n'importe quel lecteur de code QR du téléphone.

Une fois l'application chargée sur le téléphone, en rescannant le même code QR à partir du lecteur de code QR
ou à partir du bouton "Flasher un parcours" de l'application, la course se lance automatiquement.
Si lors de la création du parcours, il a été configuré pour un mode (sport/ludique),
un ordre de validation des postes (imposé/libre) et un mode de validation des postes (GPS / code QR / iBeacons),
la course se lance automatiquement avec ces caractéristiques.

## 2. La charte graphique

L'archive suivante fournit les images, logos et posters de la charte graphique de Vikazimut
que vous pouvez utiliser pour vos affichages : [Charte graphique](https://vikazimut.vikazim.fr/public/docs/graphic-charter.zip)

## 3. Organisation d'événements

Un événement permet de combiner plusieurs parcours avec un classement des participants
basé sur trois critères possibles : temps cumulé, championnat à étape ou courses aux points.

Les orienteurs inscrits à l’événement n'ont plus qu'à pousser leur résultat
à l’issue d'un parcours à partir de l’application.
Leur résultat est automatiquement intégré à l’événement et le classement général est actualisé.

> #### Limite
>
> Le nombre de participants est limité à 2000 (limite extensible sur demande).

### 3.1 Types d’événement et pénalités

La construction d’un événement consiste à choisir le type avec les pénalités, ajouter les parcours
et inscrire les participants.
   
Il y a trois types d’événement.
   
#### 1/ Championnat

Le classement est fait sur le score.
Le meilleur score est attribué au meilleur temps. Il reçoit 1000 points.
Les autres reçoivent une valeur proportionnelle à l’écart de temps avec le meilleur temps :

    score = 1000 * meilleur temps / temps

Le temps effectif en millisecondes de chaque participant tient compte des deux types de
pénalité donnés en secondes : pénalité par poste manquant et pénalité par minute entamée.

    temps (ms) = temps total
             + nombre de postes manquants * pénalité en secondes par poste manquant * 1000
             + nombre de minutes entamées * pénalité en secondes par minute entamée * 1000

#### 2/ Classement aux points

Le classement est fait sur le score.
Le score est calculé à partir de la valeur attribuée à chaque balise validée auquel
on soustrait le total des pénalités données en points :

    score = nombre de points validés
            - nombre de postes manquants * pénalité en points par poste manquant
            - nombre de minutes entamées * pénalité en points par minute entamée

#### 3/ Classement au temps cumulé

Le classement est fait à partir du temps cumulé en millisecondes, corrigé par les pénalités
données en secondes :

    Temps effectif (ms) = temps total
        + nombre de postes manquants * pénalité en secondes par poste manquant * 1000
        + nombre de minutes entamées * pénalité en secondes par minute entamée * 1000

### 3.2 Gérer les courses

Seules les courses appartenant au traceur organisateur sont intégrables à un événement.

### 3.3 Gérer les participants

Les participants sont identifiés par un pseudonyme.
Il est donc impératif que le pseudonyme d’inscription soit le même que celui qui sera utilisé
lors de l’envoi du résultat par l’application à la fin de la course.
À défaut, l’organisateur de l’événement devra faire les correspondances à la main.

Il y a quatre façons d’ajouter un participant à un événement :

1. Le participant s’inscrit lui-même sur la page de l’événement.
2. L’organisateur inscrit individuellement les orienteurs.
3. L’organisateur inscrit les orienteurs à partir d’un fichier texte contenant la liste 
des pseudonymes, un pseudonyme par ligne.
4. L’organisateur inscrit les orienteurs à partir des pseudonymes des traces d’une course
de l’événement qui a déjà eu lieu.

À l’ajout d’un participant le classement est automatiquement actualisé si le traceur a déjà réalisé
les courses de l'événement.

### 3.4 Modifier les pénalités

L’organisateur de l’événement a la possibilité de faire des corrections à la main :

1. Si le nom du participant donné depuis l’application diffère de celui de l’inscription,
l’organisateur peut soit changer le nom du participant soit récupérer la bonne trace dans une
liste déroulante.
2. L'organisateur peut corriger les pénalités attribuées automatiquement, changer le nombre
de balises manquantes et le nombre de minutes de dépassement.
