# Map georeferencing

This section provides two tutorials for creating and georeferencing maps.

## Create a map with OpenOrienteering Mapper

[OpenOrienteering Mapper](https://www.openorienteering.org/) is a free software for creating base maps and courses.

- [Access to the tutorial (in French)](https://vikazim.fr/wp-content/uploads/2020/12/Tuto-Georeferencement-Carte-Methode-SL-JV-OOMapper.pdf) (Credit Jérôme Vialard)

## Create a course with Purple Pen

[Purple Pen](https://purplepen.golde.org/) is free software for creating courses.

- [Access to the Tutorial (in French)](https://vikazim.fr/wp-content/uploads/2020/12/Creation-de-Parcours-sur-Vikazimut-EP-20201201.pdf) (Credit Jérôme Vialard & Eric Pigeon)

## Example of valid XML and KML files

Here is an example of the two files required to build a course. Both files were generated with OCAD.

- [51.xml](https://vikazimut.vikazim.fr/public/docs/51.xml)
- [51.kml](https://vikazimut.vikazim.fr/public/docs/51.kml)
