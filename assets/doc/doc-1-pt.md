# Tourist route

The tourist routes can be used with the walking and playful modes of the app.
A tourist route allows the display of multimedia content in an app window after the validation of a checkpoint.
It is therefore necessary that the course be in a geographical area with access to the mobile Internet network.

A tourist route is a good candidate for a permanent route with a QR code at the start that will launch directly
the course with the walk mode. The QR code is to be retrieved on the page of
[course creation](/#/planner/courses/-1) of planner mode.

> #### Examples
> 1. Examples of multimedia content in the three [permanent Bayeux tourist routes](https://view.genial.ly/60880167e8077b0d2f5920ff/interactive-image-le-bajo)
> 2. Video of installation and use of the app in fun walk mode [(part 1)](https://youtu.be/WqBuDK0QC5U) and [(part 2)](https://youtu.be/B3cIzl03uIg) (Credit Luc Di Pol)

### List of tasks to complete a tourist route:

1. Create the HTML pages for each checkpoint with a multimedia content.
To build an HTML page, a basic text editor (eg. Notepad of Windows) is sufficient.
Writing an HTML page is quite easy, especially using the page templates we provide below.
2. Publish the pages to a web server (that of the club, the city, the high school, the university or a free or paid site provider
3. Modify the XML file of the course to indicate which url to display at each checkpoint.
4. Test your route with the application from home.

> #### Tip
> You can try the example of the [Bayeux DDAY](https://vikazimut.vikazim.fr/web/#/course/info/619) route from your home with a mobile phone or tablet.
> In the Vikazimut application, select the walk mode then the “Bayeux DDAY” route in the list of courses with GPS validation. Then just force the validation of each checkpoint with the kebab menu (icon with three dots at the top right of the screen) to see the content page appear.

## Write a multimedia HTML page

A tourist HTML page is a page
[Classic HTML](https://developer.mozilla.org/en/docs/Learn/HTML/Introduction_to_HTML).
You can insert text, images, videos and sound.

The HTML code below is an example of a multimedia page with text, image and sound. This
[link](https://vikazimut.lescigales.org/BayeuxDDAY/35/poste35DDAY.html)
shows the rendering of the page below.

```
<html>
<head>
       <meta charset="UTF-8">
       <!-- This line allows to have a responsive page -->
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <style>
           html {
               font-size: 14pt;
           }
       </style>
</head>

<body>
<!-- The page title -->
<h1>Soldatenheim, Hotel du Luxembourg</h1>

<!-- An image -->
<p>
      <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
</p>

<!-- A paragraph of text -->
<p>
       The Hôtel du Luxembourg is requisitioned by the occupying German troops.
       The establishment is converted into a Soldatenheim, reserved for the relaxation of Wehrmacht troops.
       Located opposite the theatre, the soldiers also benefited from a casino and the Modern Cinema rue des Bouchers.
       There they met the Bayeus youth who often showed themselves to be hostile.
</p>

<!-- A soundtrack -->
<p style="text-align:center">
       <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35.mp3"></audio>
</p>
</body>
</html>
```

To make your job easier, download the archive of HTML file templates at the end of the section.
All you have to do is insert your texts, images, sound or video files.

## Write a multilingual HTML page

HTML pages can contain different language versions of text, video or soundtrack.
Each version is put in a frame enclosed by the tags
`<div> </div>`:
`<div style="display:none;" lang="??">`
text in the chosen language
`</div>`
where ?? should be replaced by the
[language code](https://www.w3schools.com/tags/ref_language_codes.asp)
(eg., "fr", "en", "it").

Below is an example of an HTML file containing a French version and an English version of the
text and soundtrack. English language is set as default language when the mobile phone is not configured
neither in French nor in English.

```
<!DOCTYPE html>
<html>
<head>
       <meta charset="UTF-8">
       <!-- This line allows to have a responsive page -->
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <style>
           html {
               font-size: 14pt;
           }

           [lang] {
               display: none;
           }

           [lang=en] {
               display: block;
           }
       </style>
       <script type="text/javascript">
           function localize(language) {
               if (!supportedLanguages.includes(language)) {
                   language = 'en';
               }
               let lang = ':lang(' + language + ')';
               let hide = '[lang]:not(' + lang + ')';
               document.querySelectorAll(hide).forEach(function (node) {
                   node. style. display = 'none';
               });
               let show = '[lang]' + lang;
               document.querySelectorAll(show).forEach(function (node) {
                   node. style. display = 'block';
               });
           }
       </script>
</head>

<body>
<!-- English version -->
<div style="display:none;" lang="en">
       <h2>Soldatenheim, Hotel du Luxembourg</h2>
       <!-- An image -->
       <p>
           <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
       </p>

      <p>
          The Luxembourg Hotel is requisitioned by German occupation troops.
          The property is located in Soldetanheim, reserved for the relaxation of the men of Wehrmacht troops.
          Located in front of the theater, the soldiers also took advantage of a casino and modern cinema rue des Bouchers.
          They met the Bayeusaine youth who often showed hostile.
       </p>

      <p style="text-align:center">
          <!--A soundtrack -->
          <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35ang.mp3"></audio>
      </p>
</div>

<!--Version française-->
<div style="display: block;" lang="fr">
      <h2>Soldatenheim, Hôtel du Luxembourg</h2>
      <p>
          <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
      </p>
      <p>
          L’hôtel du Luxembourg est réquisitionné par les troupes allemandes d’occupation.
          L’établissement est aménagé en Soldatenheim, réservé à la détente des hommes de troupes de la Wehrmacht.
          Situé face au théâtre, les soldats profitaient également d’un casino et du Modern Cinéma rue des Bouchers.
          Ils y rencontraient la jeunesse bayeusaine qui se montrait souvent hostile.
      </p>
      <p style="text-align:center">
          <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35.mp3"></audio>
      </p>
</div>

<script>
       /* List of supported languages */
       const supportedLanguages = ['fr', 'en'];
       localize(window.navigator.language.substring(0, 2));
</script>
</body>
</html>
```

To make your job easier, download the archive of HTML file templates at the end of the section.
All you have to do is insert your texts, images, sound or video files.

## Modify the XML file

The XML file is the one used to build a classic course. It must be edited by hand,
adding a line of text with the URL of an HTML page to associate with each checkpoint. The line is framed by the tags
`<text> </text>`, as in the example below.

> #### Tip
> You can retrieve the XML file of an existing course by going to the route modification page of the planner account. After including the HTML page addresses in the XML file, all you have to do is resubmit the new XML file for the course.

The example is an excerpt from an XML file modified on lines 17, 24 and 31 with the URL addresses of the pages to display
for checkpoints S1, 31 and 22.

```
<?xml version="1.0" encoding="UTF-8"?>
<CourseData iofVersion="3.0" createTime="2021-05-30T01:04:52.045+02:00" creator="OCAD 12.4.0.1684">
       <Event>
           <Name>Event</Name>
       </Event>
       <RaceCourseData>
           <Map>
               <Scale>5000</Scale>
               <MapPositionTopLeft x="-680.7" y="661.0" unit="mm"/>
               <MapPositionBottomRight x="188.0" y="16.7" unit="mm"/>
           </Map>
           <Control>
               <Id>S1</Id>
               <Position lng="-0.711518" lat="49.273183"/>
               <MapPosition x="-210.2" y="103.8" unit="mm"/>
               <!-- Internet address of the page to associate with the starting checkpoint -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/depart/departDDAY.html</Text>
           </Control>
           <Control>
               <Id>31</Id>
               <Position lng="-0.712816" lat="49.273724"/>
               <MapPosition x="-228.5" y="116.8" unit="mm"/>
               <!-- Internet address of the HTML page to associate with checkpoint 31 -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/31/poste31DDAY.html</Text>
           </Control>
           <Control>
               <Id>32</Id>
               <Position lng="-0.714457" lat="49.274719"/>
               <MapPosition x="-251.3" y="140.0" unit="mm"/>
               <!-- Internet address of the HTML page to associate with checkpoint 32 -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/32/poste32DDAY.html</Text>
           </Control>
...
</CourseData>
```

## Test your course from home

To test your course after publication, just use the app:

1. Select playful or walk mode.
2. Select the course to test.
3. Choose GPS validation.
4. Start the course with the start button.
5. Force the validation of checkpoints from the Kebab menu at the top right. The multimedia content corresponding to the validated position is displayed.

## Archive of HTML page templates

Both HTML file templates can be downloaded in the following archive. Just modify the chosen file with
the texts or the names of the image, sound or video files of his choice.

[Download the archive](https://vikazimut.vikazim.fr/public/docs/multimedia-html.zip?v0.0.1)
