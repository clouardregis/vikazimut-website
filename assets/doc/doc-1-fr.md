# Parcours touristique

Les parcours touristiques sont utilisables avec les modes promenade et ludique de l'application.
Un parcours touristique permet l'affichage d'un contenu multimédia dans une fenêtre de l'application après la validation d'un poste.
Il est donc nécessaire que le parcours soit dans une zone géographique avec un accès au réseau Internet mobile.

Un parcours touristique est un bon candidat pour un parcours permanent avec un code QR au départ qui va lancer directement
la course sur le mode promenade. Le code QR est à récupérer sur la page de
[création de parcours](/#/planner/courses/-1) du mode traceur.

> #### Exemples
>   1. Exemples de contenus multimédia dans les trois [parcours permanents touristiques de Bayeux](https://view.genial.ly/60880167e8077b0d2f5920ff/interactive-image-le-bajo)
>   2. Vidéo d'installation et d'utilisation de l'application en mode promenade [(partie 1)](https://youtu.be/WqBuDK0QC5U) et [(partie 2)](https://youtu.be/B3cIzl03uIg) (Crédit Luc Di Pol)

### Liste des tâches pour réaliser un parcours touristique :

1. Rédiger les pages HTML avec un contenu multimédia pour chaque poste.
Pour construire une page HTML, un éditeur de texte basique (eg. Bloc-notes de Windows) suffit.
La rédaction d'une page HTML est assez facile surtout en utilisant les modèles de page que nous fournissons plus bas.
2. Publier les pages HTML sur un serveur Web (celui du club, de la ville, du lycée, de l'université ou d'un fournisseur de sites gratuit ou payant). 
3. Modifier le fichier XML du parcours pour indiquer quelle page doit être affichée à chaque poste.
4. Tester son parcours avec l'application depuis chez soi.

> #### Astuce
> Vous pouvez essayer l'exemple du parcours [Bayeux DDAY](https://vikazimut.vikazim.fr/web/#/course/info/619) depuis votre domicile avec un téléphone mobile ou une tablette.
> Depuis l'application Vikazimut, sélectionnez le mode promenade puis le parcours « Bayeux DDAY » dans la liste des courses avec une validation par GPS. Il suffit ensuite de forcer la validation de chaque poste avec le menu kebab (icône avec trois points en haut à droite de l'écran) pour voir apparaître la page de contenu.

## Écrire une page HTML multimédia

Une page HTML touristique est une page
[HTML classique](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML).
Vous pouvez y insérer du texte, des images, des vidéos et du son.

Le code HTML ci-dessous est un exemple d'une page multimédia avec texte, image et son. Ce
[lien](https://vikazimut.lescigales.org/BayeuxDDAY/35/poste35DDAY.html)
montre le rendu de la page ci-dessous.

```
<html>
<head>
      <meta charset="UTF-8">
      <!-- Cette ligne permet d'avoir une page adaptative -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <style>
          html {
              font-size: 14pt;
          }
      </style>
</head>

<body>
<!-- Le titre de page -->
<h1>Soldatenheim, Hôtel du Luxembourg</h1>

<!-- Une image -->
<p>
      <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
</p>

<!-- Un paragraphe de texte -->
<p>
      L’hôtel du Luxembourg est réquisitionné par les troupes allemandes d’occupation.
      L’établissement est aménagé en Soldatenheim, réservé à la détente des hommes de troupes de la Wehrmacht.
      Situé face au théâtre, les soldats profitaient également d’un casino et du Modern Cinéma rue des Bouchers.
      Ils y rencontraient la jeunesse bayeusaine qui se montrait souvent hostile.
</p>

<!-- Une bande son -->
<p style="text-align:center">
      <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35.mp3"></audio>
</p>
</body>
</html>
```

Pour vous faciliter le travail, téléchargez l'archive des modèles de fichier HTML à la fin de la section.
Vous n'avez plus qu'à y insérer vos textes, vos images, vos fichiers son ou vidéo.

## Écrire une page HTML multilingue

Les pages HTML peuvent contenir différentes versions linguistiques du texte, de la vidéo ou de la bande son.
Chaque version est mise dans un cartouche encadré par les balises
`<div> </div>` :
`<div style="display: none;" lang="??">`
texte dans la langue choisie
`</div>`
où ?? doit être remplacé par le
[code de la langue](https://www.w3schools.com/tags/ref_language_codes.asp)
(eg., "fr", "en", "it").

Ci-dessous est donné l'exemple d'un fichier HTML contenant une version française et une version anglaise
du texte et de la bande son. La langue anglaise est fixée comme langue par défaut quand le téléphone mobile n'est paramétré
ni en français ni en anglais.

```
<!DOCTYPE html>
<html>
<head>
      <meta charset="UTF-8">
      <!-- Cette ligne permet d'avoir une page adaptative -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <style>
          html {
              font-size: 14pt;
          }

          [lang] {
              display: none;
          }

          [lang=en] {
              display: block;
          }
      </style>
      <script type="text/javascript">
          function localize(language) {
              if (!supportedLanguages.includes(language)) {
                  language = 'en';
              }
              let lang = ':lang(' + language + ')';
              let hide = '[lang]:not(' + lang + ')';
              document.querySelectorAll(hide).forEach(function (node) {
                  node.style.display = 'none';
              });
              let show = '[lang]' + lang;
              document.querySelectorAll(show).forEach(function (node) {
                  node.style.display = 'block';
              });
          }
      </script>
</head>

<body>
<!-- English version -->
<div style="display:none;" lang="en">
       <h2>Soldatenheim, Hotel du Luxembourg</h2>
       <!-- An image -->
       <p>
           <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
       </p>

      <p>
          The Luxembourg Hotel is requisitioned by German occupation troops.
          The property is located in Soldetanheim, reserved for the relaxation of the men of Wehrmacht troops.
          Located in front of the theater, the soldiers also took advantage of a casino and modern cinema rue des Bouchers.
          They met the Bayeusaine youth who often showed hostile.
       </p>

      <p style="text-align:center">
          <!--A soundtrack -->
          <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35ang.mp3"></audio>
      </p>
</div>

<!--Version française-->
<div style="display: block;" lang="fr">
      <h2>Soldatenheim, Hôtel du Luxembourg</h2>
      <p>
          <img src="https://vikazimut.lescigales.org/BayeuxDDAY/35/P6b35.jpg" width="100%">
      </p>
      <p>
          L’hôtel du Luxembourg est réquisitionné par les troupes allemandes d’occupation.
          L’établissement est aménagé en Soldatenheim, réservé à la détente des hommes de troupes de la Wehrmacht.
          Situé face au théâtre, les soldats profitaient également d’un casino et du Modern Cinéma rue des Bouchers.
          Ils y rencontraient la jeunesse bayeusaine qui se montrait souvent hostile.
      </p>
      <p style="text-align:center">
          <audio controls="controls" src="https://vikazimut.lescigales.org/BayeuxDDAY/35/35.mp3"></audio>
      </p>
</div>

<script>
      /* Liste des langues reconnues */
      const supportedLanguages = ['fr', 'en'];
      localize(window.navigator.language.substring(0, 2));
</script>
</body>
</html>
```

Pour vous faciliter le travail, téléchargez l'archive des modèles de fichier HTML à la fin de la section.
Vous n'avez plus qu'à y insérer vos textes, vos images, vos fichiers son ou vidéo.

## Modifier le fichier XML

Le fichier XML est celui utilisé pour la construction d'un parcours classique. Il doit être modifié à la main,
en ajoutant une ligne de texte avec l'adresse URL d'une page HTML à associer à chaque poste. La ligne est encadrée par les balises
`<text> </text>`, comme dans l'exemple ci-dessous.

> #### Astuce
> Vous pouvez récupérer le fichier XML d'une course existante en allant sur la page de modification du parcours du compte traceur. Après avoir inclus les adresses de pages HTML dans le fichier XML, il suffit de redéposer le nouveau fichier XML pour le parcours.

L'exemple est un extrait d'un fichier XML modifié aux lignes 17, 24 et 31 avec les adresses URL des pages à afficher
pour les postes S1, 31 et 22.

```
<?xml version="1.0" encoding="UTF-8"?>
<CourseData iofVersion="3.0" createTime="2021-05-30T01:04:52.045+02:00" creator="OCAD 12.4.0.1684">
      <Event>
          <Name>Event</Name>
      </Event>
      <RaceCourseData>
          <Map>
              <Scale>5000</Scale>
              <MapPositionTopLeft x="-680.7" y="661.0" unit="mm"/>
              <MapPositionBottomRight x="188.0" y="16.7" unit="mm"/>
          </Map>
          <Control>
              <Id>S1</Id>
              <Position lng="-0.711518" lat="49.273183"/>
              <MapPosition x="-210.2" y="103.8" unit="mm"/>
              <!-- adresse Internet de la page à associer à la balise de départ -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/depart/departDDAY.html</Text>
          </Control>
          <Control>
              <Id>31</Id>
              <Position lng="-0.712816" lat="49.273724"/>
              <MapPosition x="-228.5" y="116.8" unit="mm"/>
              <!-- adresse Internet de la page HTML à associer à la balise 31 -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/31/poste31DDAY.html</Text>
          </Control>
          <Control>
              <Id>32</Id>
              <Position lng="-0.714457" lat="49.274719"/>
              <MapPosition x="-251.3" y="140.0" unit="mm"/>
              <!-- adresse Internet de la page HTML à associer à la balise 32 -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/32/poste32DDAY.html</Text>
          </Control>
...
</CourseData>
```

## Tester son parcours depuis chez soi

Pour tester son parcours après publication, il suffit d'utiliser l'application Vikazimut sur son téléphone :

1. Sélectionnez le mode ludique ou promenade.
2. Sélectionnez le parcours à tester.
3. Choisissez une validation par GPS.
4. Lancez le parcours avec le bouton départ.
5. Forcez la validation des postes à partir du menu Kebab en haut à droite. Le contenu multimédia correspondant au poste validé s'affiche.

## Archive des modèles de pages HTML

Les deux modèles de fichiers HTML sont récupérables dans l'archive suivante. Il suffit de modifier le fichier choisi avec
les textes ou les noms des fichiers image, son ou vidéo de son choix.

[Télécharger l'archive](https://vikazimut.vikazim.fr/public/docs/multimedia-html.zip?v0.0.1)
