# Parcours ludique

Les parcours ludiques sont utilisables avec les modes ludique et promenade de l'application. Un parcours ludique permet
l'affichage d'un questionnaire dans une fenêtre de l'application après la validation d'un poste.
Chaque bonne réponse est comptabilisée pour 1 point. À la fin du parcours, l'application affiche le nombre total de points obtenus.
Son utilisation nécessite que le parcours soit dans une zone géographique avec un accès au réseau Internet mobile.

Un parcours ludique est un bon candidat pour un parcours permanent avec un code QR au départ qui va lancer directement
la course sur le mode ludique.
Le code QR est à récupérer sur la page de [création de parcours](/#/planner/courses/-1) du mode traceur.

> #### Exemple de parcours ludique
> [Vidéo](https://youtu.be/PiEDdEeVoMY) d'un parcours réalisé par les stagiaires de la formation BPJEPS spécialité Animateur mention \"Education à l'Environnement vers un Développement Durable\" de la promotion 2022/2023, en collaboration avec les formateurs du CREPS de Poitiers. (Crédit Nicolas Jimblet),

### Liste des tâches pour réaliser un parcours ludique :

1. Rédiger les pages HTML contenant un questionnaire interactif
   type QCM ou image cliquable, pouvant être multimédia et multilingue pour chaque poste.
   Pour construire une page HTML, un éditeur de texte basique (eg. Bloc-notes de Windows) suffit.
   La rédaction d'une page HTML est assez facile surtout en utilisant les modèles de page que nous fournissons plus bas.
2. Publier les pages HTML sur un serveur Web (celui du club, de la ville, du lycée, de l'université ou d'un fournisseur de sites gratuit ou payant).
3. Modifier le fichier XML du parcours pour indiquer quelle page doit être affichée à chaque poste.
4. Tester son parcours avec l'application depuis chez soi.


## Écrire une page HTML avec QCM

Une page HTML avec QCM est une page
[HTML classique](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML)
qui contient du texte des images, des vidéos et du son, mais surtout qui contient une partie questionnaire
qui se présente sous la forme d'une liste de boutons à cocher ou d'items dans un sélecteur.

Pour que le questionnaire soit utilisable par Vikazimut, il faut renvoyer le nombre de point (0 ou 1) associé
à chaque réponse à l'application Vikazimut par l'instruction :
`Vikazimut.postMessage(point);`

La page HTML présentée ci-dessous est un exemple d'un questionnaire à deux choix portant sur la visualisation
d'une vidéo. La réponse exacte est donnée si la réponse n°1 est choisie alors la valeur 1 est retournée, sinon la valeur 0 est retournée.
Dans cette version, l'instruction
`document.getElementById("result-text").innerText`
permet d'afficher le verdict dès que la réponse est donnée.

- Voici le [rendu](https://vikazimut.vikazim.fr/public/docs/page3.html) de la page ci-dessous.
- Voici un autre [exemple](https://vikazimut.vikazim.fr/public/docs/page2.html) de page interactive avec un sélecteur de réponses.
- Enfin, voici un [exemple](https://vikazimut.vikazim.fr/public/docs/page4.html) de QCM sur une bande son.

```
<!DOCTYPE html>
<html>
<head>
      <meta charset="UTF-8">
      <!-- Cette ligne permet d'avoir une page adaptative -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <style>
          html {
              font-size: 14pt;
          }

          #result-text {
              color: red;
              font-size: 28px;
          }
      </style>

      <!-- Script pour récupérer la réponse au QCM (pas besoin de le modifier) -->
      <script>
          function sendChoice(point, answer) {
              /* Désactiver les boutons pour empêcher plusieurs réponses */
              document.getElementById("answer1").disabled = true;
              document.getElementById("answer2").disabled = true;

              document.getElementById("result-text").innerText = answer;
              /* Cette ligne permet de passer le nombre de points à l'application */
              Vikazimut.postMessage(point);
          }
      </script>
</head>

<body>
      <h1>Quiz</h1>
      <p>Ceci est une démonstration de l'utilisation de boutons radio pour un questionnaire.</p>

      <!-- Un exemple de video -->
      <iframe width="420" height="315" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>

      <!-- Un exemple de QCM avec deux réponses -->
      <h2>Question</h2>
      <p>Avez-vous aimé la vidéo&nbsp;?</p>
      <div>
          <input type="radio" id="answer1" value="0" onchange="sendChoice(1, 'Bravo');">
          <label for="answer1">oui</label>

          <input type="radio" id="answer2" value="1" onchange="sendChoice(0, 'Faux');">
          <label for="answer2">non</label>
      </div>

      <div id="result-text" style="color: red"></div>
</body>
</html>
```

Pour vous faciliter le travail, téléchargez l'archive des modèles de fichier HTML que nous
fournissons plus bas dans la page.
Vous n'avez plus qu'à y insérer vos textes, vos images, vos fichiers son ou vidéo.

## Écrire une page HTML avec image interactive

Il est possible d'utiliser une image pour le questionnaire où l'utilisateur est amené à cliquer à un endroit
spécifique d'une image pour répondre à la question.
L'exemple ci-dessous correspond à une question demandant à l'utilisateur où il se situe exactement sur une vue aérienne de l'endroit.
Voici le [rendu](https://vikazimut.vikazim.fr/public/docs/page1.html)
de la page ci-dessous.

```
<html>
<head>
      <meta charset="UTF-8">
      <!-- Cette ligne permet d'avoir une page adaptative -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <style>
          html {
              font-size: 14pt;
          }
          .container {
              position: absolute;
              left: 50%;
              transform: translate(-50%, 0%);
          }
          #result-text {
              position: absolute;
              bottom: 25%;
              left: 45%;
              font-size: 28px;
          }
      </style>
      <!-- Script pour récupérer la réponse au quiz -->
      <script type="text/javascript">
          let answered = false;
          function clickOnMap(point, message) {
              if (answered) {  /* Interdire plusieurs réponses */
                  return false;
              }
              /* Ecriture du résultat sur l'image */
              document.getElementById("result-text").innerText = message;
              answered = true;

              /* Cette ligne permet de passer le résultat en nombre de points à l'application */
              Vikazimut.postMessage(point);
              return true;
          }
      </script>
</head>

<body>
<div class="container">
      <h1 style="text-align: center">Quiz à partir d'un pointage sur une image</h1>
      <p style="text-align:center">Touchez l'endroit où vous pensez être sur la photo&nbsp;?</p>

      <div style="color: white; text-align:center">
          <map name="stade">
              <area shape="rect" coords="0,0,135,128" onclick="clickOnMap(0, 'Faux')"/>
              <area shape="rect" coords="135,0,270,128" onclick="clickOnMap(1, 'Bravo')"/>
              <area shape="rect" coords="0,128,135,256" onclick="clickOnMap(0, 'Faux')"/>
              <area shape="rect" coords="135,128,270,256" onclick="clickOnMap(0, 'Faux')"/>
          </map>
          <img src="stadesite.jpg" usemap="#stade">
          <div id="result-text"></div>
      </div>
</div>
</body>
</html>
```

Pour vous faciliter le travail, téléchargez l'archive des modèles de fichier HTML
que nous fournissons plus bas dans la page.
Vous n'avez plus qu'à y insérer vos textes, vos images, vos fichiers son ou vidéo.

## Modifier le fichier XML

Le fichier XML est celui utilisé lors de la construction du parcours. Il doit être modifié à la main,
en ajoutant une ligne avec l'adresse URL de page à associer à chaque poste encadré par les balises
`<text> </text>`, comme dans l'exemple ci-dessous.

> #### Astuce
>
> Vous pouvez récupérer le fichier XML d'une course existante en allant sur la page de modification du parcours du mode traceur. Après avoir inclus les adresses de pages HTML dans le fichier XML, il suffit de redéposer le nouveau fichier XML pour le parcours.

L'exemple est un extrait d'un fichier XML modifié aux lignes 17, 24 et 31 avec les adresses URL des pages
à afficher pour les postes S1, 31 et 22.

```
<?xml version="1.0" encoding="UTF-8"?>
<CourseData iofVersion="3.0" createTime="2021-05-30T01:04:52.045+02:00" creator="OCAD 12.4.0.1684">
      <Event>
          <Name>Event</Name>
      </Event>
      <RaceCourseData>
          <Map>
              <Scale>5000</Scale>
              <MapPositionTopLeft x="-680.7" y="661.0" unit="mm"/>
              <MapPositionBottomRight x="188.0" y="16.7" unit="mm"/>
          </Map>
          <Control>
              <Id>S1</Id>
              <Position lng="-0.711518" lat="49.273183"/>
              <MapPosition x="-210.2" y="103.8" unit="mm"/>
              <!-- adresse Internet de la page à associer à la balise de départ -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/depart/departDDAY.html</Text>
          </Control>
          <Control>
              <Id>31</Id>
              <Position lng="-0.712816" lat="49.273724"/>
              <MapPosition x="-228.5" y="116.8" unit="mm"/>
              <!-- adresse Internet de la page HTML à associer à la balise 31 -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/31/poste31DDAY.html</Text>
          </Control>
          <Control>
              <Id>32</Id>
              <Position lng="-0.714457" lat="49.274719"/>
              <MapPosition x="-251.3" y="140.0" unit="mm"/>
              <!-- adresse Internet de la page HTML à associer à la balise 32 -->
              <Text>https://vikazimut.lescigales.org/BayeuxDDAY/32/poste32DDAY.html</Text>
          </Control>
...
</CourseData>
```

## Tester son parcours depuis chez soi

Pour tester son parcours après publication, il suffit d'utiliser l'application Vikazimut sur son téléphone :

1. Sélectionnez le mode ludique ou promenade.
2. Sélectionnez le parcours à tester.
3. Choisissez une validation par GPS.
4. Lancez le parcours avec le bouton départ.
5. Forcez la validation des postes à partir du menu Kebab en haut à droite. Le quiz correspondant au poste validé s'affiche.
6. En fin de parcours, le nombre de points obtenus est affiché dans les statistiques de la course.

## Archive des modèles de pages HTML

L'archive suivante regroupe des modèles de fichier HTML avec différentes formes de QCM : deux types de bouton, une image
interactive et un sélecteur de réponse. Ils sont facilement adaptables.

[Télécharger l'archive](https://vikazimut.vikazim.fr/public/docs/quiz-html.zip?0.0.1)
