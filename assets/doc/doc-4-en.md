# Organizing races and events

## 1. QR code to install the application and start a race

> #### Tip
>
> On the day of the event, a course QR code allows competitors to install the application on their phone (Android or iOS). Once the application is loaded, the same QR code automatically positions the application on the race.

[Demo Video](https://youtu.be/E9kXqeCuYS8)

After creating a course, the ["My courses"](https://vikazimut.vikazim.fr/web/#/planner/courses/-1) page displays
a “QR Codes” button for each route.
The course QR Code allows orienteers to install the app on mobile from a QR Code reader.

Once the app is installed on the phone, rescanning the same QR Code from the QR code reader
or from the "Flash a course" button in the application, starts the course automatically.
When creating the course, if it is configured for a mode (sport/playful),
a validation order (preset/free) and a validation mode (GPS / QR code / iBeacons),
it launches the course automatically with these characteristics.

## The graphic charter

The following archive provides the images, logos and posters of the Vikazimut graphic charter
that you can use for your displays: [Graphic charter](https://vikazimut.vikazim.fr/public/docs/graphic-charter.zip)

## 3. Event organization

An event allows you to combine several courses with a ranking of the participants
based on three possible criteria: cumulative time, stage championship or points races.

The orienteers registered for the event only have to send their result from the application
at the end of a course.
The result is automatically integrated into the event and the general ranking is updated.

> #### Limit
>
> The number of participants is limited to 2000 (limit extendable on request).

### 3.1 Types of event and penalties

The construction of an event consists of choosing the type with the penalties, adding the courses
and registering the participants.

There are three types of event.

#### 1/ Championship

The ranking is done on the score.
The best score is awarded to the best time. It receives 1000 points.
The others receive a value proportional to the time difference with the best time:

    score = 1000 * best time / time

The effective time in milliseconds of each participant takes into account the two types of
penalty given in seconds: penalty per missing control and penalty per minute started.

    time (ms) = total time
                + number of missing controls * penalty in seconds per missing controls * 1000
                + number of minutes started * penalty in seconds per minute started * 1000

#### 2/ Ranking by points

The ranking is done on the score.
The score is calculated from the value assigned to each validated control, from which
the total penalties given in points is subtracted:

    score = number of validated points
           - number of missing controls * penalty in points per missing control
           - number of minutes started * penalty in points per minute started

#### 3/ Ranking by cumulative time

The ranking is made from the cumulative time in milliseconds, corrected by the penalties
given in seconds:

    Effective time (ms) = total time
                         + number of missing controls * penalty in seconds per missing control * 1000
                         + number of minutes started * penalty in seconds per minute started * 1000

### 3.2 Manage races

Only races belonging to the planner can be included in an event.

### 3.3 Manage participants

Participants are identified by a pseudonym.
It is therefore imperative that the registration pseudonym is the same as the one
that will be used when sending the result from the application at the end of the race.
Otherwise, the event organizer will have to make the matches manually.

There are four ways to add a participant to an event:

1. The participant registers himself on the event page.
2. The organizer registers the orienteers individually.
3. The organizer registers the orienteers from a text file containing the list
of pseudonyms, one pseudonym per line.
4. The organizer registers the orienteers from the pseudonyms of the traces of a race
of the event that has already taken place.

When a participant is added, the ranking is automatically updated if the tracer has already
completed some races of the event.

### 3.4 Modify penalties

The event organizer has the ability to make manual corrections:

1. If the name of the participant given from the application differs from the one in the
registration, the organizer can either change the name of the participant or retrieve the correct
track from a drop-down list.
2. The organizer can change the penalties assigned automatically, change the number of missing controls
and the number of minutes of overtaking.