# Géoréférencement de carte

Cette section fournit deux tutoriels pour la création et le géoréférencement de cartes.

## Créer une carte avec OpenOrienteering Mapper

[OpenOrienteering Mapper](https://www.openorienteering.org/) est un logiciel gratuit de création de fonds de carte et de parcours.

- [Accès au tutoriel](https://vikazim.fr/wp-content/uploads/2020/12/Tuto-Georeferencement-Carte-Methode-SL-JV-OOMapper.pdf) (Crédit Jérôme Vialard)

## Créer un parcours avec Purple Pen

[Purple Pen](https://purplepen.golde.org/) est un logiciel gratuit de création de parcours.

- [Accès au Tutoriel](https://vikazim.fr/wp-content/uploads/2020/12/Creation-de-Parcours-sur-Vikazimut-EP-20201201.pdf) (Crédit Jérôme Vialard & Eric Pigeon)

## Exemple de fichiers XML et KML valides

Voici un exemple des deux fichiers requis pour construire un parcours. Les deux fichiers ont été générés avec OCAD.

- [51.xml](https://vikazimut.vikazim.fr/public/docs/51.xml)
- [51.kml](https://vikazimut.vikazim.fr/public/docs/51.kml)
