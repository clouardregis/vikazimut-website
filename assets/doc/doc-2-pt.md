# Playful route

Playful routes can be used with the application's playful and walk modes. A playful route allows
the display of a questionnaire in an application window after the validation of a position.
Each correct answer is counted for 1 point. At the end of the course, the application displays the total number of points obtained.
Its use requires that the route be in a geographical area with access to the mobile Internet network.

A playful route is a good candidate for a permanent route with a QR code at the start that will launch directly
the course on playful mode.
The QR code is to be retrieved on the [route creation](/#/planner/courses/-1) page of the planner mode.

> #### Example of a playful route
> [Video](https://youtu.be/PiEDdEeVoMY) of a course created by the trainees of the BPJEPS formation specialty mention \"Education à l'Environnement vers un Développement Durable\" of the promotion 2022/2023, in collaboration with the trainers of the CREPS of Poitiers. (Credit Nicolas Jamblet),

### List of tasks for completing a playful route:

1. Create the HTML pages for each checkpoint with an interactive questionnaire
   MCQ type or clickable image, which can be multimedia and multilingual.
   To build an HTML page, a basic text editor (eg. Notepad of Windows) is sufficient.
   Writing an HTML page is quite easy, especially using the page templates we provide below.
2. Publish the pages to a web server (that of the club, the city, the high school, the university or a free or paid site provider
3. Modify the XML file of the course to indicate which url to display at each checkpoint.
4. Test your route with the application from home.

## Write an HTML page with MCQ

An HTML page with MCQ is a
[classical HTML](https://developer.mozilla.org/en/docs/Learn/HTML/Introduction_to_HTML) page
which contains text, images, videos and sound, but above all which contains a questionnaire part
which comes in the form of a list of checkboxes or items in a selector.

For the questionnaire to be usable by Vikazimut, you must return the number of points (0 or 1) associated
at each response to Vikazimut app by the instruction:
`Vikazimut.postMessage(point);`

The HTML page shown below is an example of a two-choice questionnaire based on the visualization
of a video. The exact answer (point=1) is given if answer #1 is chosen, otherwise the value 0 is returned.
In this version, the statement
`document.getElementById("result-text").innerText`
displays the verdict as soon as the answer is given.

- Here is the [rendering](https://vikazimut.vikazim.fr/public/docs/page3.html) of the page below.
- Here is another [example](https://vikazimut.vikazim.fr/public/docs/page2.html) of an interactive page with an answer selector.
- Finally, here is an [example](https://vikazimut.vikazim.fr/public/docs/page4.html) of MCQ on a soundtrack.

```
<!DOCTYPE html>
<html>
<head>
       <meta charset="UTF-8">
       <!-- This line allows to have a responsive page -->
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <style>
           html {
               font-size: 14pt;
           }

           #result-text {
               color: red;
               font-size: 28px;
           }
       </style>

       <!-- Script to retrieve the MCQ answer (no need to modify it) -->
       <script>
           function sendChoice(point, answer) {
               /* Disable buttons to prevent multiple responses */
               document.getElementById("answer1").disabled = true;
               document.getElementById("answer2").disabled = true;

               document.getElementById("result-text").innerText = answer;
               /* This line is used to pass the number of points to the application */
               Vikazimut.postMessage(point);
           }
       </script>
</head>

<body>
       <h1>Quiz</h1>
       <p>This is a demonstration of using radio buttons for a quiz.</p>

       <!-- An example video -->
       <iframe width="420" height="315" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>

       <!-- An example of MCQ with two answers -->
       <h2>Question</h2>
       <p>Did you like the video?</p>
       <div>
           <input type="radio" id="answer1" value="0" onchange="sendChoice(1, 'Bravo');">
           <label for="answer1">yes</label>

           <input type="radio" id="answer2" value="1" onchange="sendChoice(0, 'False');">
           <label for="answer2">no</label>
       </div>

       <div id="result-text" style="color:red"></div>
</body>
</html>
```

To make your job easier, download the archive of HTML file templates that we
provide further down the page.
All you have to do is insert your texts, images, sound or video files.

## Write an HTML page with interactive image

It is possible to use an image for the questionnaire where the user is prompted to click somewhere
specific of an image to answer the question.
The example below corresponds to a question asking the user where exactly he/she is on an aerial view of the place.
Here is the [rendering](https://vikazimut.vikazim.fr/public/docs/page1.html)
from the page below.

```
<html>
<head>
       <meta charset="UTF-8">
       <!-- This line allows to have a responsive page -->
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <style>
           html {
               font-size: 14pt;
           }
           .container {
               position: absolute;
               left: 50%;
               transform: translate(-50%, 0%);
           }
           #result-text {
               position: absolute;
               bottom: 25%;
               left: 45%;
               font-size: 28px;
           }
       </style>
       <!-- Script to retrieve quiz answer -->
       <script type="text/javascript">
           let answered = false;
           function clickOnMap(point, message) {
               if (answered) { /* Disallow multiple answers */
                   return false;
               }
               /* Write the result to the image */
               document.getElementById("result-text").innerText = message;
               answered = true;

               /* This line is used to pass the result in number of points to the application */
               Vikazimut.postMessage(point);
               return true;
           }
       </script>
</head>

<body>
<div class="container">
       <h1 style="text-align: center">Quiz from hovering over an image</h1>
       <p style="text-align:center">Touch where you think you are in the photo?</p>

       <div style="color: white; text-align: center">
           <map name="stadium">
               <area shape="rect" coords="0,0,135,128" onclick="clickOnMap(0, 'False')"/>
               <area shape="rect" coords="135,0,270,128" onclick="clickOnMap(1, 'Bravo')"/>
               <area shape="rect" coords="0,128,135,256" onclick="clickOnMap(0, 'False')"/>
               <area shape="rect" coords="135,128,270,256" onclick="clickOnMap(0, 'False')"/>
           </map>
           <img src="stadesite.jpg" usemap="#stadium">
           <div id="result-text"></div>
       </div>
</div>
</body>
</html>
```

To make your job easier, download the archive of HTML file templates
which we provide further down the page.
All you have to do is insert your texts, images, sound or video files.

## Modify the XML file

The XML file is the one used when building the course. It must be edited by hand,
by adding a line with the URL address of the page to be associated with each checkpoint framed by the tags
`<text> </text>`, as in the example below.

> #### Tip
>
> You can retrieve the XML file of an existing course by going to the planner part route modification page. After including the HTML page addresses in the XML file, all you have to do is resubmit the new XML file.

The example is an excerpt from an XML file modified on lines 17, 24 and 31 with the URL addresses of the pages
to be displayed for items S1, 31 and 22.

```
<?xml version="1.0" encoding="UTF-8"?>
<CourseData iofVersion="3.0" createTime="2021-05-30T01:04:52.045+02:00" creator="OCAD 12.4.0.1684">
       <Event>
           <Name>Event</Name>
       </Event>
       <RaceCourseData>
           <Map>
               <Scale>5000</Scale>
               <MapPositionTopLeft x="-680.7" y="661.0" unit="mm"/>
               <MapPositionBottomRight x="188.0" y="16.7" unit="mm"/>
           </Map>
           <Control>
               <Id>S1</Id>
               <Position lng="-0.711518" lat="49.273183"/>
               <MapPosition x="-210.2" y="103.8" unit="mm"/>
               <!-- Internet address of the page to associate with the starting tag -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/depart/departDDAY.html</Text>
           </Control>
           <Control>
               <Id>31</Id>
               <Position lng="-0.712816" lat="49.273724"/>
               <MapPosition x="-228.5" y="116.8" unit="mm"/>
               <!-- Internet address of the HTML page to associate with tag 31 -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/31/poste31DDAY.html</Text>
           </Control>
           <Control>
               <Id>32</Id>
               <Position lng="-0.714457" lat="49.274719"/>
               <MapPosition x="-251.3" y="140.0" unit="mm"/>
               <!-- Internet address of the HTML page to associate with tag 32 -->
               <Text>https://vikazimut.lescigales.org/BayeuxDDAY/32/poste32DDAY.html</Text>
           </Control>
...
</CourseData>
```

## Test your course from home

To test your course after publication, just use the application:

1. Select playful or walk mode.
2. Select the course to test.
3. Choose GPS validation.
4. Start the course with the start button.
5. Force the validation of checkpoints from the Kebab menu at the top right. The quiz corresponding to the validated position is displayed.
6. At the end of the course, the number of points obtained is displayed in the statistics of the route.

## Archive of HTML page templates

The following archive groups HTML file templates with different forms of MCQs: two types of button, one interactive image and a response selector.
They are easily adaptable.

[Download the archive](https://vikazimut.vikazim.fr/public/docs/quiz-html.zip?0.0.1)
