part of 'planner_event_detail_page.dart';

const String _separateChar = ";";

class _CSVGenerator {
  static String createCSV(EventDetail eventDetail) {
    var csv = "";

    if (eventDetail.type == constants.CHAMPIONSHIP_EVENT) {
      csv = _CSVGeneratorForChampionshipEvent._createCSVForChampionshipEvent(eventDetail);
    } else if (eventDetail.type == constants.POINTS_EVENT) {
      csv = _CSVGeneratorForPointEvent._createCSVForPointEvent(eventDetail);
    } else if (eventDetail.type == constants.CUMULATIVE_TIME_EVENT) {
      csv = _CSVGeneratorForCumulativeTimeEvent._createCSVForCumulativeTimeEvent(eventDetail);
    }
    return csv;
  }
}

class _CSVGeneratorForChampionshipEvent extends _CSVGenerator {
  static String _createCSVForChampionshipEvent(EventDetail eventDetail) {
    var csv = "";

    // Header
    csv = L10n.getString("csv_file_nickname") + _separateChar + L10n.getString("csv_file_total_score") + _separateChar + L10n.getString("csv_file_total_time") + _separateChar + L10n.getString("csv_file_total_penalty");

    var topHeader = "";
    topHeader += _separateChar + _separateChar + _separateChar + _separateChar;
    var eventCourses = eventDetail.eventCourses;
    for (var course in eventCourses) {
      topHeader += course.courseName;
      csv += _separateChar + L10n.getString("csv_file_score") + _separateChar + L10n.getString("csv_file_time");
      csv += _separateChar + L10n.getString("csv_file_gross_score");
      csv += _separateChar + L10n.getString("csv_file_missing_punch") + _separateChar + L10n.getString("csv_file_mp_penalty");
      csv += _separateChar + L10n.getString("csv_file_overtime") + _separateChar + L10n.getString("csv_file_ot_penalty");
      topHeader += _separateChar + _separateChar + _separateChar;
      for (var checkpointId in course.checkpointIds) {
        csv += _separateChar + checkpointId;
        topHeader += _separateChar;
      }
    }

    // Name of the course in the top header
    csv = "$topHeader\n$csv";
    var participants = eventDetail.participants;
    // Result data for each participant (not sorted)
    for (var participant in participants) {
      csv += "\n";
      var tmp = "";
      csv += participant.nickname;
      for (int i = 0; i < participant.eventTracks.length; i++) {
        var courseScore = participant.eventTracks[i].totalScoreWithPenalty;
        tmp += "$_separateChar$courseScore";
        var courseTime = participant.eventTracks[i].totalTime;
        tmp += "$_separateChar${timeToStringWithMilliseconds(courseTime)}";
        tmp += "$_separateChar${participant.eventTracks[i].grossTotalScore}";

        tmp += "$_separateChar${participant.eventTracks[i].missingPunchPenaltyCount}$_separateChar${participant.eventTracks[i].missingPunchPenalty}";
        tmp += "$_separateChar${participant.eventTracks[i].overtimePenaltyCount}$_separateChar${participant.eventTracks[i].overtimePenalty}";
        if (participant.eventTracks[i].punchTimes.isNotEmpty) {
          for (var punchTime in participant.eventTracks[i].punchTimes) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(punchTime)}";
          }
        } else {
          var checkpoints = eventDetail.eventCourses[i].checkpointIds;
          for (int j = 0; j < checkpoints.length; j++) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(0)}";
          }
        }
      }
      csv += _separateChar + participant.totalScore.toString() + _separateChar + timeToStringWithMilliseconds(participant.totalTime) + _separateChar + timeToStringWithMilliseconds(participant.totalPenalty) + tmp;
    }
    return csv;
  }
}

class _CSVGeneratorForPointEvent extends _CSVGenerator {
  static String _createCSVForPointEvent(EventDetail eventDetail) {
    var csv = "";

    // Header
    csv = L10n.getString("csv_file_nickname") + _separateChar + L10n.getString("csv_file_total_score") + _separateChar + L10n.getString("csv_file_total_time") + _separateChar + L10n.getString("csv_file_gross_score") + _separateChar + L10n.getString("csv_file_total_penalty");

    var topHeader = "";
    topHeader += _separateChar + _separateChar + _separateChar + _separateChar + _separateChar;
    var eventCourses = eventDetail.eventCourses;
    for (var course in eventCourses) {
      topHeader += course.courseName;
      csv += _separateChar + L10n.getString("csv_file_score") + _separateChar + L10n.getString("csv_file_time");
      csv += _separateChar + L10n.getString("csv_file_gross_score");
      csv += _separateChar + L10n.getString("csv_file_missing_punch") + _separateChar + L10n.getString("csv_file_mp_penalty");
      csv += _separateChar + L10n.getString("csv_file_overtime") + _separateChar + L10n.getString("csv_file_ot_penalty");
      topHeader += _separateChar + _separateChar + _separateChar;
      for (var checkpointId in course.checkpointIds) {
        csv += _separateChar + checkpointId;
        topHeader += _separateChar;
      }
    }

    // Name of the course in the top header
    csv = "$topHeader\n$csv";
    var participants = eventDetail.participants;
    // Result data for each participant (not sorted)
    for (var participant in participants) {
      csv += "\n";
      var tmp = "";
      csv += participant.nickname;
      for (int i = 0; i < participant.eventTracks.length; i++) {
        var courseScore = participant.eventTracks[i].totalScoreWithPenalty;
        tmp += "$_separateChar$courseScore";
        var courseTime = participant.eventTracks[i].totalTime;
        tmp += "$_separateChar${timeToStringWithMilliseconds(courseTime)}";
        tmp += "$_separateChar${participant.eventTracks[i].grossTotalScore}";

        tmp += "$_separateChar${participant.eventTracks[i].missingPunchPenaltyCount}$_separateChar${participant.eventTracks[i].missingPunchPenalty}";
        tmp += "$_separateChar${participant.eventTracks[i].overtimePenaltyCount}$_separateChar${participant.eventTracks[i].overtimePenalty}";
        if (participant.eventTracks[i].punchTimes.isNotEmpty) {
          for (var punchTime in participant.eventTracks[i].punchTimes) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(punchTime)}";
          }
        } else {
          var checkpoints = eventDetail.eventCourses[i].checkpointIds;
          for (int j = 0; j < checkpoints.length; j++) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(0)}";
          }
        }
      }
      csv += _separateChar + participant.totalScore.toString() + _separateChar + timeToStringWithMilliseconds(participant.totalTime) + _separateChar + participant.grossScore.toString() + _separateChar + participant.totalPenalty.toString() + tmp;
    }
    return csv;
  }
}

class _CSVGeneratorForCumulativeTimeEvent extends _CSVGenerator {
  static String _createCSVForCumulativeTimeEvent(EventDetail eventDetail) {
    var csv = "";

    // Header
    csv = L10n.getString("csv_file_nickname") + _separateChar + L10n.getString("csv_file_total_time") + _separateChar + L10n.getString("csv_file_total_penalty");

    var topHeader = "";
    topHeader += _separateChar + _separateChar + _separateChar;

    var eventCourses = eventDetail.eventCourses;
    for (var course in eventCourses) {
      topHeader += course.courseName;
      csv += _separateChar + L10n.getString("csv_file_time");
      csv += _separateChar + L10n.getString("csv_file_missing_punch") + _separateChar + L10n.getString("csv_file_mp_penalty");
      csv += _separateChar + L10n.getString("csv_file_overtime") + _separateChar + L10n.getString("csv_file_ot_penalty");
      topHeader += _separateChar + _separateChar + _separateChar + _separateChar + _separateChar;
      for (var checkpointId in course.checkpointIds) {
        csv += _separateChar + checkpointId;
        topHeader += _separateChar;
      }
    }

    // Name of the course in the top header
    csv = "$topHeader\n$csv";
    var participants = eventDetail.participants;
    // Result data for each participant (not sorted)
    for (var participant in participants) {
      csv += "\n";
      var tmp = "";
      csv += participant.nickname;
      for (int i = 0; i < participant.eventTracks.length; i++) {
        tmp += "$_separateChar${timeToStringWithMilliseconds(participant.eventTracks[i].totalTime)}";
        tmp += "$_separateChar${participant.eventTracks[i].missingPunchPenaltyCount}$_separateChar${participant.eventTracks[i].missingPunchPenalty}";
        tmp += "$_separateChar${participant.eventTracks[i].overtimePenaltyCount}$_separateChar${participant.eventTracks[i].overtimePenalty}";
        if (participant.eventTracks[i].punchTimes.isNotEmpty) {
          for (var punchTime in participant.eventTracks[i].punchTimes) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(punchTime)}";
          }
        } else {
          var checkpoints = eventDetail.eventCourses[i].checkpointIds;
          for (int j = 0; j < checkpoints.length; j++) {
            tmp += "$_separateChar${timeToStringWithMilliseconds(0)}";
          }
        }
      }
      csv += _separateChar + timeToStringWithMilliseconds(participant.totalTime) + _separateChar + timeToStringWithMilliseconds(participant.totalPenalty) + tmp;
    }
    return csv;
  }
}
