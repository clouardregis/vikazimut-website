import 'package:flutter/foundation.dart';

import 'planner_event_track.dart';

@immutable
class Participant {
  final int id;
  final String nickname;
  final int rank;
  final int progress;
  final int totalScore;
  final int totalTime;
  final int grossScore;
  final int totalPenalty;
  final List<EventTrack> eventTracks;
  final List<bool> modifiedStates;

  const Participant({
    required this.id,
    required this.nickname,
    required this.rank,
    required this.progress,
    required this.totalScore,
    required this.totalTime,
    required this.grossScore,
    required this.totalPenalty,
    required this.eventTracks,
    required this.modifiedStates,
  });

  factory Participant.fromJson(Map<String, dynamic> json) {
    return Participant(
      id: json["id"] as int,
      nickname: json["nickname"] as String,
      rank: json["rank"] as int,
      progress: json["progress"] as int,
      totalScore: json["totalScore"] as int,
      totalTime: json["totalTime"] as int,
      grossScore: json["grossScore"] as int,
      totalPenalty: json["totalPenalty"] as int,
      modifiedStates: _readEventCourseModifiedStatesFromJson(json["modified"]),
      eventTracks: _readEventCoursesFromJson(json["eventCourse"]),
    );
  }

  static List<bool> _readEventCourseModifiedStatesFromJson(List<dynamic>? json) {
    if (json == null) return [];
    List<bool> eventCoursesModifiedStates = [];
    for (int i = 0; i < json.length; i++) {
      eventCoursesModifiedStates.add(json[i]);
    }
    return eventCoursesModifiedStates;
  }

  static List<EventTrack> _readEventCoursesFromJson(List<dynamic>? json) {
    if (json == null) return [];
    List<EventTrack> eventCourses = [];
    for (int i = 0; i < json.length; i++) {
      List<dynamic> row = json[i];
      eventCourses.add(EventTrack.fromJson(row));
    }
    return eventCourses;
  }
}
