// coverage:ignore-file
part of 'planner_event_course_view.dart';

class _ModifyCourseFormDialog {
  EventCourseData? _eventCourseData;

  // Note: Navigator.pop(context) will dispose the controllers
  final TextEditingController _missingPunchPenaltyTextController = TextEditingController(text: "0");
  final TextEditingController _overtimePenaltyTextController = TextEditingController(text: "0");
  final ValueNotifier<bool> _validated = ValueNotifier(false);
  int _eventType = 1;

  Future<EventCourseData?> execute(BuildContext context, EventCourse eventCourse, int eventType) async {
    _eventType = eventType;
    _eventCourseData = EventCourseData.fromEventCourse(eventCourse);
    return await showDialog<EventCourseData?>(
      context: context,
      builder: (BuildContext context) => _buildPopupAddCourse(context),
    );
  }

  Widget _buildPopupAddCourse(BuildContext context) {
    _missingPunchPenaltyTextController.text = _eventCourseData!.missingPunchPenalty.toString();
    _overtimePenaltyTextController.text = _eventCourseData!.overTimePenalty.toString();
    return AlertDialog(
      title: Text(
        L10n.getString("planner_event_courses_modify_dialog_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(L10n.getString("planner_event_courses_add_dialog_format")),
          const SizedBox(height: 12),
          StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
              return CustomDropdownButton<int>(
                value: _eventCourseData!.format,
                items: [
                  DropdownMenuItem<int>(value: 0, child: Text(L10n.getString(courseFormatToL10nKey(0)))),
                  DropdownMenuItem<int>(value: 1, child: Text(L10n.getString(courseFormatToL10nKey(1)))),
                ],
                onChanged: (int? value) {
                  dropDownState(() {
                    _eventCourseData!.format = value!;
                    _validated.value = true;
                  });
                },
              );
            },
          ),
          const SizedBox(height: 12),
          if (_eventType == 1)
            TextField(
              controller: _missingPunchPenaltyTextController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: "${L10n.getString("planner_event_courses_add_dialog_mp_penalty")} (${L10n.getString("points")})",
              ),
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              onChanged: (String value) {
                _eventCourseData!.missingPunchPenalty = int.tryParse(value) ?? 0;
                _validated.value = true;
              },
            )
          else
            DurationPicker(
              labelText: L10n.getString("planner_event_courses_add_dialog_mp_penalty"),
              initialDurationInMs: _eventCourseData!.missingPunchPenalty * 1000,
              onChanged: (int? value) {
                _validated.value = true;
                _eventCourseData!.missingPunchPenalty = value! ~/ 1000;
              },
            ),
          const SizedBox(height: 12),
          if (_eventType == 1)
            TextField(
              controller: _overtimePenaltyTextController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(labelText: "${L10n.getString("planner_event_courses_add_dialog_ot_penalty")} (${L10n.getString("points")})"),
              onChanged: (String value) {
                _validated.value = true;
                _eventCourseData!.overTimePenalty = int.tryParse(value) ?? 0;
              },
            )
          else
            DurationPicker(
              labelText: L10n.getString("planner_event_courses_add_dialog_ot_penalty"),
              initialDurationInMs: _eventCourseData!.overTimePenalty * 1000,
              onChanged: (int? value) {
                _validated.value = true;
                _eventCourseData!.overTimePenalty = value! ~/ 1000;
              },
            ),
          const SizedBox(height: 12),
          DurationPicker(
            labelText: L10n.getString("planner_event_courses_add_dialog_time_limit"),
            initialDurationInMs: _eventCourseData!.maxTimeInMs,
            onChanged: (int? value) {
              _validated.value = true;
              _eventCourseData!.maxTimeInMs = value!;
            },
          ),
        ],
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) => CustomPlainButton(
            onPressed: _validated.value ? () => Navigator.of(context).pop(_eventCourseData) : null,
            backgroundColor: kOrangeColor,
            label: L10n.getString("planner_event_courses_modify_dialog_button_ok"),
          ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(null),
          label: L10n.getString("planner_event_courses_add_dialog_button_cancel"),
        ),
      ],
    );
  }
}
