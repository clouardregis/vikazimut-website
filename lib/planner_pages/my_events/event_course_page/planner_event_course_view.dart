// coverage:ignore-file
library;

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/course_info/course_info_page_view.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/duration_picker.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/time.dart';

import '../event_detail_page/planner_event_detail_page.dart';
import '../event_participants/planner_event_participant_view.dart';
import 'planner_event_course_model.dart';
import 'planner_event_course_presenter.dart';

part 'add_course_form.dart';
part 'modify_course_form.dart';

class PlannerEventCourseView extends StatefulWidget {
  static const String routePath = '/planner/events/courses';
  final int eventId;

  PlannerEventCourseView(args) : eventId = int.parse(args);

  @override
  State<PlannerEventCourseView> createState() => PlannerEventCourseViewState();
}

class PlannerEventCourseViewState extends State<PlannerEventCourseView> {
  final ScrollController _horizontalScroll = ScrollController();
  final ScrollController _verticalScroll = ScrollController();
  late final PlannerEventCoursePagePresenter _presenter;
  Future<Event>? _event;
  final ValueNotifier<int> _eventType = ValueNotifier<int>(-1);
  String? _errorMessage;

  @override
  void initState() {
    _presenter = PlannerEventCoursePagePresenter(this);
    _event = _presenter.fetchCourses(widget.eventId);
    _event?.then((Event value) => _eventType.value = value.type);
    super.initState();
  }

  @override
  void dispose() {
    _horizontalScroll.dispose();
    _verticalScroll.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("planner_event_courses_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                  Expanded(
                    child: FutureBuilder<Event>(
                      future: _event,
                      builder: (context, AsyncSnapshot<Event> snapshot) {
                        if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        } else if (!snapshot.hasData) {
                          return const Center(child: CircularProgressIndicator());
                        } else {
                          return Scrollbar(
                            controller: _verticalScroll,
                            thumbVisibility: true,
                            child: Scrollbar(
                              controller: _horizontalScroll,
                              thumbVisibility: true,
                              notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
                              child: SingleChildScrollView(
                                controller: _verticalScroll,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(height: 10),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Text(
                                        snapshot.data!.name,
                                        style: Theme.of(context).textTheme.displaySmall,
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    Text(
                                      "${L10n.getString("planner_event_courses_page_event_type")} ${snapshot.data!.typeOfEvent}",
                                      style: Theme.of(context).textTheme.headlineMedium,
                                    ),
                                    const SizedBox(height: 10),
                                    Align(
                                      alignment: Alignment.center,
                                      child: SingleChildScrollView(
                                        controller: _horizontalScroll,
                                        scrollDirection: Axis.horizontal,
                                        primary: false,
                                        child: _buildDataTable(
                                          context,
                                          snapshot.data!,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (_errorMessage != null) ErrorContainer(_errorMessage!),
          ValueListenableBuilder<int>(
              valueListenable: _eventType,
              builder: (BuildContext context, int eventType, child) {
                return CustomPlainButton(
                  label: L10n.getString("planner_event_courses_page_button_add_course"),
                  backgroundColor: kOrangeColor,
                  onPressed: _eventType.value < 0
                      ? null
                      : () async {
                          EventCourseData? data = await _AddCourseFormDialog().execute(context, _eventType.value);
                          if (data != null) {
                            _presenter.addCourse(widget.eventId, data);
                          }
                        },
                );
              }),
          const SizedBox(height: 5),
          Wrap(
            runSpacing: 5,
            direction: Axis.horizontal,
            alignment: WrapAlignment.center,
            spacing: 5,
            children: [
              CustomPlainButton(
                label: L10n.getString("planner_event_list_page_button_detail"),
                backgroundColor: kSuccessColor,
                onPressed: () => GoRouter.of(context).go("${PlannerEventDetailPage.routePath}/${widget.eventId}"),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_event_list_page_button_participants"),
                backgroundColor: kWarningColor,
                foregroundColor: Colors.black,
                onPressed: () => GoRouter.of(context).go("${PlannerEventParticipantView.routePath}/${widget.eventId}"),
              ),
            ],
          ),
          if (!ScreenHelper.isMobile(context)) const SizedBox(height: 5),
          Footer(),
        ],
      ),
    );
  }

  DataTable _buildDataTable(BuildContext context, Event event) {
    bool isPointUnit = _eventType.value == 1;
    return DataTable(
      dataRowColor: WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
      border: TableBorder.all(
        style: BorderStyle.solid,
        width: 1,
        color: kOrangeColorDisabled,
      ),
      horizontalMargin: 5,
      columnSpacing: ScreenHelper.isDesktop(context) ? 15 : 5,
      columns: [
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header1"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header2"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header3"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header4"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header5"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header6"), textAlign: TextAlign.center))),
        DataColumn(label: Expanded(child: Text(L10n.getString("planner_event_courses_page_table_header7"), textAlign: TextAlign.center)))
      ],
      rows: event.eventCourses
          .map((eventCourse) => DataRow(cells: [
                DataCell(
                  TextButton(
                    style: TextButton.styleFrom(
                      alignment: Alignment.centerLeft,
                      foregroundColor: kOrangeColor,
                      padding: const EdgeInsets.all(5),
                    ),
                    child: Text(eventCourse.name, style: const TextStyle(fontSize: 16)),
                    onPressed: () => GoRouter.of(context).go("${CourseInfoViewPage.routePath}/${eventCourse.courseId}"),
                  ),
                ),
                DataCell(Align(alignment: Alignment.center, child: Text(L10n.getString(courseFormatToL10nKey(eventCourse.format))))),
                DataCell(Align(alignment: Alignment.center, child: Text(timeToString(eventCourse.maxTimeInMs, withHour: true)))),
                DataCell(Align(
                  alignment: Alignment.center,
                  child: (isPointUnit) ? Text('${eventCourse.missingPunchPenalty}') : Text(timeToString(eventCourse.missingPunchPenalty * 1000, withHour: true)),
                )),
                DataCell(Align(
                  alignment: Alignment.center,
                  child: (isPointUnit) ? Text('${eventCourse.overTimePenalty}') : Text(timeToString(eventCourse.overTimePenalty * 1000, withHour: true)),
                )),
                DataCell(
                  CustomPlainButton(
                    label: L10n.getString("planner_event_courses_page_button_modify"),
                    backgroundColor: kInfoColor,
                    onPressed: () async {
                      EventCourseData? data = await _ModifyCourseFormDialog().execute(context, eventCourse, _eventType.value);
                      if (data != null) _presenter.modifyCourse(widget.eventId, eventCourse.id, data);
                    },
                  ),
                ),
                DataCell(
                  CustomPlainButton(
                    label: L10n.getString("planner_event_courses_page_button_remove"),
                    backgroundColor: kDangerColor,
                    onPressed: () => showBinaryQuestionAlertDialog(
                      context: context,
                      title: L10n.getString("planner_event_courses_page_delete_title"),
                      message: L10n.getString("planner_event_courses_page_delete_message", [eventCourse.name]),
                      okMessage: L10n.getString("planner_event_courses_page_delete_ok_message"),
                      noMessage: L10n.getString("planner_event_courses_page_delete_no_message"),
                      action: () {
                        _presenter.deleteCourse(widget.eventId, eventCourse.id, eventCourse.name);
                      },
                    ),
                  ),
                ),
              ]))
          .toList(),
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _event = _presenter.fetchCourses(widget.eventId);
    });
  }

  void error(String message) {
    setState(() {
      _errorMessage = message;
    });
  }

  void displayToast(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text(message)),
      );
  }
}
