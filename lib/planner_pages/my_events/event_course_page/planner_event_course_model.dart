// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';

class PlannerEventCoursePageModel {
  static const String GET_URL = '${constants.API_SERVER}/planner/my-event/%d/event-courses';
  static const String DELETE_URL = "${constants.API_SERVER}/planner/my-event/remove-course/%d";
  static const String POST_ADD_URL = "${constants.API_SERVER}/planner/my-event/%d/add-course";
  static const String PATCH_MODIFY_URL = "${constants.API_SERVER}/planner/my-event/modify-course/%d";

  const PlannerEventCoursePageModel();

  static Future<Event> fetchCourses(int eventId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(sprintf(GET_URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return Event.fromJson(json.decode(response.body));
      } else {
        return Future.error(response.body);
      }
    }
  }

  static Future<http.Response> addCourse(int eventId, EventCourseData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      try {
        final response = await http.post(
          Uri.parse(sprintf(POST_ADD_URL, [eventId])),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: data.toJson(),
        );
        return response;
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> modifyCourse(int eventId, int eventCourseId, EventCourseData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.patch(
        Uri.parse(sprintf(PATCH_MODIFY_URL, [eventCourseId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: data.toJson(),
      );
      return response;
    }
  }

  static Future<http.Response> deleteEvent(int eventId, int eventCourseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse(sprintf(DELETE_URL, [eventCourseId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }
}

class Event {
  final String name;
  final int type;
  final List<EventCourse> eventCourses;

  const Event({
    required this.name,
    required this.type,
    required this.eventCourses,
  });

  factory Event.fromJson(Map<String, dynamic> json) {
    var courses = (json["eventCourses"] as List).map((data) => EventCourse.fromJson(data)).toList();
    return Event(
      name: json["name"],
      type: json["type"] as int,
      eventCourses: courses,
    );
  }

  String get typeOfEvent {
    switch (type) {
      case 1:
        return L10n.getString("constant_type_points");
      case 2:
        return L10n.getString("constant_type_cumulative_time");
      default:
        return L10n.getString("constant_type_championship");
    }
  }
}

class EventCourse {
  final int id;
  final int courseId;
  final String name;
  final int format;
  final int missingPunchPenalty;
  final int overTimePenalty;
  final int maxTimeInMs;

  const EventCourse({
    required this.id,
    required this.courseId,
    required this.name,
    required this.format,
    required this.missingPunchPenalty,
    required this.overTimePenalty,
    required this.maxTimeInMs,
  });

  factory EventCourse.fromJson(Map<String, dynamic> json) {
    return EventCourse(
      id: json["id"] as int,
      courseId: json["courseId"] as int,
      name: json["courseName"],
      format: json["format"] as int,
      missingPunchPenalty: json["missingPunchPenalty"] as int,
      overTimePenalty: json["overtimePenalty"] as int,
      maxTimeInMs: json["maxTime"] as int,
    );
  }
}

class EventCourseData {
  int? courseId;
  int? format;
  int missingPunchPenalty = 0;
  int overTimePenalty = 0;
  int maxTimeInMs = 0;

  EventCourseData();

  dynamic toJson() {
    return json.encode({
      "course_id": courseId,
      "format": format,
      "missing_punch_penalty": missingPunchPenalty,
      "overtime_penalty": overTimePenalty,
      "max_time": maxTimeInMs,
    });
  }

  factory EventCourseData.fromEventCourse(EventCourse eventCourse) {
    var object = EventCourseData();
    object.courseId = eventCourse.courseId;
    object.format = eventCourse.format;
    object.missingPunchPenalty = eventCourse.missingPunchPenalty;
    object.overTimePenalty = eventCourse.overTimePenalty;
    object.maxTimeInMs = eventCourse.maxTimeInMs;
    return object;
  }
}
