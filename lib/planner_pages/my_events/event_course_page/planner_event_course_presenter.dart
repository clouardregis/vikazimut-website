// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';

import 'planner_event_course_model.dart';
import 'planner_event_course_view.dart';

class PlannerEventCoursePagePresenter {
  final PlannerEventCourseViewState _view;

  const PlannerEventCoursePagePresenter(this._view);

  Future<Event> fetchCourses(int eventId) async {
    return _PlannerEventCoursePageModel.fetchCourses(eventId);
  }

  Future<void> addCourse(int eventId, EventCourseData data) async {
    http.Response response = await _PlannerEventCoursePageModel.addCourse(eventId, data);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
    } else {
      _view.error(response.body);
    }
  }

  Future<void> modifyCourse(int eventId, int eventCourseId, EventCourseData data) async {
    http.Response response = await _PlannerEventCoursePageModel.modifyCourse(eventId, eventCourseId, data);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
    } else {
      _view.error(response.body);
    }
  }

  Future<void> deleteCourse(int eventId, int eventCourseId, String eventName) async {
    http.Response response = await _PlannerEventCoursePageModel.deleteEvent(eventId, eventCourseId);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
      _view.displayToast(L10n.getString("planner_event_courses_page_delete_success_message", [eventName]));
    } else {
      _view.error(response.body);
    }
  }
}

class _PlannerEventCoursePageModel {
  static const String GET_URL = '${constants.API_SERVER}/planner/my-event/%d/event-courses';
  static const String DELETE_URL = "${constants.API_SERVER}/planner/my-event/remove-course/%d";
  static const String POST_ADD_URL = "${constants.API_SERVER}/planner/my-event/%d/add-course";
  static const String PATCH_MODIFY_URL = "${constants.API_SERVER}/planner/my-event/modify-course/%d";

  const _PlannerEventCoursePageModel();

  static Future<Event> fetchCourses(int eventId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(sprintf(GET_URL, [eventId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return Event.fromJson(json.decode(response.body));
      } else {
        return Future.error(response.body);
      }
    }
  }

  static Future<http.Response> addCourse(int eventId, EventCourseData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      try {
        final response = await http.post(
          Uri.parse(sprintf(POST_ADD_URL, [eventId])),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: data.toJson(),
        );
        return response;
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> modifyCourse(int eventId, int eventCourseId, EventCourseData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.patch(
        Uri.parse(sprintf(PATCH_MODIFY_URL, [eventCourseId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: data.toJson(),
      );
      return response;
    }
  }

  static Future<http.Response> deleteEvent(int eventId, int eventCourseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse(sprintf(DELETE_URL, [eventCourseId])),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }
}
