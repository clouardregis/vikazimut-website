// coverage:ignore-file
part of 'planner_event_course_view.dart';

class _AddCourseFormDialog {
  final EventCourseData _eventCourseData = EventCourseData();

  // Hint: Navigator.pop(context) dispose the controllers itself. No need to do it.
  final TextEditingController _missingPunchPenaltyTextController = TextEditingController(text: "0");
  final TextEditingController _overtimePenaltyTextController = TextEditingController(text: "0");
  final ValueNotifier<bool> _validated = ValueNotifier(false);
  int _eventType = 1;

  Future<EventCourseData?> execute(BuildContext context, int eventType) async {
    _eventType = eventType;
    return await showDialog<EventCourseData?>(
      context: context,
      builder: (BuildContext context) => _buildPopupAddCourse(context),
    );
  }

  Widget _buildPopupAddCourse(BuildContext context) {
    return AlertDialog(
      title: Text(
        L10n.getString("planner_event_courses_add_dialog_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(L10n.getString("planner_event_courses_add_dialog_course")),
          const SizedBox(height: 12),
          FutureBuilder<List<_CourseEvent>>(
              future: _getAllPlannerCourses(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                } else if (snapshot.hasData) {
                  return StatefulBuilder(builder: (BuildContext context, StateSetter dropDownState) {
                    return CustomDropdownButton<int>(
                      value: _eventCourseData.courseId,
                      items: snapshot.data!.map((course) => DropdownMenuItem<int>(value: course.id, child: Text(course.name))).toList(),
                      onChanged: (int? value) {
                        dropDownState(() {
                          _eventCourseData.courseId = value;
                          _validated.value = _eventCourseData.courseId != null && _eventCourseData.format != null;
                        });
                      },
                    );
                  });
                } else {
                  return CustomDropdownButton<int>(
                    value: _eventCourseData.courseId,
                    items: const [],
                    onChanged: null,
                  );
                }
              }),
          const SizedBox(height: 12),
          Text(L10n.getString("planner_event_courses_add_dialog_format")),
          const SizedBox(height: 12),
          StatefulBuilder(
            builder: (BuildContext context, StateSetter dropDownState) {
              return CustomDropdownButton<int>(
                value: _eventCourseData.format,
                items: [
                  DropdownMenuItem<int>(value: 0, child: Text(L10n.getString(courseFormatToL10nKey(0)))),
                  DropdownMenuItem<int>(value: 1, child: Text(L10n.getString(courseFormatToL10nKey(1)))),
                ],
                onChanged: (int? value) {
                  dropDownState(() {
                    _eventCourseData.format = value!;
                    _validated.value = _eventCourseData.courseId != null && _eventCourseData.format != null;
                  });
                },
              );
            },
          ),
          const SizedBox(height: 12),

          if (_eventType == 1)
            TextField(
              controller: _missingPunchPenaltyTextController,
              decoration: InputDecoration(labelText: "${L10n.getString("planner_event_courses_add_dialog_mp_penalty")} (${L10n.getString("points")})"),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              onChanged: (String value) {
                _eventCourseData.missingPunchPenalty = int.tryParse(value) ?? 0;
              },
            )
          else
            DurationPicker(
              labelText: L10n.getString("planner_event_courses_add_dialog_mp_penalty"),
              initialDurationInMs: _eventCourseData.maxTimeInMs,
              onChanged: (int? value) {
                _eventCourseData.missingPunchPenalty = value! ~/ 1000;
              },
            ),
          const SizedBox(height: 12),
          if (_eventType == 1)
            TextField(
              controller: _overtimePenaltyTextController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(labelText: "${L10n.getString("planner_event_courses_add_dialog_ot_penalty")} (${L10n.getString("points")})"),
              onChanged: (String value) {
                _eventCourseData.overTimePenalty = int.tryParse(value) ?? 0;
              },
            )
          else
            DurationPicker(
              labelText: L10n.getString("planner_event_courses_add_dialog_ot_penalty"),
              initialDurationInMs: _eventCourseData.maxTimeInMs,
              onChanged: (int? value) {
                _eventCourseData.overTimePenalty = value! ~/1000;
              },
            ),
          const SizedBox(height: 12),
          DurationPicker(
            labelText: L10n.getString("planner_event_courses_add_dialog_time_limit"),
            initialDurationInMs: _eventCourseData.maxTimeInMs,
            onChanged: (int? value) {
              _eventCourseData.maxTimeInMs = value!;
            },
          ),
        ],
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) => ElevatedButton(
            onPressed: _validated.value
                ? () {
                    Navigator.of(context).pop(_eventCourseData);
                  }
                : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
            ),
            child: Text(L10n.getString("planner_event_courses_add_dialog_button_ok")),
          ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(null),
          label: L10n.getString("planner_event_courses_add_dialog_button_cancel"),
        ),
      ],
    );
  }

  Future<List<_CourseEvent>>? _getAllPlannerCourses() async {
    const String URL = "${constants.API_SERVER}/planner/my-courses/names";
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(URL),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return (json.decode(response.body) as List).map((data) => _CourseEvent.fromJson(data)).toList();
      } else {
        return Future.error(response.body);
      }
    }
  }
}

class _CourseEvent {
  final int id;
  final String name;

  const _CourseEvent({required this.id, required this.name});

  factory _CourseEvent.fromJson(data) {
    return _CourseEvent(
      id: data["id"] as int,
      name: data["name"] as String,
    );
  }
}
