class PlannerEventParticipantModel {
  final int id;
  final String name;

  const PlannerEventParticipantModel({
    required this.id,
    required this.name,
  });

  factory PlannerEventParticipantModel.fromJson(Map<String, dynamic> json) {
    return PlannerEventParticipantModel(
      id: json["id"] as int,
      name: json["name"] as String,
    );
  }
}
