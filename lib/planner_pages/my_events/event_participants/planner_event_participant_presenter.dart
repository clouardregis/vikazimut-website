import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/l10n/l10n.dart';

import '../event_course_page/planner_event_course_model.dart';
import 'planner_event_gateway.dart';
import 'planner_event_participant_model.dart';
import 'planner_event_participant_view.dart';

class PlannerEventParticipantPresenter {
  final PlannerEventParticipantViewState _view;

  const PlannerEventParticipantPresenter(this._view);

  Future<List<PlannerEventParticipantModel>> fetchParticipants(int eventId) async {
    return PlannerEventGateway.fetchParticipants(eventId);
  }

  Future<void> deleteParticipant(int eventId, int participantId) async {
    http.Response response = await PlannerEventGateway.deleteEvent(eventId, participantId);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
    } else {
      _view.error(response.body);
    }
  }

  Future<void> modifyParticipant(int eventId, String oldNickname, String nickname) async {
    http.Response response = await PlannerEventGateway.modifyParticipant(eventId, oldNickname, nickname);
    if (response.statusCode == HttpStatus.ok) {
      _view.update();
    } else {
      var jsonMessage = jsonDecode(response.body);
      String? errorMessage = jsonMessage["message"];
      _view.error(errorMessage == null ? "" : L10n.getString(errorMessage));
    }
  }

  void addParticipants(int eventId, List<String> nicknames) async {
    http.Response response = await PlannerEventGateway.addParticipants(eventId, nicknames);
    if (response.statusCode == HttpStatus.ok) {
      _view.update(jsonDecode(response.body));
    } else {
      _view.error(response.body);
    }
  }

  void addParticipantsFromFile(int eventId, PlatformFile first) {
    if (first.bytes != null) {
      String text = String.fromCharCodes(first.bytes!);
      List<String> nicknames = text.trim().split("\n");
      for (int i = 0; i < nicknames.length; i++) {
        nicknames[i] = nicknames[i].trim();
      }
      if (nicknames.isNotEmpty) {
        addParticipants(eventId, nicknames);
      }
    }
  }

  Future<Event> fetchEventCourses(int eventId) {
    return PlannerEventGateway.fetchCourses(eventId);
  }

  Future<void> addParticipantsOfCourse(int courseId, int eventId) async {
    List<String> nicknames = await PlannerEventGateway.getListOfTrackFromCourse(courseId);
    if (nicknames.isNotEmpty) {
      addParticipants(eventId, nicknames);
    }
  }
}
