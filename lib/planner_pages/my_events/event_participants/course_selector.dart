// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';

import '../event_course_page/planner_event_course_model.dart';

class CourseSelector {
  final Future<Event> _eventCourses;
  final ValueNotifier<int> _validated = ValueNotifier(0);

  CourseSelector(this._eventCourses);

  Future<int?> execute(BuildContext context) async {
    return await showDialog<int?>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => _buildPopupAddParticipant(context),
    );
  }

  Widget _buildPopupAddParticipant(BuildContext context) {
    int? selectedCourseId;

    return AlertDialog(
      title: Text(
        L10n.getString("event_participant_course_selector_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(maxWidth: 500),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(L10n.getString("manual_penalty_form_course_label")),
            FutureBuilder(
              future: _eventCourses,
              builder: (BuildContext context, AsyncSnapshot<Event> snapshot) {
                if (!snapshot.hasData || snapshot.data == null) {
                  return const SizedBox.shrink();
                } else {
                  final Event? event = snapshot.data;
                  return StatefulBuilder(builder: (BuildContext context, StateSetter dropDownState) {
                    return CustomDropdownButton<int>(
                      value: selectedCourseId,
                      items: event!.eventCourses.map((eventCourse) {
                        return DropdownMenuItem<int>(
                          value: eventCourse.courseId,
                          child: Text(eventCourse.name),
                        );
                      }).toList(),
                      onChanged: (int? value) {
                        _validated.value++;
                        dropDownState(() {
                          selectedCourseId = value;
                        });
                      },
                    );
                  });
                }
              },
            ),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _validated,
          builder: (_, __) => ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
              padding: const EdgeInsets.all(15),
            ),
            onPressed: selectedCourseId!=null? () {
              GoRouter.of(context).pop(selectedCourseId);
            }: null,
            child: Text(
              L10n.getString("event_participant_course_selector_button"),
            ),
          ),
        ),
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () => GoRouter.of(context).pop(null),
          label: L10n.getString("event_info_participants_register_cancel"),
        ),
      ],
    );
  }
}
