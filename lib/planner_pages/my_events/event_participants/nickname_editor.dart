// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/utils.dart';

class NicknameEditor {
  final TextEditingController _textEditingController = TextEditingController();

  late final bool _modification;

  NicknameEditor([String? nickname]) {
    if (nickname != null) {
      _modification = true;
      _textEditingController.text = nickname;
    } else {
      _modification = false;
    }
  }

  Future<String?> execute(BuildContext context) async {
    return await showDialog<String?>(
      context: context,
      builder: (BuildContext context) => _buildPopupAddParticipant(context),
    );
  }

  Widget _buildPopupAddParticipant(BuildContext context) {
    return AlertDialog(
      title: Text(
        (_modification) ? L10n.getString("planner_event_participants_page_button_modify_participant") : L10n.getString("planner_event_participants_page_button_add_participant"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(maxWidth: 500),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(L10n.getString("event_info_participants_register_warning")),
            const SizedBox(height: 8),
            TextField(
              autofocus: true,
              controller: _textEditingController,
              textInputAction: TextInputAction.next,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(userIdentifierLength),
              ],
              decoration: InputDecoration(
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: kOrangeColor, width: 2.0),
                ),
                icon: const Icon(
                  Icons.person,
                  color: kOrangeColor,
                ),
                labelText: L10n.getString("event_info_participants_nickname"),
                labelStyle: const TextStyle(color: kOrangeColor),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _textEditingController,
          builder: (_, __) => ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
              padding: const EdgeInsets.all(15),
            ),
            onPressed: _textEditingController.text.trim().isNotEmpty
                ? () {
                    String nickname = _textEditingController.text.trim();
                    _textEditingController.clear();
                    GoRouter.of(context).pop(nickname);
                  }
                : null,
            child: Text(
              _modification ? L10n.getString("planner_event_participants_page_button_modify") : L10n.getString("planner_event_participants_page_button_register"),
            ),
          ),
        ),
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () => GoRouter.of(context).pop(null),
          label: L10n.getString("event_info_participants_register_cancel"),
        ),
      ],
    );
  }
}
