// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'community_course_stats_model.dart';
import 'community_course_stats_presenter.dart';
import 'community_course_stats_table.dart';

class CommunityCourseStatsView extends StatefulWidget {
  static const String routePath = '/community/stats';
  final String _token;

  CommunityCourseStatsView({required String token}) : _token = Uri.decodeComponent(token);

  @override
  State<CommunityCourseStatsView> createState() => CommunityCourseStatsViewState();
}

class CommunityCourseStatsViewState extends State<CommunityCourseStatsView> {
  late final CommunityCourseStatsPresenter _presenter;
  late final Future<List<CommunityCourseStatsModel>> _courses;
  final ScrollController _verticalScrollController = ScrollController();
  final GlobalKey<PaginatedDataTableState> _tableKey = GlobalKey<PaginatedDataTableState>();

  @override
  void initState() {
    _presenter = CommunityCourseStatsPresenter();
    _courses = _presenter.fetchCourses(this, widget._token);
    super.initState();
  }

  @override
  void dispose() {
    _verticalScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Padding(
        padding: const EdgeInsets.all(0),
        child: Column(
          children: [
            Flexible(
              child: ContainerDecoration(
                constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
                child: Column(
                  children: [
                    Text(
                      L10n.getString("community_course_stats_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                      textAlign: TextAlign.center,
                    ),
                    Expanded(
                      child: FutureBuilder<List<CommunityCourseStatsModel>>(
                        future: _courses,
                        builder: (context, snapshot) {
                          if (snapshot.hasError) {
                            return GlobalErrorWidget(snapshot.error.toString());
                          } else if (snapshot.hasData) {
                            var partners = snapshot.data!.reversed.toList();
                            return Scrollbar(
                              controller: _verticalScrollController,
                              thumbVisibility: true,
                              interactive: true,
                              child: SingleChildScrollView(
                                controller: _verticalScrollController,
                                scrollDirection: Axis.vertical,
                                child: SizedBox(
                                  width: ScreenHelper.isMobile(context) ? ScreenHelper.mobileMaxWidth : ScreenHelper.tabletMaxWidth,
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      textTheme: const TextTheme(bodySmall: TextStyle(color: kOrangeColor)),
                                      cardTheme: CardTheme(color: Theme.of(context).scaffoldBackgroundColor),
                                      iconTheme: const IconThemeData(color: Colors.white),
                                    ),
                                    child: CommunityCourseDataTable(_tableKey, _presenter, partners, token: widget._token),
                                  ),
                                ),
                              ),
                            );
                          } else {
                            return const Center(child: CircularProgressIndicator());
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Footer(),
          ],
        ),
      ),
    );
  }
}
