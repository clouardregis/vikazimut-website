
import 'package:vikazimut_website/common/discipline.dart';

class CommunityCourseStatsModel {
  static const int MONTH_IN_MS = 2629746000;
  final int id;
  final String name;
  final int createdAtInMilliseconds;
  final int downloadCount;
  late double downloadPerMonth;
  final bool touristic;
  final bool closed;
  final int missingControlCount;
  final Discipline? discipline;

  CommunityCourseStatsModel({
    required this.id,
    required this.name,
    required this.createdAtInMilliseconds,
    required this.downloadCount,
    required this.touristic,
    required this.closed,
    required this.missingControlCount,
    required this.discipline,
  }) {
    downloadPerMonth = computeDownloadPerMonth_(downloadCount, DateTime.now().millisecondsSinceEpoch - createdAtInMilliseconds);
  }

  factory CommunityCourseStatsModel.fromJson(json) {
    return CommunityCourseStatsModel(
      id: json["id"] as int,
      name: json["name"] as String,
      createdAtInMilliseconds: json["createdAtInMilliseconds"] as int,
      downloadCount: json["downloadCount"] as int,
      touristic: json["touristic"] as bool,
      closed: json["closed"] as bool,
      missingControlCount: json["missingControls"] as int,
      discipline: Discipline.toEnum(json["discipline"] as int?),
    );
  }

  static double computeDownloadPerMonth_(int downloadCount, int delay) {
    final int laps = delay ~/ MONTH_IN_MS + 1;
    return downloadCount / laps;
  }
}
