import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/courses/course_info/course_info_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/diacritic.dart';
import 'package:vikazimut_website/utils/time.dart' as time_utils;

import 'community_course_missing_checkpoints_page.dart';
import 'community_course_stats_model.dart';
import 'community_course_stats_presenter.dart';

class _TableRowData extends DataTableSource {
  late final List<CommunityCourseStatsModel> _courses;
  final BuildContext context;
  final String token;

  _TableRowData(List<CommunityCourseStatsModel> partnerList, String searchText, this.token, {required this.context}) {
    if (searchText.isNotEmpty) {
      var rawSearchText = searchText.toLowerCase().toRawString();
      _courses = partnerList.where((element) => element.name.toLowerCase().toRawString().contains(rawSearchText)).toList();
    } else {
      _courses = partnerList;
    }
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _courses.length;

  @override
  int get selectedRowCount => 0;

  @override
  DataRow getRow(int index) {
    var course = _courses[index];
    return DataRow(
      color: index.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
      cells: [
        DataCell(
          TextButton(
            onPressed: () {
              GoRouter.of(context).go("${CourseInfoViewPage.routePath}/${course.id}");
            },
            child: Row(
              children: [
                Icon(
                  Discipline.getIcon(course.discipline),
                  color: kOrangeColor,
                ),
                const SizedBox(
                  width: 15,
                ),
                Text(
                  course.name,
                  style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
                ),
                if (course.closed)
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(Icons.alarm, color: kWarningColor),
                  ),
                if (course.touristic)
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(Icons.quiz_outlined, color: kSuccessColor),
                  ),
              ],
            ),
          ),
        ),
        DataCell(
          Align(alignment: Alignment.center, child: Text(time_utils.getLocalizedDate(course.createdAtInMilliseconds))),
        ),
        DataCell(
          Center(child: Text('${course.downloadCount}')),
        ),
        DataCell(
          Center(child: Text(L10n.formatNumber(course.downloadPerMonth, 1))),
        ),
        DataCell(
          Center(
            child: SizedBox(
              width: 80,
              child: ElevatedButton(
                onPressed: (course.missingControlCount <= 0)
                    ? null
                    : () => GoRouter.of(context).go(Uri(
                          path: CommunityCourseMissingCheckpointsPage.routePath,
                          queryParameters: {'course_id': '${course.id}', 'token': token},
                        ).toString()),
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
                    if (states.contains(WidgetState.disabled)) {
                      return null;
                    }
                    return kWarningColor;
                  }),
                  padding: WidgetStateProperty.all<EdgeInsets?>(EdgeInsets.zero),
                ),
                child: Text(
                  '${course.missingControlCount}',
                  style: const TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class CommunityCourseDataTable extends StatefulWidget {
  final GlobalKey<PaginatedDataTableState> tableKey;
  final CommunityCourseStatsPresenter presenter;
  final List<CommunityCourseStatsModel> items;
  final String token;

  const CommunityCourseDataTable(this.tableKey, this.presenter, this.items, {required this.token});

  @override
  State<CommunityCourseDataTable> createState() => CommunityCourseDataTableState();
}

class CommunityCourseDataTableState extends State<CommunityCourseDataTable> {
  _TableRowData? _partnerDataSource;
  final TextEditingController _searchFieldController = TextEditingController();
  late List<CommunityCourseStatsModel> _initialPartnerList;
  static String _currentSearchUsername = "";
  static int? _sortColumnIndex;
  static bool _sortAscending = false;

  List<Map<String, dynamic>> tableColumnHeaders = [
    {"label": L10n.getString("community_course_stats_table_column1"), "sortable": false, "numeric": false},
    {"label": L10n.getString("community_course_stats_table_column3"), "sortable": true, "numeric": true},
    {"label": L10n.getString("community_course_stats_table_column2"), "sortable": true, "numeric": true},
    {"label": L10n.getString("community_course_stats_table_column5"), "sortable": true, "numeric": true},
    {"label": L10n.getString("community_course_stats_table_column4"), "sortable": true, "numeric": true},
  ];

  @override
  void initState() {
    _initialPartnerList = widget.items.toList(growable: false);
    _searchFieldController.text = _currentSearchUsername;
    if (_sortColumnIndex != null) _sort(_sortColumnIndex!, _sortAscending);
    _partnerDataSource = _TableRowData(_initialPartnerList, _currentSearchUsername, widget.token, context: context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final int rowPerPage = (height - 360) ~/ 45;
    return PaginatedDataTable(
      key: widget.tableKey,
      source: _partnerDataSource!,
      initialFirstRowIndex: CommunityCourseStatsPresenter.currentDisplayedRowIndex,
      dataRowMaxHeight: 45,
      dataRowMinHeight: 45,
      columnSpacing: 10.0,
      horizontalMargin: 0,
      rowsPerPage: rowPerPage,
      showFirstLastButtons: true,
      showEmptyRows: false,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
      showCheckboxColumn: false,
      arrowHeadColor: kOrangeColor,
      onPageChanged: (int value) => CommunityCourseStatsPresenter.currentDisplayedRowIndex = value,
      header: Padding(
        padding: const EdgeInsets.all(3),
        child: TextField(
          controller: _searchFieldController,
          style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: const BorderSide(
                width: 2,
                color: kOrangeColor,
              ),
            ),
            labelText: L10n.getString("community_course_stats_hint_text"),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          onChanged: (searchText) {
            setState(() {
              _currentSearchUsername = searchText;
              widget.tableKey.currentState!.pageTo(0);
              _partnerDataSource = _TableRowData(_initialPartnerList, searchText, widget.token, context: context);
            });
          },
        ),
      ),
      columns: tableColumnHeaders
          .map(
            (Map<String, dynamic> tableColumnHeader) => DataColumn(
              label: Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    tableColumnHeader["label"],
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              numeric: tableColumnHeader["numeric"],
              onSort: (tableColumnHeader["sortable"])
                  ? (columnIndex, ascending) {
                      setState(() {
                        widget.tableKey.currentState!.pageTo(0);
                        if (columnIndex >= 1 && columnIndex <= 4) {
                          if (_sortColumnIndex != columnIndex) {
                            _sortColumnIndex = columnIndex;
                          } else {
                            _sortAscending = ascending;
                          }
                          _sort(columnIndex, _sortAscending);
                        }
                      });
                    }
                  : null,
            ),
          )
          .toList(),
    );
  }

  void _sort(int columnIndex, bool ascending) {
    _initialPartnerList.sort((CommunityCourseStatsModel a, CommunityCourseStatsModel b) {
      if (columnIndex == 1) {
        if (ascending) {
          return a.createdAtInMilliseconds.compareTo(b.createdAtInMilliseconds);
        } else {
          return b.createdAtInMilliseconds.compareTo(a.createdAtInMilliseconds);
        }
      } else if (columnIndex == 2) {
        if (ascending) {
          return a.downloadCount.compareTo(b.downloadCount);
        } else {
          return b.downloadCount.compareTo(a.downloadCount);
        }
      } else {
        if (ascending) {
          return a.downloadPerMonth.compareTo(b.downloadPerMonth);
        } else {
          return b.downloadPerMonth.compareTo(a.downloadPerMonth);
        }
      }
    });

    setState(() {
      _partnerDataSource = _TableRowData(_initialPartnerList, _currentSearchUsername, widget.token, context: context);
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }
}
