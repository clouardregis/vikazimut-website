import 'package:vikazimut_website/constants.dart' as constants;

import 'community_course_stats_gateway.dart';
import 'community_course_stats_model.dart';
import 'community_course_stats_view.dart';

class CommunityCourseStatsPresenter {
  static const _URL = '${constants.SERVER}/data/community/courses';

  static int currentDisplayedRowIndex = 0;

  Future<List<CommunityCourseStatsModel>> fetchCourses(CommunityCourseStatsViewState view, String token) async {
    try {
      return CommunityCourseGateway.fetchCourses("$_URL/$token");
    } catch (_) {
      return Future.error("Access token error");
    }
  }
}
