// coverage:ignore-file
library;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/help_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'community_data.dart';
import 'community_data_table.dart';
import 'community_list_presenter.dart';

class CommunityListPageView extends StatefulWidget {
  static const String routePath = '/planner/community/list';
  late final int plannerId;

  CommunityListPageView(String? args) {
    if (args == null) {
      plannerId = -1;
    } else {
      var x = int.tryParse(args);
      if (x == null) {
        plannerId = -1;
      } else {
        plannerId = x;
      }
    }
  }

  @override
  State<CommunityListPageView> createState() => CommunityListPageViewState();
}

class CommunityListPageViewState extends State<CommunityListPageView> {
  late final CommunityListPagePresenter _presenter;
  late Future<List<CommunityData>> _communities;
  final ScrollController _scrollController = ScrollController();
  final GlobalKey<PaginatedDataTableState> _tableKey = GlobalKey<PaginatedDataTableState>();
  String? _errorMessage;

  @override
  void initState() {
    _presenter = const CommunityListPagePresenter();
    _communities = _presenter.fetchCommunities(widget.plannerId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          L10n.getString("community_list_page_title"),
                          style: Theme.of(context).textTheme.displayMedium,
                        ),
                        IconButton(
                          padding: const EdgeInsets.all(0.0),
                          color: kOrangeColor,
                          icon: const Icon(
                            Icons.help_outline,
                          ),
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) => HelpWidget(
                                L10n.getString("help_label"),
                                L10n.getString("community_list_page_description"),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: FutureBuilder<List<CommunityData>>(
                      future: _communities,
                      builder: (context, AsyncSnapshot<List<CommunityData>> snapshot) {
                        if (snapshot.hasError) {
                          return GlobalErrorWidget(snapshot.error.toString());
                        } else if (!snapshot.hasData) {
                          return const Center(child: CircularProgressIndicator());
                        } else {
                          var communities = snapshot.data!;
                          return Scrollbar(
                            controller: _scrollController,
                            thumbVisibility: true,
                            child: SingleChildScrollView(
                              controller: _scrollController,
                              child: SizedBox(
                                width: 1200,
                                child: CommunityDataTable(_tableKey, _presenter, communities),
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (_errorMessage != null) ErrorContainer(_errorMessage!),
          if (!ScreenHelper.isMobile(context)) const SizedBox(height: 8),
          Footer(),
        ],
      ),
    );
  }
}
