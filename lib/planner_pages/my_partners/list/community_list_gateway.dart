import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/authentication/unauthorized_exception.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'community_data.dart';

class CommunityListGateway {
  static const String _URL = '${constants.API_SERVER}/planner/communities';

  static Future<List<CommunityData>> fetchCommunities(int plannerId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      throw UnauthorizedException();
    } else {
      try {
        final response = await http.get(
          Uri.parse("$_URL/$plannerId"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          List<CommunityData> x = (json.decode(response.body) as List).map((data) => CommunityData.fromJson(data)).toList();
          x.sort((a, b) {
            return a.name.trim().toLowerCase().compareTo(b.name.trim().toLowerCase());
          });
          return x;
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }
}
