// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/login/login_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'planner_profile_page.dart';

class PlannerProfileEditView extends StatefulWidget {
  static const String routePath = '/planner/modify-personal-info';
  late final int plannerId;

  PlannerProfileEditView(String? args) {
    if (args == null) {
      plannerId = -1;
    } else {
      var x = int.tryParse(args);
      if (x == null) {
        plannerId = -1;
      } else {
        plannerId = x;
      }
    }
  }

  @override
  State<PlannerProfileEditView> createState() => PlannerProfileEditViewState();
}

class PlannerProfileEditViewState extends State<PlannerProfileEditView> {
  late final Future<_ProfileModel> _profileInformation;
  late final _PlannerProfilePagePresenter _plannerProfilePagePresenter;
  final ValueNotifier<int> _validated = ValueNotifier(0);

  _ProfileModel? _data;
  String? _errorMessage;

  @override
  void initState() {
    _plannerProfilePagePresenter = _PlannerProfilePagePresenter(this);
    _profileInformation = _plannerProfilePagePresenter.fetchCourses(widget.plannerId);
    super.initState();
  }

  @override
  void dispose() {
    _validated.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: SingleChildScrollView(
        child: Column(
          children: [
            ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.mobileMaxWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    L10n.getString("planner_profile_edit_page_title"),
                    style: Theme.of(context).textTheme.displayMedium,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 8),
                  FutureBuilder<_ProfileModel>(
                    future: _profileInformation,
                    builder: (BuildContext context, AsyncSnapshot<_ProfileModel> snapshot) {
                      if (snapshot.hasError) {
                        return ErrorContainer(snapshot.error.toString());
                      } else if (snapshot.hasData) {
                        _data = snapshot.data;
                        return Column(
                          children: [
                            _CustomListTile(
                              label: L10n.getString("planner_profile_edit_page_identifier"),
                              value: _data!.username,
                              onChanged: (String value) => _data!.username = value,
                              iconData: Icons.person_pin,
                              enabled: false,
                              inputFormatter: FilteringTextInputFormatter.allow(userIdentifierRegExp),
                              obscureText: false,
                            ),
                            _CustomListTile(
                              label: L10n.getString("planner_profile_edit_page_password"),
                              value: _data!.password1,
                              onChanged: (String value) {
                                _data!.password1 = value;
                                _validated.value++;
                              },
                              iconData: Icons.lock,
                              enabled: true,
                              inputFormatter: null,
                              obscureText: true,
                            ),
                            _CustomListTile(
                              label: L10n.getString("planner_profile_edit_page_repassword"),
                              value: _data!.password2,
                              onChanged: (String value) {
                                _data!.password2 = value;
                                _validated.value++;
                              },
                              iconData: Icons.lock,
                              enabled: true,
                              inputFormatter: null,
                              obscureText: true,
                            ),
                            _CustomListTile(
                              label: L10n.getString("admin_planner_add_dialog_lastname"),
                              value: _data!.lastName,
                              onChanged: (String value) {
                                _data!.lastName = value;
                                _validated.value++;
                              },
                              iconData: Icons.person_outlined,
                              enabled: true,
                              inputFormatter: FilteringTextInputFormatter.allow(userIdentifierRegExp),
                              obscureText: false,
                            ),
                            _CustomListTile(
                              label: L10n.getString("admin_planner_add_dialog_firstname"),
                              value: _data!.firstName,
                              onChanged: (String value) {
                                _data!.firstName = value;
                                _validated.value++;
                              },
                              iconData: Icons.person_add_outlined,
                              enabled: true,
                              inputFormatter: FilteringTextInputFormatter.allow(userIdentifierRegExp),
                              obscureText: false,
                            ),
                            _CustomListTile(
                              label: L10n.getString("planner_profile_edit_page_email"),
                              value: _data!.email,
                              onChanged: (String value) {
                                _data!.email = value;
                                _validated.value++;
                              },
                              iconData: Icons.mail_outline,
                              enabled: true,
                              inputFormatter: null,
                              obscureText: false,
                            ),
                            _CustomListTile(
                              label: L10n.getString("planner_profile_edit_page_phone"),
                              value: _data!.phone,
                              onChanged: (String value) {
                                _data!.phone = value;
                                _validated.value++;
                              },
                              iconData: Icons.phone,
                              enabled: true,
                              inputFormatter: FilteringTextInputFormatter.allow(phoneRegExp),
                              obscureText: false,
                            ),
                          ],
                        );
                      } else {
                        return const Align(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(
                            color: kOrangeColor,
                          ),
                        );
                      }
                    },
                  ),
                  if (_errorMessage != null)
                    Container(
                      margin: const EdgeInsets.only(bottom: 30),
                      padding: const EdgeInsets.all(20),
                      width: double.infinity,
                      color: const Color(0xFFf8d7da),
                      child: Text(_errorMessage!, style: const TextStyle(color: Color(0xFFa92f29))),
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AnimatedBuilder(
                        animation: _validated,
                        builder: (_, __) => CustomPlainButton(
                          backgroundColor: kOrangeColor,
                          label: L10n.getString("planner_profile_edit_page_submit"),
                          onPressed: (_data != null)
                              ? () {
                                  if (_data!.password1 == _data!.password2) {
                                    _plannerProfilePagePresenter.sendNewProfile(widget.plannerId, _data!);
                                  } else {
                                    showError(L10n.getString("planner_profile_edit_page_error_password"));
                                  }
                                }
                              : null,
                        ),
                      ),
                      const SizedBox(width: 16),
                      CustomOutlinedButton(
                        foregroundColor: kOrangeColor,
                        backgroundColor: kOrangeColor,
                        onPressed: () => GoRouter.of(context).pop(),
                        label: L10n.getString("planner_profile_edit_page_cancel"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Footer(),
          ],
        ),
      ),
    );
  }

  void redirect(String page) {
    GoRouter.of(context).replace(page);
  }

  void showError(String message) {
    setState(() {
      _errorMessage = message;
    });
  }
}

class _CustomListTile extends StatelessWidget {
  final String? label;
  final String? value;
  final ValueChanged<String>? onChanged;
  final IconData iconData;
  final bool enabled;
  final TextInputFormatter? inputFormatter;
  final bool obscureText;

  const _CustomListTile({
    required this.label,
    required this.value,
    required this.onChanged,
    required this.iconData,
    required this.enabled,
    required this.inputFormatter,
    required this.obscureText,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        obscureText: obscureText,
        keyboardType: TextInputType.name,
        inputFormatters: [
          LengthLimitingTextInputFormatter(60),
          if (inputFormatter != null) inputFormatter!,
        ],
        onChanged: onChanged,
        initialValue: value,
        decoration: InputDecoration(
          enabled: enabled,
          disabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: kCaptionColor,
            ),
          ),
          labelText: label,
        ),
      ),
      leading: Icon(
        iconData,
        color: kOrangeColor,
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
    );
  }
}

class _PlannerProfilePagePresenter {
  final PlannerProfileEditViewState _view;

  const _PlannerProfilePagePresenter(this._view);

  Future<_ProfileModel> fetchCourses(int plannerId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse(sprintf("${constants.API_SERVER}/planner/profile/%d", [plannerId])),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          try {
            return _ProfileModel.fromJson(json.decode(response.body));
          } catch (_) {
            return Future.error(L10n.getString("Bad response data"));
          }
        } else {
          return Future.error(json.decode(response.body));
        }
      } catch (e) {
        return Future.error("Server connection error");
      }
    }
  }

  Future<void> sendNewProfile(int plannerId, _ProfileModel data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      _view.redirect(LoginPageView.routePath);
    } else {
      const String URL = "${constants.API_SERVER}/planner/profile/modify";
      try {
        final response = await http.patch(
          Uri.parse("$URL/$plannerId"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: data.toJson(),
        );
        if (response.statusCode == HttpStatus.ok) {
          _view.redirect("${PlannerProfilePage.routePath}/$plannerId");
        } else {
          _view.showError(L10n.translateIfPossible(response.body));
        }
      } catch (e) {
        _view.showError(e.toString());
      }
    }
  }
}

class _ProfileModel {
  String username;
  String email;
  String? lastName;
  String? firstName;
  String? password1;
  String? password2;
  String? phone;

  _ProfileModel({
    required this.username,
    required this.email,
    required this.phone,
    required this.lastName,
    required this.firstName,
  });

  factory _ProfileModel.fromJson(Map<String, dynamic> json) {
    return _ProfileModel(
      username: json["username"],
      email: json["email"],
      phone: json["phone"],
      lastName: json["lastname"],
      firstName: json["firstname"],
    );
  }

  dynamic toJson() {
    return json.encode({
      "username": username,
      "password": password1,
      "email": email,
      "phone": phone,
      "lastname": lastName,
      "firstname": firstName,
    });
  }
}
