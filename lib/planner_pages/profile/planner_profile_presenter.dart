import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/authentication/unauthorized_exception.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';

import 'planner_profile_edit_view.dart';
import 'planner_profile_page.dart';

class PlannerProfilePagePresenter {
  final PlannerProfilePageState _view;
  bool _isAdmin = false;

  PlannerProfilePagePresenter(this._view);

  Future<ProfileModel> fetchProfile(int plannerId) async {
    try {
      var user = await IdentifiedUser.getUser();
      _isAdmin = user?.admin ?? false;
      return _PlannerProfileModel.fetchProfile(plannerId);
    } catch (_) {
      _view.redirectTo(PlannerProfileEditView.routePath);

      return Future.error('');
    }
  }

  Future<String?> deletePlanner() async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return "Access token error";
    } else {
      final response = await http.delete(
        Uri.parse("${constants.API_SERVER}/planner/delete-planner"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        user?.logout();
        return null;
      } else {
        var error = json.decode(response.body);
        return error["message"];
      }
    }
  }

  bool isAdmin() {
    return _isAdmin;
  }
}

class _PlannerProfileModel {
  static const String URL = '${constants.API_SERVER}/planner/profile';

  static Future<ProfileModel> fetchProfile(int plannerId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      throw UnauthorizedException();
    } else {
      try {
        final response = await http.get(
          Uri.parse("$URL/$plannerId"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          try {
            return ProfileModel.fromJson(json.decode(response.body));
          } catch (_) {
            return Future.error(L10n.getString("Bad response data"));
          }
        } else {
          return Future.error(L10n.getString(jsonDecode(response.body)));
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }
}

class ProfileModel {
  final String username;
  final String email;
  final String? phone;
  final String? lastName;
  final String? firstName;

  const ProfileModel({
    required this.username,
    required this.email,
    required this.phone,
    required this.lastName,
    required this.firstName,
  });

  factory ProfileModel.fromJson(Map<String, dynamic> json) {
    return ProfileModel(
      username: json["username"],
      email: json["email"],
      phone: json["phone"],
      lastName: json["lastname"],
      firstName: json["firstname"],
    );
  }
}
