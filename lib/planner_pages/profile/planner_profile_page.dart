// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/home/home_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'planner_profile_edit_view.dart';
import 'planner_profile_presenter.dart';

class PlannerProfilePage extends StatefulWidget {
  static const String routePath = '/planner/personal-info';
  late final int plannerId;

  PlannerProfilePage(String? args) {
    if (args == null) {
      plannerId = -1;
    } else {
      var x = int.tryParse(args);
      if (x == null) {
        plannerId = -1;
      } else {
        plannerId = x;
      }
    }
  }

  @override
  State<PlannerProfilePage> createState() => PlannerProfilePageState();
}

class PlannerProfilePageState extends State<PlannerProfilePage> {
  late final Future<ProfileModel> _profileInformation;
  late final PlannerProfilePagePresenter _presenter;

  @override
  void initState() {
    _presenter = PlannerProfilePagePresenter(this);
    _profileInformation = _presenter.fetchProfile(widget.plannerId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: SingleChildScrollView(
        child: Column(
          children: [
            ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.mobileMaxWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("profile_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                  const SizedBox(height: 18),
                  Text(
                    L10n.getString("profile_page_privacy_note"),
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                  FutureBuilder<ProfileModel>(
                    future: _profileInformation,
                    builder: (BuildContext context, AsyncSnapshot<ProfileModel> snapshot) {
                      if (snapshot.hasError) {
                        return ErrorContainer(snapshot.error.toString());
                      } else if (snapshot.hasData) {
                        return Column(
                          children: [
                            _CustomListTile(snapshot.data!.username, Icons.person_pin),
                            _CustomListTile(snapshot.data!.lastName, Icons.person_outlined),
                            _CustomListTile(snapshot.data!.firstName, Icons.person_add_outlined),
                            _CustomListTile(snapshot.data!.email, Icons.mail_outline),
                            _CustomListTile(snapshot.data!.phone, Icons.phone),
                            Wrap(
                              alignment: WrapAlignment.center,
                              runAlignment: WrapAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: CustomPlainButton(
                                    onPressed: () => GoRouter.of(context).push("${PlannerProfileEditView.routePath}/${widget.plannerId}"),
                                    label: L10n.getString("profile_page_modify_button"),
                                    backgroundColor: kOrangeColor,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: CustomPlainButton(
                                    label: L10n.getString("profile_page_delete_button"),
                                    backgroundColor: kRed,
                                    onPressed: _presenter.isAdmin()
                                        ? null
                                        : () async {
                                            showBinaryQuestionAlertDialog(
                                              context: context,
                                              title: L10n.getString("profile_page_delete_title"),
                                              message: L10n.getString("profile_page_delete_message"),
                                              okMessage: L10n.getString("profile_page_delete_ok_message"),
                                              noMessage: L10n.getString("profile_page_delete_no_message"),
                                              action: () async {
                                                String? error = await _presenter.deletePlanner();
                                                if (error != null) {
                                                  if (context.mounted) {
                                                    showErrorDialog(
                                                      context,
                                                      title: L10n.getString("profile_page_delete_error_title"),
                                                      message: L10n.getString(error),
                                                    );
                                                  }
                                                } else {
                                                  if (context.mounted) {
                                                    GoRouter.of(context).replace(HomePageView.routePath);
                                                  }
                                                }
                                              },
                                            );
                                          },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        );
                      } else {
                        return const Align(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(
                            color: kOrangeColor,
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
            Footer(),
          ],
        ),
      ),
    );
  }

  void redirectTo(String routePath) {
    GoRouter.of(context).replace(routePath);
  }
}

class _CustomListTile extends StatelessWidget {
  final String? label;
  final IconData iconData;

  const _CustomListTile(this.label, this.iconData);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(width: 1, color: kCaptionColor),
        ),
        padding: const EdgeInsets.all(12),
        child: SelectableText(label ?? ''),
      ),
      leading: Icon(
        iconData,
        color: kOrangeColor,
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
    );
  }
}
