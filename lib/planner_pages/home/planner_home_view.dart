// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/planner_pages/home/planner_doc_page/planner_doc_page.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'planner_home_presenter.dart';

class PlannerHomeView extends StatelessWidget {
  static const String routePath = '/planner/home';

  const PlannerHomeView();

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: SingleChildScrollView(
        child: Padding(
          padding: ScreenHelper.isDesktop(context) ? const EdgeInsets.all(0) : const EdgeInsets.all(8),
          child: Column(
            children: [
              _Notification(),
              Container(
                constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Text(
                        L10n.getString("planner_home_page_title"),
                        style: Theme.of(context).textTheme.displayMedium,
                      ),
                    ),
                    SizedBox(height: ScreenHelper.isMobile(context) ? 10 : 40),
                    Text(
                      L10n.getString("planner_home_page_introduction"),
                    ),
                    const SizedBox(height: 15),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Wrap(
                        children: [
                          _ButtonCard(
                            icon: Icons.school_sharp,
                            title: L10n.getString("planner_home_page_box1_title"),
                            text: L10n.getString("planner_home_page_box1_text"),
                            onPressed: () => launchUrl(Uri.parse("${constants.SERVER}/public/docs/doc-vikazimut.pdf")),
                          ),
                          Banner(
                            message: L10n.getString("beginner"),
                            location: BannerLocation.topStart,
                            color: kBlueColor,
                            textStyle: const TextStyle(
                              color: Colors.white,
                              fontSize: 12 * 0.85,
                              fontWeight: FontWeight.w900,
                              height: 1.0,
                            ),
                            child: _ButtonCard(
                              icon: Icons.personal_video_sharp,
                              title: L10n.getString("planner_home_page_box2_title"),
                              text: L10n.getString("planner_home_page_box2_text"),
                              onPressed: () => launchUrl(Uri.parse("${constants.SERVER}/public/tutorials.html")),
                            ),
                          ),
                          _ButtonCard(
                            icon: Icons.location_on,
                            title: L10n.getString("planner_home_page_box3_title"),
                            text: L10n.getString("planner_home_page_box3_text"),
                            credit: L10n.getString("planner_home_page_box3_credit"),
                            onPressed: () => GoRouter.of(context).go("${PlannerDocPage.routePath}?section=3"),
                          ),
                          _ButtonCard(
                            icon: Icons.smartphone,
                            title: L10n.getString("planner_home_page_box4_title"),
                            text: L10n.getString("planner_home_page_box4_text"),
                            onPressed: () => GoRouter.of(context).go("${PlannerDocPage.routePath}?section=1"),
                          ),
                          _ButtonCard(
                            icon: Icons.smartphone,
                            title: L10n.getString("planner_home_page_box5_title"),
                            text: L10n.getString("planner_home_page_box5_text"),
                            onPressed: () => GoRouter.of(context).go("${PlannerDocPage.routePath}?section=2"),
                          ),
                          // _ButtonCard(
                          //   icon: Icons.construction,
                          //   title: I18n.getString("planner_home_page_box6_title"),
                          //   text: I18n.getString("planner_home_page_box6_text"),
                          //   credit: I18n.getString("planner_home_page_box6_credit"),
                          //   onPressed: () => launchUrl(Uri.parse("https://github.com/fbd38/ViKairn")),
                          // ),
                          _ButtonCard(
                            icon: Icons.inventory_outlined,
                            title: L10n.getString("planner_home_page_box8_title"),
                            text: L10n.getString("planner_home_page_box8_text"),
                            onPressed: () => GoRouter.of(context).go("${PlannerDocPage.routePath}?section=4"),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Footer(),
            ],
          ),
        ),
      ),
    );
  }
}

class _Notification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<dynamic>>(
        future: PlannerHomePresenter.fetchMissingCheckpoints(),
        builder: (context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasData && snapshot.data!.isNotEmpty) {
            return Container(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              margin: const EdgeInsets.symmetric(vertical: 10),
              color: kWarningColor,
              child: ListTile(
                leading: const Icon(
                  Icons.warning_amber,
                  size: 48,
                  color: kOrangeColor,
                ),
                title: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        text: L10n.getString("planner_home_page_missing_notification"),
                      ),
                      TextSpan(
                        text: "  ${snapshot.data}",
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }
}

class _ButtonCard extends StatelessWidget {
  final IconData icon;
  final String title;
  final String text;
  final String? credit;
  final VoidCallback onPressed;

  const _ButtonCard({
    required this.icon,
    required this.title,
    required this.text,
    required this.onPressed,
    this.credit,
  });

  @override
  Widget build(BuildContext context) {
    Widget content;
    if (credit != null) {
      content = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(text, style: Theme.of(context).textTheme.titleMedium!),
          const SizedBox(height: 10),
          Text(credit!, style: Theme.of(context).textTheme.titleMedium!.copyWith(fontSize: 11)),
        ],
      );
    } else {
      content = Text(text, style: Theme.of(context).textTheme.titleMedium!);
    }

    return SizedBox(
      width: ScreenHelper.isMobile(context) ? 270 : 350,
      height: ScreenHelper.isMobile(context) ? 190 : 210,
      child: GestureDetector(
        onTap: onPressed,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: const BorderSide(color: kOrangeColor),
          ),
          elevation: 10,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Icon(
                  icon,
                  size: 48,
                  color: kOrangeColor,
                ),
                const SizedBox(width: 5),
                Flexible(
                  child: Column(
                    children: [
                      Text(
                        title,
                        style: const TextStyle(color: kOrangeColor, fontSize: 20),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: content,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
