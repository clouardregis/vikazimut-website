import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class PlannerHomePresenter {
  static const String _URL = '${constants.API_SERVER}/planner/missing-checkpoints';

  static Future<List<dynamic>> fetchMissingCheckpoints() async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      final response = await http.get(
        Uri.parse(_URL),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        var jsonResponse = json.decode(response.body);
        return jsonResponse['missingCheckpoints'] as List<dynamic>;
      } else {
        return Future.error(response.body);
      }
    }
  }
}
