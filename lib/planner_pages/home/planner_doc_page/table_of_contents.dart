import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'planner_doc_page.dart';

class TableOfContents extends StatelessWidget {
  static const double _textSize = 16;
  final ScrollController controller = ScrollController();
  final List<IndexInTableOfContents> tableOfContents;

  TableOfContents({required this.tableOfContents});

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      thumbVisibility: true,
      controller: controller,
      child: SingleChildScrollView(
        controller: controller,
        scrollDirection: Axis.vertical,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 250),
          child: Column(
            children: _buildTableOfContentPanel(context, tableOfContents),
          ),
        ),
      ),
    );
  }

  Widget _displaySection(body) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: body,
      ),
    );
  }

  Widget _displayIndexSection(BuildContext context, int index, String text, IconData icon) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Icon(icon, color: kOrangeColor),
          const SizedBox(width: 8),
          Expanded(
            child: RichText(
              text: TextSpan(
                text: text,
                style: const TextStyle(color: kOrangeColor, fontSize: _textSize + 2),
                recognizer: TapGestureRecognizer()..onTap = () => _scrollToItem(index),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _displayIndexSubSection(BuildContext context, int index, String text) {
    return Container(
      margin: const EdgeInsets.only(left: 20),
      decoration: const BoxDecoration(
        border: Border(
          left: BorderSide(color: kOrangeColorLight, width: 4),
        ),
      ),
      padding: const EdgeInsets.all(8.0),
      child: RichText(
        text: TextSpan(
          text: text,
          style: TextStyle(color: Theme.of(context).hintColor, fontSize: _textSize),
          recognizer: TapGestureRecognizer()..onTap = () => _scrollToItem(index),
        ),
      ),
    );
  }

  void _scrollToItem(int index) {
    final BuildContext context = tableOfContents[index].key.currentContext!;
    Scrollable.ensureVisible(context);
  }

  List<Widget> _buildTableOfContentPanel(BuildContext context, List<IndexInTableOfContents> tableOfContents) {
    List<Widget> children = [];
    List<Widget> contents = [];
    for (int i = 0; i < tableOfContents.length; i++) {
      IndexInTableOfContents x = tableOfContents[i];
      if (x.level == 1) {
        if (contents.isNotEmpty) {
          children.add(_displaySection(contents));
          contents = [];
        }
        contents.add(_displayIndexSection(context, i, x.title, Icons.toc));
      } else {
        contents.add(_displayIndexSubSection(context, i, x.title));
      }
    }
    if (contents.isNotEmpty) {
      children.add(_displaySection(contents));
    }
    return children;
  }
}
