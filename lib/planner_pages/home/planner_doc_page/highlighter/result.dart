import 'node.dart';
import 'mode.dart';

class Result {
  int? relevance;
  List<Node>? nodes;
  String? language;
  Mode? top;
  Result? secondBest;

  Result({
    this.relevance,
    this.nodes,
    this.language,
    this.top,
    this.secondBest,
  });
}
