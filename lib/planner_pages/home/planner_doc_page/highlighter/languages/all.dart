import 'css.dart';
import 'javascript.dart';
import 'json.dart';
import 'xml.dart';

final builtinLanguages = {
  'css': css,
  'javascript': javascript,
  'json': json,
  'xml': xml,
};
final allLanguages = builtinLanguages;
