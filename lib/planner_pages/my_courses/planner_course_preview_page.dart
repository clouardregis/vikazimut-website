// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_or_rigid_sidebar.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'planner_course_add_edit_page/planner_course_edit_view.dart';

class PlannerCoursePreviewPage extends StatefulWidget {
  static const String routePath = '/planner/course/preview';
  final int courseId;
  late final bool reload;

  PlannerCoursePreviewPage(args, String? reload) : courseId = int.parse(args) {
    this.reload = reload != null && reload == "true";
  }

  @override
  State<PlannerCoursePreviewPage> createState() => _PlannerCoursePreviewPageState();
}

class _PlannerCoursePreviewPageState extends State<PlannerCoursePreviewPage> with TickerProviderStateMixin {
  final _PlannerCoursePreviewPresenter _courseInfoPresenter = _PlannerCoursePreviewPresenter();
  late final Future<_CourseDetail?> _courseDetail;
  late final Future<_CourseData> _courseData;
  late final ValueNotifier<String> _courseName;
  late final MapZoomController _mapZoomController;
  late final Future<String> useThisReferenceToAvoidCallAfterDispose;
  bool _isMapShowed = true;

  @override
  void initState() {
    _courseDetail = _courseInfoPresenter.loadCourseDetail(widget.courseId);
    _courseData = _courseInfoPresenter.loadCourseData(widget.courseId);
    _courseName = ValueNotifier<String>("");
    useThisReferenceToAvoidCallAfterDispose = _courseData.then((_CourseData value) => _courseName.value = value.courseReference.name);
    _mapZoomController = MapZoomController(this);
    super.initState();
  }

  @override
  void dispose() {
    useThisReferenceToAvoidCallAfterDispose.ignore();
    _courseName.dispose();
    _mapZoomController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var overlayImages = <CustomBaseOverlayImage>[];
    return ResponsiveScaffoldWidget(
      body: FutureBuilder<_CourseDetail?>(
        future: _courseDetail,
        builder: (BuildContext context, AsyncSnapshot<_CourseDetail?> snapshotCourse) {
          var titleText = ValueListenableBuilder<String>(
              valueListenable: _courseName,
              builder: (context, value, widget) {
                return Text(
                  value,
                  style: Theme.of(context).textTheme.headlineMedium?.copyWith(color: kOrangeColor),
                );
              });
          if (snapshotCourse.hasData) {
            return CollapsibleOrRigidSidebar(
              isCollapsible: !ScreenHelper.isDesktop(context),
              sidebarTitle: titleText,
              sidebarContents: [
                Center(
                  child: CustomPlainButton(
                    backgroundColor: kOrangeColor,
                    label: L10n.getString("information_discipline_modify_button"),
                    onPressed: () => GoRouter.of(context).go("${PlannerCourseEditPage.routePath}/${widget.courseId}"),
                  ),
                ),
                _ItemWidget(
                  label: L10n.getString("information_discipline_tile"),
                  icon: Icon(
                    Discipline.getIcon(snapshotCourse.data!.discipline),
                    color: Theme.of(context).iconTheme.color,
                    size: 32,
                  ),
                ),
                _ItemWidget(
                  label: L10n.getString("information_author_tile"),
                  value: snapshotCourse.data!.creator,
                ),
                _ItemWidget(
                  label: L10n.getString("information_club_name_tile"),
                  value: (snapshotCourse.data!.club != null) ? snapshotCourse.data!.club! : "",
                ),
                if (snapshotCourse.data!.clubUrl != null)
                  _ItemWidget(
                    label: L10n.getString("information_club_url_tile"),
                    url: (snapshotCourse.data!.clubUrl != null) ? snapshotCourse.data!.clubUrl! : "",
                  ),
                _ItemWidget(
                  label: L10n.getString("information_date_tile"),
                  value: getLocalizedDate(convertISO8601DateToMilliseconds(snapshotCourse.data!.date)),
                ),
                _ItemWidget(
                  label: L10n.getString("information_length_tile"),
                  value: '${L10n.formatNumber(snapshotCourse.data!.length, 1)} m',
                ),
                if (snapshotCourse.data!.touristic)
                  _ItemWidget(
                    label: L10n.getString("information_note_title"),
                    valueColor: kInfoColor,
                    value: L10n.getString("information_note_text"),
                  ),
                _ItemWidget(
                  label: L10n.getString("planner_course_preview_checkpoint_label"),
                ),
                FutureBuilder<_CourseData>(
                  future: _courseData,
                  builder: (BuildContext context, AsyncSnapshot<_CourseData> snapshot) {
                    if (snapshot.data != null) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 4, right: 15),
                        child: _buildDataTable(snapshot.data!.checkpoints),
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
              ],
              contents: FutureBuilder<_CourseData>(
                future: _courseData,
                builder: (BuildContext context, AsyncSnapshot<_CourseData> snapshot) {
                  if (snapshot.hasError) {
                    return GlobalErrorWidget(snapshot.error.toString());
                  } else if (snapshot.data != null) {
                    var latLngBounds = snapshot.data!.courseReference.latLngBounds();
                    var rotation = -snapshot.data!.courseReference.bounds.rotation;
                    LatLngBounds bounds = LatLngBounds(LatLng(latLngBounds[3][0], latLngBounds[3][1]), LatLng(latLngBounds[1][0], latLngBounds[1][1]));
                    _mapZoomController.bounds = bounds;
                    overlayImages = <CustomRotatedOverlayImage>[
                      CustomRotatedOverlayImage(
                        bounds: bounds,
                        opacity: 0.6,
                        angleInDegree: rotation,
                        imageProvider: loadNetworkImage(_courseInfoPresenter.user, snapshot.data!.courseReference.imageId, widget.reload),
                      ),
                    ];
                    return Stack(
                      alignment: Alignment.topRight,
                      children: [
                        _Map(
                          mapController: _mapZoomController.mapController,
                          bounds: bounds,
                          startLatitude: snapshot.data!.courseReference.startLatitude,
                          startLongitude: snapshot.data!.courseReference.startLongitude,
                          showedMap: _isMapShowed,
                          overlayImages: overlayImages,
                          checkpoints: snapshot.data!.checkpoints,
                        ),
                        Positioned(
                          right: 5.0,
                          top: 15.0,
                          child: Column(
                            children: [
                              if (ScreenHelper.isDesktop(context))
                                _MapButton(
                                  onPressed: () => _mapZoomController.zoomIn(),
                                  iconData: Icons.add,
                                ),
                              if (ScreenHelper.isDesktop(context))
                                _MapButton(
                                  onPressed: () => _mapZoomController.zoomOut(),
                                  iconData: Icons.remove,
                                ),
                              if (!ScreenHelper.isMobile(context))
                                _MapButton(
                                  onPressed: () => _mapZoomController.reset(),
                                  iconData: Icons.my_location,
                                ),
                              _MapButton(
                                onPressed: () => setState(() => _isMapShowed = !_isMapShowed),
                                iconData: (_isMapShowed) ? Icons.layers_clear : Icons.layers,
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            );
          } else if (snapshotCourse.hasError) {
            return GlobalErrorWidget(L10n.getString(snapshotCourse.error.toString()));
          }
          return Container();
        },
      ),
    );
  }

  DataTable _buildDataTable(List<Checkpoint> checkpoints) {
    return DataTable(
      headingTextStyle: const TextStyle(fontSize: 13, color: Colors.black),
      dataTextStyle: Theme.of(context).textTheme.bodySmall,
      headingRowHeight: 20,
      dataRowMinHeight: 20,
      dataRowMaxHeight: 20,
      columnSpacing: 0,
      columns: [
        DataColumn(label: Text(L10n.getString("planner_course_preview_table_header1"))),
        DataColumn(label: Text(L10n.getString("planner_course_preview_table_header2"))),
        DataColumn(label: Text(L10n.getString("planner_course_preview_table_header3"))),
        DataColumn(label: Text(L10n.getString("planner_course_preview_table_header4"))),
      ],
      rows: checkpoints
          .map((checkpoint) => DataRow(cells: [
                DataCell(Text("${checkpoint.index}")),
                DataCell(Text(checkpoint.id)),
                DataCell(Text(checkpoint.type)),
                DataCell(Text("${checkpoint.score}")),
              ]))
          .toList(),
    );
  }
}

class _MapButton extends StatelessWidget {
  final VoidCallback onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final String label;
  final String? value;
  final Color? valueColor;
  final String? url;
  final Widget? icon;

  const _ItemWidget({
    required this.label,
    this.icon,
    this.value,
    this.valueColor,
    this.url,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 5.0),
      title: Text(
        label,
        style: const TextStyle(color: kOrangeColor),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: url != null
            ? InkWell(
                onTap: () => launchUrl(Uri.parse(url!)),
                child: Text(
                  _removeHttpFromUrl(url!),
                  textScaler: const TextScaler.linear(1.2),
                  style: const TextStyle(color: kInfoColor),
                ),
              )
            : (value != null)
                ? Text(
                    value!,
                    textScaler: const TextScaler.linear(1.2),
                    style: TextStyle(color: valueColor),
                  )
                : (icon != null)
                    ? Align(
                        alignment: Alignment.topLeft,
                        child: icon,
                      )
                    : null,
      ),
    );
  }

  _removeHttpFromUrl(String url) {
    if (url.contains("https://")) return url.substring(8);
    if (url.contains("http://")) return url.substring(7);
    return url;
  }
}

class _Map extends StatefulWidget {
  final MapController mapController;
  final LatLngBounds bounds;
  final double startLatitude;
  final double startLongitude;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;
  final List<Checkpoint> checkpoints;

  const _Map({
    required this.mapController,
    required this.bounds,
    required this.startLatitude,
    required this.startLongitude,
    required this.showedMap,
    required this.overlayImages,
    required this.checkpoints,
  });

  @override
  State<_Map> createState() => _MapState();
}

class _MapState extends State<_Map> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 19,
        minZoom: 8,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        if (widget.showedMap) CustomOverlayImageLayer(overlayImages: widget.overlayImages),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        MarkerLayer(
          markers: [
            ..._buildCheckpointMarkers(widget.checkpoints),
            Marker(
              alignment: Alignment.topCenter,
              width: 48.0,
              height: 48.0,
              point: LatLng(widget.startLatitude, widget.startLongitude),
              child: const Icon(
                Icons.location_pin,
                color: Colors.blueAccent,
                size: 48,
              ),
            ),
          ],
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }

  List<Marker> _buildCheckpointMarkers(List<Checkpoint> checkpoints) {
    Map<LatLng, String> tooltips = _createTooltipsFromCheckpointIds(checkpoints);
    List<Marker> markers = [];
    for (var checkpoint in checkpoints) {
      markers.add(
        Marker(
          alignment: Alignment.center,
          width: 24.0,
          height: 24.0,
          point: LatLng(checkpoint.latitude, checkpoint.longitude),
          child: Tooltip(
            message: tooltips[_getPosition(checkpoint.latitude, checkpoint.longitude)],
            child: const Icon(
              Icons.circle,
              color: Colors.red,
              size: 16,
            ),
          ),
        ),
      );
    }
    return markers;
  }

  LatLng _getPosition(double latitude, double longitude) {
    double lat = double.parse(latitude.toStringAsFixed(4));
    double lon = double.parse(longitude.toStringAsFixed(4));
    return LatLng(lat, lon);
  }

  Map<LatLng, String> _createTooltipsFromCheckpointIds(List<Checkpoint> checkpoints) {
    Map<LatLng, String> tooltips = {};
    for (var checkpoint in checkpoints) {
      var key = _getPosition(checkpoint.latitude, checkpoint.longitude);
      if (tooltips.containsKey(key)) {
        tooltips[key] = "${tooltips[key]!}, ${checkpoint.id}";
      } else {
        tooltips[key] = checkpoint.id;
      }
    }
    return tooltips;
  }
}

class _PlannerCoursePreviewPresenter {
  IdentifiedUser? user;

  Future<_CourseDetail?> loadCourseDetail(int courseId) async {
    user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    try {
      final response = await http.get(
        Uri.parse("${constants.API_SERVER}/courses/detail/$courseId"),
        headers: headers,
      );
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        return Future.value(_CourseDetail.fromJson(data));
      } else {
        return Future.error(response.body);
      }
    } catch (_) {
      return Future.error("Server connection error");
    }
  }

  Future<_CourseData> loadCourseData(int courseId) async {
    CourseGeoreference map = await CourseGeoreference.loadCourseMap(courseId);
    List<Checkpoint> checkpoints = await Checkpoint.loadCourseCheckpoints(courseId);
    return _CourseData(map, checkpoints);
  }
}

@immutable
class _CourseDetail {
  final String name;
  final String creator;
  final int length;
  final int checkpointCount;
  final Discipline? discipline;
  final String? club;
  final String? clubUrl;
  final bool touristic;
  final String date;

  const _CourseDetail({
    required this.name,
    required this.creator,
    required this.length,
    required this.checkpointCount,
    required this.date,
    this.discipline,
    this.club,
    this.clubUrl,
    this.touristic = false,
  });

  factory _CourseDetail.fromJson(Map<String, dynamic> json) {
    return _CourseDetail(
      name: json["name"] as String,
      creator: json["creator"] as String,
      length: json["length"] as int,
      checkpointCount: json["checkpointCount"] as int,
      discipline: Discipline.toEnum(json["discipline"] as int?),
      club: json["club_name"] as String?,
      clubUrl: json["club_url"] as String?,
      date: json["date"] as String,
      touristic: (json["touristRoute"] as bool?) ?? false,
    );
  }
}

class _CourseData {
  final CourseGeoreference courseReference;
  final List<Checkpoint> checkpoints;

  const _CourseData(this.courseReference, this.checkpoints);
}
