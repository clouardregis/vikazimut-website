import 'dart:typed_data';

import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:xml/xml.dart';

import 'image_helper.dart';
import 'map_image_data.dart';

class KmlConverter {
  static (XmlElement, MapImageData?) convertTileImagesToSingleImage(
    Iterable<XmlElement> groundOverlays,
    List<MapImageData>? mapImageData,
  ) {
    final LatLonBox bounds = _calculateImageBounds(groundOverlays);
    final int numCols = _calculateNumberOfColumns(groundOverlays, bounds);
    final Uint8List image = ImageHelper.combineImages(mapImageData!, numCols);
    final MapImageData imageData = MapImageData(imageContent: image, imageFilename: "files/tile_0_0.jpg");
    final xml = """
<groundoverlay>
<name>tile_0_0.jpg</name>
<icon>
<href>files/tile_0_0.jpg</href>
</icon>
<latlonbox>
<north>${bounds.north}</north>
<south>${bounds.south}</south>
<east>${bounds.east}</east>
<west>${bounds.west}</west>
<rotation>${bounds.rotation}</rotation>
</latlonbox>
</groundoverlay>""";
    final XmlDocument document = XmlDocument.parse(xml);
    return (document.rootElement, imageData);
  }

  static LatLonBox _calculateImageBounds(Iterable<XmlElement> groundOverlays) {
    double north = double.negativeInfinity;
    double south = double.infinity;
    double east = double.negativeInfinity;
    double west = double.infinity;
    double rotation = 0;
    for (int i = 0; i < groundOverlays.length; i++) {
      var groundOverlay = groundOverlays.elementAt(i);
      Iterable<XmlElement> latLonBox = groundOverlay.findAllElements("latlonbox");
      if (latLonBox.isNotEmpty) {
        XmlElement first = latLonBox.first;
        var n = double.parse(first.getElement("north")!.innerText);
        var s = double.parse(first.getElement("south")!.innerText);
        var e = double.parse(first.getElement("east")!.innerText);
        var w = double.parse(first.getElement("west")!.innerText);
        double r;
        if (first.getElement("rotation") == null) {
          r = 0;
        } else {
          r = double.parse(first.getElement("rotation")!.innerText);
        }
        if (n > north) north = n;
        if (s < south) south = s;
        if (w < west) west = w;
        if (e > east) east = e;
        rotation = r;
      } else {
        throw Exception("Bad KML file");
      }
    }
    return LatLonBox(north: north, east: east, south: south, west: west, rotation: rotation);
  }

  static int _calculateNumberOfColumns(Iterable<XmlElement> groundOverlays, LatLonBox bounds) {
    double longitudeRange = bounds.east - bounds.west;
    var groundOverlay = groundOverlays.elementAt(0);
    Iterable<XmlElement> latLonBox = groundOverlay.findAllElements("latlonbox");
    var west = double.parse(latLonBox.first.getElement("west")!.innerText);
    var east = double.parse(latLonBox.first.getElement("east")!.innerText);
    double tileRange = east - west;
    return (longitudeRange / tileRange).ceil();
  }
}
