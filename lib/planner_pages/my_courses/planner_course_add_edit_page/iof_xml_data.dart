import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/xml_utils.dart';
import 'package:xml/xml.dart';

class IofXmlData {
  final String courseName;
  final String contents;

  const IofXmlData({required this.courseName, required this.contents});

  static List<IofXmlData> getAllCoursesFromXML(String? xmlAsString) {
    if (xmlAsString == null || xmlAsString.isEmpty) throw Exception("planner_course_edit_page_error12");
    final courses = <IofXmlData>[];
    final xmlText = convertXmlTagsToLowerCase(xmlAsString);
    final xmlDocument = XmlDocument.parse(xmlText);
    final courseFolder = xmlDocument.findAllElements("course");
    for (final course in courseFolder) {
      final name = course.getElement("name")!.innerText;
      courses.add(IofXmlData(courseName: name, contents: xmlAsString));
    }
    return courses;
  }

  static List<String> getAllCourseNamesFromXML(String xmlAsString) {
    final List<String> courseNames = [];
    final xmlDocument = XmlDocument.parse(xmlAsString);
    final courses = xmlDocument.findAllElements("Course");
    for (final course in courses) {
      courseNames.add(course.getElement("Name")!.innerText);
    }
    return courseNames;
  }

  String toXml() => purifyXml(contents, courseName);

  static String purifyXml(String xmlContent, String courseName) {
    var xml = IofXmlData.removeAllButTheSelectedCourseXml_(xmlContent, courseName);
    xml = _replaceEmptyScoreTagWithZero(xml);
    xml = IofXmlData.removeIntermediateStarts_(xml);
    return minifyXmlContent_(xml);
  }

  @visibleForTesting
  static String minifyXmlContent_(String xml) {
    final xmlDocument = XmlDocument.parse(xml);
    return xmlDocument.toXmlString(pretty: true, indent: "", newLine: "");
  }

  static String prettyPrint(String xml) {
    final xmlDocument = XmlDocument.parse(xml);
    return xmlDocument.toXmlString(pretty: true);
  }

  @visibleForTesting
  static String removeAllButTheSelectedCourseXml_(String contents, String selectedName) {
    final xmlDocument = XmlDocument.parse(contents);
    Iterable<XmlElement> courseFolder = xmlDocument.findAllElements("Course");
    XmlElement? raceCourseData = xmlDocument.getElement("CourseData")?.getElement("RaceCourseData");
    for (final course in courseFolder) {
      String name = course.getElement("Name")!.innerText;
      if (name != selectedName) {
        raceCourseData?.children.remove(course);
      }
    }
    return xmlDocument.toXmlString();
  }

  // Keep this for sake of back compatibility (fixed in the mobile application since 03/22/24)
  static String _replaceEmptyScoreTagWithZero(String contents) {
    return contents.replaceAll("<Score/>", "<Score>1</Score>").replaceAll("<Score></Score>", "<Score>1</Score>");
  }

  @visibleForTesting
  static String removeIntermediateStarts_(String xml) {
    final xmlDocument = XmlDocument.parse(xml);
    final Iterable<XmlElement> courses = xmlDocument.findAllElements("Course");
    final XmlElement course = courses.first;
    final Iterable<XmlElement> courseControls = course.findAllElements("CourseControl");
    bool started = false;
    for (final courseControl in courseControls) {
      String? name = courseControl.getAttribute("type");
      if (name == "Start") {
        if (!started) {
          started = true;
        } else {
          courses.first.children.remove(courseControl);
        }
      }
    }
    return xmlDocument.toString();
  }

  static bool checkFormat(String xmlAsString) {
    try {
      final XmlDocument document = XmlDocument.parse(xmlAsString);
      final declaration = document.declaration;
      final courseData = document.getElement("CourseData");
      return declaration != null && courseData != null;
    } catch (_) {
      return false;
    }
  }

  static String removeUnusedTags(String xmlContent) {
    final xmlDocument = XmlDocument.parse(xmlContent);
    final XmlElement raceCourseData = xmlDocument.findAllElements("RaceCourseData").first;
    final List<XmlElement> toRemove = [];
    for (XmlElement tag in raceCourseData.childElements) {
      final String name = tag.name.toString();
      if (!["Map", "Control", "Course"].contains(name)) {
        toRemove.add(tag);
      }
    }
    for (final tag in toRemove) {
      raceCourseData.children.remove(tag);
    }
    return xmlDocument.toXmlString();
  }
}
