abstract class PlannerCourseView {
  Future<void> displayErrors(List<dynamic> errorMessage);

  void redirect(String url);
}
