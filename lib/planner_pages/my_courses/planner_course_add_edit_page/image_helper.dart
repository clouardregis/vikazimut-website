import 'dart:math' as math;
import 'dart:typed_data';

import 'package:image/image.dart';

import 'map_image_data.dart';

class ImageHelper {
  static const double _imageMaxDimensionInPixels = 2000;

  static Uint8List combineImages(List<MapImageData> tileImages, int tilesPerRow) {
    // Get the first tile's image dimensions
    final Image firstTile = decodeImage(tileImages.first.imageContent!)!;
    final int tileWidth = firstTile.width;
    final int tileHeight = firstTile.height;

    // Calculate the size of the final image
    final int rows = (tileImages.length / tilesPerRow).ceil();
    final int finalWidth = tileWidth * tilesPerRow;
    final int finalHeight = tileHeight * rows;

    // Create a blank canvas for the final image
    final Image finalImage = Image(width: finalWidth, height: finalHeight);
    // Place each tile in the correct position on the final canvas
    for (int i = 0; i < tileImages.length; i++) {
      final int x = (i % tilesPerRow) * tileWidth;
      final int y = finalHeight - tileHeight - (i ~/ tilesPerRow) * tileHeight;
      final Image tile = decodeImage(tileImages[i].imageContent!)!;
      compositeImage(finalImage, tile, dstX: x, dstY: y);
    }
    final Image reducedImage = _reduceImageSize(finalImage);
    return Uint8List.fromList(encodeJpg(reducedImage, quality: 90));
  }

  static Uint8List reduceImageSizeToReasonableSize(Uint8List data) {
    final Image? image = decodeImage(data);
    if (image == null) {
      return data;
    }
    var maxDimension = math.max(image.width, image.height);
    if (maxDimension > _imageMaxDimensionInPixels) {
      final ratio = _imageMaxDimensionInPixels / maxDimension;
      final width = (image.width * ratio).toInt();
      final height = (image.height * ratio).toInt();
      final resizedImage = copyResize(image, width: width, height: height);
      return Uint8List.fromList(encodeJpg(resizedImage, quality: 90));
    }
    return data;
  }

  static Image _reduceImageSize(Image image) {
    final maxDimension = math.max(image.width, image.height);
    if (maxDimension > _imageMaxDimensionInPixels) {
      final ratio = _imageMaxDimensionInPixels / maxDimension;
      final width = (image.width * ratio).toInt();
      final height = (image.height * ratio).toInt();
      return copyResize(image, width: width, height: height);
    }
    return image;
  }
}
