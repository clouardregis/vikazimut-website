import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/common/xml_utils.dart';
import 'package:vikazimut_website/utils/utils.dart';
import 'package:xml/xml.dart';

import 'kml_converter.dart';
import 'kml_data.dart';
import 'map_image_data.dart';

class KmlParser {
  static (List<KmlData>, List<MapImageData>?) getAllGeoreferencedMapsFromKml(String kmlAsString, [List<MapImageData>? mapImageData]) {
    final kmlText = convertXmlTagsToLowerCase(kmlAsString);
    final XmlDocument document = XmlDocument.parse(kmlText);
    if (!checkKmlHeader_(document)) {
      throw const KmlException(-21, "planner_course_edit_page_error11");
    }

    XmlElement? mainFolder = document.getElement("kml")!.getElement("folder");
    if (mainFolder == null) {
      mainFolder = document.getElement("kml")!.getElement("groundoverlay");
      if (mainFolder == null) {
        throw const KmlException(-22, "planner_course_edit_page_error11");
      }
      mainFolder = document.getElement("kml")!;
    }
    final mapFolders = mainFolder.findElements("folder");
    List<KmlData> maps;
    if (mapFolders.isEmpty) {
      final (map, imageData) = _extractOneMap(mainFolder, mapImageData);
      maps = [map];
      mapImageData = imageData != null ? [imageData] : null;
    } else {
      maps = _extractMultipleMaps(mapFolders);
    }
    return (maps, mapImageData);
  }

  static (KmlData, MapImageData?) _extractOneMap(XmlElement folder, List<MapImageData>? mapImageData) {
    final groundOverlays = folder.findElements("groundoverlay");
    if (groundOverlays.length == 1) {
      KmlData kmlData = extractLatLonBoxAndImageName_(groundOverlays.first);
      return (kmlData, mapImageData?.first);
    } else {
      final (groundOverlay, image) = KmlConverter.convertTileImagesToSingleImage(groundOverlays, mapImageData);
      KmlData kmlData = extractLatLonBoxAndImageName_(groundOverlay);
      return (kmlData, image);
    }
  }

  static List<KmlData> _extractMultipleMaps(Iterable<XmlElement> mapFolders) {
    return mapFolders.map((folder) => extractLatLonQuadAndImage_(folder)).toList();
  }

  @visibleForTesting
  static KmlData extractLatLonBoxAndImageName_(XmlElement groundOverlay) {
    Iterable<XmlElement> latLonBox = groundOverlay.findAllElements("latlonbox");
    if (latLonBox.isNotEmpty) {
      XmlElement first = latLonBox.first;
      final north = double.parse(first.getElement("north")!.innerText);
      final south = double.parse(first.getElement("south")!.innerText);
      final east = double.parse(first.getElement("east")!.innerText);
      final west = double.parse(first.getElement("west")!.innerText);
      final double rotation;
      if (first.getElement("rotation") == null) {
        rotation = 0;
      } else {
        rotation = double.parse(first.getElement("rotation")!.innerText);
      }
      final bounds = LatLonBox(north: north, east: east, south: south, west: west, rotation: rotation);
      XmlElement? icon = groundOverlay.getElement("icon")?.getElement("href");
      String imageFilename;
      if (icon != null) {
        imageFilename = icon.innerText;
      } else {
        imageFilename = "";
      }
      return KmlData(georeferencing: bounds, imageFilename: imageFilename, courseName: null);
    } else {
      throw Exception("Bad KML file");
    }
  }

  @visibleForTesting
  static bool checkKmlHeader_(XmlDocument document) {
    XmlElement? element = document.getElement("kml");
    return element != null;
  }

  static bool checkFormat(String xmlAsString) {
    try {
      final XmlDocument document = XmlDocument.parse(xmlAsString);
      final declaration = document.declaration;
      final kmlTag = document.getElement("kml");
      return declaration != null && kmlTag != null;
    } catch (_) {
      return false;
    }
  }

  @visibleForTesting
  static KmlData extractLatLonQuadAndImage_(XmlElement folder) {
    String? courseName;
    Iterable<XmlElement>? subFolder = folder.findElements("folder");
    if (subFolder.isNotEmpty) {
      XmlElement? nameTag = subFolder.first.getElement("name");
      if (nameTag != null) {
        courseName = nameTag.innerText;
      }
    }

    Iterable<XmlElement> latLonQuad = folder.findAllElements("gx:latlonquad");
    if (latLonQuad.isNotEmpty) {
      XmlElement? first = latLonQuad.first.getElement("coordinates");
      if (first == null) {
        throw const KmlException(-24, "planner_course_edit_page_error11");
      }
      RegExp exp = RegExp(r'^ *(-?\d*\.?\d*),(-?\d*\.?\d*),?\d* *(-?\d*\.?\d*),(-?\d*\.?\d*),?\d* *(-?\d*\.?\d*),(-?\d*\.?\d*),?\d* *(-?\d*\.?\d*),(-?\d*\.?\d*),?\d*');
      Iterable<RegExpMatch> matches = exp.allMatches(first.innerText.trim());
      if (matches.first.groupCount != 8) {
        throw const KmlException(-25, "planner_course_edit_page_error11");
      }
      List<double> coordinates = List<double>.filled(8, 0);
      for (var i = 0; i < 8; ++i) {
        coordinates[i] = double.parse(matches.first.group(i + 1)!);
      }
      LatLonBox bounds = convertLatLonQuadToLatLonBox_(coordinates);
      XmlElement? icon = folder.getElement("groundoverlay")?.getElement("icon")?.getElement("href");
      String imageFilename;
      if (icon != null) {
        imageFilename = path.basename(icon.innerText);
      } else {
        imageFilename = "";
      }
      return KmlData(georeferencing: bounds, imageFilename: imageFilename, courseName: courseName);
    } else {
      throw Exception("planner_course_edit_page_error11");
    }
  }

  @visibleForTesting
  static LatLonBox convertLatLonQuadToLatLonBox_(List<double> coordinates) {
    final squish = math.cos(deg2rad((coordinates[5] + coordinates[7] + coordinates[1] + coordinates[3]) / 4));
    double west = (coordinates[0] + coordinates[6]) / 2.0;
    double east = (coordinates[2] + coordinates[4]) / 2.0;
    double north = (coordinates[5] + coordinates[7]) / 2.0;
    double south = (coordinates[1] + coordinates[3]) / 2.0;
    double rotationInDegree = rad2deg(math.atan2(coordinates[3] - coordinates[1], (coordinates[2] - coordinates[0]) * squish));
    return LatLonBox(north: north, east: east, south: south, west: west, rotation: rotationInDegree);
  }
}

class KmlException implements Exception {
  final int errorCode;
  final String message;

  const KmlException(this.errorCode, this.message);
}
