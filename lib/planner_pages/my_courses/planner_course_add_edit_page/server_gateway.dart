import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart' as path;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'course_data.dart';

class ServerGateway {
  static const String _GET_URL = "${constants.API_SERVER}/planner/my-course/data";
  static const String _POST_ADD_URL = "${constants.API_SERVER}/planner/my-course/upload-course";

  static Future<List<dynamic>> sendCourse(CourseData data) async {
    return sendCourseData(-1, data);
  }

  static Future<List<dynamic>> sendCourseData(int courseId, CourseData data) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return [
        {"code": -10, "message": "Access token error"}
      ];
    } else {
      var request = http.MultipartRequest("POST", Uri.parse("$_POST_ADD_URL/$courseId"));
      request.headers.addAll({
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      });
      request.fields['data'] = data.toJson();
      if (data.imageFilename != null) {
        final extension = path.extension(data.imageFilename!).substring(1).toLowerCase();
        request.files.add(
          http.MultipartFile.fromBytes(
            'image_map',
            data.imageContent!.toList(),
            contentType: MediaType('image', extension == "png" ? 'png' : 'jpeg'),
          ),
        );
      }
      try {
        var response = await http.Response.fromStream(await request.send());
        if (response.statusCode == HttpStatus.ok) {
          var body = jsonDecode(response.body);
          var courseId = body["course_id"];
          return [
            {"code": 0, "message": courseId}
          ];
        } else if (response.body.startsWith("<!DOCTYPE HTML")) {
          final htmlMessage = response.body;
          final startIndex = htmlMessage.indexOf("<h1>") + 4;
          final endIndex = htmlMessage.indexOf("</h1>", startIndex);
          final errorMessage = htmlMessage.substring(startIndex, endIndex);
          return [
            {"code": response.statusCode, "message": errorMessage}
          ];
        } else {
          return jsonDecode(response.body);
        }
      } catch (_) {
        return [
          {"code": 1, "message": "Server connection error"}
        ];
      }
    }
  }

  static Future<CourseData> fetchCourseData(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse("$_GET_URL/$courseId"),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return CourseData.fromJson(courseId, json.decode(response.body));
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }
}
