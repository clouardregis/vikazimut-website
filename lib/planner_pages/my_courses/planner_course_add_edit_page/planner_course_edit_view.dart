// coverage:ignore-file
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/common/html_utils.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/constants.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/course_data.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_list_page.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/date_time_picker_tile.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/help_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/slider_component_shape.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'iof_xml_data.dart';
import 'planner_course_presenter.dart';
import 'planner_course_view.dart';

class PlannerCourseEditPage extends StatefulWidget {
  static const String routePath = '/planner/course/edit';
  final int courseId;

  PlannerCourseEditPage(args) : courseId = int.parse(args);

  @override
  State<PlannerCourseEditPage> createState() => PlannerCourseEditPageState();
}

class PlannerCourseEditPageState extends State<PlannerCourseEditPage> implements PlannerCourseView {
  late final PlannerCoursePagePresenter _presenter;
  late final Future<CourseData> _courseData;
  final DateTimePickerController _startDateController = DateTimePickerController();
  final DateTimePickerController _endDateController = DateTimePickerController();
  late final ValueNotifier<String> _nameEditingController;

  String? _kmzFilename;
  String? _kmlFilename;
  String? _xmlFilename;
  String? _imageFilename;
  List<String> _coursesInXml = [];
  List<String> _imagesInKmz = [];

  bool _showSecretKey = true;
  CourseData? _response;
  bool _isDownloading = false;

  @override
  void initState() {
    _presenter = PlannerCoursePagePresenter(this);
    _courseData = _presenter.fetchCourseData(widget.courseId);
    _nameEditingController = ValueNotifier("");
    super.initState();
  }

  @override
  Future<void> displayErrors(List<dynamic> errorMessage) async {
    var messageString = "";
    for (var error in errorMessage) {
      messageString += "${L10n.getString(error["message"].toString())} (errno ${error["code"]})\n";
    }
    await showErrorDialog(context, title: L10n.getString("page_error_server_title"), message: messageString);
  }

  List<Map<String, Object>> _checkValidity(CourseData courseData) {
    List<Map<String, Object>> errors = [];
    if (courseData.name.isEmpty) {
      errors.add({"code": -20, "message": "planner_course_edit_page_error1"});
    }
    return errors;
  }

  @override
  void redirect(String url) => GoRouter.of(context).go(url);

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                children: [
                  Text(
                    L10n.getString("planner_course_edit_page_title2"),
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  const SizedBox(height: 8),
                  _buildHeaderPane(),
                  const SizedBox(height: 8),
                  _buildContentPane(),
                  const SizedBox(height: 8),
                  _buildButtonPane(context),
                ],
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  Widget _buildHeaderPane() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Container(
              decoration: BoxDecoration(
                color: kDangerBackgroundColor,
                borderRadius: BorderRadius.circular(4),
                border: Border.all(width: 1, color: kDangerFontColor),
              ),
              padding: const EdgeInsets.all(5),
              child: Text(
                L10n.getString("planner_course_edit_page_section2_label7"),
                style: const TextStyle(
                  color: kDangerFontColor,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        ),
        CustomPlainButton(
          label: L10n.getString("planner_course_edit_page_manual_button"),
          backgroundColor: kOrangeColor,
          onPressed: () => launchUrl(Uri.parse("${constants.SERVER}/public/docs/doc-vikazimut.pdf")),
        ),
      ],
    );
  }

  Widget _buildContentPane() {
    return Expanded(
      child: FutureBuilder<CourseData>(
        future: _courseData,
        builder: (BuildContext context, AsyncSnapshot<CourseData> snapshot) {
          if (snapshot.hasError) {
            return GlobalErrorWidget(snapshot.error.toString());
          } else if (snapshot.hasData) {
            _response = snapshot.data;
            _startDateController.dateInMillisecond = CourseData.startDateInMillisecond;
            _endDateController.dateInMillisecond = CourseData.endDateInMillisecond;
            WidgetsBinding.instance.addPostFrameCallback((_) {
              _nameEditingController.value = _response!.name;
            });
            return Container(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.circular(4),
                border: Border.all(width: 1, color: kOrangeColorDisabled),
              ),
              child: DefaultTabController(
                length: 6,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TabBar(
                      isScrollable: true,
                      labelColor: Colors.white,
                      tabs: [
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section1_title"), textAlign: TextAlign.center)),
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section2_title"), textAlign: TextAlign.center)),
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section6_title"), textAlign: TextAlign.center)),
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section3_title"), textAlign: TextAlign.center)),
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section4_title"), textAlign: TextAlign.center)),
                        Tab(child: Text(L10n.getString("planner_course_edit_page_section5_title"), textAlign: TextAlign.center)),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          SingleChildScrollView(
                            child: _buildTabCourseId(context),
                          ),
                          SingleChildScrollView(
                            child: _buildTabCartographicData(context),
                          ),
                          SingleChildScrollView(
                            child: _buildTabTimeBarrier(context),
                          ),
                          SingleChildScrollView(
                            child: _buildTabCourseSettings(context),
                          ),
                          SingleChildScrollView(
                            child: _buildExecutionSettings(context),
                          ),
                          SingleChildScrollView(
                            child: _buildTabAccessKey(context),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildButtonPane(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Align(
          alignment: Alignment.center,
          child: AnimatedBuilder(
            animation: _nameEditingController,
            builder: (_, __) {
              if (_isDownloading) {
                return ElevatedButton(
                  onPressed: null,
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.all(15),
                  ),
                  child: Row(
                    children: [
                      const SizedBox(width: 20, height: 20, child: CircularProgressIndicator()),
                      const SizedBox(width: 8),
                      Text(L10n.getString("planner_course_edit_page_send_button")),
                    ],
                  ),
                );
              } else {
                return CustomPlainButton(
                  label: L10n.getString("planner_course_edit_page_validate_button"),
                  backgroundColor: kOrangeColor,
                  onPressed: (_response?.name == null || _nameEditingController.value.isEmpty)
                      ? null
                      : () async {
                          List<Map<String, Object>> errorMessage = _checkValidity(_response!);
                          if (errorMessage.isNotEmpty) {
                            displayErrors(errorMessage);
                          } else {
                            try {
                              setState(() {
                                _isDownloading = true;
                              });
                              await _presenter.modifyCourseOnServer(courseId: widget.courseId);
                            } finally {
                              setState(() {
                                _isDownloading = false;
                              });
                            }
                          }
                        },
                );
              }
            },
          ),
        ),
        const SizedBox(width: 16),
        Align(
          alignment: Alignment.center,
          child: CustomOutlinedButton(
            label: L10n.getString("planner_course_edit_page_cancel_button"),
            backgroundColor: kOrangeColor,
            foregroundColor: kOrangeColor,
            onPressed: () async {
              GoRouter.of(context).go("${PlannerCourseListPage.routePath}/-1");
            },
          ),
        ),
      ],
    );
  }

  Padding _buildTabCourseId(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Field course name
            TextFormField(
              initialValue: _response?.name,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(50),
              ],
              onChanged: (String value) {
                _response!.name = value;
                _nameEditingController.value = value;
              },
              decoration: InputDecoration(
                labelText: L10n.getString("planner_course_edit_page_section1_label1"),
                filled: true,
                fillColor: Theme.of(context).scaffoldBackgroundColor,
              ),
            ),
            const SizedBox(height: 12),
            // Field club name
            TextFormField(
              autofillHints: const [AutofillHints.organizationName],
              onEditingComplete: () => TextInput.finishAutofillContext(),
              initialValue: CourseData.clubName,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(80),
              ],
              onChanged: (String value) {
                value = value.trim();
                setState(() {
                  CourseData.clubName = value;
                });
              },
              decoration: InputDecoration(
                labelText: L10n.getString("planner_course_edit_page_section1_label2"),
                filled: true,
                fillColor: Theme.of(context).scaffoldBackgroundColor,
              ),
            ),
            const SizedBox(height: 12),
            // Field club url
            TextFormField(
              autofillHints: const [AutofillHints.url],
              onEditingComplete: () => TextInput.finishAutofillContext(),
              initialValue: CourseData.clubUrl ?? "https://",
              inputFormatters: [
                FilteringTextInputFormatter.deny(";,"),
                LengthLimitingTextInputFormatter(256),
              ],
              onChanged: (String value) {
                value = value.trim();
                setState(() {
                  if (value == "https://" || value == "http://") {
                    CourseData.clubUrl = null;
                  } else {
                    CourseData.clubUrl = value;
                  }
                });
              },
              decoration: InputDecoration(
                hintText: "eg. https://vikazim.fr",
                labelText: L10n.getString("planner_course_edit_page_section1_label3"),
                filled: true,
                fillColor: Theme.of(context).scaffoldBackgroundColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Padding _buildTabCartographicData(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(5),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            OutlinedButton.icon(
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1, color: kDangerColor),
              ),
              icon: const Icon(
                Icons.warning,
                color: kDangerColor,
                size: 32,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => HelpWidget(
                    L10n.getString("planner_course_edit_page_section2_introduction"),
                    L10n.getString("planner_course_edit_page_error6"),
                  ),
                );
              },
              label: Text(
                L10n.getString("planner_course_edit_page_section2_introduction"),
                style: const TextStyle(color: kDangerColor),
              ),
            ),
            const SizedBox(height: 12),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: kInfoBackgroundColor,
                borderRadius: BorderRadius.circular(4),
                border: Border.all(width: 1, color: kInfoTextColor),
              ),
              padding: const EdgeInsets.all(5),
              child: Text(
                L10n.getString("planner_course_edit_page_section2_label8"),
                style: const TextStyle(
                  color: kInfoTextColor,
                  fontSize: informationTextFontSize,
                ),
              ),
            ),
            const SizedBox(height: 25),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                // XML file field
                SizedBox(
                  width: 600,
                  child: InputDecorator(
                    decoration: _textfieldDecoration("planner_course_add_page_textfield_xml_label"),
                    child: GestureDetector(
                      onTap: () async {
                        FilePickerResult? result = await FilePicker.platform.pickFiles(
                          type: FileType.custom,
                          allowedExtensions: ["xml"],
                        );
                        if (result != null) {
                          setState(() {
                            _xmlFilename = L10n.getString("planner_course_add_page_loading_text");
                          });
                          await Future.delayed(const Duration(milliseconds: 100));
                          final errors = _presenter.checkAndLoadXmlAndStore(result.files.single.bytes!);
                          if (errors == null) {
                            setState(() {
                              _coursesInXml = _presenter.getAllCoursesFromXML(result.files.single.bytes);
                              _xmlFilename = result.files.single.name;
                            });
                          } else {
                            await displayErrors(errors);
                            setState(() {
                              _xmlFilename = null;
                            });
                          }
                        }
                      },
                      child: Text(
                        _xmlFilename ?? L10n.getString("planner_course_edit_page_section2_browser1"),
                        style: _xmlFilename != null ? null : Theme.of(context).inputDecorationTheme.hintStyle,
                      ),
                    ),
                  ),
                ),
                // Course list
                if (_coursesInXml.length > 1)
                  Column(
                    children: List.generate(
                      _coursesInXml.length,
                      (index) => RadioListTile<int>(
                        activeColor: kOrangeColor,
                        title: Text(_coursesInXml[index]),
                        dense: true,
                        value: index,
                        groupValue: CourseData.selectedXmlCourse,
                        onChanged: (int? value) {
                          setState(() {
                            CourseData.selectedXmlCourse = value ?? 0;
                            _response!.xmlData = IofXmlData(courseName: _coursesInXml[CourseData.selectedXmlCourse], contents: _response!.xmlData!.contents);
                          });
                        },
                      ),
                    ),
                  ),
                // XML file Download button
                Padding(
                  padding: const EdgeInsets.only(left: 50, top: 12),
                  child: CustomPlainButton(
                    label: L10n.getString("planner_course_edit_page_download_button"),
                    backgroundColor: kWarningColor,
                    foregroundColor: Colors.black,
                    onPressed: () {
                      downloadFromUrl("${constants.SERVER}/api/data/${widget.courseId}/xml", "${widget.courseId}.xml", prettyPrinter: IofXmlData.prettyPrint);
                    },
                  ),
                ),
                const SizedBox(height: 12),
                // Map panel
                SizedBox(
                  height: 280,
                  child: DefaultTabController(
                    length: 2,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TabBar(
                          indicator: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(25),
                              topLeft: Radius.circular(25),
                            ),
                            color: kOrangeColor,
                          ),
                          labelColor: Colors.white,
                          tabs: [
                            Tab(text: L10n.getString("planner_course_add_page_tab_kmz")),
                            Tab(text: L10n.getString("planner_course_add_page_tab_kml")),
                          ],
                        ),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).colorScheme.surface,
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(width: 1, color: kOrangeColor),
                            ),
                            child: TabBarView(
                              children: [
                                // KMZ panel
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 8),
                                        child: InputDecorator(
                                          decoration: _textfieldDecoration("planner_course_add_page_textfield_kmz_label"),
                                          child: GestureDetector(
                                            onTap: () async {
                                              final result = await FilePicker.platform.pickFiles(
                                                type: FileType.custom,
                                                allowedExtensions: ["kmz"],
                                              );
                                              if (result != null) {
                                                setState(() {
                                                  _kmzFilename = L10n.getString("planner_course_add_page_loading_text");
                                                });
                                                await Future.delayed(const Duration(milliseconds: 100));
                                                final errors = _presenter.extractKmlAndImageFromKmzArchiveAndStore(result.files.single.bytes!);
                                                if (errors != null) {
                                                  await displayErrors(errors);
                                                  setState(() {
                                                    _kmzFilename = null;
                                                  });
                                                } else {
                                                  setState(() {
                                                    _imagesInKmz = _presenter.getImageNames();
                                                    _kmzFilename = result.files.single.name;
                                                    _kmlFilename = null;
                                                    _imageFilename = null;
                                                  });
                                                }
                                              }
                                            },
                                            child: Text(
                                              _kmzFilename ?? L10n.getString("planner_course_add_page_textfield_helper2"),
                                              style: _kmzFilename != null ? null : Theme.of(context).inputDecorationTheme.hintStyle,
                                            ),
                                          ),
                                        ),
                                      ),
                                      if (_imagesInKmz.length > 1)
                                        Expanded(
                                          child: SingleChildScrollView(
                                            child: Column(
                                              children: List.generate(
                                                _imagesInKmz.length,
                                                (index) {
                                                  return RadioListTile<int>(
                                                    activeColor: kOrangeColor,
                                                    title: Text(_imagesInKmz[index]),
                                                    dense: true,
                                                    value: index,
                                                    groupValue: CourseData.selectedImage,
                                                    onChanged: (int? value) {
                                                      setState(() {
                                                        CourseData.selectedImage = value ?? 0;
                                                        _imageFilename = _presenter.mapImageData![CourseData.selectedImage].imageFilename;
                                                        _presenter.selectMapImage(CourseData.selectedImage);
                                                      });
                                                    },
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                    ],
                                  ),
                                ),
                                // KML & Image panel
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(height: 12),
                                    // KML file
                                    Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: InputDecorator(
                                        decoration: _textfieldDecoration("planner_course_edit_page_section2_kml_label"),
                                        child: GestureDetector(
                                          onTap: () async {
                                            FilePickerResult? result = await FilePicker.platform.pickFiles(
                                              type: FileType.custom,
                                              allowedExtensions: ["kml"],
                                            );
                                            if (result != null) {
                                              setState(() {
                                                _kmlFilename = L10n.getString("planner_course_add_page_loading_text");
                                              });
                                              await Future.delayed(const Duration(milliseconds: 100));
                                              final errors = _presenter.checkAndLoadKmlAndStore(result.files.single.bytes!);
                                              if (errors == null) {
                                                setState(() {
                                                  _kmlFilename = result.files.single.name;
                                                  _kmzFilename = null;
                                                });
                                              } else {
                                                await displayErrors(errors);
                                                setState(() {
                                                  _kmlFilename = null;
                                                });
                                              }
                                            }
                                          },
                                          child: Text(
                                            _kmlFilename ?? L10n.getString("planner_course_edit_page_section2_browser3"),
                                            style: _kmlFilename != null ? null : Theme.of(context).inputDecorationTheme.hintStyle,
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 12),
                                    // Download KML file
                                    Padding(
                                      padding: const EdgeInsets.only(left: 50, right: 8),
                                      child: CustomPlainButton(
                                        label: L10n.getString("planner_course_edit_page_download_button"),
                                        backgroundColor: kWarningColor,
                                        foregroundColor: Colors.black,
                                        onPressed: () {
                                          downloadFromUrl("${constants.SERVER}/api/data/${widget.courseId}/kml", "${widget.courseId}.kml", prettyPrinter: _presenter.recreatePrettyPrintedKML);
                                        },
                                      ),
                                    ),
                                    const SizedBox(height: 18),
                                    // Image file
                                    Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: InputDecorator(
                                        decoration: _textfieldDecoration("planner_course_edit_page_section2_img_label"),
                                        child: GestureDetector(
                                          onTap: () async {
                                            FilePickerResult? result = await FilePicker.platform.pickFiles(
                                              type: FileType.custom,
                                              allowedExtensions: ["jpg", "png", "jpeg"],
                                            );
                                            if (result != null) {
                                              setState(() {
                                                _imageFilename = L10n.getString("planner_course_add_page_loading_text");
                                              });
                                              await Future.delayed(const Duration(milliseconds: 100));
                                              var errors = _presenter.checkImageAndStore(result.files.single.name, result.files.single.bytes!);
                                              if (errors == null) {
                                                setState(() {
                                                  _imageFilename = result.files.single.name;
                                                  _kmzFilename = null;
                                                });
                                              } else {
                                                await displayErrors(errors);
                                                setState(() {
                                                  _imageFilename = null;
                                                });
                                              }
                                            }
                                          },
                                          child: Text(
                                            _imageFilename ?? L10n.getString("planner_course_edit_page_section2_browser4"),
                                            style: _imageFilename != null ? null : Theme.of(context).inputDecorationTheme.hintStyle,
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(height: 12),
                                    // Download Image file
                                    Padding(
                                      padding: const EdgeInsets.only(left: 50, right: 8),
                                      child: CustomPlainButton(
                                        label: L10n.getString("planner_course_edit_page_download_button"),
                                        backgroundColor: kWarningColor,
                                        foregroundColor: Colors.black,
                                        onPressed: () {
                                          downloadFromUrl("${constants.SERVER}/api/data/${widget.courseId}/image", "${widget.courseId}");
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SwitchListTile(
              title: Text(L10n.getString("planner_course_edit_page_section2_label6")),
              value: CourseData.isPrintable ?? false,
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool value) {
                setState(() {
                  CourseData.isPrintable = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  Padding _buildTabTimeBarrier(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 12),
            // Field date barrier
            Text(L10n.getString("planner_course_edit_page_section2_label5")),
            const SizedBox(height: 12),
            Row(
              children: [
                SizedBox(
                  width: 180,
                  child: Text(L10n.getString("planner_course_edit_page_section2_label5-1")),
                ),
                DateTimePickerTile(
                  onSelect: ((int? date) {
                    CourseData.startDateInMillisecond = date;
                  }),
                  controller: _startDateController,
                ),
              ],
            ),
            const SizedBox(height: 12),
            Row(
              children: [
                SizedBox(
                  width: 180,
                  child: Text(L10n.getString("planner_course_edit_page_section2_label5-2")),
                ),
                DateTimePickerTile(
                  onSelect: ((int? date) {
                    CourseData.endDateInMillisecond = date;
                  }),
                  controller: _endDateController,
                ),
              ],
            ),
            const SizedBox(height: 12),
            CustomOutlinedButton(
              onPressed: () {
                setState(() {
                  CourseData.startDateInMillisecond = null;
                  _startDateController.dateInMillisecond = null;
                  CourseData.endDateInMillisecond = null;
                  _endDateController.dateInMillisecond = null;
                });
              },
              label: L10n.getString("planner_course_edit_page_section2_label5-3"),
              backgroundColor: kOrangeColor,
              foregroundColor: kOrangeColor,
            ),
            const SizedBox(height: 12),
          ],
        ),
      ),
    );
  }

  Padding _buildTabCourseSettings(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(L10n.getString("planner_course_edit_page_section3_description1")),
            SizedBox(
              width: 225,
              child: Row(
                children: [
                  SizedBox(
                    width: 20,
                    child: Checkbox(
                        value: CourseData.detectionRadius != null,
                        onChanged: (bool? value) {
                          setState(() {
                            if (value!) {
                              CourseData.detectionRadius = 22;
                            } else {
                              CourseData.detectionRadius = null;
                            }
                          });
                        }),
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                      thumbShape: PolygonSliderThumb(
                        thumbRadius: 15.0,
                        sliderValue: CourseData.detectionRadius,
                      ),
                      showValueIndicator: ShowValueIndicator.never,
                    ),
                    child: Slider(
                      min: 5.0,
                      max: 55.0,
                      divisions: 55 - 5 + 1,
                      value: CourseData.detectionRadius != null ? CourseData.detectionRadius as double : 12.0,
                      label: '${CourseData.detectionRadius ?? ''}',
                      onChanged: CourseData.detectionRadius == null
                          ? null
                          : (double value) {
                              setState(() {
                                CourseData.detectionRadius = value.round();
                              });
                            },
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 12),
            // Field discipline
            Text(L10n.getString("planner_course_edit_page_section3_label2")),
            CustomDropdownButton<int>(
              width: 225,
              onChanged: (int? newValue) {
                if (newValue! < 0) {
                  newValue = null;
                }
                setState(() => CourseData.discipline = newValue);
              },
              value: CourseData.discipline,
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_discipline_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_discipline_urbano"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_discipline_foresto"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_discipline_mtbo"))),
                DropdownMenuItem<int>(value: 3, child: Text(L10n.getString("course_discipline_skio"))),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Padding _buildExecutionSettings(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            OutlinedButton.icon(
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1, color: kInfoTextColor),
              ),
              icon: const Icon(
                Icons.help_outline,
                color: kInfoTextColor,
                size: 32,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => HelpWidget(
                    L10n.getString("help_label"),
                    L10n.getString("planner_course_edit_page_section4_introduction"),
                  ),
                );
              },
              label: Text(
                L10n.getString("help_label"),
                style: const TextStyle(color: kInfoTextColor),
              ),
            ),
            const SizedBox(height: 12),
            // Field course type
            Text(L10n.getString("planner_course_edit_page_section4_label1")),
            CustomDropdownButton<int>(
              width: 270,
              value: CourseData.courseType,
              onChanged: (int? newValue) {
                if (newValue! < 0) {
                  newValue = null;
                }
                setState(() => CourseData.courseType = newValue);
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_type_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_type_sport"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_type_playful"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_type_walk"))),
              ],
            ),
            const SizedBox(height: 12),
            // Field course format
            Text(L10n.getString("planner_course_edit_page_section4_label2")),
            CustomDropdownButton<int>(
              width: 270,
              value: CourseData.courseFormat,
              onChanged: (int? newValue) {
                if (newValue == -1) {
                  newValue = null;
                }
                setState(() => CourseData.courseFormat = newValue);
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_format_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_format_preset"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_format_free"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_format_unsupervised_preset"))),
              ],
            ),
            // Field course mode
            const SizedBox(height: 12),
            Text(L10n.getString("planner_course_edit_page_section4_label3")),
            CustomDropdownButton<int>(
              width: 270,
              value: CourseData.courseMode,
              onChanged: (int? newValue) {
                if (newValue! < 0) {
                  newValue = null;
                }
                setState(() => CourseData.courseMode = newValue);
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_mode_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_mode_gps"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_mode_qrcode"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_mode_beacon"))),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Padding _buildTabAccessKey(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            OutlinedButton.icon(
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1, color: kDangerColor),
              ),
              icon: const Icon(
                Icons.warning,
                color: kDangerColor,
                size: 32,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => HelpWidget(
                    L10n.getString("planner_course_edit_page_section2_introduction"),
                    L10n.getString("planner_course_edit_page_section5_introduction"),
                  ),
                );
              },
              label: Text(
                L10n.getString("planner_course_edit_page_section2_introduction"),
                style: const TextStyle(color: kDangerColor),
              ),
            ),
            const SizedBox(height: 12),
            Text(L10n.getString("planner_course_edit_page_section5_description1")),
            const SizedBox(height: 12),
            SizedBox(
              width: 150,
              child: Align(
                alignment: Alignment.center,
                child: TextFormField(
                  initialValue: CourseData.secretKey,
                  obscureText: _showSecretKey,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(8),
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  decoration: InputDecoration(
                    labelText: L10n.getString("planner_course_edit_page_section5_label1"),
                    filled: true,
                    fillColor: Theme.of(context).scaffoldBackgroundColor,
                    suffixIcon: IconButton(
                      splashRadius: 20,
                      onPressed: () {
                        setState(() => _showSecretKey = !_showSecretKey);
                      },
                      icon: Icon(_showSecretKey == true ? Icons.remove_red_eye : Icons.password),
                    ),
                  ),
                  onChanged: ((String value) {
                    setState(() {
                      if (value.trim().isEmpty) {
                        CourseData.secretKey = null;
                      } else {
                        CourseData.secretKey = value;
                      }
                    });
                  }),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  InputDecoration _textfieldDecoration(String title) {
    return InputDecoration(
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: kOrangeColor, width: 2.0),
      ),
      filled: true,
      fillColor: Theme.of(context).scaffoldBackgroundColor,
      icon: const Icon(
        Icons.upload_file,
        color: kOrangeColor,
      ),
      labelText: L10n.getString(title),
    );
  }
}
