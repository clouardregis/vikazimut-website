import 'dart:convert';

import 'package:archive/archive.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart' as path;

import '../planner_course_list_page.dart';
import '../planner_course_preview_page.dart';
import 'course_data.dart';
import 'image_helper.dart';
import 'iof_xml_data.dart';
import 'kml_data.dart';
import 'kml_parser.dart';
import 'map_image_data.dart';
import 'oom_course.dart';
import 'planner_course_view.dart';
import 'server_gateway.dart';

class PlannerCoursePagePresenter {
  static const double _xmlFileMaximumSizeInMB = 1.0;

  final PlannerCourseView _view;

  List<MapImageData>? _mapImageData;
  List<IofXmlData>? _xmlData;
  List<KmlData>? _kmlData;
  final List<CourseData> _courseData = [];

  PlannerCoursePagePresenter(this._view);

  List<MapImageData>? get mapImageData => _mapImageData;

  List<MapImageData> _extractImageFilesFromKmzArchive(Uint8List bytes) {
    List<MapImageData> images = [];
    final Archive archive = ZipDecoder().decodeBytes(bytes);
    for (var file in archive.files) {
      if (file.isFile) {
        final extension = path.extension(file.name).substring(1).toLowerCase();
        if (extension == "jpg" || extension == "jpeg" || extension == "png") {
          final reducedImage = ImageHelper.reduceImageSizeToReasonableSize(file.content);
          images.add(MapImageData(imageContent: reducedImage, imageFilename: path.basename(file.name)));
        }
      }
    }
    return images;
  }

  String? _extractKmlFilesFromKmzArchive(Uint8List bytes) {
    final Archive archive = ZipDecoder().decodeBytes(bytes);
    for (final file in archive.files) {
      if (file.isFile) {
        final extension = path.extension(file.name).substring(1).toLowerCase();
        if (extension == "kml") {
          try {
            return utf8.decode(file.content);
          } catch (_) {
            return null;
          }
        }
      }
    }
    return null;
  }

  void buildListOfCourses() {
    _courseData.clear();
    if (isSimpleKml()) {
      CourseData.kmlType = 0;
      List<String> coursesInXml = IofXmlData.getAllCourseNamesFromXML(_xmlData![0].contents);
      for (var i = 0; i < coursesInXml.length; ++i) {
        _courseData.add(CourseData(
          name: coursesInXml[i],
          xmlData: _xmlData![i],
          kmlData: _kmlData![0],
          mapImageData: _mapImageData![0],
        ));
      }
    } else {
      CourseData.kmlType = 1;
      for (var i = 0; i < _xmlData!.length; ++i) {
        for (var j = 0; j < _kmlData!.length; ++j) {
          for (var k = 0; k < _mapImageData!.length; ++k) {
            if (_xmlData![i].courseName == _kmlData![j].courseName && _kmlData![j].imageFilename == _mapImageData![k].imageFilename) {
              _courseData.add(CourseData(
                name: _xmlData![i].courseName,
                xmlData: _xmlData![i],
                kmlData: _kmlData![j],
                mapImageData: _mapImageData![k],
              ));
            }
          }
        }
      }
    }
  }

  bool isSimpleKml() => (_kmlData != null && _kmlData!.length == 1 && _kmlData![0].courseName == null);

  List<CourseData> getAllCourses() => _courseData;

  Future<void> createCoursesOnServer() async {
    if (CourseData.kmlType == 0) {
      List response = await ServerGateway.sendCourse(_courseData[CourseData.selectedXmlCourse]);
      if (response.length != 1 || response[0]["code"] != 0) {
        _view.displayErrors(response);
        return;
      }
    } else {
      for (var i = 0; i < _courseData.length; ++i) {
        if (_courseData[i].isSelected) {
          List response = await ServerGateway.sendCourse(_courseData[i]);
          if (response.length != 1 || response[0]["code"] != 0) {
            _view.displayErrors(response);
            return;
          }
        }
      }
    }
    _view.redirect("${PlannerCourseListPage.routePath}/-1");
  }

  Future<void> modifyCourseOnServer({required int courseId}) async {
    List response = await ServerGateway.sendCourseData(courseId, _courseData[0]);
    if (response.length == 1 && response[0]["code"] == 0) {
      final newCourseId = response[0]["message"];
      _view.redirect("${PlannerCoursePreviewPage.routePath}/$newCourseId?reload=true");
    } else {
      _view.displayErrors(response);
    }
  }

  List<Map<String, Object>>? checkAndLoadXmlAndStore(Uint8List data) {
    final error = checkAndLoadXml(data);
    if (error == null) {
      _courseData[0].xmlData = _xmlData![0];
    }
    return error;
  }

  List<Map<String, Object>>? checkAndLoadXml(Uint8List data) {
    try {
      String decodedData = utf8.decode(data);
      decodedData = OOMXmlCourse.convertToIofSchema(decodedData);
      decodedData = IofXmlData.removeUnusedTags(decodedData);
      if (!IofXmlData.checkFormat(decodedData)) {
        return [
          {"code": -11, "message": "planner_course_edit_page_error7"}
        ];
      } else if (_fileSizeInMb(data) > _xmlFileMaximumSizeInMB) {
        return [
          {"code": -12, "message": "planner_course_edit_page_error5"}
        ];
      } else {
        _xmlData = IofXmlData.getAllCoursesFromXML(decodedData);
        return null;
      }
    } catch (_) {
      return [
        {"code": -11, "message": "planner_course_edit_page_error7"}
      ];
    }
  }

  List<dynamic>? checkAndLoadKmlAndStore(Uint8List data) {
    final errors = checkAndLoadKml(data);
    if (errors == null) {
      _courseData[0].kmlData = _kmlData![0];
    }
    return errors;
  }

  List<dynamic>? checkAndLoadKml(Uint8List data) {
    if (!KmlParser.checkFormat(utf8.decode(data))) {
      return [
        {"code": -13, "message": "planner_course_edit_page_error8"}
      ];
    } else if (_fileSizeInMb(data) > _xmlFileMaximumSizeInMB) {
      return [
        {"code": -14, "message": "planner_course_edit_page_error5"}
      ];
    } else {
      try {
        final (kmlData, mapImageData) = KmlParser.getAllGeoreferencedMapsFromKml(utf8.decode(data));
        _kmlData = kmlData;
      } on KmlException catch (e) {
        return [
          {"code": -20, "message": e.message}
        ];
      }
      return null;
    }
  }

  List<dynamic>? checkImageAndStore(String filename, Uint8List data) {
    final errors = checkImage(filename, data);
    if (errors == null) {
      _courseData[0].mapImageData = _mapImageData![0];
    }
    return errors;
  }

  List<dynamic>? checkImage(String filename, Uint8List data) {
    if (!MapImageData.checkFormat(filename, data)) {
      return [
        {"code": -15, "message": "planner_course_edit_page_error9"}
      ];
    }
    data = ImageHelper.reduceImageSizeToReasonableSize(data);
    _mapImageData = [MapImageData(imageFilename: filename, imageContent: data)];
    return null;
  }

  List<dynamic>? extractKmlAndImageFromKmzArchiveAndStore(Uint8List data) {
    final errors = extractKmlAndImageFromKmzArchive(data);
    if (errors == null) {
      _courseData[0].mapImageData = _mapImageData![0];
      _courseData[0].kmlData = _kmlData![0];
    }
    return errors;
  }

  List<dynamic>? extractKmlAndImageFromKmzArchive(Uint8List data) {
    final String? kmlFileContent = _extractKmlFilesFromKmzArchive(data);
    if (kmlFileContent == null) {
      return [
        {"code": -17, "message": "planner_course_edit_page_error10"}
      ];
    }
    List<MapImageData> mapImageData = _extractImageFilesFromKmzArchive(data);
    if (mapImageData.isEmpty) {
      return [
        {"code": -18, "message": "planner_course_edit_page_error10"}
      ];
    }
    List<KmlData> kmlData;
    try {
      (kmlData, mapImageData!) = KmlParser.getAllGeoreferencedMapsFromKml(kmlFileContent, mapImageData);
    } on KmlException catch (e) {
      return [
        {"code": e.errorCode, "message": e.message}
      ];
    } catch (_) {
      return [
        {"code": -23, "message": "planner_course_edit_page_error10"}
      ];
    }
    for (final mapImage in mapImageData) {
      if (mapImage.imageContent == null || !MapImageData.checkFormat(mapImage.imageFilename, mapImage.imageContent!)) {
        return [
          {"code": -15, "message": "planner_course_edit_page_error9"}
        ];
      }
    }
    _mapImageData = mapImageData;
    _kmlData = kmlData;
    return null;
  }

  double _fileSizeInMb(Uint8List bytes) => bytes.length / (1024 * 1024);

  List<String> getAllCoursesFromXML(Uint8List? bytes) {
    if (bytes == null) return [];
    String xmlToString = String.fromCharCodes(bytes);
    return IofXmlData.getAllCourseNamesFromXML(xmlToString);
  }

  Future<CourseData> fetchCourseData(int courseId) async {
    _courseData.add(await ServerGateway.fetchCourseData(courseId));
    return _courseData[0];
  }

  String recreatePrettyPrintedKML(String kml) {
    String entireKml;
    if (kml.trim().startsWith("<LatLonBox>")) {
      entireKml = '<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://www.opengis.net/kml/2.2"><Folder><name></name><GroundOverlay><name></name><Icon><href>files/tile_0_0.jpg</href></Icon>$kml</GroundOverlay></Folder></kml>';
    } else {
      entireKml = kml;
    }
    return IofXmlData.prettyPrint(entireKml);
  }

  List<String> getImageNames() {
    List<String> imageNames = [];
    for (int i = 0; i < _mapImageData!.length; i++) {
      imageNames.add(_mapImageData![i].imageFilename);
    }
    return imageNames;
  }

  void selectMapImage(int selectedImage) {
    _courseData[0].mapImageData = _mapImageData![selectedImage];
  }
}
