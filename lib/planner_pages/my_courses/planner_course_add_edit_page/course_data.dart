import 'dart:convert';
import 'dart:typed_data';

import 'iof_xml_data.dart';
import 'kml_data.dart';
import 'map_image_data.dart';

class CourseData {
  static const String _SEPARATOR_BETWEEN_PREFIX_AND_NAME = " - ";

  String name;
  IofXmlData? xmlData;
  KmlData? kmlData;
  MapImageData? mapImageData;

  // Common to all courses
  static String prefixForCourseName = "";
  static int kmlType = 0;
  static int selectedXmlCourse = 0;
  static int selectedImage = 0;

  static String? clubName;
  static String? clubUrl;
  static int? detectionRadius;
  static int? discipline;
  static int? courseType;
  static bool? isPrintable;

  static int? courseFormat;
  static int? courseMode;
  static String? secretKey;
  static int? startDateInMillisecond;
  static int? endDateInMillisecond;

  bool isSelected = true;

  CourseData({
    required this.name,
    this.xmlData,
    this.kmlData,
    this.mapImageData,
  });

  String? get xmlContent {
    return xmlData?.toXml();
  }

  factory CourseData.fromJson(int courseId, dynamic json) {
    var detectionRadius = json["detection_radius"] as int?;
    if (detectionRadius != null && (detectionRadius < 5 || detectionRadius > 55)) {
      detectionRadius = null;
    }
    CourseData.clubName = json["club_name"];
    CourseData.clubUrl = json["club_url"];
    CourseData.startDateInMillisecond = json["start_date"];
    CourseData.endDateInMillisecond = json["end_date"];
    CourseData.isPrintable = json["is_printable"];
    CourseData.detectionRadius = detectionRadius;
    CourseData.discipline = json["discipline"] as int?;
    CourseData.courseType = json["course_type"] as int?;
    CourseData.courseFormat = json["course_format"] as int?;
    CourseData.courseMode = json["course_mode"] as int?;
    CourseData.secretKey = json["secret_key"];

    return CourseData(name: json["name"]);
  }

  String? get imageFilename => mapImageData?.imageFilename;

  String? get kmlContent => kmlData?.toXml();

  Uint8List? get imageContent => mapImageData?.imageContent;

  static String buildFullName(String name) {
    if (prefixForCourseName.isEmpty) {
      return name;
    } else {
      return prefixForCourseName + _SEPARATOR_BETWEEN_PREFIX_AND_NAME + name;
    }
  }

  String toJson() {
    return json.encode({
      "name": buildFullName(name),
      "club_name": clubName,
      "club_url": clubUrl,
      "start_date": startDateInMillisecond,
      "end_date": endDateInMillisecond,
      "is_printable": isPrintable,
      "detection_radius": detectionRadius,
      "discipline": discipline,
      "course_type": courseType,
      "course_format": courseFormat,
      "course_mode": courseMode,
      "secret_key": secretKey,
      "xml": xmlContent,
      "kml": kmlContent,
      "image": imageFilename,
      "selected_xml_course": 0,
    });
  }
}
