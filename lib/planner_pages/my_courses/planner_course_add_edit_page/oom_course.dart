import 'package:xml/xml.dart';

/// This class is defined because OpenOrienteering Mapper 0.9.5. simple course
/// export does not comply with the IOF XML schema
class OOMXmlCourse {
  static String convertToIofSchema(String xmlAsString) {
    try {
      final XmlDocument document = XmlDocument.parse(xmlAsString);
      final erroneousCourseDataTag = document.getElement("CourseDate");
      if (erroneousCourseDataTag != null) {
        // 1. Add <Map><Scale>5001</Scale></Map>
        final builder = XmlBuilder();
        builder.element('Map', nest: () {
          builder.element('Scale', nest: 5001); // Should be > 5000 to set detection radius to 22 metres
        });
        final XmlElement? raceCourseData = erroneousCourseDataTag.getElement("RaceCourseData");
        raceCourseData?.children.insert(0, builder.buildFragment());

        // 2. Replace all <CourseControl lat=".." lng="..."> by <Position lat=".." lng="...">
        for (final control in raceCourseData!.findElements("Control")) {
          var courseControlElement = control.getElement("CourseControl");
          String? lng = courseControlElement?.getAttribute("lng");
          String? lat = courseControlElement?.getAttribute("lat");
          final builder = XmlBuilder();
          builder.element('Position', attributes: {"lng": lng!, "lat": lat!});
          control.children.add(builder.buildFragment());
          control.children.remove(courseControlElement);
        }

        // 3. Replace tag CourseDate by CourseData
        final newXmlContents = document.toXmlString();
        return newXmlContents.replaceFirst("<CourseDate", "<CourseData").replaceFirst("</CourseDate", "</CourseData");
      }
    } catch (_) {}
    return xmlAsString;
  }
}
