import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:mime/mime.dart';

class MapImageData {
  final Uint8List? imageContent;
  final String imageFilename;

  const MapImageData({required this.imageContent, required this.imageFilename});

  static bool checkFormat(String filename, Uint8List data) {
    const recognizedTypes = ["image/jpeg", "image/png"];
    final mimeType = lookupMimeType(filename, headerBytes: data.sublist(0, 10));
    return recognizedTypes.contains(mimeType);
  }

}
