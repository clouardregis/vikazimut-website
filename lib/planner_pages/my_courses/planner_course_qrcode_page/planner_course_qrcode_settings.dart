import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/utils/custom_dropdown_button.dart';
import 'package:vikazimut_website/utils/help_widget.dart';

import 'planner_course_qrcode_data.dart';

class PlannerCourseQrcodeSettingsWidget extends StatefulWidget {
  final int courseId;
  final PlannerCourseDataModel courseData;

  const PlannerCourseQrcodeSettingsWidget(this.courseId, this.courseData);

  @override
  State<PlannerCourseQrcodeSettingsWidget> createState() => PlannerCourseQrcodeSettingsWidgetState();
}

class PlannerCourseQrcodeSettingsWidgetState extends State<PlannerCourseQrcodeSettingsWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                L10n.getString("qrcode_generator_checkpoints_setting_title"),
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            IconButton(
              padding: const EdgeInsets.all(0.0),
              icon: const Icon(
                Icons.help_outline,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => HelpWidget(
                    L10n.getString("help_label"),
                    L10n.getString("planner_course_edit_page_section4_introduction"),
                  ),
                );
              },
            ),
          ],
        ),
        Column(
          children: [
            Text(L10n.getString("planner_course_edit_page_section4_label1")),
            const SizedBox(width: 16),
            CustomDropdownButton<int>(
              width: 270,
              value: widget.courseData.courseType,
              onChanged: (int? newValue) async {
                int? oldValue = widget.courseData.courseType;
                if (newValue == -1) {
                  newValue = null;
                }
                widget.courseData.courseType = newValue;
                String? error = await PlannerCourseDataModel.sendToServer(widget.courseId, widget.courseData);
                if (error == null) {
                  setState(() {});
                } else {
                  widget.courseData.courseType = oldValue;
                  // setState(() => widget.courseData.courseType = oldValue);
                  _displayError(error);
                }
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_type_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_type_sport"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_type_playful"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_type_walk"))),
              ],
            ),
          ],
        ),
        const SizedBox(height: 12),
        Column(
          children: [
            Text(L10n.getString("planner_course_edit_page_section4_label2")),
            const SizedBox(width: 16),
            CustomDropdownButton<int>(
              width: 270,
              value: widget.courseData.courseFormat,
              onChanged: (int? newValue) async {
                int? oldValue = widget.courseData.courseFormat;
                if (newValue == -1) {
                  newValue = null;
                }
                widget.courseData.courseFormat = newValue;
                String? error = await PlannerCourseDataModel.sendToServer(widget.courseId, widget.courseData);
                if (error == null) {
                  setState(() {});
                } else {
                  widget.courseData.courseFormat = oldValue;
                  _displayError(error);
                }
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_format_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_format_preset"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_format_free"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_format_unsupervised_preset"))),
              ],
            ),
          ],
        ),
        const SizedBox(height: 12),
        Column(
          children: [
            Text(L10n.getString("planner_course_edit_page_section4_label3")),
            const SizedBox(width: 16),
            CustomDropdownButton<int>(
              width: 270,
              value: widget.courseData.courseMode,
              onChanged: (int? newValue) async {
                int? oldValue = widget.courseData.courseMode;
                if (newValue == -1) {
                  newValue = null;
                }
                widget.courseData.courseMode = newValue;
                String? error = await PlannerCourseDataModel.sendToServer(widget.courseId, widget.courseData);
                if (error == null) {
                  setState(() {});
                } else {
                  widget.courseData.courseMode = oldValue;
                  _displayError(error);
                }
              },
              items: [
                DropdownMenuItem<int>(value: -1, child: Text(L10n.getString("course_mode_none"))),
                DropdownMenuItem<int>(value: 0, child: Text(L10n.getString("course_mode_gps"))),
                DropdownMenuItem<int>(value: 1, child: Text(L10n.getString("course_mode_qrcode"))),
                DropdownMenuItem<int>(value: 2, child: Text(L10n.getString("course_mode_beacon"))),
              ],
            ),
          ],
        ),
      ],
    );
  }

  void _displayError(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text("${L10n.getString("page_error_server_title")}. ${L10n.getString(message)}")),
      );
  }
}
