// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/html_utils.dart';

import 'planner_course_qrcode_data.dart';
import 'planner_course_qrcode_presenter.dart';
import 'planner_course_qrcode_settings.dart';

class PlannerCourseQrCodePage extends StatefulWidget {
  static const String routePath = '/planner/courses/qrcodes';
  final int courseId;

  PlannerCourseQrCodePage(args) : courseId = int.parse(args);

  @override
  State<PlannerCourseQrCodePage> createState() => _PlannerCourseQrCodePageState();
}

class _PlannerCourseQrCodePageState extends State<PlannerCourseQrCodePage> {
  late final Future<PlannerCourseDataModel?> _courseData;
  bool _isDownloading1 = false;
  bool _isDownloading2 = false;

  @override
  void initState() {
    super.initState();
    _courseData = QrCodeGeneratorPresenter.fetchCourseData(courseId: widget.courseId);
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Expanded(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: 800),
              child: FutureBuilder<PlannerCourseDataModel?>(
                future: _courseData,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(child: Text(L10n.getString("qrcode_generator_server_error")));
                  } else if (snapshot.hasData) {
                    return Align(
                      alignment: AlignmentDirectional.topCenter,
                      child: SingleChildScrollView(
                        primary: true,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Align(
                              alignment: Alignment.topCenter,
                              child: Text(
                                "${L10n.getString("qrcode_generator_header_label")} \"${snapshot.data?.name}\"",
                                style: Theme.of(context).textTheme.displayMedium,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            const SizedBox(height: 15),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(width: 1, color: kCaptionColor),
                              ),
                              child: ListTile(
                                contentPadding: const EdgeInsets.all(0),
                                horizontalTitleGap: 0,
                                leading: RawMaterialButton(
                                  onPressed: () async {
                                    setState(() => _isDownloading1 = true);
                                    await Future.delayed(const Duration(milliseconds: 100));
                                    await _downloadCheckpointQrCodes(widget.courseId, snapshot.data!.checkpoints);
                                    setState(() => _isDownloading1 = false);
                                  },
                                  elevation: 2.0,
                                  fillColor: kOrangeColor,
                                  shape: const CircleBorder(),
                                  child: _isDownloading1 ? const Icon(Icons.donut_large_outlined, color: Colors.white) : const Icon(Icons.file_download, color: Colors.white),
                                ),
                                title: Text(
                                  L10n.getString("qrcode_generator_checkpoints_title"),
                                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                ),
                                subtitle: Text(
                                  L10n.getString("qrcode_generator_checkpoints_text"),
                                  style: Theme.of(context).textTheme.titleMedium,
                                ),
                                iconColor: Colors.black,
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                border: Border.all(width: 1, color: kCaptionColor),
                              ),
                              child: Column(
                                children: [
                                  ListTile(
                                    horizontalTitleGap: 0,
                                    contentPadding: const EdgeInsets.all(0),
                                    leading: RawMaterialButton(
                                      onPressed: () async {
                                        setState(() => _isDownloading2 = true);
                                        await Future.delayed(const Duration(milliseconds: 100));
                                        await _downloadCourseQrCode(widget.courseId, snapshot.data!.name);
                                        setState(() => _isDownloading2 = false);
                                      },
                                      elevation: 2.0,
                                      fillColor: kOrangeColor,
                                      shape: const CircleBorder(),
                                      child: _isDownloading2 ? const Icon(Icons.donut_large_outlined, color: Colors.white) : const Icon(Icons.file_download, color: Colors.white),
                                    ),
                                    title: Text(
                                      L10n.getString("qrcode_generator_course_title"),
                                      style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Text(
                                      L10n.getString("qrcode_generator_course_text"),
                                      style: Theme.of(context).textTheme.titleMedium,
                                    ),
                                    iconColor: Colors.black,
                                  ),
                                  Container(
                                    margin: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      border: Border.all(width: 1, color: kCaptionColor),
                                    ),
                                    padding: const EdgeInsets.all(15),
                                    child: PlannerCourseQrcodeSettingsWidget(widget.courseId, snapshot.data!),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  } else {
                    return const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                      ],
                    );
                  }
                },
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  Future<void> _downloadCourseQrCode(int courseId, String name) async {
    List<int> data = await QrCodeGeneratorPresenter.downloadCourseQrCode(courseId, name);
    downloadDataInFile(data, "course-$courseId.png");
  }

  Future<void> _downloadCheckpointQrCodes(int courseId, List<Checkpoint> checkpoints) async {
    var data = await QrCodeGeneratorPresenter.downloadArchiveWithCourseQrCodes(widget.courseId, checkpoints);
    downloadDataInFile(data, "checkpoints-${widget.courseId}.zip");
  }
}
