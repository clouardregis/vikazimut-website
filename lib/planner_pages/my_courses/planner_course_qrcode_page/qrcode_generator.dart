import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';

class QrCodeGenerator {
  static const double _IMAGE_SIZE = 450;

  const QrCodeGenerator();

  static Future<List<int>> downloadCourseQrCode(int courseId, String name) async {
    String encodedName = Uri.encodeComponent(name);
    return await _createQrCode("${constants.SERVER}/course/?id=$courseId&name=$encodedName");
  }

  static Future<List<int>> downloadArchiveWithCourseQrCodes(int courseId, List<Checkpoint> checkpoints) async {
    final archive = Archive();
    for (final checkpoint in checkpoints) {
      String text;
      if (checkpoint.id.startsWith("http")) {
        text = checkpoint.id;
      } else {
        text = "${constants.SERVER}/landing/?cp=${checkpoint.id}";
      }
      List<int>? bytes = await _createQrCode(text);
      final archiveFile = ArchiveFile("${checkpoint.id}.png", bytes.length, bytes);
      archive.addFile(archiveFile);
    }
    final zipEncoder = ZipEncoder();
    return zipEncoder.encode(archive);
  }

  static Future<List<int>> _createQrCode(String text) async {
    //  Fixed bug in https://pub.dev/packages/qr_flutter
    //  Bug since the border is not painted: add this followings lines
    //  208   if (emptyColor != null) {
    //  209     Paint paint1 = Paint()..color = emptyColor!;
    //  210     ui.Rect r = ui.Rect.fromLTRB(0, 0, size.width, size.height);
    //  211     canvas.drawRect(r, paint1);
    //  212   }
    final QrPainter painter = QrPainter(
      gapless: true,
      // ignore: deprecated_member_use
      emptyColor: Colors.white,
      data: text,
      version: QrVersions.auto,
    );
    final ByteData? imageData = await painter.toImageData(_IMAGE_SIZE, format: ui.ImageByteFormat.png);
    return imageData!.buffer.asUint8List();
  }
}
