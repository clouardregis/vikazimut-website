// coverage:ignore-file

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class PlannerCourseDeleteTracksPage extends StatefulWidget {
  static const String routePath = '/planner/course/delete-tracks';
  final int courseId;

  PlannerCourseDeleteTracksPage(args) : courseId = int.parse(args);

  @override
  State<PlannerCourseDeleteTracksPage> createState() => _PlannerCourseDeleteTracksPageState();
}

class _PlannerCourseDeleteTracksPageState extends State<PlannerCourseDeleteTracksPage> {
  late final _PlannerCourseTrackListPagePresenter _presenter;
  final ScrollController _scrollController = ScrollController();
  Future<List<_Track>>? _tracks;
  List<_Track>? _displayedTracks;
  String? _errorMessage;

  @override
  void initState() {
    _presenter = _PlannerCourseTrackListPagePresenter(this);
    _tracks = _presenter.fetchTracks(widget.courseId);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("planner_course_track_list_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                  const SizedBox(height: 10),
                  FutureBuilder<List<_Track>>(
                    future: _tracks,
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return Expanded(child: GlobalErrorWidget(snapshot.error.toString()));
                      } else if (!snapshot.hasData) {
                        return const Expanded(child: Center(child: CircularProgressIndicator()));
                      } else {
                        _displayedTracks = snapshot.data!;
                        if (_displayedTracks!.isEmpty) {
                          return Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  border: Border.all(width: 1, color: kCaptionColor),
                                ),
                                child: Center(child: Text(L10n.getString("track_no_gps_track_found")))),
                          );
                        }
                        return Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(width: 1, color: kCaptionColor),
                            ),
                            child: Scrollbar(
                              controller: _scrollController,
                              thumbVisibility: true,
                              child: ListView.builder(
                                controller: _scrollController,
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, i) {
                                  return CheckboxListTile(
                                    controlAffinity: ListTileControlAffinity.leading,
                                    title: Text(snapshot.data![i].name),
                                    activeColor: kOrangeColor,
                                    checkColor: Colors.white,
                                    value: _displayedTracks![i].selected,
                                    onChanged: (bool? value) {
                                      setState(() {
                                        _displayedTracks![i].selected = value!;
                                      });
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                  if (_errorMessage != null) ErrorContainer(_errorMessage!),
                  const SizedBox(height: 5),
                  Wrap(
                    spacing: 5,
                    children: [
                      CustomPlainButton(
                        label: L10n.getString("planner_course_track_list_page_delete_button"),
                        backgroundColor: kOrangeColor,
                        onPressed: (_displayedTracks != null && _displayedTracks!.any((element) => element.selected))
                            ? () => showBinaryQuestionAlertDialog(
                                  context: context,
                                  title: L10n.getString("planner_course_track_list_page_dialog_delete_title"),
                                  message: L10n.getString("planner_course_track_list_page_dialog_delete_message"),
                                  okMessage: L10n.getString("planner_course_track_list_page_dialog_delete_ok_message"),
                                  noMessage: L10n.getString("planner_course_track_list_page_dialog_delete_no_message"),
                                  action: () {
                                    _presenter.deleteTracks(widget.courseId, _displayedTracks!);
                                  },
                                )
                            : null,
                      ),
                      CustomOutlinedButton(
                        label: L10n.getString("planner_course_track_list_page_cancel_button"),
                        backgroundColor: kOrangeColor,
                        foregroundColor: kOrangeColor,
                        onPressed: () => GoRouter.of(context).pop(),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Wrap(
                    spacing: 5,
                    children: [
                      CustomPlainButton(
                        label: L10n.getString("planner_course_track_list_page_select_button"),
                        backgroundColor: kInfoColor,
                        onPressed: () => _selectAll(_tracks!),
                      ),
                      CustomPlainButton(
                        label: L10n.getString("planner_course_track_list_page_unselect_button"),
                        backgroundColor: kInfoColor,
                        onPressed: () => _unselectAll(_tracks!),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _tracks = _presenter.fetchTracks(widget.courseId);
    });
  }

  void error(String message) {
    setState(() {
      _errorMessage = L10n.getString(message);
    });
  }

  _selectAll(Future<List<_Track>> tracks) {
    setState(() {
      _displayedTracks?.forEach((element) {
        element.selected = true;
      });
    });
  }

  _unselectAll(Future<List<_Track>> tracks) {
    setState(() {
      _displayedTracks?.forEach((element) {
        element.selected = false;
      });
    });
  }
}

class _PlannerCourseTrackListPagePresenter {
  static const String URL_GET = '${constants.API_SERVER}/planner/my-course/tracks';
  static const String URL_DELETE = '${constants.API_SERVER}/planner/my-course/delete-tracks';
  final _PlannerCourseDeleteTracksPageState view;

  _PlannerCourseTrackListPagePresenter(this.view);

  Future<List<_Track>> fetchTracks(int courseId) async {
    return _PlannerEventListPageModel.fetchTracks("$URL_GET/$courseId");
  }

  Future<void> deleteTracks(int courseId, List<_Track> tracks) async {
    List<_Track> selectedTracks = tracks.where((element) => element.selected).toList();
    http.Response response = await _PlannerEventListPageModel.deleteTracks("$URL_DELETE/$courseId", selectedTracks);
    if (response.statusCode == HttpStatus.ok) {
      view.update();
    } else {
      view.error(response.body);
    }
  }
}

class _PlannerEventListPageModel {
  static Future<List<_Track>> fetchTracks(String url) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse(url),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return (json.decode(response.body) as List).map((data) => _Track.fromJson(data)).toList();
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> deleteTracks(String url, List<_Track> tracks) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      List<int> trackList = tracks.map((track) => track.id).toList();
      final response = await http.delete(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: jsonEncode({"tracks": trackList}),
      );
      return response;
    }
  }
}

class _Track {
  final int id;
  final String name;
  bool selected = false;

  _Track({
    required this.id,
    required this.name,
  });

  factory _Track.fromJson(Map<String, dynamic> json) {
    return _Track(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }
}
