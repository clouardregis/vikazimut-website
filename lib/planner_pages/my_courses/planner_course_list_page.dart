// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/authentication/unauthorized_exception.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/live_tracking/live_tracking_page.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/diacritic.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'planner_course_add_edit_page/planner_course_add_view.dart';
import 'planner_course_delete_tracks_page.dart';
import 'planner_course_add_edit_page/planner_course_edit_view.dart';
import 'planner_course_missing_checkpoints_page.dart';
import 'planner_course_preview_page.dart';
import 'planner_course_qrcode_page/planner_course_qrcode_page.dart';

class PlannerCourseListPage extends StatefulWidget {
  static const String routePath = "/planner/courses";
  late final int plannerId;

  PlannerCourseListPage(String? args) {
    if (args == null) {
      plannerId = -1;
    } else {
      var x = int.tryParse(args);
      if (x == null) {
        plannerId = -1;
      } else {
        plannerId = x;
      }
    }
  }

  @override
  State<PlannerCourseListPage> createState() => PlannerCourseListPageState();
}

class PlannerCourseListPageState extends State<PlannerCourseListPage> {
  final ScrollController _scrollController = ScrollController();
  late final _PlannerCourseListPagePresenter _presenter;
  Future<List<_PlannerCourseData>>? _courses;
  final ValueNotifier<int> _courseCount = ValueNotifier<int>(-1);
  String? _errorMessage;
  final _listViewKey = GlobalKey<_CourseListViewState>();

  @override
  void initState() {
    _presenter = _PlannerCourseListPagePresenter(this);
    _courses = _presenter.fetchCourses(widget.plannerId);
    _courses?.then<void>((List<_PlannerCourseData> list) => _courseCount.value = list.length).catchError((_) {});
    super.initState();
  }

  @override
  void dispose() {
    _courseCount.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: ValueListenableBuilder<int>(
                      valueListenable: _courseCount,
                      builder: (context, value, widget) {
                        return Text(
                          L10n.getString("planner_course_list_page_title") + (value < 0 ? "" : " ($value)"),
                          style: Theme.of(context).textTheme.displayMedium,
                        );
                      },
                    ),
                  ),
                  _SearchField(onChanged: (value) => _presenter._filterSearchResults(value, _listViewKey)),
                  const SizedBox(height: 10),
                  Expanded(
                    child: FutureBuilder<List<_PlannerCourseData>>(
                      future: _courses,
                      builder: (context, AsyncSnapshot<List<_PlannerCourseData>> snapshot) {
                        if (snapshot.hasError) {
                          return GlobalErrorWidget(snapshot.error.toString());
                        } else if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(width: 1, color: kCaptionColor),
                              color: Theme.of(context).scaffoldBackgroundColor,
                            ),
                            child: Scrollbar(
                              controller: _scrollController,
                              thumbVisibility: true,
                              child: _CourseListView(
                                key: _listViewKey,
                                controller: _scrollController,
                                items: _presenter.selectedCourses,
                                presenter: _presenter,
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (_errorMessage != null) ErrorContainer(_errorMessage!),
          if (widget.plannerId <= 0)
            Padding(
              padding: (ScreenHelper.isMobile(context)) ? const EdgeInsets.only(top: 0) : const EdgeInsets.only(top: 5),
              child: CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_add_course"),
                backgroundColor: kOrangeColor,
                onPressed: () => GoRouter.of(context).go(PlannerCourseAddPage.routePath),
              ),
            ),
          Footer(),
        ],
      ),
    );
  }

  void update() {
    setState(() {
      _errorMessage = null;
      _courses = _presenter.fetchCourses(widget.plannerId);
      _courses?.then<void>((List<_PlannerCourseData> list) => _courseCount.value = list.length).catchError((_) {});
    });
  }

  void displayToast(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
  }

  void error(String message) {
    setState(() {
      _errorMessage = L10n.getString(message);
    });
  }

  void redirectTo(String routePath) {
    GoRouter.of(context).replace(routePath);
  }
}

class _SearchField extends StatelessWidget {
  final ValueChanged<String>? onChanged;

  const _SearchField({required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        cursorColor: kOrangeColor,
        style: const TextStyle(color: kOrangeColor),
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: const BorderSide(
              width: 2,
              color: kOrangeColor,
            ),
          ),
          labelText: L10n.getString("planner_course_list_page_search_hint_text"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),
        onChanged: onChanged,
      ),
    );
  }
}

class _CourseListView extends StatefulWidget {
  final ScrollController controller;
  final List<_PlannerCourseData> items;
  final _PlannerCourseListPagePresenter presenter;

  const _CourseListView({
    super.key,
    required this.controller,
    required this.items,
    required this.presenter,
  });

  @override
  State<_CourseListView> createState() => _CourseListViewState();
}

class _CourseListViewState extends State<_CourseListView> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      key: const PageStorageKey<String>("planner_course_list"),
      controller: widget.controller,
      itemCount: widget.items.length,
      itemBuilder: (context, i) {
        final index = widget.items.length - 1 - i;
        return _buildCourseControlPanel(
          context,
          widget.presenter,
          widget.items[index],
        );
      },
    );
  }

  Widget _buildCourseControlPanel(
    BuildContext context,
    _PlannerCourseListPagePresenter presenter,
    _PlannerCourseData courseData,
  ) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Theme.of(context).disabledColor),
      ),
      child: Column(
        children: [
          Wrap(
            children: [
              Icon(
                Discipline.getIcon(courseData.discipline),
                color: kOrangeColor,
              ),
              const SizedBox(width: 5),
              Text(
                courseData.name,
                style: Theme.of(context).textTheme.headlineMedium,
                textAlign: TextAlign.center,
              ),
              if (courseData.private) const SizedBox(width: 10),
              if (courseData.private)
                Tooltip(
                  message: L10n.getString("planner_course_list_page_private_tooltip"),
                  child: const Icon(
                    Icons.lock_outline,
                    color: kDangerColor,
                  ),
                ),
              if (courseData.closed) const SizedBox(width: 10),
              if (courseData.closed)
                Tooltip(
                  message: L10n.getString("planner_course_list_page_closed_tooltip"),
                  child: const Icon(Icons.alarm, color: kWarningColor),
                ),
              if (courseData.touristic) const SizedBox(width: 10),
              if (courseData.touristic)
                Tooltip(
                  message: L10n.getString("planner_course_list_page_multimedia_tooltip"),
                  child: const Icon(
                    Icons.quiz_outlined,
                    color: kSuccessColor,
                  ),
                ),
            ],
          ),
          const SizedBox(height: 20),
          Wrap(
            direction: Axis.horizontal,
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 5,
            runSpacing: 5,
            children: [
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_preview"),
                backgroundColor: kVioline,
                onPressed: () => GoRouter.of(context).go("${PlannerCoursePreviewPage.routePath}/${courseData.id}"),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_modify"),
                backgroundColor: kBlueColor,
                onPressed: () => GoRouter.of(context).go("${PlannerCourseEditPage.routePath}/${courseData.id}"),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_qrcodes"),
                backgroundColor: kSuccessColor,
                onPressed: () => GoRouter.of(context).go("${PlannerCourseQrCodePage.routePath}/${courseData.id}"),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Theme.of(context).colorScheme.surface,
                  border: Border.all(color: kOrangeColorDisabled),
                ),
                padding: EdgeInsets.symmetric(vertical: 6),
                child: Column(
                  children: [
                    CustomPlainButton(
                      label: "${L10n.getString("planner_course_list_page_button_tracks")} (${courseData.trackCount})",
                      backgroundColor: kOrangeColor,
                      onPressed: () => GoRouter.of(context).go("${RouteListPageView.routePath}/${courseData.id}"),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 100),
                      child: Tooltip(
                        message: L10n.getString("planner_course_list_page_hidden_tooltip"),
                        child: Row(
                          children: [
                            Checkbox(
                                visualDensity: VisualDensity.compact,
                                value: courseData.isTracksHidden,
                                onChanged: (bool? value) {
                                  setState(() {
                                    courseData.isTracksHidden = value!;
                                  });
                                  presenter.updateHiddenCourse(courseData.id, value!).then((bool success) {
                                    if (!success) {
                                      setState(() {
                                        courseData.isTracksHidden = !value;
                                      });
                                    }
                                  });
                                }),
                            Expanded(
                              child: Text(
                                L10n.getString("planner_course_list_page_hidden_button"),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_clean"),
                backgroundColor: kWarningColor,
                foregroundColor: Colors.black,
                onPressed: () => GoRouter.of(context).push("${PlannerCourseDeleteTracksPage.routePath}/${courseData.id}"),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_live_tracking"),
                backgroundColor: kGreenColorLight,
                foregroundColor: Colors.black,
                onPressed: () => GoRouter.of(context).go("${LiveTrackingPage.routePathPlanner}/${courseData.id}"),
              ),
              CustomPlainButton(
                label: "${L10n.getString("planner_course_list_page_button_missing_checkpoints")} (${courseData.missingCheckpointCount})",
                backgroundColor: kInfoColor,
                onPressed: (courseData.missingCheckpointCount <= 0) ? null : () => GoRouter.of(context).go("${PlannerCourseMissingCheckpointsPage.routePath}/${courseData.id}"),
              ),
              CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_delete"),
                backgroundColor: kDangerColor,
                onPressed: () => showBinaryQuestionAlertDialog(
                  context: context,
                  title: L10n.getString("planner_course_list_page_delete_title"),
                  message: L10n.getString("planner_course_list_page_delete_message", [courseData.name]),
                  okMessage: L10n.getString("planner_course_list_page_delete_ok_message"),
                  noMessage: L10n.getString("planner_course_list_page_delete_no_message"),
                  action: () {
                    widget.presenter.deleteCourse(courseData.id, courseData.name);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void updateList(VoidCallback fn) {
    setState(fn);
  }
}

class _PlannerCourseListPagePresenter {
  final PlannerCourseListPageState _view;
  List<_PlannerCourseData> _allPlanners = [];
  final List<_PlannerCourseData> _selectedCourses = [];

  List<_PlannerCourseData> get selectedCourses => _selectedCourses;

  _PlannerCourseListPagePresenter(this._view);

  void _setPlannerList(List<_PlannerCourseData> list) {
    _allPlanners = list;
    _selectedCourses.clear();
    _selectedCourses.addAll(list);
  }

  Future<List<_PlannerCourseData>> fetchCourses(int plannerId) async {
    if (plannerId < -1) {
      return Future.error("Unauthorized request");
    }
    try {
      var courses = await _PlannerCourseListPageModel.fetchCourses(plannerId);
      _setPlannerList(courses);
      return courses;
    } catch (_) {
      _view.redirectTo(PlannerCourseListPage.routePath);
      return Future.error("Access token error");
    }
  }

  Future<void> deleteCourse(int courseId, String courseName) async {
    http.Response response = await _PlannerCourseListPageModel.deleteCourse(courseId);
    if (response.statusCode == HttpStatus.ok) {
      _view.displayToast(L10n.getString("delete_course_success", [courseName]));
      _view.update();
    } else {
      String message = L10n.getString("delete_course_error1", [courseName]);
      _view.displayToast("$message ${jsonDecode(response.body)}");
    }
  }

  void _filterSearchResults(String query, GlobalKey<_CourseListViewState> listViewKey) {
    String queryLowerCase = removeDiacritics(query.toLowerCase());
    if (queryLowerCase.isNotEmpty) {
      List<_PlannerCourseData> dummyListData = [];
      for (var item in _allPlanners) {
        var itemName = removeDiacritics(item.name.toLowerCase());
        if (itemName.contains(queryLowerCase)) {
          dummyListData.add(item);
        }
      }
      listViewKey.currentState?.updateList(() {
        _selectedCourses.clear();
        _selectedCourses.addAll(dummyListData);
      });
    } else {
      listViewKey.currentState?.updateList(() {
        _selectedCourses.clear();
        _selectedCourses.addAll(_allPlanners);
      });
    }
  }

  Future<bool> updateHiddenCourse(int courseId, bool value) async {
    return await _PlannerCourseListPageModel.updateIsHiddenCourse(courseId, value);
  }
}

class _PlannerCourseListPageModel {
  static Future<List<_PlannerCourseData>> fetchCourses(int plannerId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      throw UnauthorizedException();
    } else {
      try {
        const String URL = '${constants.API_SERVER}/planner/my-courses';
        final response = await http.get(
          Uri.parse('$URL/$plannerId'),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return (json.decode(response.body) as List).map((data) => _PlannerCourseData.fromJson(data)).toList();
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> deleteCourse(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      const String URL = '${constants.API_SERVER}/planner/my-course/delete-course';
      final response = await http.delete(
        Uri.parse("$URL/$courseId"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }

  static Future<bool> updateIsHiddenCourse(int courseId, bool value) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      throw UnauthorizedException();
    } else {
      try {
        const String URL = "${constants.API_SERVER}/planner/my-course/update-hidden-course";
        final response = await http.patch(
          Uri.parse('$URL/$courseId'),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: jsonEncode({
            "isHidden": value,
          }),
        );
        if (response.statusCode == HttpStatus.ok) {
          return true;
        } else {
          return false;
        }
      } catch (_) {
        return false;
      }
    }
  }
}

class _PlannerCourseData {
  final int id;
  final String name;
  final bool private;
  final bool closed;
  final bool touristic;
  final int missingCheckpointCount;
  final Discipline? discipline;
  final int trackCount;
  bool isTracksHidden = false;

  _PlannerCourseData({
    required this.id,
    required this.name,
    required this.private,
    required this.closed,
    required this.touristic,
    required this.missingCheckpointCount,
    required this.discipline,
    required this.trackCount,
    required this.isTracksHidden,
  });

  factory _PlannerCourseData.fromJson(Map<String, dynamic> json) {
    return _PlannerCourseData(
      id: json["id"] as int,
      name: json["name"] as String,
      private: json["private"] as bool,
      closed: json["closed"] as bool,
      touristic: json["touristic"] as bool,
      missingCheckpointCount: json["missing_checkpoint_count"] as int,
      discipline: Discipline.toEnum(json["discipline"] as int?),
      trackCount: json["track_count"] as int,
      isTracksHidden: json["hidden"] as bool,
    );
  }
}
