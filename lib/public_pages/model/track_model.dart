import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/leg_helper.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/time/time_column_helper.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'checkpoint.dart';
import 'elevation_helper.dart';
import 'geodesic_point.dart';
import 'leg.dart';
import 'punch_time.dart';

class TrackModel {
  final int id;
  final String nickname;
  late int totalTimeInMilliseconds;
  late List<PunchTime> punchTimes;
  final ValidationOrder courseFormat;
  final bool imported;
  final bool isCheating;
  final int score;
  final int runCount;
  bool isCompleted;

  List<GeodesicPoint>? route;
  late List<Leg> _legs;

  TrackModel(
    this.id,
    this.nickname,
    this.imported,
    this.totalTimeInMilliseconds,
    this.route,
    this.punchTimes,
    this.courseFormat, {
    this.isCompleted = true,
    this.isCheating = false,
    this.score = 1,
    this.runCount = 1,
    List<double>? legActualDistances,
  });

  factory TrackModel.fromJson(List<dynamic> json) {
    final id = json[0] as int;
    final nickname = json[1];
    final totalTimeInMs = json[2];
    List<PunchTime> punchTimes = _punchTimesFromJson(json[3]);
    ValidationOrder courseFormat = ValidationOrder.toEnum(json[4]);
    final imported = json[5];
    final isCheating = (json[6] as bool?) ?? false;
    final score = json[7] as int;
    final runCount = json[8] as int;
    return TrackModel(
      id,
      nickname,
      imported,
      totalTimeInMs,
      null,
      punchTimes,
      courseFormat,
      isCompleted: _isComplete(punchTimes),
      isCheating: isCheating,
      score: score,
      runCount: runCount,
    );
  }

  List<int> get punchTimesInMillisecond => punchTimes.map((PunchTime x) => x.timestampInMillisecond).toList();

  int get countMissingPunches => countMissingPunches_(punchTimes);

  List<GeodesicPoint>? getWaypointsOfLeg(int legIndex) {
    return _legs[legIndex].waypoints;
  }

  @visibleForTesting
  static int countMissingPunches_(List<PunchTime> punchTimes) {
    int count = 0;
    for (int i = 0; i < punchTimes.length; i++) {
      if (punchTimes[i].timestampInMillisecond <= 0 && i != 0) {
        count++;
      }
    }
    return count;
  }

  int get countForcedCheckpoints => countForcedCheckpoints_(punchTimes);

  int get legCount => _legs.length;

  List<Leg> get legs => _legs;

  @visibleForTesting
  set legs(List<Leg> legs) => _legs = legs;

  @visibleForTesting
  static int countForcedCheckpoints_(List<PunchTime> punchTimes) {
    int count = 0;
    for (int i = 0; i < punchTimes.length; i++) {
      if (punchTimes[i].forced) {
        count++;
      }
    }
    return count;
  }

  void loadModel(int totalTimeInMs, List<dynamic> punchTimesAsJson) {
    List<PunchTime> punchTimes = _punchTimesFromJson(punchTimesAsJson);
    totalTimeInMilliseconds = totalTimeInMs;
    this.punchTimes = punchTimes;
  }

  void loadRoute(dynamic json, List<Checkpoint> checkpoints) {
    route = _jsonToGeodesicList(json);
    // WARNING these methods modify the waypoints altitude
    ElevationHelper.cleanAndFilterElevationValues(route!);
    _legs = LegHelper.buildLegsFromPunchTimes(checkpoints.length, punchTimes);
    LegHelper.sortLegsByPunchTimes(_legs);
    LegHelper.splitRouteIntoLegs(route!, _legs);
    if (courseFormat.isPresetOrder) {
      LegHelper.sortLegsByControlIndex(_legs);
    }
  }

  String buildGpxTrace() {
    var gpxContents = '<gpx version="1.0"><trk><name></name><trkseg>';
    for (int i = 0; i < route!.length; i++) {
      final latitude = route![i].latitude;
      final longitude = route![i].longitude;
      final int milliseconds = route![i].timestampInMillisecond;
      final time = DateTime.fromMillisecondsSinceEpoch(milliseconds, isUtc: true);
      String isoDate = time.toIso8601String();
      final elevation = route![i].altitude;
      gpxContents += '<trkpt lat="$latitude" lon="$longitude"><time>$isoDate</time><ele>$elevation</ele></trkpt>';
    }
    gpxContents += '</trkseg></trk></gpx>';
    return gpxContents;
  }

  static List<GeodesicPoint> _jsonToGeodesicList(List<dynamic> jsonContents) {
    List<GeodesicPoint> route = [];
    for (int i = 0; i < jsonContents.length; i++) {
      final latitude = double.parse(jsonContents[i][0]);
      final longitude = double.parse(jsonContents[i][1]);
      final int milliseconds = jsonContents[i][2];
      final altitude = double.parse(jsonContents[i][3]);
      route.add(GeodesicPoint(latitude, longitude, milliseconds, altitude));
    }
    return route;
  }

  String getFilename() {
    final duration = Duration(milliseconds: totalTimeInMilliseconds);
    int seconds = duration.inSeconds.remainder(60);
    int minutes = duration.inMinutes.remainder(60);
    int hours = duration.inHours;
    String stringSeconds = seconds < 10 ? "0$seconds" : "$seconds";
    String stringMinutes = minutes < 10 ? "0$minutes" : "$minutes";
    String timedNickname = "${nickname}_${hours}_${stringMinutes}_$stringSeconds";
    return "$timedNickname.gpx";
  }

  String getTime() {
    final duration = Duration(milliseconds: totalTimeInMilliseconds);
    int seconds = duration.inSeconds.remainder(60);
    int minutes = duration.inMinutes.remainder(60);
    int hours = duration.inHours;
    String stringSeconds = seconds < 10 ? "0$seconds" : "$seconds";
    String stringMinutes = minutes < 10 ? "0$minutes" : "$minutes";
    String timedNickname = "$hours:$stringMinutes:$stringSeconds ";
    return timedNickname;
  }

  static bool _isComplete(List<PunchTime> punchTimes) {
    var isMissingCP = true;
    for (int i = 1; i < punchTimes.length; i++) {
      if (punchTimes[i].timestampInMillisecond <= 0) {
        isMissingCP = false;
      }
    }
    return isMissingCP;
  }

  static List<PunchTime> _punchTimesFromJson(List<dynamic> jsonContents) {
    List<PunchTime> punchTimes = [];
    for (int i = 0; i < jsonContents.length; i++) {
      PunchTime punchTime = PunchTime.fromJson(jsonContents[i]);
      punchTimes.add(punchTime);
    }
    return punchTimes;
  }

  static List<int> sortCheckpointsByPunchTimes(
    List<PunchTime> punchTimes,
  ) {
    int orderByPunchTime(PunchTime a, PunchTime b) {
      if (b.timestampInMillisecond <= 0 && a.timestampInMillisecond <= 0) {
        return a.checkpoint.compareTo(b.checkpoint);
      }
      if (b.timestampInMillisecond <= 0) {
        return -1;
      } else {
        if (a.timestampInMillisecond <= 0) {
          return 1;
        } else {
          return a.timestampInMillisecond.compareTo(b.timestampInMillisecond);
        }
      }
    }

    List<PunchTime> legsWithoutStartAndEnd = [];
    for (int i = 1; i < punchTimes.length - 1; i++) {
      legsWithoutStartAndEnd.add(PunchTime(punchTimes[i].timestampInMillisecond, checkpoint: i));
    }
    legsWithoutStartAndEnd.sort(orderByPunchTime);
    List<int> checkpoints = [0];
    for (int i = 0; i < legsWithoutStartAndEnd.length; i++) {
      checkpoints.add(legsWithoutStartAndEnd[i].checkpoint);
    }
    checkpoints.add(punchTimes.length - 1);
    return checkpoints;
  }

  int calculateLegDuration(int legIndex) {
    int checkpoint2 = _legs[legIndex].index;
    if (checkpoint2 >= punchTimes.length || punchTimes[checkpoint2].timestampInMillisecond < 0) {
      return -1;
    }
    int checkpoint1 = _legs[legIndex - 1].index;
    return punchTimes[checkpoint2].timestampInMillisecond - punchTimes[checkpoint1].timestampInMillisecond;
  }

  double calculateLegActualDistance(int legIndex) {
    final leg = _legs[legIndex];
    if (leg.waypoints == null) {
      return 0;
    }
    var distance = 0.0;
    for (int j = 1; j < leg.waypoints!.length; j++) {
      distance += leg.waypoints![j].distanceToInMeter(leg.waypoints![j - 1]);
    }
    return distance;
  }

  double calculateLegElevationGain(int legIndex) {
    if (_legs[legIndex].waypoints != null) {
      return ElevationHelper.calculateCumulativeElevationGain(_legs[legIndex].waypoints!);
    } else {
      return 0.0;
    }
  }

  String legSpeedToString(int legIndex) {
    final double legDistance = calculateLegActualDistance(legIndex);
    final int elapsedPunchTime = calculateLegDuration(legIndex);
    final speedAsString = TimeColumnHelper.formatSpeed(TimeColumnHelper.calculateSpeedInKmh(elapsedPunchTime, legDistance));
    return speedAsString;
  }

  String legPaceToString(int legIndex, List<Checkpoint> checkpoints) {
    int checkpoint1 = _legs[legIndex - 1].index;
    for (int i = legIndex - 1; i >= 0; i--) {
      final leg = _legs[i].index;
      if (punchTimes[leg].timestampInMillisecond > 0) {
        checkpoint1 = leg;
        break;
      }
    }
    int checkpoint2 = _legs[legIndex].index;
    final legDistance = GeodesicPoint.distanceBetweenInMeter(checkpoints[checkpoint1].latitude, checkpoints[checkpoint1].longitude, checkpoints[checkpoint2].latitude, checkpoints[checkpoint2].longitude);
    final elapsedPunchTime = calculateLegDuration(legIndex);
    final pace = TimeColumnHelper.calculatePaceInMillisecondPerKm(elapsedPunchTime, legDistance);
    return timeToString(pace, withHour: false);
  }

  getCheckpointIndex(int i) {
    return _legs[i].index;
  }
}
