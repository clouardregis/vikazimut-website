import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/common/xml_utils.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:xml/xml.dart';

class CourseGeoreference {
  static const NORTH = "north";
  static const SOUTH = "south";
  static const EAST = "east";
  static const WEST = "west";
  static const ROTATION = "rotation";

  final String name;
  final String imageId;
  final LatLonBox bounds;
  final double startLatitude;
  final double startLongitude;
  final Discipline? discipline;
  final ValidationOrder? format;
  final int hiddenLevel;
  Image? _image;

  Image get image => _image!;

  CourseGeoreference(
    this.name,
    this.bounds,
    this.imageId,
    this.startLatitude,
    this.startLongitude,
    this.discipline,
    this.format,
    this.hiddenLevel,
  );

  factory CourseGeoreference.fromJson(Map<String, dynamic> json) {
    final String name = json['name'];
    final LatLonBox bounds = parseKml(json["kml"]);
    final String imageId = json["image"];
    final double latitude = double.parse(json["latitude"]);
    final double longitude = double.parse(json["longitude"]);
    final Discipline? discipline = Discipline.toEnum(json["discipline"] as int?);
    final ValidationOrder format = ValidationOrder.toEnum(json["format"] as int?);
    final int hiddenLevel = json["hiddenLevel"] as int;
    return CourseGeoreference(name, bounds, imageId, latitude, longitude, discipline, format, hiddenLevel);
  }

  static Future<CourseGeoreference> loadCourseMap(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    final response = await http.get(
      Uri.parse("${constants.API_SERVER}/courses/info/$courseId"),
      headers: headers,
    );
    if (response.statusCode == HttpStatus.ok) {
      try {
        var data = jsonDecode(response.body);
        var course = CourseGeoreference.fromJson(data);
        course._image = _loadImage(courseId, course, headers);
        return course;
      } catch (e) {
        return Future.error(e);
      }
    } else {
      final responseContent = jsonDecode(response.body);
      final String message;
      if (responseContent is Map && responseContent.containsKey("message")) {
        message = responseContent["message"];
      } else {
        message = responseContent;
      }
      return Future.error(message);
    }
  }

  static Image _loadImage(int routeId, CourseGeoreference course, Map<String, String>? headers) {
    return Image.network("${constants.API_SERVER}/courses/image/$routeId", headers: headers);
  }

  static LatLonBox parseKml(xmlContents) {
    try {
      var xmlText = convertXmlTagsToLowerCase(xmlContents);
      final XmlDocument document = XmlDocument.parse(xmlText);
      Iterable<XmlElement> latLonBox = document.findAllElements("latlonbox");
      if (latLonBox.isNotEmpty) {
        XmlElement first = latLonBox.first;
        var north = double.parse(first.getElement(NORTH)!.innerText);
        var south = double.parse(first.getElement(SOUTH)!.innerText);
        var east = double.parse(first.getElement(EAST)!.innerText);
        var west = double.parse(first.getElement(WEST)!.innerText);
        double rotation;
        if (first.getElement(ROTATION) == null) {
          rotation = 0.0;
        } else {
          rotation = double.parse(first.getElement(ROTATION)!.innerText);
        }
        return LatLonBox(north: north, east: east, south: south, west: west, rotation: rotation);
      } else {
        throw Exception("Bad KML file");
      }
    } on Exception catch (_) {
      throw Exception("Bad KML file");
    }
  }

  List<List<double>> latLngBounds() => bounds.calculateFourQuadrilateralCorners();
}
