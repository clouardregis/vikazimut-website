import 'dart:math';

import 'package:vikazimut_website/utils/utils.dart';

class GeodesicPoint {
  static const int _EARTH_RADIUS_IN_METERS = 6378137;
  final double longitude;
  final double latitude;
  int timestampInMillisecond;
  double altitude;

  GeodesicPoint(this.latitude, this.longitude, [this.timestampInMillisecond = 0, this.altitude = 0]);

  /// @see <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine formula</a>
  double distanceToInMeter(final GeodesicPoint other) {
    return distanceBetweenInMeter(latitude, longitude, other.latitude, other.longitude);
  }

  static double distanceBetweenInMeter(
    double startLatitude,
    double startLongitude,
    double endLatitude,
    double endLongitude,
  ) {
    double sqr(double x) => x * x;
    var deltaLatitude = deg2rad(endLatitude - startLatitude);
    var deltaLongitude = deg2rad(endLongitude - startLongitude);
    var a = sqr(sin(deltaLatitude / 2)) + sqr(sin(deltaLongitude / 2)) * cos(deg2rad(startLatitude)) * cos(deg2rad(endLatitude));
    var c = 2 * asin(sqrt(a));
    return _EARTH_RADIUS_IN_METERS * c;
  }
}
