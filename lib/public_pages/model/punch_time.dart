import 'package:flutter/foundation.dart';

@immutable
class PunchTime {
  final int timestampInMillisecond;
  final int checkpoint;
  final bool forced;

  const PunchTime(this.timestampInMillisecond, {this.checkpoint = 0, this.forced = false});

  factory PunchTime.fromJson(Map<String, dynamic> json) {
    return PunchTime(
      json["punchTime"] as int,
      checkpoint: json["controlPoint"] as int,
      forced: (json["forced"] as bool?) ?? false,
    );
  }
}
