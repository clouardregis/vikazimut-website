import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/profile/speed_calculator.dart';

typedef RouteSegmentType = List<GeodesicPoint>;

class RouteHelper {
  static const double areaRadiusInM = 40;

  static List<RouteSegmentType> splitRouteIntoLegs(
    List<GeodesicPoint> route,
    List<int> punchTimes,
  ) {
    punchTimes.sort(comparePunchTimes_);
    List<List<GeodesicPoint>> legs = [];
    int legStartIndex = 0;
    for (int i = 1; i < punchTimes.length; i++) {
      if (punchTimes[i] >= 0) {
        List<GeodesicPoint> legTrack = [];
        for (int j = legStartIndex; j < route.length; j++) {
          if (route[j].timestampInMillisecond < punchTimes[i]) {
            legTrack.add(route[j]);
          } else {
            legTrack.add(route[j]);
            legs.add(legTrack);
            legTrack = [];
            legStartIndex = j;
            break;
          }
        }
        if (legTrack.isNotEmpty) {
          legs.add(legTrack);
        }
      }
    }
    return legs;
  }

  static List<List<List<GeodesicPoint>>> splitLegIntoAreas(List<List<GeodesicPoint>> legRoutes) {
    final List<List<List<GeodesicPoint>>> legAreaTracks = List.filled(legRoutes.length, [[], [], []], growable: false);
    for (int leg = 0; leg < legRoutes.length; leg++) {
      final List<GeodesicPoint> legRoute = legRoutes[leg];
      if (legRoute.isEmpty) continue;
      legAreaTracks[leg] = splitTrackInto3Areas(
        legRoute,
        legRoute.first,
        legRoute.last,
        areaRadiusInM,
      );
    }
    return legAreaTracks;
  }

  static (double, double) computeAverageSpeedInAreas(List<List<GeodesicPoint>> legRoutes, List<List<List<GeodesicPoint>>> legAreaTracks) {
    double totalDistanceInHandrailAreasInM = 0;
    int totalTimeInHandrailAreasInMs = 0;
    double totalDistanceInControlAttackAndExitAreasInM = 0;
    int totalTimeInControlAttackAndExitAreasInMs = 0;
    for (final leg in legAreaTracks) {
      var (distance0, time0) = computeAverageDistanceAndTime_(leg[0]);
      totalTimeInControlAttackAndExitAreasInMs += time0;
      totalDistanceInControlAttackAndExitAreasInM += distance0;

      var (distance1, time1) = computeAverageDistanceAndTime_(leg[1]);
      totalTimeInHandrailAreasInMs += time1;
      totalDistanceInHandrailAreasInM += distance1;

      var (distance2, time2) = computeAverageDistanceAndTime_(leg[2]);
      totalTimeInControlAttackAndExitAreasInMs += time2;
      totalDistanceInControlAttackAndExitAreasInM += distance2;
    }
    final averageSpeedInHandrailAreasInKmh = totalDistanceInHandrailAreasInM * 3600 / totalTimeInHandrailAreasInMs;
    final averageSpeedInControlAttachAndExitAreasInKmh = totalDistanceInControlAttackAndExitAreasInM * 3600 / totalTimeInControlAttackAndExitAreasInMs;
    return (averageSpeedInHandrailAreasInKmh, averageSpeedInControlAttachAndExitAreasInKmh);
  }

  static double computeAverageSpeedEffortInKmh(List<GeodesicPoint> route) {
    List<double> speedsEffortInKmh = computeInstantaneousEffortSpeedsInKmh(route);
    double totalDistance = 0;
    double totalTime = 0;
    for (int i = 0; i < speedsEffortInKmh.length; i++) {
      var time = route[i + 1].timestampInMillisecond - route[i].timestampInMillisecond;
      totalDistance += speedsEffortInKmh[i] * time;
      totalTime += time;
    }
    if (totalTime == 0) {
      return 0;
    } else {
      return totalDistance / totalTime;
    }
  }

  static List<double> computeInstantaneousEffortSpeedsInKmh(List<GeodesicPoint> route) {
    List<double> speeds = [];
    for (int i = 0; i < route.length - 1; i++) {
      int timeDifferenceInMs = route[i + 1].timestampInMillisecond - route[i].timestampInMillisecond;
      double distanceInM = route[i].distanceToInMeter(route[i + 1]);
      var speedInKmH = SpeedCalculator.computeSpeedInKmhWeightedByElevationGain(
        distanceInM,
        timeDifferenceInMs,
        route[i + 1].altitude - route[i].altitude,
      );
      speeds.add(speedInKmH);
    }
    return filterSpeeds_(speeds, route);
  }

  static GeodesicPoint createPointAtGivenTime(GeodesicPoint point1, GeodesicPoint point2, int timeInMs) {
    final ratio = (timeInMs - point1.timestampInMillisecond) / (point2.timestampInMillisecond - point1.timestampInMillisecond);
    final latitude = (point2.latitude - point1.latitude) * ratio + point1.latitude;
    final longitude = (point2.longitude - point1.longitude) * ratio + point1.longitude;
    final altitude = (point2.altitude - point1.altitude) * ratio + point1.altitude;
    return GeodesicPoint(latitude, longitude, timeInMs, altitude);
  }

  static List<RouteSegmentType> splitTrackInto3Areas(
    List<GeodesicPoint> track,
    GeodesicPoint startLocation,
    GeodesicPoint endLocation,
    double areaRadiusInM,
  ) {
    int indexFirstArea = findIndexOfLastPointInsideRadius_(
      track,
      startLocation,
      areaRadiusInM,
    );
    if (indexFirstArea < 0) {
      return [[], [], []];
    }
    int indexThirdArea = findIndexOfFirstPointInsideRadius_(
      track,
      endLocation,
      areaRadiusInM,
    );
    if (indexThirdArea < 0) {
      return [[], [], []];
    }

    // Case of area circle overlapping -> crop each area
    if (indexFirstArea > indexThirdArea) {
      // Control attack has priority
      indexFirstArea = indexThirdArea;
    }

    final controlExitTrack = track.sublist(0, indexFirstArea);
    final handrailTrack = track.sublist(indexFirstArea, indexThirdArea + 1);
    final controlAttackTrack = track.sublist(indexThirdArea + 1, track.length);
    return [controlExitTrack, handrailTrack, controlAttackTrack];
  }

  @visibleForTesting
  static int comparePunchTimes_(int a, int b) {
    if (a == b) return 0;
    if (a < 0) return 1;
    if (b < 0) return -1;
    return a.compareTo(b);
  }

  @visibleForTesting
  static (double, int) computeAverageDistanceAndTime_(List<GeodesicPoint> legAreaTrack) {
    final double averageSpeedInKmh;
    final int durationInMs;
    if (legAreaTrack.isNotEmpty) {
      averageSpeedInKmh = computeAverageSpeedEffortInKmh(legAreaTrack);
      durationInMs = legAreaTrack.last.timestampInMillisecond - legAreaTrack.first.timestampInMillisecond;
    } else {
      durationInMs = 0;
      averageSpeedInKmh = 0;
    }
    return ((averageSpeedInKmh * durationInMs) / 3600, durationInMs);
  }

  @visibleForTesting
  static int findIndexOfLastPointInsideRadius_(final List<GeodesicPoint> points, GeodesicPoint checkpointLocation, double distanceMax) {
    for (int i = points.length - 2; i >= 0; i--) {
      final distance = checkpointLocation.distanceToInMeter(points[i]);
      if (distance <= distanceMax) {
        return i + 1;
      }
    }
    return -1;
  }

  @visibleForTesting
  static List<double> filterSpeeds_(List<double> speeds, List<GeodesicPoint> route) {
    void finalBorderFiltering(List<GeodesicPoint> route, List<double> speeds, List<double> filteredSpeeds) {
      final time1 = route[speeds.length - 1].timestampInMillisecond - route[speeds.length - 2].timestampInMillisecond;
      final time2 = route[speeds.length].timestampInMillisecond - route[speeds.length - 1].timestampInMillisecond;
      final speed = (speeds[speeds.length - 1 - 1] * time1 + 2 * speeds[speeds.length - 1] * time2) / (time1 + 2 * time2);
      filteredSpeeds.add(speed);
    }

    void innerFiltering(List<double> speeds, List<GeodesicPoint> route, List<double> filteredSpeeds) {
      for (int i = 1; i < speeds.length - 1; i++) {
        final time1 = route[i].timestampInMillisecond - route[i - 1].timestampInMillisecond;
        final time2 = route[i + 1].timestampInMillisecond - route[i].timestampInMillisecond;
        final time3 = route[i + 2].timestampInMillisecond - route[i + 1].timestampInMillisecond;
        final speed = (speeds[i - 1] * time1 + speeds[i] * time2 + speeds[i + 1] * time3) / (time1 + time2 + time3);
        filteredSpeeds.add(speed);
      }
    }

    void initialBorderFiltering(List<GeodesicPoint> route, List<double> speeds, List<double> filteredSpeeds) {
      final time2 = route[1].timestampInMillisecond - route[0].timestampInMillisecond;
      final time3 = route[2].timestampInMillisecond - route[1].timestampInMillisecond;
      final speed = (2 * speeds[0] * time2 + speeds[0 + 1] * time3) / (2 * time2 + time3);
      filteredSpeeds.add(speed);
    }

    if (speeds.isEmpty || route.length < 3) {
      return [];
    }
    List<double> filteredSpeeds = [];
    initialBorderFiltering(route, speeds, filteredSpeeds);
    innerFiltering(speeds, route, filteredSpeeds);
    finalBorderFiltering(route, speeds, filteredSpeeds);
    return filteredSpeeds;
  }

  @visibleForTesting
  static int findIndexOfFirstPointInsideRadius_(final List<GeodesicPoint> points, GeodesicPoint checkpointLocation, double threshold) {
    for (int i = 1; i < points.length; i++) {
      double distance = checkpointLocation.distanceToInMeter(points[i]);
      if (distance <= threshold) {
        return i - 1;
      }
    }
    return -1;
  }
}
