import 'package:flutter/foundation.dart';

@immutable
class Course {
  final int id;
  final String name;
  final double latitude;
  final double longitude;

  const Course({
    required this.id,
    required this.name,
    required this.latitude,
    required this.longitude,
  });

  factory Course.fromJson(Map<String, dynamic> json) {
    return Course(
      id: json["id"] as int,
      name: json["name"] as String,
      latitude: double.parse(json["latitude"] as String),
      longitude: double.parse(json["longitude"] as String),
    );
  }
}
