import 'geodesic_point.dart';

class Leg {
  final int index;
  late int punchTime;
  List<GeodesicPoint>? waypoints;

  Leg(this.index, {this.punchTime = 0});
}
