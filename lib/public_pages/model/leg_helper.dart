import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'geodesic_point.dart';
import 'leg.dart';
import 'punch_time.dart';

// Note: Common file between the app and the website
@immutable
class LegHelper {
  static void splitRouteIntoLegs(
    List<GeodesicPoint> route,
    List<Leg> legs,
  ) {
    List<GeodesicPoint> augmentedRoute = addGpsPointAtPunchTimes_(route, legs);
    List<GeodesicPoint> legWaypoints = [];
    int currentLeg = 1;
    for (final currentWaypoint in augmentedRoute) {
      if (currentLeg >= legs.length) {
        break;
      }
      final nextLegTime = legs[currentLeg].punchTime;
      final currentWaypointTime = currentWaypoint.timestampInMillisecond;
      legWaypoints.add(currentWaypoint);
      if (currentWaypointTime >= nextLegTime) {
        legs[currentLeg].waypoints = legWaypoints;
        currentLeg++;
        legWaypoints = [currentWaypoint];
      }
    }
    if (currentLeg < legs.length) {
      legs[currentLeg].waypoints = legWaypoints;
    }
  }

  static void sortLegsByPunchTimes(List<Leg> legs) {
    legs.sort((a, b) {
      if (a.punchTime == b.punchTime) {
        return 0;
      }
      if (a.punchTime < 0) {
        return 1;
      }
      if (b.punchTime < 0) {
        return -1;
      }
      return a.punchTime.compareTo(b.punchTime);
    });
  }

  static void sortLegsByControlIndex(List<Leg> legs) => legs.sort((a, b) => a.index.compareTo(b.index));

  @visibleForTesting
  static List<GeodesicPoint> addGpsPointAtPunchTimes_(
    List<GeodesicPoint> originalRoute,
    List<Leg> legs,
  ) {
    List<GeodesicPoint> route = [];
    int punchIndex = 1;
    for (int i = 0; i < originalRoute.length; i++) {
      final punchTime = legs[punchIndex].punchTime;
      if (punchTime < 0) {
        break;
      }
      if (originalRoute[i].timestampInMillisecond == punchTime) {
        punchIndex++;
      } else if (originalRoute[i].timestampInMillisecond > punchTime) {
        if (i > 0) {
          GeodesicPoint newPoint = createPointAtSpecifiedTime_(originalRoute[i - 1], originalRoute[i], punchTime);
          route.add(newPoint);
        }
        punchIndex++;
      }
      route.add(originalRoute[i]);
      if (punchIndex == legs.length) {
        break;
      }
    }
    return route;
  }

  @visibleForTesting
  static GeodesicPoint createPointAtSpecifiedTime_(
    GeodesicPoint point1,
    GeodesicPoint point2,
    int timeInMs,
  ) {
    final ratio = (timeInMs - point1.timestampInMillisecond) / (point2.timestampInMillisecond - point1.timestampInMillisecond);
    final latitude = (point2.latitude - point1.latitude) * ratio + point1.latitude;
    final longitude = (point2.longitude - point1.longitude) * ratio + point1.longitude;
    final altitude = (point2.altitude - point1.altitude) * ratio + point1.altitude;
    return GeodesicPoint(latitude, longitude, timeInMs, altitude);
  }

  static List<Leg> buildLegsFromPunchTimes(int count, List<PunchTime> punchTimes) {
    List<Leg> legs = List.generate(count, (index) => Leg(index));
    for (int i = 0; i < count; i++) {
      final leg = legs[i];
      final punchTime = (i < punchTimes.length) ? punchTimes[i] : const PunchTime(-1);
      if (i == 0) {
        leg.punchTime = 0;
      } else {
        leg.punchTime = punchTime.timestampInMillisecond;
      }
    }
    return legs;
  }
}
