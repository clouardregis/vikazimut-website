// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'event_info_detail_widget.dart';
import 'event_page_presenter.dart';
import 'model/event_details.dart';

class EventDetailPageView extends StatefulWidget {
  final int eventId;

  EventDetailPageView(args) : eventId = int.parse(args);

  @override
  State<EventDetailPageView> createState() => EventDetailPageViewState();
}

class EventDetailPageViewState extends State<EventDetailPageView> {
  final EventPagePresenter _eventPagePresenter = EventPagePresenter();
  Future<EventDetail?>? _eventDetail;
  final TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    _eventPagePresenter.setView(this);
    _eventDetail = _eventPagePresenter.loadEventInfo(widget.eventId);
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Expanded(
            child: FutureBuilder<EventDetail?>(
              future: _eventDetail,
              builder: (BuildContext context, AsyncSnapshot<EventDetail?> snapshot) {
                if (snapshot.hasError) {
                  return GlobalErrorWidget(snapshot.error.toString());
                } else if (snapshot.hasData) {
                  return ContainerDecoration(
                    child: Column(
                      children: [
                        Text(
                          snapshot.data!.name,
                          style: Theme.of(context).textTheme.displayMedium,
                        ),
                        Expanded(
                          child: EventInfoDetailWidget(
                            eventDetails: snapshot.data!,
                            presenter: _eventPagePresenter,
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
          if (!ScreenHelper.isMobile(context)) const SizedBox(height: 8),
          Center(
            child: CustomPlainButton(
              label: L10n.getString("event_info_participants_register"),
              backgroundColor: kOrangeColor,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: _buildPopupRegister,
                );
              },
            ),
          ),
          Footer(),
        ],
      ),
    );
  }

  void update(dynamic body) {
    if (mounted) {
      setState(() {
        _eventDetail = _eventPagePresenter.loadEventInfo(widget.eventId);
      });
      if (body.isNotEmpty) {
        showErrorDialog(
          context,
          title: L10n.getString("page_error_server_title"),
          message: L10n.getString("event_info_participants_register_error", [body]),
        );
      }
    }
  }

  void showError1(String messageKey) {
    showErrorDialog(
      context,
      title: L10n.getString("page_error_server_title"),
      message: L10n.getString(messageKey),
    );
  }

  void showError2(String message) {
    showErrorDialog(
      context,
      title: L10n.getString("page_error_server_title"),
      message: L10n.getString("Too many participants", [message]),
    );
  }

  Widget _buildPopupRegister(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.all(2.0),
      actionsPadding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
      contentPadding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
      title: Text(
        L10n.getString("event_info_participants_register_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: Container(
        constraints: const BoxConstraints(maxWidth: 500),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(L10n.getString("event_info_participants_register_warning")),
            const SizedBox(height: 8),
            TextField(
              autofocus: true,
              controller: _textEditingController,
              textInputAction: TextInputAction.next,
              inputFormatters: [
                FilteringTextInputFormatter.allow(userIdentifierRegExp),
                LengthLimitingTextInputFormatter(userIdentifierLength),
              ],
              decoration: InputDecoration(
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: kOrangeColor, width: 2.0),
                ),
                icon: const Icon(
                  Icons.person,
                  color: kOrangeColor,
                ),
                labelText: L10n.getString("event_info_participants_nickname"),
                labelStyle: const TextStyle(color: kOrangeColor),
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _textEditingController,
          builder: (_, __) => CustomPlainButton(
            label: L10n.getString("event_info_participants_register"),
            backgroundColor: kOrangeColor,
            onPressed: _textEditingController.text.trim().isNotEmpty
                ? () {
                    _eventPagePresenter.sendRegisterParticipantToServer(
                      nickname: _textEditingController.text,
                      eventId: widget.eventId,
                    );
                    GoRouter.of(context).pop();
                  }
                : null,
          ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(),
          label: L10n.getString("event_info_participants_register_cancel"),
        ),
      ],
    );
  }
}
