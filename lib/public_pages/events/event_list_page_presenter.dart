import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/constants.dart' as constants;

import 'model/events_item.dart';

class EventListPagePresenter {
  Future<List<EventItem>> fetchEventList() async {
    try {
      final response = await http.get(Uri.parse("${constants.SERVER}/data/events"));
      if (response.statusCode == HttpStatus.ok) {
        return parseEventList(response.body);
      } else {
        return [];
      }
    } catch (_) {
      return Future.error("Server connection error");
    }
  }

  @visibleForTesting
  static List<EventItem> parseEventList(String responseBody) {
    if (responseBody.isEmpty) {
      return [];
    }
    var parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<EventItem>((json) => EventItem.fromJson(json)).toList();
  }
}
