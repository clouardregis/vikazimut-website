// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'model/event_course.dart';

class EventInfoCoursesWidget extends StatelessWidget {
  final List<EventCourse> eventCourses;
  final int eventType;
  final ScrollController _horizontalScrollController = ScrollController();
  final ScrollController _verticalScrollController = ScrollController();

  EventInfoCoursesWidget({required this.eventCourses, required this.eventType});

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _verticalScrollController,
      interactive: true,
      thumbVisibility: true,
      child: Scrollbar(
        notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
        thumbVisibility: true,
        controller: _horizontalScrollController,
        child: SingleChildScrollView(
          controller: _verticalScrollController,
          scrollDirection: Axis.vertical,
          child: SingleChildScrollView(
            controller: _horizontalScrollController,
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: _buildDataTable(context),
            ),
          ),
        ),
      ),
    );
  }

  DataTable _buildDataTable(BuildContext context) {
    return DataTable(
      border: TableBorder.all(
        style: BorderStyle.solid,
        width: 1,
        color: kOrangeColorDisabled,
      ),
      dataRowColor: WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
      showBottomBorder: true,
      horizontalMargin: 5,
      columnSpacing: ScreenHelper.isDesktop(context) ? 15 : 5,
      columns: <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_course"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_order"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_time_limit"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_pm_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_courses_overtime_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
      rows: _buildRows(context),
    );
  }

  List<DataRow> _buildRows(
    BuildContext context,
  ) {
    bool isPointUnit = eventType == 1;
    List<DataRow> dataRows = [];
    for (var i = 0; i < eventCourses.length; ++i) {
      EventCourse course = eventCourses[i];
      var dataRow = DataRow(
        color: i.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
        cells: <DataCell>[
          DataCell(
            SizedBox(
              width: ScreenHelper.isDesktop(context) ? 300 : 120,
              child: TextButton(
                onPressed: () {
                  GoRouter.of(context).go('${RouteListPageView.routePath}/${course.courseId}');
                },
                style: TextButton.styleFrom(
                  alignment: Alignment.centerLeft,
                  foregroundColor: kOrangeColor,
                  padding: const EdgeInsets.all(5),
                ),
                child: Text(course.courseName, style: const TextStyle(fontSize: 16, height: 1)),
              ),
            ),
          ),
          DataCell(
            Container(
              alignment: Alignment.center,
              child: Text(
                (course.format == 0) ? L10n.getString("event_info_courses_order_preset") : L10n.getString("event_info_courses_order_free"),
              ),
            ),
          ),
          DataCell(
            Container(
              alignment: Alignment.center,
              child: Text(
                course.maxTime == 0 ? "-" : timeToString(course.maxTime, withHour: true),
              ),
            ),
          ),
          DataCell(
            Container(
              alignment: Alignment.center,
              child: (isPointUnit) ? Text('${course.missingPunchPenalty}') : Text(timeToString(course.missingPunchPenalty * 1000, withHour: true)),
            ),
          ),
          DataCell(
            Container(
              alignment: Alignment.center,
              child: (isPointUnit) ? Text('${course.overtimePenalty}') : Text(timeToString(course.overtimePenalty * 1000, withHour: true)),
            ),
          ),
        ],
      );
      dataRows.add(dataRow);
    }
    return dataRows;
  }
}
