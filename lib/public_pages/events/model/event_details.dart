import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

import 'event_course.dart';
import 'participant.dart';

@immutable
class EventDetail {
  final int id;
  final String name;
  final int type;
  final List<EventCourse> eventCourses;
  final List<Participant> participants;

  const EventDetail({
    required this.id,
    required this.name,
    required this.type,
    required this.eventCourses,
    required this.participants,
  });

  factory EventDetail.fromJson(Map<String, dynamic> json) {
    return EventDetail(
      id: json["id"] as int,
      name: json["name"] as String,
      type: json["type"] as int,
      eventCourses: readEventCourseFromJson(json["courses"]),
      participants: readParticipantsFromJson(json["participants"]),
    );
  }

  String get typeName {
    switch (type) {
      case 0:
        return L10n.getString("constant_type_championship");
      case 1:
        return L10n.getString("constant_type_points");
      case 2:
        return L10n.getString("constant_type_cumulative_time");
      default:
        return L10n.getString("constant_type_championship");
    }
  }

  static List<EventCourse> readEventCourseFromJson(List<dynamic> json) {
    List<EventCourse> courses = [];
    for (int i = 0; i < json.length; i++) {
      courses.add(EventCourse.fromJson(json[i]));
    }
    return courses;
  }

  static List<Participant> readParticipantsFromJson(List<dynamic> json) {
    List<Participant> participants = [];
    for (int i = 0; i < json.length; i++) {
      participants.add(Participant.fromJson(json[i]));
    }
    return participants;
  }

  static void readRankFromJson(json) {}
}
