// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/time.dart';

import 'model/event_course.dart';
import 'model/event_details.dart';

class EventInfoParticipantsWidget extends StatelessWidget {
  final EventDetail eventDetails;
  final ScrollController _horizontalScrollController = ScrollController();
  final ScrollController _verticalScrollController = ScrollController();

  EventInfoParticipantsWidget({required this.eventDetails});

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _verticalScrollController,
      interactive: true,
      thumbVisibility: true,
      child: Scrollbar(
        notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
        controller: _horizontalScrollController,
        interactive: true,
        thumbVisibility: true,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          controller: _verticalScrollController,
          child: SingleChildScrollView(
            controller: _horizontalScrollController,
            scrollDirection: Axis.horizontal,
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: _buildTable(context),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTable(BuildContext context) {
    return DataTable(
      border: TableBorder.all(
        style: BorderStyle.solid,
        width: 1,
        color: kOrangeColorDisabled,
      ),
      horizontalMargin: 6,
      columnSpacing: ScreenHelper.isDesktop(context) ? 15 : 5,
      columns: <DataColumn>[
        const DataColumn(
          label: Expanded(
            child: Text(
              "#",
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_nickname"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        if (eventDetails.type == constants.CHAMPIONSHIP_EVENT || eventDetails.type == constants.POINTS_EVENT)
          DataColumn(
            label: Expanded(
              child: Text(
                L10n.getString("event_info_participants_total_score"),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_total_time"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        if (eventDetails.type == constants.POINTS_EVENT)
          DataColumn(
            label: Expanded(
              child: Text(
                L10n.getString("event_info_participants_gross_score"),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_total_penalty"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              L10n.getString("event_info_participants_progress"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        for (EventCourse course in eventDetails.eventCourses)
          DataColumn(
            label: Expanded(
              child: Text(
                "${course.courseName}\n${L10n.getString("event_info_participants_penalties")}",
                textAlign: TextAlign.center,
              ),
            ),
          ),
      ],
      rows: [
        for (int i = 0; i < eventDetails.participants.length; i++)
          DataRow(
            color: i.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
            cells: <DataCell>[
              DataCell(
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    "${i + 1}",
                  ),
                ),
              ),
              DataCell(
                Text(eventDetails.participants[i].nickname),
              ),
              if (eventDetails.type == constants.CHAMPIONSHIP_EVENT || eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${eventDetails.participants[i].totalScore}")),
                ),
              DataCell(
                Container(alignment: Alignment.center, child: Text(timeToString(eventDetails.participants[i].totalTime, withHour: true))),
              ),
              if (eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${eventDetails.participants[i].grossScore}")),
                ),
              if (eventDetails.type == constants.CHAMPIONSHIP_EVENT || eventDetails.type == constants.CUMULATIVE_TIME_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text(timeToString(eventDetails.participants[i].totalPenalty, withHour: true))),
                ),
              if (eventDetails.type == constants.POINTS_EVENT)
                DataCell(
                  Container(alignment: Alignment.center, child: Text("${eventDetails.participants[i].totalPenalty}")),
                ),
              DataCell(
                Container(alignment: Alignment.center, child: Text("${eventDetails.participants[i].progress} / ${eventDetails.eventCourses.length}")),
              ),
              if (eventDetails.type < 2)
                for (int j = 0; j < eventDetails.eventCourses.length; j++)
                  DataCell(
                    Container(
                      alignment: Alignment.center,
                      child: Text("${eventDetails.participants[i].eventCourses[j][0]} (${eventDetails.participants[i].eventCourses[j][1]}|${eventDetails.participants[i].eventCourses[j][2]})"),
                    ),
                  )
              else
                for (int j = 0; j < eventDetails.eventCourses.length; j++)
                  DataCell(
                    Container(
                      alignment: Alignment.center,
                      child: Text("${_getTimeFromString(eventDetails.participants[i].eventCourses[j][3])} (${eventDetails.participants[i].eventCourses[j][1]}|${eventDetails.participants[i].eventCourses[j][2]})"),
                    ),
                  ),
            ],
          ),
      ],
    );
  }

  String _getTimeFromString(String string) {
    try {
      var integer = int.parse(string);
      return timeToString(integer, withHour: true);
    } catch (_) {
      return string;
    }
  }
}
