// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'event_list_page_presenter.dart';
import 'event_page_view.dart';
import 'model/events_item.dart';

class EventListPageView extends StatelessWidget {
  static const String routePath = '/event/list';

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: 800),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("event_list_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                    ),
                  ),
                  const SizedBox(height: 8),
                  const Expanded(child: _EventListWidget()),
                ],
              ),
            ),
          ),
          Footer(),
        ],
      ),
    );
  }
}

class _EventListWidget extends StatefulWidget {
  const _EventListWidget();

  @override
  State<_EventListWidget> createState() => _EventListWidgetState();
}

class _EventListWidgetState extends State<_EventListWidget> {
  final ScrollController _scrollController = ScrollController();
  late final Future<List<EventItem>>? _events;
  final EventListPagePresenter _presenter = EventListPagePresenter();

  @override
  void initState() {
    _events = _presenter.fetchEventList();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<EventItem>>(
      future: _events,
      builder: (BuildContext context, AsyncSnapshot<List<EventItem>> snapshot) {
        if (snapshot.hasError) {
          return GlobalErrorWidget(snapshot.error.toString());
        } else if (snapshot.hasData) {
          return Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(width: 1, color: kCaptionColor),
            ),
            child: Scrollbar(
              controller: _scrollController,
              thumbVisibility: true,
              child: ListView.builder(
                controller: _scrollController,
                itemCount: snapshot.data!.length,
                itemBuilder: (context, i) {
                  final index = snapshot.data!.length - 1 - i;
                  return _EventListItem(event: snapshot.data![index]);
                },
              ),
            ),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class _EventListItem extends StatefulWidget {
  final EventItem event;

  const _EventListItem({required this.event});

  @override
  State<StatefulWidget> createState() => _EventListItemState();
}

class _EventListItemState extends State<_EventListItem> {
  bool isHovering = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: isHovering ? Theme.of(context).hoverColor : null,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(width: 1, color: Theme.of(context).colorScheme.inversePrimary),
        ),
        child: Flex(
          direction: ScreenHelper.isMobile(context) ? Axis.vertical : Axis.horizontal,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.event.name,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
              decoration: BoxDecoration(
                color: kOrangeColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                " ${L10n.getString("events_list_courses")} ${widget.event.courses}, ${L10n.getString("events_list_participants")} ${widget.event.participants}",
                style: const TextStyle(
                  color: kOrangeColorLight,
                ),
                textScaler: const TextScaler.linear(0.8),
              ),
            ),
          ],
        ),
      ),
      onTap: () => GoRouter.of(context).go("${EventPageView.routePath}/${widget.event.id}"),
      onHover: (hovering) {
        setState(() => isHovering = hovering);
      },
    );
  }
}
