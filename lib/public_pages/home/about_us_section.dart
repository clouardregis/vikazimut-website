// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'team_item.dart';

class AboutUsSection extends StatelessWidget {
  const AboutUsSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kOrangeColorLight,
      child: ScreenHelper(
        desktop: (BuildContext context) => _buildUi(context, ScreenHelper.desktopMinWidth),
        tablet: (BuildContext context) => _buildUi(context, ScreenHelper.tabletMaxWidth),
        mobile: (BuildContext context) => _buildUi(context, getMobileMaxWidth(context) * .9),
      ),
    );
  }

  double getMobileMaxWidth(BuildContext context) => MediaQuery.of(context).size.width;

  Widget _buildUi(BuildContext context, double width) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 30,
            ),
            Text(
              L10n.getString("homepage_about_us_title"),
              style: Theme.of(context).textTheme.displayMedium,
            ),
            const SizedBox(
              height: 5.0,
            ),
            Text(
              L10n.getString("homepage_about_vikazimut"),
              style: const TextStyle(color: Colors.black),
            ),
            Image.asset(
              'assets/images/app2.png',
              height: 200,
            ),
            Text(
              L10n.getString("homepage_about_students_application"),
              style: const TextStyle(color: Colors.black),
            ),
            _TeamWidget(applicationTeamList),
            Text(
              L10n.getString("homepage_about_students_website"),
              style: const TextStyle(color: Colors.black),
            ),
            _TeamWidget(websiteTeamList),
            Text.rich(
              style: const TextStyle(color: Colors.black),
              TextSpan(
                children: [
                  TextSpan(
                    text: '${L10n.getString("homepage_about_tutors")} ',
                    style: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                  ),
                  const TextSpan(
                    text: 'Régis Clouard, Alain Lebret, Eric Pigeon, François Rioult',
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            Text.rich(
              style: const TextStyle(color: Colors.black),
              TextSpan(
                children: [
                  TextSpan(
                    text: '${L10n.getString("homepage_about_translators")} ',
                    style: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                  ),
                  const TextSpan(
                    text: "Margarida GONÇALVES NOVO (Português), Olivier PATTUS (Italiano), Koni EHRBAR & Eric HOYOIS (Deutsch), Petra KAŇÁKOVÁ & Pavla CLAQUIN (Čeština), Oliwier KUSMIERCZYK (Polski)",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}

class _TeamWidget extends StatelessWidget {
  final List<TeamItem> team;

  const _TeamWidget(this.team);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          margin: const EdgeInsets.only(top: 20),
          child: Wrap(
            spacing: 20.0,
            children: team
                .map(
                  (project) => SizedBox(
                    width: 170,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          project.period,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16.0,
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(height: 5.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: project.students.map((student) {
                            return Text(
                              "\u2022 $student",
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                              ),
                            );
                          }).toList(),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                      ],
                    ),
                  ),
                )
                .toList(),
          ),
        );
      },
    );
  }
}
