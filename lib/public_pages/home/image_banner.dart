// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/theme/theme.dart';

class ImageBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height * 0.5,
          ),
          child: Image.asset(
            'assets/images/banner.jpg',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
        ),
        Container(
          width: double.infinity,
          height: 64,
          color: Colors.black.withValues(alpha: 0.6),
          alignment: Alignment.center,
          child: const Text(
            "VIKAZIMUT",
            style: TextStyle(
              inherit: true,
              fontSize: 48,
              color: kOrangeColor,
            ),
          ),
        ),
      ],
    );
  }
}
