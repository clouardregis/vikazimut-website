// coverage:ignore-file
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class MainSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenHelper(
      desktop: (BuildContext context) => _buildLargeScreenUi(context, 1),
      tablet: (BuildContext context) => _buildSmallScreenUi(context, 1),
      mobile: (BuildContext context) => _buildSmallScreenUi(context, 0.8),
    );
  }

  Widget _buildSmallScreenUi(BuildContext context, double scale) {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: _PresentationWidget(),
          ),
          SizedBox(height: 50.0),
          _ApplicationImage(0.8),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: _LicenceWidget(),
          ),
          Padding(
            padding: EdgeInsets.all(20.0),
            child: PrivacyWidget(),
          ),
        ],
      ),
    );
  }

  Widget _buildLargeScreenUi(BuildContext context, double scale) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const _PresentationWidget(),
              const SizedBox(height: 50.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 280,
                    margin: const EdgeInsets.all(10),
                    child: const _LicenceWidget(),
                  ),
                  Container(
                    width: 280,
                    margin: const EdgeInsets.all(10),
                    child: const PrivacyWidget(),
                  ),
                ],
              ),
            ],
          ),
          const Padding(
            padding: EdgeInsets.only(top: 60),
            child: _ApplicationImage(1),
          ),
        ],
      ),
    );
  }
}

class _LicenceWidget extends StatelessWidget {
  const _LicenceWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          L10n.getString("homepage_app_licence_title"),
          style: Theme.of(context).textTheme.displaySmall,
        ),
        Container(
          margin: const EdgeInsets.all(10),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: [
                TextSpan(
                  text: L10n.getString("homepage_app_licence_text"),
                ),
                TextSpan(
                  style: const TextStyle(color: kOrangeColor),
                  text: L10n.getString("homepage_app_licence_url_name"),
                  recognizer: TapGestureRecognizer()..onTap = () => launchUrl(Uri.parse(L10n.getString("homepage_app_licence_url"))),
                ),
              ],
            ),
          ),
        ),
        CustomOutlinedButton(
          onPressed: () {
            launchUrl(Uri.parse("https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app"));
          },
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          label: "${L10n.getString("homepage_app_source_code")} \u00BB",
        ),
        const SizedBox(
          height: 10,
        ),
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () {
            launchUrl(Uri.parse("https://gitlab.com/clouardregis/vikazimut-server"));
          },
          label: "${L10n.getString("homepage_webserver_source_code")} \u00BB",
        ),
        const SizedBox(
          height: 10,
        ),
        CustomOutlinedButton(
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
          onPressed: () {
            launchUrl(Uri.parse("https://gitlab.com/clouardregis/vikazimut-website"));
          },
          label: "${L10n.getString("homepage_website_source_code")} \u00BB",
        ),
      ],
    );
  }
}

class _ApplicationImage extends StatelessWidget {
  final double scale;

  const _ApplicationImage(this.scale);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/app1.png',
      fit: BoxFit.cover,
      width: 400 * scale,
    );
  }
}

class PrivacyWidget extends StatelessWidget {
  const PrivacyWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          L10n.getString("homepage_rules_title"),
          style: Theme.of(context).textTheme.displaySmall,
        ),
        Container(
          margin: const EdgeInsets.all(10),
          child: Text(
            L10n.getString("homepage_rules_description"),
          ),
        ),
        CustomOutlinedButton(
          onPressed: () {
            launchUrl(Uri.parse(L10n.getString("homepage_uses_conditions_url")));
          },
          label: "${L10n.getString("homepage_uses_conditions")} \u00BB",
          backgroundColor: kOrangeColor,
          foregroundColor: kOrangeColor,
        ),
      ],
    );
  }
}

class _PresentationWidget extends StatelessWidget {
  const _PresentationWidget();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30.0),
        Text(
          L10n.getString("homepage_app_title"),
          style: Theme.of(context).textTheme.displayMedium!,
        ),
        Container(
          width: 550,
          margin: const EdgeInsets.all(10),
          child: Text(L10n.getString("homepage_app_description")),
        ),
        Container(
          width: 550,
          margin: const EdgeInsets.all(10),
          child: Text(L10n.getString("homepage_app_features")),
        ),
        SizedBox(
          width: 550,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                padding: const EdgeInsets.all(0),
                onPressed: () {
                  launchUrl(Uri.parse("https://play.google.com/store/apps/details?id=fr.ensicaen.vikazimut"));
                },
                icon: Image.asset(
                  'assets/images/google_play_badge.png',
                  fit: BoxFit.cover,
                ),
              ),
              IconButton(
                padding: const EdgeInsets.all(0),
                onPressed: () {
                  launchUrl(Uri.parse(L10n.getString("homepage_app_ios_download_url")));
                },
                icon: Image.asset(
                  'assets/images/app_store_badge.png',
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
