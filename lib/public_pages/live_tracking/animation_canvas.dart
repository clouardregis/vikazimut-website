import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/public_pages/live_tracking/settings_view.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/utils/map_coordinate_util.dart';

import 'animation_presenter.dart';
import 'orienteer_item.dart';

GlobalKey<AnimationCanvasState> _currentState = GlobalKey<AnimationCanvasState>();

class AnimationCanvas extends StatefulWidget {
  final Size imageSize;
  final LatLonBox terrainBounds;
  final AnimationPresenter animationPresenter;

  AnimationCanvas({
    required this.imageSize,
    required this.terrainBounds,
    required this.animationPresenter,
  }) : super(key: _currentState) {
    animationPresenter.canvas = this;
  }

  @override
  AnimationCanvasState createState() => AnimationCanvasState();

  void drawOrienteers(List<OrienteerItem> orienteers) => _currentState.currentState!.updateOrienteerList(orienteers);
}

class AnimationCanvasState extends State<AnimationCanvas> {
  final TransformationController _transformationController = TransformationController();
  List<OrienteerItem> _orienteers = [];
  int _id = 0;

  @override
  void dispose() {
    _transformationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: [
        SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: CustomPaint(
            painter: _DotPainter(
              imageSize: widget.imageSize,
              controller: _transformationController,
              orienteers: _orienteers,
              terrainBounds: widget.terrainBounds,
              preferences: widget.animationPresenter.animationSettings,
              id: _id,
            ),
          ),
        ),
      ],
    );
  }

  void updateOrienteerList(List<OrienteerItem> orienteers) {
    setState(() {
      _id += 1;
      _orienteers = orienteers;
    });
  }
}

class _DotPainter extends CustomPainter {
  final Size imageSize;
  final TransformationController controller;
  final List<OrienteerItem> orienteers;
  final LatLonBox terrainBounds;
  final LiveTrackingPreferences _preferences;
  final int id;

  _DotPainter({
    required this.imageSize,
    required this.terrainBounds,
    required this.controller,
    required this.orienteers,
    required this.id,
    required LiveTrackingPreferences preferences,
  }) : _preferences = preferences;

  @override
  void paint(Canvas canvas, Size size) {
    final vScale = size.width / imageSize.width;
    final hScale = size.height / imageSize.height;
    final double scale = math.min(vScale, hScale);
    canvas.translate((size.width - imageSize.width * scale) / 2.0, (size.height - imageSize.height * scale) / 2.0);
    canvas.scale(scale);
    _drawAllOrienteers(canvas, scale * controller.value.row0[0], orienteers);
  }

  @override
  bool shouldRepaint(_DotPainter oldDelegate) => id != oldDelegate.id;

  void _drawAllOrienteers(Canvas canvas, double scale, final List<OrienteerItem> orienteers) {
    for (final orienteer in orienteers) {
      _drawOrienteerMarker(canvas, orienteer, scale);
    }
    for (final orienteer in orienteers) {
      _drawOrienteerLabel(canvas, orienteer, scale);
    }
  }

  void _drawOrienteerMarker(Canvas canvas, OrienteerItem orienteer, double scale) {
    final zoom = 1.0 / scale;
    late final Paint dotPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2 * zoom
      ..color = orienteer.color;
    late final Paint tailPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 7 * zoom
      ..strokeCap = StrokeCap.round
      ..color = orienteer.color.withValues(alpha: 0.7);

    final currentPosition = orienteer.currentPosition;
    if (_preferences.withQueue) {
      var tailPositions = orienteer.tailPositions;
      _drawTail(currentPosition, tailPositions, canvas, tailPaint);
    }
    final double headXOffset = 15 * zoom;
    final double headYOffset = 25 * zoom;
    _drawDot(canvas, currentPosition, headXOffset, headYOffset, dotPaint);
  }

  void _drawOrienteerLabel(Canvas canvas, OrienteerItem orienteer, double scale) {
    final zoom = 1.0 / scale;
    final double captionTextFontSize = 18 * zoom;
    final Offset captionOffset = Offset(-20 * zoom, -45 * zoom);
    final Paint dotPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2 * zoom
      ..color = orienteer.color;
    var currentPosition = orienteer.currentPosition;
    _drawLabelAboveHead(canvas, orienteer, captionTextFontSize, currentPosition, captionOffset, dotPaint);
  }

  void _drawLabelAboveHead(Canvas canvas, OrienteerItem orienteer, double captionTextFontSize, GeodesicPoint currentPosition, Offset captionOffset, Paint dotPaint) {
    Offset headPosition = MapCoordinateUtil.convertGeoPointIntoMapCoordinates(
      currentPosition.latitude,
      currentPosition.longitude,
      terrainBounds,
      imageSize.width.toInt(),
      imageSize.height.toInt(),
    );
    Color backgroundColor = orienteer.color.computeLuminance() < 0.5 ? Colors.white.withValues(alpha: 0.6) : Colors.black.withValues(alpha: 0.6);
    if (_preferences.labelAbovePosition == LabelAbovePositionType.abbreviation) {
      var abbreviatedName = orienteer.nickname.substring(0, math.min(3, orienteer.nickname.length));
      _drawText(canvas, captionTextFontSize, headPosition, captionOffset, abbreviatedName, backgroundColor, dotPaint);
    } else if (_preferences.labelAbovePosition == LabelAbovePositionType.nickname) {
      String nickname = orienteer.nickname;
      _drawText(canvas, captionTextFontSize, headPosition, captionOffset, nickname, backgroundColor, dotPaint);
    }
  }

  void _drawTail(GeodesicPoint headPosition, List<GeodesicPoint> tailPositions, Canvas canvas, Paint tailPaint) {
    if (tailPositions.isEmpty) {
      return;
    }
    Path path = Path();
    Offset pt1 = MapCoordinateUtil.convertGeoPointIntoMapCoordinates(
      headPosition.latitude,
      headPosition.longitude,
      terrainBounds,
      imageSize.width.toInt(),
      imageSize.height.toInt(),
    );
    path.moveTo(pt1.dx, pt1.dy);
    for (int i = 0; i < tailPositions.length; i++) {
      Offset pt2 = MapCoordinateUtil.convertGeoPointIntoMapCoordinates(
        tailPositions[i].latitude,
        tailPositions[i].longitude,
        terrainBounds,
        imageSize.width.toInt(),
        imageSize.height.toInt(),
      );
      path.lineTo(pt2.dx, pt2.dy);
    }
    canvas.drawPath(path, tailPaint);
  }

  void _drawDot(Canvas canvas, GeodesicPoint currentPosition, double xOffset, double yOffset, Paint dotPaint) {
    Offset pt2 = MapCoordinateUtil.convertGeoPointIntoMapCoordinates(
      currentPosition.latitude,
      currentPosition.longitude,
      terrainBounds,
      imageSize.width.toInt(),
      imageSize.height.toInt(),
    );
    const icon = Icons.location_on;
    TextPainter textPainter = TextPainter(textDirection: TextDirection.rtl);
    textPainter.text = TextSpan(
      text: String.fromCharCode(icon.codePoint),
      style: TextStyle(
        fontSize: xOffset * 2,
        fontFamily: icon.fontFamily,
        color: dotPaint.color,
      ),
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(pt2.dx - xOffset, pt2.dy - yOffset));
  }

  void _drawText(Canvas canvas, final double captionTextFontSize, Offset dotPosition, Offset captionOffset, String label, Color color, dotPaint) {
    TextPainter backgroundTextPainter = TextPainter(
        text: TextSpan(
          style: TextStyle(
            fontSize: captionTextFontSize,
            fontFamily: 'RobotoMono',
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = dotPaint.strokeWidth * 1.5
              ..color = (color.computeLuminance() > 0.5) ? Colors.white : Colors.black,
          ),
          text: label,
        ),
        textDirection: TextDirection.ltr);
    backgroundTextPainter.layout();
    backgroundTextPainter.paint(canvas, Offset(dotPosition.dx + captionOffset.dx, dotPosition.dy + captionOffset.dy));

    TextPainter foregroundTextPainter = TextPainter(
        text: TextSpan(
          style: TextStyle(
            color: dotPaint.color,
            fontFamily: 'RobotoMono',
            fontSize: captionTextFontSize,
          ),
          text: label,
        ),
        textDirection: TextDirection.ltr);
    foregroundTextPainter.layout();
    foregroundTextPainter.paint(canvas, Offset(dotPosition.dx + captionOffset.dx, dotPosition.dy + captionOffset.dy));
  }
}
