// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/live_tracking/live_tracking_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/help_widget.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

class SettingsView extends StatefulWidget {
  final LiveTrackingPreferences livePreferences;
  final LiveTrackingPresenter presenter;

  const SettingsView(this.livePreferences, this.presenter);

  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("live_tracking_settings_title"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      children: [
        if (widget.presenter.isLiveTrackingCreator)
          Center(
            child: CustomOutlinedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => HelpWidget(
                    L10n.getString("live_tracking_help_title"),
                    L10n.getString("live_tracking_help_message"),
                  ),
                );
              },
              label: L10n.getString("help_label"),
              foregroundColor: kOrangeColor,
              backgroundColor: kOrangeColor,
            ),
          ),
        const SizedBox(height: 20),
        ListTile(
          title: Text(
            L10n.getString("live_tracking_settings_dot_label_title"),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Column(
            children: <Widget>[
              ListTile(
                title: Text(L10n.getString("live_tracking_settings_dot_label_nothing")),
                leading: Radio<LabelAbovePositionType>(
                  activeColor: kOrangeColor,
                  value: LabelAbovePositionType.none,
                  groupValue: widget.livePreferences.labelAbovePosition,
                  onChanged: (LabelAbovePositionType? value) {
                    setState(() {
                      widget.livePreferences._labelAbovePosition = value!;
                      widget.livePreferences.savePreferencesLabel();
                    });
                  },
                ),
              ),
              ListTile(
                title: Text(L10n.getString("live_tracking_settings_dot_label_speed")),
                leading: Radio<LabelAbovePositionType>(
                  activeColor: kOrangeColor,
                  value: LabelAbovePositionType.abbreviation,
                  groupValue: widget.livePreferences.labelAbovePosition,
                  onChanged: (LabelAbovePositionType? value) {
                    setState(() {
                      widget.livePreferences._labelAbovePosition = value!;
                      widget.livePreferences.savePreferencesLabel();
                    });
                  },
                ),
              ),
              ListTile(
                title: Text(L10n.getString("live_tracking_settings_dot_label_name")),
                leading: Radio<LabelAbovePositionType>(
                  activeColor: kOrangeColor,
                  value: LabelAbovePositionType.nickname,
                  groupValue: widget.livePreferences.labelAbovePosition,
                  onChanged: (LabelAbovePositionType? value) {
                    setState(() {
                      widget.livePreferences._labelAbovePosition = value!;
                      widget.livePreferences.savePreferencesLabel();
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        ListTile(
          title: Text(
            L10n.getString("live_tracking_settings_queue_title"),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: ListTile(
            title: Text(L10n.getString("live_tracking_settings_queue_button")),
            leading: Switch(
              value: widget.livePreferences._withQueue,
              onChanged: (value) {
                setState(() {
                  widget.livePreferences._withQueue = value;
                  widget.livePreferences.savePreferencesQueue();
                });
              },
            ),
          ),
        ),
        ListTile(
          title: Text(
            L10n.getString("live_tracking_settings_sort_title"),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Column(
            children: <Widget>[
              ListTile(
                title: Text(L10n.getString("live_tracking_settings_sort_time")),
                leading: Radio<SortCriteria>(
                  activeColor: kOrangeColor,
                  value: SortCriteria.time,
                  groupValue: widget.livePreferences.sortCriteria,
                  onChanged: (SortCriteria? value) {
                    setState(() {
                      widget.livePreferences._sortCriteria = value!;
                      widget.livePreferences.savePreferencesCriteria();
                    });
                  },
                ),
              ),
              ListTile(
                title: Text(L10n.getString("live_tracking_settings_sort_score")),
                leading: Radio<SortCriteria>(
                  activeColor: kOrangeColor,
                  value: SortCriteria.score,
                  groupValue: widget.livePreferences.sortCriteria,
                  onChanged: (SortCriteria? value) {
                    setState(() {
                      widget.livePreferences._sortCriteria = value!;
                      widget.livePreferences.savePreferencesCriteria();
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

enum LabelAbovePositionType { none, abbreviation, nickname }

enum SortCriteria { time, score }

class LiveTrackingPreferences {
  static const int periodicityInSecond = 10;
  LabelAbovePositionType _labelAbovePosition = LabelAbovePositionType.abbreviation;
  SortCriteria _sortCriteria = SortCriteria.time;
  bool _withQueue = true;

  bool get withQueue => _withQueue;

  LabelAbovePositionType get labelAbovePosition => _labelAbovePosition;

  SortCriteria get sortCriteria => _sortCriteria;

  @visibleForTesting
  set sortCriteria(SortCriteria value) => _sortCriteria = value;

  void loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final label = prefs.getInt("live_tracking_preference_label") ?? 1;
    _labelAbovePosition = LabelAbovePositionType.values[label];
    _withQueue = prefs.getBool("live_tracking_preference_queue") ?? true;
    final criteria = prefs.getInt("live_tracking_preference_sort") ?? 0;
    _sortCriteria = SortCriteria.values[criteria];
  }

  void savePreferencesCriteria() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt("live_tracking_preference_sort", _sortCriteria.index);
  }

  void savePreferencesLabel() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt("live_tracking_preference_label", _labelAbovePosition.index);
  }

  void savePreferencesQueue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("live_tracking_preference_queue", _withQueue);
  }
}
