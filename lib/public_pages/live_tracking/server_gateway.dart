import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/network_image.dart' as network;
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/utils/cryptography.dart';

import 'orienteer_item.dart';

class ServerGateway {
  LiveCredential? _liveCredential;

  bool get isLiveTrackingCreator => _liveCredential != null && _liveCredential!.isCreator;

  Future<void> startLiveTracking(int courseId) async {
    _liveCredential = await _sendStartLiveTracking(courseId);
  }

  Future<void> joinLiveTracking(int courseId) async {
    _liveCredential = await _sendJoinLiveTracking(courseId);
  }

  Future<bool> isRunningLiveTrackingFor(int courseId) async {
    return await _sendIsRunningLiveTrackingFor(courseId);
  }

  Future<List<OrienteerItem>?> getOrienteerList() async {
    return await _getOrienteerPositions(_liveCredential!.liveId);
  }

  void stopLiveTracking() async {
    if (_liveCredential != null) {
      await _sendStopLiveTracking(_liveCredential!.liveId);
      _liveCredential = null;
    }
  }

  void dismissOrienteer(OrienteerItem orienteer) async {
    _dismissOrienteer(orienteer.id);
  }

  Future<ui.Image> loadImage(String? imageId) async {
    if (imageId == null) {
      return Future.error("No such map image found...");
    }
    try {
      var completer = Completer<ImageInfo>();
      var user = await IdentifiedUser.getUser();
      var img = network.loadNetworkImage(user, imageId);
      img.resolve(const ImageConfiguration()).addListener(ImageStreamListener((info, _) {
        completer.complete(info);
      }));
      ImageInfo imageInfo = await completer.future;
      return imageInfo.image;
    } catch (e) {
      return Future.error("No such map image found...");
    }
  }

  NetworkImage loadNetworkImage(String imageId) {
    return network.loadNetworkImage(null, imageId);
  }

  static Future<LiveCredential> _sendStartLiveTracking(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      var jsonMessage = jsonEncode({"course_id": courseId});
      String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
      final response = await http.post(
        Uri.parse("${constants.API_SERVER}/planner/live-tracking/start"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: encryptedMessage,
      );
      if (response.statusCode == HttpStatus.ok) {
        var jsonResponse = json.decode(response.body);
        return LiveCredential(
          liveId: jsonResponse['liveId'] as int,
          isCreator: jsonResponse['isCreator'] as bool,
        );
      } else {
        throw Exception(jsonDecode(response.body));
      }
    }
  }

  static Future<LiveCredential> _sendJoinLiveTracking(int courseId) async {
    var jsonMessage = jsonEncode({"course_id": courseId});
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    final response = await http.post(
      Uri.parse("${constants.SERVER}/live-tracking/join"),
      body: encryptedMessage,
    );
    if (response.statusCode == HttpStatus.ok) {
      var jsonResponse = jsonDecode(response.body);
      return LiveCredential(liveId: jsonResponse["liveId"] as int, isCreator: false);
    } else if (response.statusCode == HttpStatus.notFound) {
      throw EmptyLiveException();
    } else {
      throw Exception(jsonDecode(response.body));
    }
  }

  static Future<bool> _sendIsRunningLiveTrackingFor(int courseId) async {
    var jsonMessage = jsonEncode({"course_id": courseId});
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    final response = await http.post(
      Uri.parse("${constants.SERVER}/live-tracking/join"),
      body: encryptedMessage,
    );
    return response.statusCode == HttpStatus.ok;
  }

  static Future<void> _sendStopLiveTracking(int liveId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        var jsonMessage = jsonEncode({"liveId": liveId});
        String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
        await http.post(
          Uri.parse("${constants.API_SERVER}/planner/live-tracking/stop"),
          body: encryptedMessage,
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
      } catch (_) {
        // Nothing to do
      }
    }
  }

  static Future<List<OrienteerItem>?> _getOrienteerPositions(int liveId) async {
    try {
      var jsonMessage = jsonEncode({"liveId": liveId});
      String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
      final response = await http.post(
        Uri.parse("${constants.SERVER}/live-tracking/fetch"),
        body: encryptedMessage,
      );
      if (response.statusCode == HttpStatus.ok) {
        var jsonResponse = json.decode(response.body);
        return extractOrienteerListFromJson_(jsonResponse);
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  Future<void> _dismissOrienteer(int orienteerId) async {
    try {
      var jsonMessage = jsonEncode({
        "orienteer_id": orienteerId,
      });
      String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
      await http.delete(
        Uri.parse("${constants.SERVER}/live-tracking/delete_orienteer"),
        body: encryptedMessage,
      );
    } catch (_) {}
  }

  @visibleForTesting
  static List<OrienteerItem> extractOrienteerListFromJson_(List<dynamic> jsonContents) {
    List<OrienteerItem> orienteers = [];
    for (int i = 0; i < jsonContents.length; i++) {
      OrienteerItem orienteer = OrienteerItem.fromJson(jsonContents[i]);
      orienteers.add(orienteer);
    }
    return orienteers;
  }
}

class LiveCredential {
  int liveId;
  bool isCreator;

  LiveCredential({required this.liveId, required this.isCreator});
}

class EmptyLiveException implements Exception {}
