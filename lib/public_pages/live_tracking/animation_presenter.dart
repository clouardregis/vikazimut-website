import 'animation_canvas.dart';
import 'orienteer_item.dart';
import 'settings_view.dart';

class AnimationPresenter {
  late AnimationCanvas _animationCanvas;
  final LiveTrackingPreferences _animationSettings;

  AnimationPresenter(this._animationSettings);

  LiveTrackingPreferences get animationSettings => _animationSettings;

  set canvas(AnimationCanvas mapView) => _animationCanvas = mapView;

  void displayOrienteerPositions(List<OrienteerItem> orienteers) => _animationCanvas.drawOrienteers(orienteers);
}
