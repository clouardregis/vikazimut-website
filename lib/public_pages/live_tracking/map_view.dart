// coverage:ignore-file
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/live_tracking/settings_view.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'animation_canvas.dart';
import 'animation_presenter.dart';
import 'live_tracking_presenter.dart';

class MapView extends StatefulWidget {
  final CourseGeoreference courseData;
  final LiveTrackingPresenter presenter;

  const MapView({
    required this.courseData,
    required this.presenter,
  });

  @override
  MapViewState createState() => MapViewState();
}

class MapViewState extends State<MapView> with TickerProviderStateMixin {
  late final AnimationPresenter? _mapPresenter;
  bool _isMapShowed = true;
  MapZoomController? _mapZoomController;

  @override
  void initState() {
    _mapZoomController = MapZoomController(this);
    _mapPresenter = AnimationPresenter(widget.presenter.liveTrackingPreferences);
    widget.presenter.animationPresenter = _mapPresenter!;
    super.initState();
  }

  @override
  void dispose() {
    _mapZoomController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ui.Image>(
      future: widget.presenter.loadImage(),
      builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
        if (snapshot.hasError) {
          return GlobalErrorWidget(snapshot.error.toString());
        } else if (snapshot.hasData && snapshot.data != null) {
          final CourseGeoreference courseGeoreference = widget.courseData;
          final ui.Image orienteeringMapImage = snapshot.data!;
          final Size orienteeringMapImageSize = Size(orienteeringMapImage.width.toDouble(), orienteeringMapImage.height.toDouble());
          LatLngBounds terrainBounds = LatLngBounds(
            LatLng(courseGeoreference.latLngBounds()[3][0], courseGeoreference.latLngBounds()[3][1]),
            LatLng(courseGeoreference.latLngBounds()[1][0], courseGeoreference.latLngBounds()[1][1]),
          );
          _mapZoomController!.bounds = terrainBounds;
          AnimationCanvas? animationCanvas = AnimationCanvas(
            imageSize: orienteeringMapImageSize,
            terrainBounds: widget.presenter.courseData!.bounds,
            animationPresenter: _mapPresenter!,
          );
          List<CustomBaseOverlayImage>? overlayImages = <CustomBaseOverlayImage>[
            CustomRotatedOverlayImage(
              bounds: terrainBounds,
              opacity: 0.8,
              angleInDegree: -courseGeoreference.bounds.rotation,
              imageProvider: widget.presenter.loadNetworkImage(courseGeoreference.imageId),
            ),
            CustomRotatedOverlayCanvas(
              bounds: terrainBounds,
              opacity: 0,
              angleInDegree: -courseGeoreference.bounds.rotation,
              animationCanvas: animationCanvas,
            ),
          ];
          return Stack(
            alignment: Alignment.topRight,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: SizedBox(
                      height: double.infinity,
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: [
                          _OpenStreetMapWithCourseMap(
                            mapController: _mapZoomController!.mapController,
                            bounds: terrainBounds,
                            showedMap: _isMapShowed,
                            overlayImages: overlayImages,
                          ),
                          Positioned(
                            right: 5.0,
                            top: 15.0,
                            child: Column(
                              children: [
                                if (ScreenHelper.isDesktop(context))
                                  _MapButton(
                                    iconData: Icons.add,
                                    onPressed: () {
                                      _mapZoomController?.zoomIn();
                                    },
                                  ),
                                if (ScreenHelper.isDesktop(context))
                                  _MapButton(
                                    iconData: Icons.remove,
                                    onPressed: () {
                                      _mapZoomController?.zoomOut();
                                    },
                                  ),
                                if (!ScreenHelper.isMobile(context))
                                  _MapButton(
                                    iconData: Icons.my_location,
                                    onPressed: () => _mapZoomController?.reset(),
                                  ),
                                _MapButton(
                                  onPressed: () => setState(() => _isMapShowed = !_isMapShowed),
                                  iconData: (_isMapShowed) ? Icons.layers_clear : Icons.layers,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SettingsView(widget.presenter.liveTrackingPreferences, widget.presenter),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class _OpenStreetMapWithCourseMap extends StatefulWidget {
  final MapController mapController;
  final LatLngBounds bounds;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;

  @override
  State<_OpenStreetMapWithCourseMap> createState() => _OpenStreetMapWithCourseMapState();

  const _OpenStreetMapWithCourseMap({
    required this.mapController,
    required this.bounds,
    required this.showedMap,
    required this.overlayImages,
  });
}

class _OpenStreetMapWithCourseMapState extends State<_OpenStreetMapWithCourseMap> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 18,
        minZoom: 10,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        (widget.showedMap) ? CustomOverlayImageLayer(overlayImages: widget.overlayImages) : CustomOverlayImageLayer(overlayImages: [widget.overlayImages[1]]),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }
}

class _MapButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}
