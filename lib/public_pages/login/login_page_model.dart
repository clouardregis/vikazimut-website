import 'dart:convert';

import 'package:vikazimut_website/authentication/authentication_helper.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class LoginPageModel {
  static Future<IdentifiedUser?> sendCredentials(String username, String password) async {
    final jsonMessage = jsonEncode({"username": username, "password": password});
    return await AuthenticationHelper.postUserAuthenticationRequest("${constants.API_SERVER}/login_check", body: jsonMessage);
  }
}
