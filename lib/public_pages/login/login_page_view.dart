// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/planner_pages/home/planner_home_view.dart';
import 'package:vikazimut_website/public_pages/login/forgotten_password_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'login_page_presenter.dart';

class LoginPageView extends StatefulWidget {
  static const String routePath = '/login';
  final String? redirectTo;

  const LoginPageView({redirectTo}) : redirectTo = redirectTo ?? PlannerHomeView.routePath;

  @override
  State<LoginPageView> createState() => LoginPageViewState();
}

class LoginPageViewState extends State<LoginPageView> {
  late final LoginPagePresenter _loginPagePresenter;
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final ValueNotifier<String> _formController = ValueNotifier<String>("");
  String? _errorMessage;

  @override
  void initState() {
    _loginPagePresenter = LoginPagePresenter(this);
    super.initState();
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _formController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: ScreenHelper.isDesktop(context) ? const EdgeInsets.only(top: 20) : const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                ContainerDecoration(
                  constraints: const BoxConstraints(maxWidth: 400),
                  child: AutofillGroup(
                    child: Column(
                      children: [
                        Text(
                          L10n.getString("login_page_title"),
                          style: Theme.of(context).textTheme.displayMedium,
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 30),
                        if (_errorMessage != null) ErrorContainer(_errorMessage!),
                        TextField(
                          autofillHints: const [AutofillHints.username],
                          autofocus: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.name,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(userIdentifierLength),
                            FilteringTextInputFormatter.allow(userIdentifierRegExp),
                          ],
                          controller: _usernameController,
                          onChanged: (value) => _formController.value = value,
                          onTap: () => _formController.value = _usernameController.text,
                          decoration: InputDecoration(
                            labelText: L10n.getString("login_page_username_textfield_placeholder"),
                          ),
                        ),
                        const SizedBox(height: 15),
                        TextField(
                          autofillHints: const [AutofillHints.password],
                          textInputAction: TextInputAction.next,
                          obscureText: true,
                          enableSuggestions: false,
                          autocorrect: false,
                          keyboardType: TextInputType.visiblePassword,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(30),
                          ],
                          controller: _passwordController,
                          onChanged: (value) => _formController.value = value,
                          onTap: () => _formController.value = _passwordController.text,
                          decoration: InputDecoration(
                            labelText: L10n.getString("login_page_password_textfield_placeholder"),
                          ),
                          onSubmitted: (value) {
                            if (_completed()) _connect();
                          },
                        ),
                        const SizedBox(height: 15),
                        AnimatedBuilder(
                          animation: _formController,
                          builder: (_, __) => SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: _completed()
                                  ? () {
                                      _connect();
                                    }
                                  : null,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: kOrangeColor,
                                padding: const EdgeInsets.all(20),
                              ),
                              child: Text(L10n.getString("login_page_submit_button_label"), style: const TextStyle(fontSize: 16)),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: TextButton(
                            onPressed: () {
                              GoRouter.of(context).go(ResetPasswordView.routePath);
                            },
                            child: Text(
                              L10n.getString("login_page_reset_button_label"),
                              style: const TextStyle(color: kOrangeColor),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 40),
                Footer(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _completed() => _usernameController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  void _connect() {
    TextInput.finishAutofillContext();
    _loginPagePresenter.sendLoginRequest(_usernameController.text, _passwordController.text);
  }

  void redirect() {
    GoRouter.of(context).replace(widget.redirectTo!);
  }

  void displayError(String message) {
    if (mounted) {
      setState(() {
        _passwordController.clear();
        _errorMessage = message;
      });
    }
  }
}
