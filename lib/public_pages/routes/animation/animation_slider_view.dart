// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';

import 'animation_tab_presenter.dart';

class AnimationSlider extends StatefulWidget {
  final Duration totalTime;
  final AnimationTabPresenter animationPresenter;

  const AnimationSlider({
    required this.totalTime,
    required this.animationPresenter,
  });

  @override
  AnimationSliderState createState() => AnimationSliderState();
}

class AnimationSliderState extends State<AnimationSlider> with TickerProviderStateMixin {
  late double _sliderValue = 0;
  late bool _userIsMovingSlider = false;
  final _RunState _runState = _RunState();
  int _currentTime = 0;

  @override
  void initState() {
    _sliderValue = 0;
    _userIsMovingSlider = false;
    _currentTime = 0;
    widget.animationPresenter.view = this;
    super.initState();
  }

  @override
  void dispose() {
    widget.animationPresenter.dispose();
    super.dispose();
  }

  Future<void> reset() async {
    await SchedulerBinding.instance.endOfFrame;
    if (_currentTime != 0 && mounted) {
      setState(() {
        _currentTime = 0;
        _runState.reset();
      });
    }
  }

  void update(value) async {
    await SchedulerBinding.instance.endOfFrame;
    if (_currentTime != value && mounted) {
      setState(() {
        _currentTime = value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!_userIsMovingSlider) {
      _sliderValue = _getSliderValue();
    }
    return Container(
      decoration: const BoxDecoration(
        color: kOrangeColorLight,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 1.0),
            blurRadius: 10.0,
          ),
        ],
      ),
      child: Row(
        children: [
          _buildStopButton(),
          _buildPlayContinuouslyPauseButton(),
          _buildPlayControlByControlPauseButton(),
          _buildCurrentTimeLabel(),
          _buildAnimationSliderBar(context),
          _buildTotalTimeLabel(),
          const SizedBox(width: 5),
        ],
      ),
    );
  }

  Widget _buildCurrentTimeLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 15.0, 0, 10.0),
      child: Text(
        _getTimeString(_sliderValue),
        style: const TextStyle(
          fontSize: 12,
          fontFamily: 'RobotoMono',
          color: Colors.black,
        ),
      ),
    );
  }

  Widget _buildTotalTimeLabel() {
    return Text(
      _getTimeString(1.0),
      style: const TextStyle(
        fontSize: 12,
        fontFamily: 'RobotoMono',
        color: Colors.black,
      ),
    );
  }

  Widget _buildAnimationSliderBar(BuildContext context) {
    return Expanded(
      child: SliderTheme(
        data: SliderTheme.of(context).copyWith(
          trackHeight: 6.0,
          disabledInactiveTrackColor: kOrangeColorDisabled,
          disabledActiveTrackColor: kOrangeColor,
          inactiveTrackColor: kOrangeColorDisabled,
          disabledThumbColor: kOrangeColorDisabled,
        ),
        child: Slider(
          value: _sliderValue,
          onChangeStart: (_runState.isRunning) ? null : (value) => _userIsMovingSlider = true,
          onChanged: (_runState.isRunning || widget.animationPresenter.maxAnimationTime == 0)
              ? null
              : (value) {
                  var time = (value * widget.totalTime.inMilliseconds).toInt();
                  if (time >= widget.animationPresenter.minAnimationTime && time <= widget.animationPresenter.maxAnimationTime) {
                    setState(() {
                      _sliderValue = value;
                      _currentTime = time;
                      _moveAnimationManually(_currentTime);
                    });
                  }
                },
          onChangeEnd: (_runState.isRunning) ? null : (value) => _userIsMovingSlider = false,
        ),
      ),
    );
  }

  Widget _buildPlayContinuouslyPauseButton() {
    return IconButton(
      tooltip: L10n.getString("animation_run_continuously_button_tooltip"),
      splashRadius: 20,
      icon: (_runState.isRunningContinuously) ? const Icon(Icons.pause_circle_outline, size: 32) : const Icon(Icons.play_circle_outline, size: 32),
      color: kOrangeColor,
      disabledColor: kOrangeColorDisabled,
      onPressed: (_runState.isRunningControlByControl || _runState.isPausingControlByControl)
          ? null
          : () {
              if (widget.animationPresenter.isTrackListEmpty) {
                showWarningDialog(context, title: "", message: L10n.getString("time_sheet_split_time_no_orienteer"), yesButtonText: L10n.getString("button_ok_label"));
              } else {
                setState(() {
                  if (!_runState.isRunningContinuously) {
                    if (!_runState.isPausingContinuously) {
                      _runAnimation(false);
                    } else {
                      _resumeAnimation(false);
                      _runState.isPausingContinuously = false;
                    }
                    _runState.isRunningContinuously = true;
                  } else {
                    _runState.isRunningContinuously = false;
                    _runState.isPausingContinuously = true;
                    _pauseAnimation();
                  }
                });
              }
            },
    );
  }

  Widget _buildPlayControlByControlPauseButton() {
    return IconButton(
      tooltip: L10n.getString("animation_run_control_by_control_button_tooltip"),
      splashRadius: 20,
      icon: (_runState.isRunningControlByControl) ? const Icon(Icons.pause_circle_outline, size: 32) : const Icon(Icons.not_started_outlined, size: 32),
      color: kOrangeColor,
      disabledColor: kOrangeColorDisabled,
      onPressed: (_runState.isRunningContinuously || _runState.isPausingContinuously)
          ? null
          : () {
              if (widget.animationPresenter.isTrackListEmpty) {
                showWarningDialog(context, title: "", message: L10n.getString("time_sheet_split_time_no_orienteer"), yesButtonText: L10n.getString("button_ok_label"));
              } else {
                setState(() {
                  if (!_runState.isRunningControlByControl) {
                    if (!_runState.isPausingControlByControl) {
                      _runAnimation(true);
                    } else {
                      _resumeAnimation(true);
                      _runState.isPausingControlByControl = false;
                    }
                    _runState.isRunningControlByControl = true;
                  } else {
                    _runState.isRunningControlByControl = false;
                    _runState.isPausingControlByControl = true;
                    _pauseAnimation();
                  }
                });
              }
            },
    );
  }

  Widget _buildStopButton() {
    return IconButton(
      tooltip: L10n.getString("animation_stop_button_tooltip"),
      splashRadius: 20,
      icon: const Icon(Icons.stop_circle_outlined, size: 32),
      color: kOrangeColor,
      onPressed: () {
        if (widget.animationPresenter.isTrackListEmpty) return;
        setState(() {
          if (_runState.isRunning || _runState.isPausing) {
            _runState.reset();
            _stopAnimation();
          }
        });
      },
    );
  }

  double _getSliderValue() {
    if (widget.totalTime.inMilliseconds == 0) return 0;
    return math.min(_currentTime / widget.totalTime.inMilliseconds, 1.0);
  }

  Duration _getDuration(double sliderValue) {
    final milliSeconds = widget.totalTime.inMilliseconds * sliderValue;
    return Duration(milliseconds: milliSeconds.toInt());
  }

  String _getTimeString(double sliderValue) {
    String twoDigits(int n) {
      return (n.abs() >= 10) ? "$n" : "0$n";
    }

    Duration time = _getDuration(sliderValue);
    bool withHours = widget.totalTime.inHours > 0;
    final minutes = twoDigits(time.inMinutes.remainder(Duration.minutesPerHour));
    final seconds = twoDigits(time.inSeconds.remainder(Duration.secondsPerMinute));
    final hours = withHours ? '${time.inHours}:' : '';
    return "$hours$minutes:$seconds";
  }

  void _moveAnimationManually(int time) {
    widget.animationPresenter.changeCurrentPositionManually(time);
  }

  void _runAnimation(bool controlByControlMode) {
    widget.animationPresenter.whenComplete(() {
      setState(() {
        _runState.reset();
      });
    });
    widget.animationPresenter.startAnimation(_currentTime, controlByControlMode);
  }

  void _pauseAnimation() {
    widget.animationPresenter.pause();
  }

  void _resumeAnimation(bool controlByControlMode) {
    widget.animationPresenter.resume();
    widget.animationPresenter.startAnimation(_currentTime, controlByControlMode);
  }

  void _stopAnimation() {
    widget.animationPresenter.stop();
  }
}

class _RunState {
  bool isRunningContinuously = false;
  bool isRunningControlByControl = false;
  bool isPausingContinuously = false;
  bool isPausingControlByControl = false;

  void reset() {
    isRunningContinuously = false;
    isPausingContinuously = false;
    isRunningControlByControl = false;
    isPausingControlByControl = false;
  }

  bool get isRunning => isRunningContinuously || isRunningControlByControl;

  bool get isPausing => isPausingContinuously || isPausingControlByControl;
}
