import 'dart:async';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';

import '../gps_route.dart';
import 'animation_handler.dart';
import 'animation_slider_view.dart';
import 'canvas/animated_route_layer.dart';
import 'canvas/animation_canvas_view.dart';
import 'settings_side_bar.dart';
import 'subtrack.dart';

class AnimationTabPresenter {
  static const _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS = 8000;

  late AnimationCanvas _animationCanvas;
  AnimationSliderState? _animationSlideView;
  AnimationController? _controller;
  final AnimationPreferences _animationSettings;
  Function()? _postAction;

  late List<GpsRoute> _tracks;
  late final List<int> _timeBarriers;
  late int _firstLeg;
  late int _lastLeg;

  int _minTime = 0;
  int _maxTime = 0;
  bool _isPausing = true;
  int _animationSpeed = 1;
  int _baseTime = 0;
  int _currentLeg = 1;
  bool _controlByControlMode = false;
  final MapZoomController _mapZoomController;

  AnimationTabPresenter(int? numberOfCheckpoints, this._animationSettings, this._mapZoomController) {
    _firstLeg = 0;
    if (numberOfCheckpoints == null) {
      _lastLeg = 0;
      _timeBarriers = [];
    } else {
      _lastLeg = numberOfCheckpoints - 1;
      _timeBarriers = List<int>.filled(numberOfCheckpoints, 0, growable: false);
    }
  }

  void dispose() {
    _animationSlideView?.reset();
    _controller?.stop();
    _controller?.dispose();
    _controller = null;
  }

  set view(AnimationSliderState view) => _animationSlideView = view;

  set canvas(AnimationCanvas mapView) => _animationCanvas = mapView;

  void whenComplete(void Function() action) => _postAction = action;

  int get minAnimationTime => _minTime;

  int get maxAnimationTime => _maxTime;

  bool get isTrackListEmpty => _tracks.isEmpty;

  void setSelectedTracks(List<GpsRoute> tracks, int totalTime) {
    _tracks = tracks;
    if (tracks.isNotEmpty) {
      AnimationHandler.computeTimeBarriersForEachCheckpoint(tracks, _timeBarriers, totalTime);
      _minTime = _timeBarriers[_firstLeg];
      _maxTime = _timeBarriers[_lastLeg];
    }
    _notify(_minTime);
  }

  void startAnimation(int currentTime, bool controlByControlMode) {
    if (_tracks.isEmpty) {
      return;
    }
    _controlByControlMode = controlByControlMode;
    _baseTime = currentTime > 0 ? currentTime : _minTime;
    final duration = Duration(milliseconds: AnimationHandler.computeDurationFromAnimationSpeed(_maxTime - _baseTime, _animationSpeed));
    _controller = AnimationController(
      vsync: _animationSlideView!,
      duration: duration,
    );
    _isPausing = false;
    Animation trackAnimation = IntTween(begin: _baseTime, end: _maxTime).animate(_controller!);
    _controller!.addListener(() {
      List<AnimatedRouteLayer> animatedRouteLayers = _animationCanvas.getLayers();
      _moveAllTracks(animatedRouteLayers, trackAnimation.value);
      _notify(trackAnimation.value);
    });
    _controller!.forward().whenComplete(() => stop());
  }

  void changeCurrentPositionManually(int currentTime) {
    var animatedRouteLayers = _animationCanvas.getLayers();
    _moveAllTracks(animatedRouteLayers, currentTime);
  }

  void changeSpeed(double speed) {
    _animationSpeed = speed.round();
    if (!_isPausing) {
      _controller?.stop(canceled: true);
      _controller?.duration = Duration(milliseconds: AnimationHandler.computeDurationFromAnimationSpeed(_maxTime - _baseTime, _animationSpeed));
      var percent = _controller?.value;
      _controller?.forward(from: percent).whenComplete(() {
        stop();
      });
    }
  }

  void changeStartCheckpoint(int value) {
    stop();
    _firstLeg = value;
    _minTime = _timeBarriers[value];
    _notify(_minTime);
  }

  void changeEndCheckpoint(int value) {
    stop();
    _lastLeg = value;
    _maxTime = _timeBarriers[value];
  }

  void pause() {
    _controller?.stop(canceled: true);
    _isPausing = true;
  }

  void resume() {
    _isPausing = false;
  }

  void stop() {
    _clearAllTracks();
    dispose();
    _notify(_minTime);
    _isPausing = false;
    if (_postAction != null) {
      _postAction!();
    }
  }

  void _moveAllTracks(List<AnimatedRouteLayer> animatedRoutes, int currentValue) {
    _Barycenter barycenter;
    if (_controlByControlMode) {
      barycenter = _moveAllTracksControlByControl(animatedRoutes, currentValue);
    } else {
      barycenter = _moveAllTracksContinuously(animatedRoutes, currentValue);
    }
    if (barycenter._numberOfPositions > 0 && _animationSettings.selfCentered) {
      _mapZoomController.mapController.move(LatLng(barycenter.getLatitude(), barycenter.getLongitude()), _mapZoomController.mapController.camera.zoom);
    } else {
      _animationCanvas.refresh();
    }
  }

  _Barycenter _moveAllTracksContinuously(List<AnimatedRouteLayer> animatedRoutes, int currentValue) {
    final _Barycenter barycenter = _Barycenter();
    for (int i = 0; i < animatedRoutes.length; i++) {
      // Workaround: in case the planner changes the course features after the track has been stored.
      final punchTimes = _tracks[i].model.punchTimes;
      final sortedCheckpoints = TrackModel.sortCheckpointsByPunchTimes(_tracks[i].model.punchTimes);
      final endCheckpoint = math.min(_lastLeg, punchTimes.length - 1);
      final lastPunchTime = punchTimes[sortedCheckpoints[endCheckpoint]].timestampInMillisecond > 0 ? punchTimes[sortedCheckpoints[endCheckpoint]].timestampInMillisecond : _maxTime;
      final valueRelativeToTrack = currentValue - _minTime + punchTimes[sortedCheckpoints[_firstLeg]].timestampInMillisecond;
      final List<GeodesicPoint> subTrack = SubTrack.getSubTrack(_tracks[i].model.route!, valueRelativeToTrack, lastPunchTime);
      if (subTrack.isNotEmpty) {
        barycenter.addPosition(subTrack.last.latitude, subTrack.last.longitude);
      }
      final isPunched = (valueRelativeToTrack > lastPunchTime) ? false : SubTrack.isPunchLocation(valueRelativeToTrack, punchTimes, _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS);
      animatedRoutes[i].drawRoute(
        subTrack,
        isPunchLocation: isPunched,
      );
    }
    return barycenter;
  }

  _Barycenter _moveAllTracksControlByControl(List<AnimatedRouteLayer> animatedRoutes, int currentValue) {
    int leg = AnimationHandler.getLegFromCurrentTime(currentValue, _timeBarriers);
    _Barycenter barycenter = _Barycenter();
    for (int i = 0; i < animatedRoutes.length; i++) {
      // Workaround: in case the planner changes the course features after the track has been stored.
      final punchTimes = _tracks[i].model.punchTimes;
      final sortedCheckpoints = TrackModel.sortCheckpointsByPunchTimes(_tracks[i].model.punchTimes);
      final legMinTime = _timeBarriers[leg - 1];
      final legMaxTime = punchTimes[sortedCheckpoints[leg]].timestampInMillisecond > 0 ? punchTimes[sortedCheckpoints[leg]].timestampInMillisecond : _maxTime;
      final valueRelativeToTrack = currentValue - legMinTime + punchTimes[sortedCheckpoints[leg - 1]].timestampInMillisecond;
      final subTrack = SubTrack.getSubTrack(_tracks[i].model.route!, valueRelativeToTrack, legMaxTime);
      if (subTrack.isNotEmpty) {
        barycenter.addPosition(subTrack.last.latitude, subTrack.last.longitude);
      }
      final isPunched = (valueRelativeToTrack > legMaxTime) ? false : SubTrack.isPunchLocation(valueRelativeToTrack, punchTimes, _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS);
      animatedRoutes[i].drawRoute(
        subTrack,
        isPunchLocation: isPunched,
      );
    }
    if (_controller != null && _controller!.isAnimating && leg != _lastLeg && leg != _currentLeg) {
      _currentLeg = leg;
      _controller?.stop();
      var percent = _controller?.value;
      Future.delayed(const Duration(seconds: 1)).then((value) {
        if (!_isPausing) _controller?.forward(from: percent).whenComplete(() => stop());
      });
    }
    return barycenter;
  }

  void _clearAllTracks() {
    if (_controller != null) {
      _controller!.value = _controller!.upperBound;
      List<AnimatedRouteLayer> animatedRouteLayers = _animationCanvas.getLayers();
      for (var layer in animatedRouteLayers) {
        layer.drawRoute([]);
      }
    }
  }

  void _notify(value) => _animationSlideView?.update(value);

  static Future<ui.Image> loadImage(IdentifiedUser? user, String? imageId) => AnimationHandler.loadImage(user, imageId);
}

class _Barycenter {
  double _latitudeSum = 0;
  double _longitudeSum = 0;
  int _numberOfPositions = 0;

  double getLatitude() => _latitudeSum / _numberOfPositions;

  double getLongitude() => _longitudeSum / _numberOfPositions;

  void addPosition(double latitude, double longitude) {
    _latitudeSum += latitude;
    _longitudeSum += longitude;
    _numberOfPositions++;
  }
}
