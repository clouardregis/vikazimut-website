import 'dart:async';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';

import '../gps_route.dart';

class AnimationHandler {
  static int computeDurationFromAnimationSpeed(int totalTime, int animationSpeed) {
    return totalTime ~/ math.pow(animationSpeed, 2.5);
  }

  static void computeTimeBarriersForEachCheckpoint(
    List<GpsRoute> tracks,
    List<int> timeBarriers,
    int totalTime,
  ) {
    for (var leg = 1; leg < timeBarriers.length; ++leg) {
      int max = -1;
      for (final track in tracks) {
        // Defensive programming: in case the planner changes the number of points
        // of the course after the track has been recorded in the database.
        if (leg < track.model.punchTimes.length) {
          List<int> sortedCheckpoints = TrackModel.sortCheckpointsByPunchTimes(track.model.punchTimes);
          final trackPunchTime = track.model.punchTimes[sortedCheckpoints[leg]].timestampInMillisecond;
          if (trackPunchTime >= 0 && trackPunchTime > max) {
            max = trackPunchTime;
          }
        }
      }
      if (max >= 0) {
        timeBarriers[leg] = max;
      } else {
        timeBarriers[leg] = timeBarriers[leg - 1];
      }
    }
    timeBarriers.last = totalTime;
  }

  static int getLegFromCurrentTime(int currentValue, List<int> timeBarriers) {
    int leg;
    for (leg = 1; leg < timeBarriers.length; ++leg) {
      if (currentValue <= timeBarriers[leg]) {
        break;
      }
    }
    if (leg >= timeBarriers.length) {
      leg = timeBarriers.length - 1;
    }
    return leg;
  }

  static Future<ui.Image> loadImage(IdentifiedUser? user, String? imageId) async {
    if (imageId == null) {
      return Future.error("No such map image found...");
    }
    var completer = Completer<ImageInfo>();
    var img = loadNetworkImage(user, imageId);
    img.resolve(const ImageConfiguration()).addListener(ImageStreamListener((info, _) {
      completer.complete(info);
    }));
    ImageInfo imageInfo = await completer.future;
    return imageInfo.image;
  }
}
