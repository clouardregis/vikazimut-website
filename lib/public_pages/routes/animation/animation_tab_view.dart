// coverage:ignore-file
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'animation_slider_view.dart';
import 'animation_tab_presenter.dart';
import 'canvas/animated_route_layer.dart';
import 'canvas/animation_canvas_view.dart';
import 'settings_side_bar.dart';

class AnimationTabView extends StatefulWidget {
  final RouteListPagePresenter presenter;
  final Future<CourseData> courseData;

  const AnimationTabView({
    required this.presenter,
    required this.courseData,
  });

  @override
  AnimationTabViewState createState() => AnimationTabViewState();
}

class AnimationTabViewState extends State<AnimationTabView> with TickerProviderStateMixin {
  late final AnimationTabPresenter? _animationPresenter;
  late final AnimationPreferences _animationPreferences;
  AnimationCanvas? _animationCanvas;
  bool _isMapShowed = true;
  MapZoomController? _mapZoomController;

  @override
  void initState() {
    super.initState();
    _mapZoomController = MapZoomController(this);
    _animationPreferences = AnimationPreferences();
    _animationPresenter = AnimationTabPresenter(widget.presenter.courseData?.checkpoints.length, _animationPreferences, _mapZoomController!);
  }

  @override
  void dispose() {
    _animationCanvas?.removeAllLayers();
    _animationPresenter?.dispose();
    _mapZoomController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List>(
      future: Future.wait([widget.courseData, AnimationTabPresenter.loadImage(widget.presenter.user, widget.presenter.courseData?.courseGeoreference.imageId)]),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasError) {
          return GlobalErrorWidget(snapshot.error.toString());
        } else if (snapshot.hasData && snapshot.data != null) {
          final CourseGeoreference courseGeoreference = snapshot.data![0].courseGeoreference;
          final ui.Image orienteeringMapImage = snapshot.data![1];
          final Size orienteeringMapImageSize = Size(orienteeringMapImage.width.toDouble(), orienteeringMapImage.height.toDouble());
          LatLngBounds terrainBounds = LatLngBounds(
            LatLng(courseGeoreference.latLngBounds()[3][0], courseGeoreference.latLngBounds()[3][1]),
            LatLng(courseGeoreference.latLngBounds()[1][0], courseGeoreference.latLngBounds()[1][1]),
          );
          _mapZoomController!.bounds = terrainBounds;
          if (_animationCanvas != null) {
            _resetAnimation(_animationCanvas!, _animationPresenter!);
          }
          _animationCanvas = AnimationCanvas(
            imageSize: orienteeringMapImageSize,
            terrainBounds: widget.presenter.courseData!.courseGeoreference.bounds,
            animationPresenter: _animationPresenter!,
          );
          List<CustomBaseOverlayImage>? overlayImages = <CustomBaseOverlayImage>[
            CustomRotatedOverlayImage(
              bounds: terrainBounds,
              opacity: 0.8,
              angleInDegree: -courseGeoreference.bounds.rotation,
              imageProvider: loadNetworkImage(widget.presenter.user, courseGeoreference.imageId),
            ),
            CustomRotatedOverlayCanvas(
              bounds: terrainBounds,
              opacity: 0,
              angleInDegree: -courseGeoreference.bounds.rotation,
              animationCanvas: _animationCanvas!,
            ),
          ];
          int maxDurationInMillisecond = _computeMaxDurationInMillisecond(widget.presenter.selectedTracks);
          _animationPresenter!.setSelectedTracks(widget.presenter.selectedTracks, maxDurationInMillisecond);
          SchedulerBinding.instance.addPostFrameCallback((_) => _addRouteOnMapAsLayer(_animationCanvas!));
          return Stack(
            alignment: Alignment.topRight,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: kOrangeColorLight,
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      runAlignment: WrapAlignment.center,
                      alignment: WrapAlignment.center,
                      children: [
                        const SizedBox(width: constants.COLLAPSIBLE_MIN_WIDTH),
                        _SpeedSlider(_animationPresenter!),
                        if (ScreenHelper.isDesktop(context)) const _ToolBarDivider(),
                        _CheckpointSelector(_animationPresenter!, widget.presenter.courseData!.checkpoints.length),
                        Container(),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Stack(
                      alignment: Alignment.topRight,
                      children: [
                        SizedBox(
                          height: double.infinity,
                          child: Stack(
                            alignment: Alignment.topRight,
                            children: [
                              _OpenStreetMapWithCourseMap(
                                mapController: _mapZoomController!.mapController,
                                bounds: terrainBounds,
                                showedMap: _isMapShowed,
                                overlayImages: overlayImages,
                              ),
                              Positioned(
                                right: 5.0,
                                top: 15.0,
                                child: Column(
                                  children: [
                                    if (ScreenHelper.isDesktop(context))
                                      _MapButton(
                                        iconData: Icons.add,
                                        onPressed: () {
                                          _mapZoomController?.zoomIn();
                                        },
                                      ),
                                    if (ScreenHelper.isDesktop(context))
                                      _MapButton(
                                        iconData: Icons.remove,
                                        onPressed: () {
                                          _mapZoomController?.zoomOut();
                                        },
                                      ),
                                    if (!ScreenHelper.isMobile(context))
                                      _MapButton(
                                        iconData: Icons.my_location,
                                        onPressed: () => _mapZoomController?.reset(),
                                      ),
                                    _MapButton(
                                      onPressed: () => setState(() => _isMapShowed = !_isMapShowed),
                                      iconData: (_isMapShowed) ? Icons.layers_clear : Icons.layers,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  AnimationSlider(
                    animationPresenter: _animationPresenter!,
                    totalTime: Duration(milliseconds: maxDurationInMillisecond),
                  ),
                ],
              ),
              SettingsSideBar(_animationPreferences),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }

  void _addRouteOnMapAsLayer(AnimationCanvas canvas) {
    for (int i = 0; i < widget.presenter.selectedTracks.length; i++) {
      var layer = AnimatedRouteLayer(
        canvas,
        widget.presenter.selectedTracks[i].color,
        _animationPreferences,
        widget.presenter.selectedTracks[i].model.nickname,
      );
      canvas.addLayer(layer);
    }
  }

  int _computeMaxDurationInMillisecond(List<GpsRoute> selectedTracks) {
    int maxTime = 0;
    for (var track in selectedTracks) {
      final trackTime = track.model.totalTimeInMilliseconds;
      if (maxTime < trackTime) {
        maxTime = trackTime;
      }
    }
    return maxTime;
  }

  void _resetAnimation(AnimationCanvas canvas, AnimationTabPresenter animationPresenter) {
    animationPresenter.dispose();
    canvas.removeAllLayers();
  }
}

class _OpenStreetMapWithCourseMap extends StatefulWidget {
  final MapController mapController;
  final LatLngBounds bounds;
  final bool showedMap;
  final List<CustomBaseOverlayImage> overlayImages;

  @override
  State<_OpenStreetMapWithCourseMap> createState() => _OpenStreetMapWithCourseMapState();

  const _OpenStreetMapWithCourseMap({
    required this.mapController,
    required this.bounds,
    required this.showedMap,
    required this.overlayImages,
  });
}

class _OpenStreetMapWithCourseMapState extends State<_OpenStreetMapWithCourseMap> {
  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      mapController: widget.mapController,
      options: MapOptions(
        maxZoom: 19,
        minZoom: 10,
        interactionOptions: InteractionOptions(
          cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
          flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
          scrollWheelVelocity: 0.002,
        ),
        initialCameraFit: CameraFit.bounds(bounds: widget.bounds, padding: const EdgeInsets.all(20)),
      ),
      children: [
        TileLayer(
          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
          userAgentPackageName: 'fr.vikazim.vikazimut',
          tileProvider: CancellableNetworkTileProvider(),
        ),
        (widget.showedMap) ? CustomOverlayImageLayer(overlayImages: widget.overlayImages) : CustomOverlayImageLayer(overlayImages: [widget.overlayImages[1]]),
        ScaleLayerWidget(
          options: ScaleLayerPluginOption(
            lineColor: Colors.blue,
            lineWidth: 2,
            textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
          ),
        ),
        OpenStreetMapAttributionWidget(),
      ],
    );
  }
}

class _ToolBarDivider extends StatelessWidget {
  const _ToolBarDivider();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38,
      width: 3,
      margin: const EdgeInsetsDirectional.only(start: 0, end: 10),
      decoration: const BoxDecoration(
        border: Border(
          left: BorderSide(
            color: kOrangeColorDisabled,
            width: 1,
          ),
        ),
      ),
    );
  }
}

class _CheckpointSelector extends StatefulWidget {
  final AnimationTabPresenter animationPresenter;
  final int numberOfCheckpoints;

  const _CheckpointSelector(this.animationPresenter, this.numberOfCheckpoints);

  @override
  State<_CheckpointSelector> createState() => _CheckpointSelectorState();
}

class _CheckpointSelectorState extends State<_CheckpointSelector> {
  late RangeValues _currentRangeValues;
  late final double _endCheckpoint;

  @override
  void initState() {
    _endCheckpoint = widget.numberOfCheckpoints - 1;
    _currentRangeValues = RangeValues(0, _endCheckpoint);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Tooltip(
            message: L10n.getString("animation_legs_selector_tooltip"),
            child: const Icon(
              Icons.flag_outlined,
              color: kOrangeColor,
            ),
          ),
          SizedBox(
            width: 300,
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                rangeValueIndicatorShape: const PaddleRangeSliderValueIndicatorShape(),
                valueIndicatorColor: kGreenColor,
                valueIndicatorTextStyle: const TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                ),
              ),
              child: RangeSlider(
                values: _currentRangeValues,
                min: 0,
                max: _endCheckpoint,
                divisions: (_endCheckpoint + 1).toInt(),
                labels: RangeLabels(
                  _toCheckpointLabel(_currentRangeValues.start),
                  _toCheckpointLabel(_currentRangeValues.end),
                ),
                onChanged: (RangeValues values) {
                  setState(() {
                    if (_currentRangeValues.start != values.start) {
                      if (values.start < _currentRangeValues.end) {
                        widget.animationPresenter.changeStartCheckpoint(values.start.round());
                        _currentRangeValues = values;
                      }
                    } else if (_currentRangeValues.end != values.end) {
                      if (values.end > _currentRangeValues.start) {
                        widget.animationPresenter.changeEndCheckpoint(values.end.round());
                        _currentRangeValues = values;
                      }
                    }
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _toCheckpointLabel(double checkpoint) {
    if (checkpoint == 0) {
      return "S";
    } else if (checkpoint == _endCheckpoint) {
      return "F";
    } else {
      return checkpoint.round().toString();
    }
  }
}

class _SpeedSlider extends StatefulWidget {
  static const double _SPEED_MIN = 1;
  static const double _SPEED_MAX = 10;
  static const double _SPEED_INIT = 3.7;
  final AnimationTabPresenter animationPresenter;

  const _SpeedSlider(this.animationPresenter);

  @override
  State<_SpeedSlider> createState() => _SpeedSliderState();
}

class _SpeedSliderState extends State<_SpeedSlider> {
  double _currentSpeedValue = _SpeedSlider._SPEED_INIT;

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) => widget.animationPresenter.changeSpeed(_currentSpeedValue));
    return SizedBox(
      height: 35,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Tooltip(
            message: L10n.getString("animation_speed_slider_tooltip"),
            child: const Icon(
              Icons.speed,
              color: kOrangeColor,
            ),
          ),
          SliderTheme(
            data: SliderTheme.of(context).copyWith(
              valueIndicatorShape: const PaddleSliderValueIndicatorShape(),
              valueIndicatorColor: kGreenColor,
              valueIndicatorTextStyle: const TextStyle(
                color: Colors.white,
                fontSize: 12.0,
              ),
              inactiveTrackColor: kOrangeColorDarkLight,
              inactiveTickMarkColor: kOrangeColor,
            ),
            child: Slider(
              value: _currentSpeedValue,
              min: _SpeedSlider._SPEED_MIN,
              max: _SpeedSlider._SPEED_MAX,
              divisions: (_SpeedSlider._SPEED_MAX - _SpeedSlider._SPEED_MIN + 1).toInt(),
              label: "${_currentSpeedValue.round().toString()}x",
              onChanged: (double value) {
                widget.animationPresenter.changeSpeed(value);
                setState(() => _currentSpeedValue = value);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class _MapButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final IconData iconData;

  const _MapButton({
    required this.onPressed,
    required this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kOrangeColorUltraLight,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(width: 1, color: kCaptionColor),
      ),
      child: IconButton(
        icon: Icon(iconData, size: 24, color: Colors.black),
        onPressed: onPressed,
      ),
    );
  }
}
