// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import '../settings_side_bar.dart';
import 'animation_canvas_view.dart';

class AnimatedRouteLayer {
  final AnimationCanvas canvas;
  final Color _color;
  final AnimationPreferences _preferences;
  List<GeodesicPoint>? _route;
  GeodesicPoint? _lastLocation;
  late final String _abbreviatedName;
  bool? _isPunchLocation;
  late final Paint _tailPaint;
  late final Paint _dotAreaPaint;
  late final Paint _dotBorderPaint;
  late final Paint _textBackgroundPaint;

  AnimatedRouteLayer(this.canvas, this._color, this._preferences, String name) {
    _tailPaint = Paint()
      ..color = _color.withValues(alpha: 0.8)
      ..style = PaintingStyle.stroke;
    _dotAreaPaint = Paint();
    _dotBorderPaint = Paint()
      ..color = _color
      ..style = PaintingStyle.stroke;
    _textBackgroundPaint = Paint()..color = Colors.grey.withValues(alpha: 0.6);
    _abbreviatedName = name.substring(0, math.min(3, name.length));
  }

  void draw(Canvas canvas, double scale) {
    if (_route != null && _route!.isNotEmpty) {
      _drawPolyLine(canvas, _route!, _lastLocation, _isPunchLocation, scale);
    }
  }

  void drawRoute(List<GeodesicPoint> route, {GeodesicPoint? lastLocation, bool? isPunchLocation}) {
    _route = route;
    _lastLocation = lastLocation;
    _isPunchLocation = isPunchLocation;
  }

  void _drawPolyLine(
    Canvas canvas,
    List<GeodesicPoint> route,
    GeodesicPoint? lastLocation,
    bool? isPunchLocation,
    double scale,
  ) {
    final zoom = 1.0 / scale;
    Offset? captionOffset = Offset(24 * zoom, -44 * zoom);
    double? captionTextFontSize = 20 * zoom;
    double? speedUnitFontSize = 18 * zoom;
    double? headRadius = 10 * zoom;
    _tailPaint.strokeWidth = 7 * zoom;
    _dotBorderPaint.strokeWidth = 2 * zoom;

    double distance = drawTail(route, canvas);
    Offset headPosition = drawTrackHead(route, isPunchLocation, canvas, headRadius);

    if (_preferences.labelAboveDot == LabelAboveDotType.speed) {
      drawAverageSpeedWithTackHead(route, distance, captionTextFontSize, speedUnitFontSize, canvas, headPosition, captionOffset);
    } else if (_preferences.labelAboveDot == LabelAboveDotType.name) {
      drawOrienteerNameWithTrackHead(captionTextFontSize, canvas, headPosition, captionOffset);
    }
  }

  void drawOrienteerNameWithTrackHead(
    double captionTextFontSize,
    Canvas canvas,
    Offset headPosition,
    Offset captionOffset,
  ) {
    TextPainter tp = TextPainter(
        text: TextSpan(
          style: TextStyle(
            color: _dotBorderPaint.color,
            fontFamily: 'RobotoMono',
            fontSize: captionTextFontSize,
          ),
          text: _abbreviatedName,
        ),
        textDirection: TextDirection.ltr);
    tp.layout();
    canvas.drawRRect(RRect.fromRectAndRadius(Rect.fromLTWH(headPosition.dx + captionOffset.dx, headPosition.dy + captionOffset.dy, tp.width, tp.height), const Radius.circular(5.0)), _textBackgroundPaint);
    tp.paint(canvas, Offset(headPosition.dx + captionOffset.dx, headPosition.dy + captionOffset.dy));
  }

  void drawAverageSpeedWithTackHead(List<GeodesicPoint> route, double distance, double captionTextFontSize, double speedUnitFontSize, Canvas canvas, Offset headPosition, Offset captionOffset) {
    int laps = (route.last.timestampInMillisecond - route[0].timestampInMillisecond) + 1; // +1 to prevent division by zero
    double speed = 1000 * 3.6 * distance / laps;
    TextSpan span = TextSpan(
      style: TextStyle(
        color: _dotBorderPaint.color,
        fontFamily: 'RobotoMono',
      ),
      children: [
        TextSpan(
          style: TextStyle(
            fontSize: captionTextFontSize,
            fontWeight: FontWeight.bold,
          ),
          text: sprintf("%2.0f", [speed]),
        ),
        TextSpan(
          style: TextStyle(
            fontSize: speedUnitFontSize,
          ),
          text: "km/h",
        )
      ],
    );
    TextPainter tp = TextPainter(text: span, textDirection: TextDirection.ltr);
    tp.layout();
    canvas.drawRRect(RRect.fromRectAndRadius(Rect.fromLTWH(headPosition.dx + captionOffset.dx, headPosition.dy + captionOffset.dy, tp.width, tp.height), const Radius.circular(5.0)), _textBackgroundPaint);
    tp.paint(canvas, Offset(headPosition.dx + captionOffset.dx, headPosition.dy + captionOffset.dy));
  }

  Offset drawTrackHead(List<GeodesicPoint> route, bool? isPunchLocation, Canvas canvas, double headRadius) {
    GeodesicPoint lastLocation = route.last;
    Offset pt2 = _convertGeodesicPointToMapPoint(lastLocation);
    if (isPunchLocation ?? true) {
      _dotAreaPaint.color = Colors.yellowAccent;
    } else {
      _dotAreaPaint.color = _color.withValues(alpha: 0.5);
    }
    canvas.drawCircle(pt2, headRadius, _dotAreaPaint);
    canvas.drawCircle(pt2, headRadius, _dotBorderPaint);
    return pt2;
  }

  double drawTail(List<GeodesicPoint> route, Canvas canvas) {
    Path path = Path();
    Offset pt1 = _convertGeodesicPointToMapPoint(route[0]);
    path.moveTo(pt1.dx, pt1.dy);
    double distance = 0;
    for (int i = 1; i < route.length; i++) {
      Offset pt2 = _convertGeodesicPointToMapPoint(route[i]);
      distance += GeodesicPoint.distanceBetweenInMeter(
        route[i].latitude,
        route[i].longitude,
        route[i - 1].latitude,
        route[i - 1].longitude,
      );
      path.lineTo(pt2.dx, pt2.dy);
    }
    canvas.drawPath(path, _tailPaint);
    return distance;
  }

  Offset _convertGeodesicPointToMapPoint(GeodesicPoint currentPoint) {
    return canvas.convertGeoPointIntoMapCoordinates(currentPoint.latitude, currentPoint.longitude);
  }
}
