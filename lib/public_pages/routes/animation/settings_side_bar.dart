import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

class SettingsSideBar extends StatefulWidget {
  final AnimationPreferences animationSettings;

  const SettingsSideBar(this.animationSettings);

  @override
  State<SettingsSideBar> createState() => _SettingsSideBarState();
}

class _SettingsSideBarState extends State<SettingsSideBar> {
  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      widget.animationSettings.selfCentered = ScreenHelper.isMobile(context) ? true : false;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("animation_settings_title"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      children: [
        ListTile(
          title: Text(L10n.getString("animation_settings_dot_label_title")),
          subtitle: Column(
            children: <Widget>[
              ListTile(
                title: Text(L10n.getString("animation_settings_dot_label_nothing")),
                leading: Radio<LabelAboveDotType>(
                  activeColor: kOrangeColor,
                  value: LabelAboveDotType.none,
                  groupValue: widget.animationSettings.labelAboveDot,
                  onChanged: (LabelAboveDotType? value) {
                    setState(() {
                      widget.animationSettings.labelAboveDot = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text(L10n.getString("animation_settings_dot_label_speed")),
                leading: Radio<LabelAboveDotType>(
                  activeColor: kOrangeColor,
                  value: LabelAboveDotType.speed,
                  groupValue: widget.animationSettings.labelAboveDot,
                  onChanged: (LabelAboveDotType? value) {
                    setState(() {
                      widget.animationSettings.labelAboveDot = value!;
                    });
                  },
                ),
              ),
              ListTile(
                title: Text(L10n.getString("animation_settings_dot_label_name")),
                leading: Radio<LabelAboveDotType>(
                  activeColor: kOrangeColor,
                  value: LabelAboveDotType.name,
                  groupValue: widget.animationSettings.labelAboveDot,
                  onChanged: (LabelAboveDotType? value) {
                    setState(() {
                      widget.animationSettings.labelAboveDot = value!;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        CheckboxListTile(
          title: Text(L10n.getString("animation_settings_centered_label")),
          activeColor: kOrangeColor,
          checkColor: Colors.white,
          value: widget.animationSettings.selfCentered,
          onChanged: (bool? value) {
            setState(() {
              widget.animationSettings.selfCentered = value!;
            });
          },
        ),
      ],
    );
  }
}

enum LabelAboveDotType { none, speed, name }

class AnimationPreferences {
  LabelAboveDotType labelAboveDot = LabelAboveDotType.speed;
  bool selfCentered = false;
}
