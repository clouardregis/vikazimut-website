import 'package:flutter/material.dart';

@immutable
class GraphData {
  late final List<int> legDistance;
  late final List<Color?> colors;
  late final List<List<int>> data;

  GraphData(int numberOfCheckpoints, int numberOfTracks) {
    colors = List.filled(numberOfTracks, null, growable: false);
    legDistance = List.filled(numberOfCheckpoints, 0, growable: false);
    data = List.generate(numberOfTracks, (i) => List.filled(numberOfCheckpoints, 0, growable: false), growable: false);
  }
}
