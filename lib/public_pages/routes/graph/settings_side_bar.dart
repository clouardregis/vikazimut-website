import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

class SettingsSideBar extends StatefulWidget {
  final GraphSettings settings;

  const SettingsSideBar(this.settings);

  @override
  State<SettingsSideBar> createState() => _SettingsSideBarState();
}

class _SettingsSideBarState extends State<SettingsSideBar> {
  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("time_behind_leader_graph_settings"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(10.0),
      children: [
        Text(L10n.getString("time_behind_leader_graph_settings1_title")),
        DropdownButton<GraphType>(
          value: widget.settings.graphType.value,
          iconEnabledColor: kOrangeColor,
          focusColor: Theme.of(context).scaffoldBackgroundColor,
          style: const TextStyle(color: kOrangeColor),
          underline: Container(
            height: 2,
            color: kOrangeColor,
          ),
          onChanged: (newValue) {
            setState(() {
              widget.settings.graphType.value = newValue!;
            });
          },
          items: GraphType.values.map((GraphType classType) {
            return DropdownMenuItem<GraphType>(value: classType, child: Text(L10n.getString(classType.name())));
          }).toList(),
        ),
      ],
    );
  }
}

enum GraphType { CUMULATIVE, LEG }

extension ParseToString on GraphType {
  String name() => "time_behind_leader_graph_settings1_${toString().split('.').last.toLowerCase()}";
}

class GraphSettings {
  ValueNotifier<GraphType> graphType = ValueNotifier(GraphType.CUMULATIVE);

  void dispose() {
    graphType.dispose();
  }
}
