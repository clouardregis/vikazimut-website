import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'graph_data.dart';
import 'settings_side_bar.dart';

class TimeBehindLeaderGraphModel {
  static const double MIN_WIDTH_BETWEEN_CHECKPOINT = 30.0;

  static GraphData buildTable(
    List<GpsRoute> presetOrderSelectedTracks,
    List<Checkpoint> checkpoints,
    GraphType graphType,
  ) {
    List<int> legDistances = computeLegStraightDistances_(checkpoints);
    var numberOfCheckpoints = legDistances.length;
    var numberOfTracks = presetOrderSelectedTracks.length;
    GraphData timeTable = GraphData(numberOfCheckpoints, numberOfTracks);
    _buildHeadTable(timeTable, legDistances, graphType, presetOrderSelectedTracks);
    return timeTable;
  }

  static List<GpsRoute> selectCompletePresetOrderTracks(List<GpsRoute> selectedTracks) {
    List<GpsRoute> presetOrderSelectedTracks = [];
    for (int i = 0; i < selectedTracks.length; i++) {
      if (selectedTracks[i].model.courseFormat.isPresetOrder && !containMissingControls_(selectedTracks[i])) {
        presetOrderSelectedTracks.add(selectedTracks[i]);
      }
    }
    return presetOrderSelectedTracks;
  }

  static int getTotalDistance(GraphData table) {
    return table.legDistance.reduce((a, b) => a + b);
  }

  static int indexOfLeaderInTheColumn(List<List<int>> timeCell, int column) {
    int minValue = timeCell[0][column];
    int minIndex = 0;
    for (int row = 1; row < timeCell.length; row++) {
      if (timeCell[row][column] < minValue) {
        minValue = timeCell[row][column];
        minIndex = row;
      }
    }
    return minIndex;
  }

  static int calculateMaxTimeDifference(GraphData timeCell) {
    int maxTimeDifference = 1;
    for (int column = 0; column < timeCell.data[0].length; column++) {
      int indexFirst = indexOfLeaderInTheColumn(timeCell.data, column);
      int min = timeCell.data[indexFirst][column];
      int max = 0;
      for (int row = 0; row < timeCell.data.length; row++) {
        if (max < timeCell.data[row][column]) {
          max = timeCell.data[row][column];
        }
      }
      if (max - min > maxTimeDifference) {
        maxTimeDifference = max - min;
      }
    }
    return maxTimeDifference;
  }

  static double calculateGraphWidthFromCheckpointCount(int checkpointCount) {
    return math.max(570, checkpointCount * MIN_WIDTH_BETWEEN_CHECKPOINT);
  }

  @visibleForTesting
  static List<int> computeLegStraightDistances_(List<Checkpoint> checkpoints) {
    List<int> legDistances = [0];
    for (int i = 1; i < checkpoints.length; i++) {
      var distance = GeodesicPoint.distanceBetweenInMeter(
        checkpoints[i - 1].latitude,
        checkpoints[i - 1].longitude,
        checkpoints[i].latitude,
        checkpoints[i].longitude,
      );
      legDistances.add(distance.toInt());
    }
    return legDistances;
  }

  static void _buildHeadTable(
    GraphData timeTable,
    List<int> legDistances,
    GraphType graphType,
    List<GpsRoute> presetOrderSelectedTracks,
  ) {
    for (int i = 0; i < timeTable.colors.length; i++) {
      timeTable.colors[i] = presetOrderSelectedTracks[i].color.withValues(alpha: 0.7);
    }

    for (int i = 0; i < legDistances.length; i++) {
      timeTable.legDistance[i] = legDistances[i];
    }

    for (int row = 0; row < timeTable.data.length; row++) {
      for (int column = 0; column < timeTable.data[0].length; column++) {
        if (graphType == GraphType.CUMULATIVE) {
          // Defensive programming: in case the planner change the course features after the track has been done..
          if (column >= presetOrderSelectedTracks[row].model.punchTimes.length) {
            timeTable.data[row][column] = 0;
          } else {
            timeTable.data[row][column] = presetOrderSelectedTracks[row].model.punchTimes[column].timestampInMillisecond;
          }
        } else {
          // Defensive programming: in case the planner change the course features after the track has been done..
          if (column < 1 || column >= presetOrderSelectedTracks[row].model.punchTimes.length) {
            timeTable.data[row][column] = 0;
          } else {
            timeTable.data[row][column] = presetOrderSelectedTracks[row].model.punchTimes[column].timestampInMillisecond - presetOrderSelectedTracks[row].model.punchTimes[column - 1].timestampInMillisecond;
          }
        }
      }
    }
  }

  @visibleForTesting
  static bool containMissingControls_(GpsRoute gpsRoute) {
    for (int i = 1; i < gpsRoute.model.punchTimes.length; i++) {
      if (gpsRoute.model.punchTimes[i].timestampInMillisecond <= 0) {
        return true;
      }
    }
    return false;
  }
}
