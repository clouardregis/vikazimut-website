import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'graph_data.dart';
import 'settings_side_bar.dart';
import 'time_behind_leader_graph_model.dart';

class TimeBehindLeaderGraphPresenter {
  static const double MIN_WIDTH_BETWEEN_CHECKPOINT = 30.0;

  List<GpsRoute> _presetOrderSelectedTracks = [];

  List<GpsRoute> get presetOrderSelectedTracks => _presetOrderSelectedTracks;

  GraphData buildTable(List<GpsRoute> selectedTracks, List<Checkpoint> checkpoints, GraphType graphType) {
    _presetOrderSelectedTracks = TimeBehindLeaderGraphModel.selectCompletePresetOrderTracks(selectedTracks);
    return TimeBehindLeaderGraphModel.buildTable(_presetOrderSelectedTracks, checkpoints, graphType);
  }

  static int getTotalDistance(GraphData table) => TimeBehindLeaderGraphModel.getTotalDistance(table);

  static int indexOfLeaderInTheColumn(List<List<int>> timeCell, int column) => TimeBehindLeaderGraphModel.indexOfLeaderInTheColumn(timeCell, column);

  static int calculateMaxTimeDifference(GraphData timeCell) => TimeBehindLeaderGraphModel.calculateMaxTimeDifference(timeCell);

  static double calculateGraphWidthFromCPCount(int checkpointCount) => TimeBehindLeaderGraphModel.calculateGraphWidthFromCheckpointCount(checkpointCount);
}
