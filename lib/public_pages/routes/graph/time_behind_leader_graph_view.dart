// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'chart_graph_painter.dart';
import 'settings_side_bar.dart';
import 'time_behind_leader_graph_presenter.dart';

class TimeBehindLeaderGraphView extends StatefulWidget {
  final RouteListPagePresenter presenter;

  const TimeBehindLeaderGraphView({required this.presenter});

  @override
  State<TimeBehindLeaderGraphView> createState() => _TimeBehindLeaderGraphViewState();
}

class _TimeBehindLeaderGraphViewState extends State<TimeBehindLeaderGraphView> {
  late final GraphSettings _graphSettings;
  late final ScrollController _horizontalScroll;

  @override
  void initState() {
    _graphSettings = GraphSettings();
    _horizontalScroll = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _graphSettings.dispose();
    _horizontalScroll.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.presenter.selectedTracks.isNotEmpty) {
      final isAllDisplayable = widget.presenter.selectedTracks.any((element) {
        return element.courseFormat.isFreeOrder || !element.model.isCompleted;
      });
      return Stack(
        alignment: Alignment.topRight,
        children: [
          Scrollbar(
            controller: _horizontalScroll,
            thumbVisibility: true,
            child: CustomScrollView(
              scrollDirection: Axis.horizontal,
              controller: _horizontalScroll,
              primary: false,
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50, top: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          L10n.getString("time_behind_leader_graph_description"),
                        ),
                        if (isAllDisplayable)
                          Text(
                            L10n.getString("time_behind_leader_graph_nota_bene"),
                            style: const TextStyle(fontSize: 12.0, color: Colors.red),
                          ),
                        ValueListenableBuilder(
                          valueListenable: _graphSettings.graphType,
                          builder: (context, value, child) {
                            var graphWidth = TimeBehindLeaderGraphPresenter.calculateGraphWidthFromCPCount(widget.presenter.courseData!.checkpoints.length);
                            return Flexible(
                              child: CustomPaint(
                                foregroundPainter: ChartGraphPainter(widget.presenter, _graphSettings.graphType.value),
                                child: Container(
                                  constraints: BoxConstraints(minWidth: graphWidth),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SettingsSideBar(_graphSettings),
        ],
      );
    } else {
      return Center(child: Text(L10n.getString("time_behind_leader_graph_no_selected_track")));
    }
  }
}
