import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';

import 'track_legs_sidebar.dart';

class TrackTabPresenter {
  final ValueNotifier<bool> legData = ValueNotifier(false);
  late final LegStatsSidebarState _legStatsSideBar;

  void dispose() => legData.dispose();

  void selectLeg(TaggedColoredPolyline polyline) => legData.value = polyline.setToggleSelected();

  void updateLegStatistics(int value) => _legStatsSideBar.update(value);

  void setSideBarView(LegStatsSidebarState legStatsSideBar) => _legStatsSideBar = legStatsSideBar;
}
