// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/discipline.dart';

import 'track_tab_presenter.dart';

class SpeedColorCaptionWidget extends StatelessWidget {
  final Discipline? discipline;
  final TrackTabPresenter trackTabPresenter;

  const SpeedColorCaptionWidget(this.discipline, this.trackTabPresenter);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: trackTabPresenter.legData,
      builder: (context, value, child) {
        if (value) {
          return Container(
            color: Colors.white,
            child: Column(
              children: [
                Image.asset(
                  (Discipline.isFootOrienteering(discipline)) ? 'assets/images/footo_speed_range.png' : 'assets/images/mtbo_speed_range.png',
                ),
              ],
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
