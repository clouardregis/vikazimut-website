import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'effort_bin.dart';

class PhysicalPerformanceProcessor {
  static const int _minGradient = -30;
  static const int _maxGradient = 20;
  static const int _gradientBinSize = 5;
  static const int gradientLength = (_maxGradient - _minGradient) ~/ _gradientBinSize + 1;

  static List<EffortBin> computeSpeedEffortDifferenceWithAverageSpeedPerGradient(
    GpsRoute track,
    List<Checkpoint> checkpoints,
  ) {
    final legs = track.model.legs;
    final List<List<GeodesicPoint>> legRoutes = [];
    for (int i = 1; i < legs.length; i++) {
      if (legs[i].punchTime >= 0) {
        legRoutes.add(legs[i].waypoints ?? []);
      }
    }
    List<List<List<GeodesicPoint>>> legAreaTracks = RouteHelper.splitLegIntoAreas(legRoutes);
    List<(double, int)> chartData = List.generate(gradientLength, (_) => (0, 0));
    const int handrailArea = 1;
    for (var leg = 0; leg < legAreaTracks.length; leg++) {
      final List<GeodesicPoint> route = legAreaTracks[leg][handrailArea];
      computeDistanceAndTimeEffortPerGradient_(route, chartData);
    }
    return computeSpeedEffortPerGradient_(chartData);
  }

  @visibleForTesting
  static void computeDistanceAndTimeEffortPerGradient_(
    List<GeodesicPoint> route,
    List<(double, int)> chartData,
  ) {
    if (route.length < 10) {
      return;
    }
    List<double> speedsEffortInKmh = RouteHelper.computeInstantaneousEffortSpeedsInKmh(route);
    for (int i = 0; i < speedsEffortInKmh.length; i++) {
      final timeInMs = route[i + 1].timestampInMillisecond - route[i].timestampInMillisecond;
      final elevationInMeter = route[i + 1].altitude - route[i].altitude;
      final distanceInMeter = speedsEffortInKmh[i] * timeInMs / 3600.0;
      final gradientInPercent = (distanceInMeter == 0) ? 1.0 : elevationInMeter * 100.0 / distanceInMeter;
      int index = fromGradientToIndex_(gradientInPercent);
      // print("Vitesse -> ${speedsEffortInKmh[i]} pour index = $index (gradient = $gradientInPercent)");
      if (index >= 0 && index < chartData.length) {
        final (d, t) = chartData[index];
        chartData[index] = (d + distanceInMeter, t + timeInMs);
      }
    }
  }

  static List<EffortBin> computeSpeedEffortPerGradient_(List<(double, int)> chartData) {
    // TODO ne pas calculer avec tous les points, uniquement ceux avec un gradient = 0;
    double averageSpeed = computeAverageSpeed_(chartData);
    List<EffortBin> speedsEffortInKmh = computeGradientSpeedDifferences_(chartData, averageSpeed);
    return speedsEffortInKmh;
  }

  @visibleForTesting
  static List<EffortBin> computeGradientSpeedDifferences_(List<(double, int)> chartData, double averageSpeed) {
    List<EffortBin> speedsEffortInKmh = [];
    for (int i = 0; i < chartData.length; i++) {
      var (d, t) = chartData[i];
      const int tenSeconds = 10 * 1000;
      if (d > 0 && t > tenSeconds) {
        final speedInKmh = d * 3600 / t;
        final effortPercent = 100.0 * (speedInKmh - averageSpeed) / averageSpeed;
        speedsEffortInKmh.add(EffortBin(fromIndexToGradientBin_(i), effortPercent));
      }
    }
    return speedsEffortInKmh;
  }

  @visibleForTesting
  static int fromIndexToGradientBin_(int i) => (i * _gradientBinSize + _minGradient);

  @visibleForTesting
  static double computeAverageSpeed_(List<(double, int)> chartData) {
    // TODO Old version
    // var totalDistance = 0.0;
    // var totalTime = 0;
    //
    // for (int i = 0; i < chartData.length; i++) {
    //   var (d, t) = chartData[i];
    //   totalDistance += d;
    //   totalTime += t;
    // }
    // final averageSpeed = totalDistance * 3600 / totalTime;
    // return averageSpeed;
    const int zeroGradient = -_minGradient ~/ _gradientBinSize;
    var (d, t) = chartData[zeroGradient];
    final averageSpeedInKmh = d * 3600 / t;
    // print("Average speed with no gradient : ${averageSpeedInKmh.toStringAsFixed(2)} km/h");
    return averageSpeedInKmh;
  }

  @visibleForTesting
  static int fromGradientToIndex_(double gradientInPercent) {
    return (gradientInPercent ~/ _gradientBinSize) - (_minGradient ~/ _gradientBinSize);
  }
}
