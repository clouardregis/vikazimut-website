// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

class SideBar extends StatefulWidget {
  const SideBar();

  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  @override
  Widget build(BuildContext context) {

    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("physical_performance_documentation_title"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(10.0),
      children: [
        Text(L10n.getString("physical_performance_documentation_text")),
        // TODO ajouter la documentation pdf
        // CustomOutlinedButton(
        //   onPressed: () => launchUrl(Uri.parse("${constants.SERVER}/public/docs/performances-orientation.pdf")),
        //   onPressed: () {
        //     showDialog(
        //       context: context,
        //       builder: (BuildContext context) => HelpWidget(
        //         I18n.getString("orienteer_profile_documentation_title"),
        //         I18n.getString("orienteer_profile_documentation_text"),
        //       ),
        //     );
        //   },
        //   label: I18n.getString("read_more"),
        //   foregroundColor: kOrangeColor,
        //   backgroundColor: kOrangeColor,
        // ),
      ],
    );
  }
}
