// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'effort_bin.dart';

class LineChartWidget extends CustomPainter {
  final List<EffortBin> data;
  final BuildContext context;

  LineChartWidget(this.context, {required this.data});

  @override
  void paint(Canvas canvas, Size size) {
    final textColor = Theme.of(context).textTheme.titleMedium!.color!;
    const maxEffortPercent = 150;
    const chartOffset = Offset(50, 50);
    final Size(width: width, height: height) = size;
    final chartWidth = width - chartOffset.dx;
    final chartHeight = height - chartOffset.dy;
    const int gradientIntervalSize = 5;
    const int gradientCount = (30 + 20) ~/ gradientIntervalSize + 1;
    final cx = size.width / 2 + chartOffset.dx;
    final cy = size.height / 2;
    final (stepX, stepY) = (chartWidth / gradientCount, chartHeight / (maxEffortPercent * 2));

    _drawReferenceFrame(canvas, cx, cy, chartWidth, chartHeight, textColor);
    _drawData(canvas, gradientIntervalSize, cx, cy, stepX, stepY, textColor);
    _drawXAxisUnits(canvas, gradientCount, gradientIntervalSize, chartOffset, chartWidth, height);
    _drawXAxisLegend(canvas, chartOffset, width, height);
    _drawYAxisLegend(canvas, chartHeight);
  }

  void _drawData(
    Canvas canvas,
    int gradientIntervalSize,
    double cx,
    double cy,
    double stepX,
    double stepY,
    Color textColor,
  ) {
    for (EffortBin value in data) {
      if (value.effortPercent != 0) {
        var x = value.gradientInPercent / gradientIntervalSize * stepX + cx;
        var y = cy - value.effortPercent * stepY;
        var position = Offset(x, y);
        _drawBar(canvas, position, stepX, stepY, cy, value.effortPercent, textColor);
      }
    }
  }

  void _drawYAxisLegend(
    Canvas canvas,
    double chartHeight,
  ) {
    canvas.save;
    canvas.rotate(-math.pi / 2);
    var yAxisCaption = TextPainter(
      text: TextSpan(
        text: L10n.getString("physical_performance_legend_effort"),
        style: TextStyle(color: kTextColorDarkTheme, fontSize: 18, fontWeight: FontWeight.bold),
      ),
      textDirection: TextDirection.ltr,
    );
    yAxisCaption.layout();
    yAxisCaption.paint(canvas, Offset(-chartHeight / 2 - yAxisCaption.width / 2, 0));
    canvas.restore;
  }

  void _drawXAxisLegend(
    Canvas canvas,
    Offset chartOffset,
    double width,
    double height,
  ) {
    var xAxisCaption = TextPainter(
      text: TextSpan(
        text: L10n.getString("physical_performance_legend_gradient"),
        style: TextStyle(color: kTextColorDarkTheme, fontSize: 18, fontWeight: FontWeight.bold),
      ),
      textDirection: TextDirection.ltr,
    );
    xAxisCaption.layout();
    xAxisCaption.paint(canvas, Offset(chartOffset.dx + width / 2 - xAxisCaption.width / 2, height - 24));
  }

  void _drawXAxisUnits(
    Canvas canvas,
    int gradientCount,
    int gradientIntervalSize,
    Offset chartOffset,
    double chartWidth,
    double height,
  ) {
    for (int i = 0; i < gradientCount; i++) {
      var xAxisCaption = TextPainter(
        text: TextSpan(
          text: "${-30 + i * gradientIntervalSize}%",
          style: TextStyle(color: kTextColorDarkTheme, fontSize: 12),
        ),
        textDirection: TextDirection.ltr,
      );
      xAxisCaption.layout();
      xAxisCaption.paint(canvas, Offset(chartOffset.dx + i * (chartWidth / gradientCount), height - 36));
    }
  }

  void _drawReferenceFrame(
    Canvas canvas,
    double cx,
    double cy,
    double chartWidth,
    double chartHeight,
    Color textColor,
  ) {
    final paint = Paint()..color = textColor;
    canvas.drawLine(Offset(cx, 0), Offset(cx, chartHeight), paint);
    canvas.drawLine(Offset(36, cy), Offset(32 + chartWidth, cy), paint);
  }

  void _drawBar(
    Canvas canvas,
    Offset position,
    double xStep,
    double yStep,
    double cy,
    double effortPercent,
    Color textColor,
  ) {
    const double padding = 2;
    final paint = Paint()..color = effortPercent >= 0 ? Colors.green.shade700 : Colors.red;
    final Rect rect = Rect.fromLTRB(position.dx - xStep / 2 + padding, position.dy, position.dx + xStep / 2 - padding, cy);
    canvas.drawRect(rect, paint);
    var xAxisCaption = TextPainter(
      text: TextSpan(
        text: effortPercent.toStringAsFixed(1),
        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: textColor),
        children: [
          TextSpan(
            text: "%",
            style: TextStyle(fontSize: 8, fontWeight: FontWeight.bold, color: textColor),
          ),
        ],
      ),
      textDirection: TextDirection.ltr,
    );
    xAxisCaption.layout();
    if (effortPercent > 0) {
      xAxisCaption.paint(canvas, Offset(position.dx - xStep / 2 + padding + 2, position.dy - 25));
    } else {
      xAxisCaption.paint(canvas, Offset(position.dx - xStep / 2 + padding +2, position.dy + 5));
    }
  }

  @override
  bool shouldRepaint(covariant LineChartWidget oldDelegate) {
    return false;
  }
}
