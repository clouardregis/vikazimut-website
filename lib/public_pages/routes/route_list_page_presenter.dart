import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:latlong2/latlong.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/color_picker.dart';

import 'gps_route.dart';

abstract class RouteListPageInterface {
  void updateTrackList();

  void updateTrackSelection() {
    /* Empty by default */
  }

  void displayToastMessage(String message, {Color? backgroundColor, Color? textColor});
}

class RouteListPagePresenter {
  final int courseId;
  final RouteListPageInterface _view;
  final ColorPicker _colorPicker = ColorPicker();
  IdentifiedUser? _user;
  CourseData? _courseData;
  final List<GpsRoute> selectedTracks = [];
  List<GpsRoute> _presetOrderTrackList = [];
  List<GpsRoute> _freeOrderTrackList = [];

  RouteListPagePresenter(this.courseId, this._view);

  IdentifiedUser? get user => _user;

  CourseData? get courseData => _courseData;

  List<GpsRoute> get presetOrderTrackList => _presetOrderTrackList;

  List<GpsRoute> get freeOrderTrackList => _freeOrderTrackList;

  List<GpsRoute> get selectedTracksWithoutSuperman => selectedTracksWithoutSuperman_(selectedTracks);

  Future<List<GpsRoute>> getTrackList() async {
    selectedTracks.clear();
    _user = await IdentifiedUser.getUser();
    var accessToken = _user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    final http.Response response;
    try {
      response = await http.get(
        Uri.parse("${constants.API_SERVER}/courses/tracks/$courseId"),
        headers: headers,
      );
    } catch (_) {
      return Future.error("Internal error");
    }
    if (response.statusCode == HttpStatus.ok) {
      var data = jsonDecode(response.body);
      List<GpsRoute> routeList = [];
      List<dynamic> tracks = data as List<dynamic>;
      for (int i = 0; i < tracks.length; i++) {
        var track = TrackModel.fromJson(tracks[i]);
        routeList.add(GpsRoute(i, track));
      }
      _setTrackList(routeList);
      return routeList;
    } else {
      return [];
    }
  }

  Future<CourseData> loadCourseData() async {
    final map = await CourseGeoreference.loadCourseMap(courseId);
    List<Checkpoint> checkpoints = await Checkpoint.loadCourseCheckpoints(courseId);
    _courseData = CourseData(map, checkpoints);
    return _courseData!;
  }

  List<List<LatLng>> getAllTracksAsGpsPositions({int legIndex = 0}) => GpsRoute.getAllTracksAsGpsPoints(selectedTracks, legIndex: legIndex);

  List<List<PunchPosition>> getAllPunchesAsPositions({int legIndex = 0}) => GpsRoute.getAllPunchesAsPositions(selectedTracks, legIndex: legIndex);

  List<List<int>> getAllSpeedsAsColor(Discipline? discipline, {legIndex = 0}) => GpsRoute.getAllSpeedsAsColor(discipline, selectedTracks, legIndex: legIndex);

  void updateTrackList() => _view.updateTrackList();

  Future<void> onClick(GpsRoute track) async {
    if (!selectedTracks.contains(track)) {
      await _selectTrack(track);
    } else {
      _unselectTrack(track);
    }
    _view.updateTrackSelection();
  }

  void unselectTracks() {
    for (int i = selectedTracks.length - 1; i >= 0; i--) {
      _unselectTrack(selectedTracks[i]);
    }
    _view.updateTrackSelection();
  }

  void _unselectTrack(GpsRoute track) {
    selectedTracks.remove(track);
    track.deleteRouteData();
    _colorPicker.restoreColor(track.hexColor);
    track.hexColor = "00000000";
  }

  Future<void> _selectTrack(GpsRoute track) async {
    if (courseData!.hiddenLevel > 1) {
      _view.displayToastMessage(L10n.getString("track_message_hidden_course"), backgroundColor: Colors.red, textColor: Colors.white);
      return;
    }
    if (selectedTracks.length >= constants.MAX_NUMBER_OF_SELECTED_TRACKS) {
      _view.displayToastMessage(sprintf(L10n.getString("track_sidebar_selection_note"), [constants.MAX_NUMBER_OF_SELECTED_TRACKS]));
      return;
    }
    if (track.courseFormat.isFreeOrder || (track.courseFormat.isPresetOrder && track.displayedFormat == 1)) {
      track.displayedFormat = track.courseFormat.index;
    }
    if (track.id < 0) {
      await track.loadBestRouteData(_user, courseId, _courseData!.checkpoints);
      if (track.route.isEmpty) {
        _view.displayToastMessage(L10n.getString("track_superman_error"));
        return;
      }
    } else {
      await track.loadRouteData(_user, _courseData!.checkpoints);
    }
    selectedTracks.add(track);
    track.hexColor = _colorPicker.pickColor();
  }

  @visibleForTesting
  static int comparePresetOrderTracks_(GpsRoute track1, GpsRoute track2) {
    if (track1.model.score == track2.model.score) {
      return track1.model.totalTimeInMilliseconds.compareTo(track2.model.totalTimeInMilliseconds);
    } else {
      return track2.model.score.compareTo(track1.model.score);
    }
  }

  @visibleForTesting
  static int compareFreeOrderTracks_(GpsRoute track1, GpsRoute track2) {
    if (track1.model.score == track2.model.score) {
      if (track1.model.runCount == track2.model.runCount) {
        return track1.model.totalTimeInMilliseconds.compareTo(track2.model.totalTimeInMilliseconds);
      } else {
        return track1.model.runCount.compareTo(track2.model.runCount);
      }
    } else {
      return track2.model.score.compareTo(track1.model.score);
    }
  }

  void _setTrackList(List<GpsRoute> routeList) {
    _presetOrderTrackList = routeList.where((GpsRoute track) => track.model.courseFormat.isPresetOrder).toList();
    _presetOrderTrackList.sort(comparePresetOrderTracks_);
    if (_presetOrderTrackList.length > 1) {
      _presetOrderTrackList.insert(
        0,
        GpsRoute(
          -1000,
          TrackModel(-1, "Superman", false, 0, null, [], ValidationOrder.PRESET_ORDER, isCompleted: true),
        ),
      );
    }
    _freeOrderTrackList = routeList.where((GpsRoute track) => track.model.courseFormat.isFreeOrder).toList();
    _freeOrderTrackList.sort(compareFreeOrderTracks_);
  }

  @visibleForTesting
  static List<GpsRoute> selectedTracksWithoutSuperman_(List<GpsRoute> tracks) {
    List<GpsRoute> selectedTracks = [];
    for (int i = 0; i < tracks.length; i++) {
      if (tracks[i].id >= 0) {
        selectedTracks.add(tracks[i]);
      }
    }
    return selectedTracks;
  }
}

class CourseData {
  final CourseGeoreference courseGeoreference;
  final List<Checkpoint> checkpoints;

  int get hiddenLevel => courseGeoreference.hiddenLevel;

  CourseData(this.courseGeoreference, this.checkpoints);
}
