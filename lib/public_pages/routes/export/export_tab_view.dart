// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/html_utils.dart';

class ExportTabView extends StatelessWidget {
  final RouteListPagePresenter presenter;

  const ExportTabView({required this.presenter});

  @override
  Widget build(BuildContext context) {
    List<GpsRoute> selectedTracks = presenter.selectedTracksWithoutSuperman;
    return Container(
      alignment: Alignment.topCenter,
      child: ContainerDecoration(
        constraints: const BoxConstraints(maxWidth: 500),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                L10n.getString("track_export_gpx_title"),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.displaySmall!.copyWith(color: kOrangeColor),
              ),
            ),
            Flexible(
              child: SizedBox(
                width: 380,
                child: ListView.builder(
                  itemCount: selectedTracks.length,
                  shrinkWrap: true,
                  primary: false,
                  itemBuilder: (context, i) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        onPressed: () {
                          downloadStringInFile(selectedTracks[i].filename, selectedTracks[i].getGpxContents());
                        },
                        style: ElevatedButton.styleFrom(backgroundColor: kOrangeColor, foregroundColor: Colors.white),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: Text(
                                selectedTracks[i].filename,
                                maxLines: 1,
                                overflow: TextOverflow.clip,
                              ),
                            ),
                            const SizedBox(width: 5),
                            const Icon(Icons.file_download),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
