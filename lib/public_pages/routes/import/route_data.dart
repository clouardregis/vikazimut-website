import 'package:flutter/foundation.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'gpx_exception.dart';

class RouteData {
  static const _GPS_COORDINATE_TIMED_FORMAT = "%.6f,%.6f,%d,%.1f;";
  static const double _MAXIMUM_DISTANCE_TO_CP_IN_METERS = 35;

  final int courseId;
  final String nickname;
  final int format;
  final int totalTimeInMilliseconds;
  final String gpsTrace;
  final List<int> punchTimes;

  const RouteData({
    required this.nickname,
    required this.format,
    required this.totalTimeInMilliseconds,
    required this.gpsTrace,
    required this.courseId,
    required this.punchTimes,
  });

  Map<String, dynamic> toJson() {
    List<Map> checkpoints = [];
    for (int i = 0; i < punchTimes.length; i++) {
      Map<String, int> jo = {"controlPoint": i, "punchTime": punchTimes[i]};
      checkpoints.add(jo);
    }
    return {
      "orienteer": nickname,
      "format": format.toString(),
      "totalTime": totalTimeInMilliseconds,
      "courseId": courseId,
      "controlPoints": checkpoints,
      "trace": gpsTrace,
      "imported": true,
    };
  }

  static RouteData checkDataAndCreateRoute({
    required int courseId,
    required String? nickname,
    required int format,
    required List<GeodesicPoint> waypoints,
    required CourseData courseData,
  }) {
    if (waypoints.isEmpty) {
      throw GpxException(L10n.getString("import_gpx_no_waypoints_error"));
    }
    if (!checkTrackIsInMapBound_(waypoints, courseData.courseGeoreference.bounds)) {
      throw GpxException(L10n.getString("import_gpx_out_of_bound_error"));
    }
    List<int> punchTimes = extractPunchTimes_(waypoints, courseData.checkpoints, format);
    punchTimes = refinePunchTimesPosition_(waypoints, courseData.checkpoints, punchTimes);
    waypoints = removeTrackPointsBeforeStartAndAfterEnd_(waypoints, punchTimes);
    waypoints = changeAbsoluteTimeToRelativeTime_(waypoints, punchTimes);

    return RouteData(
      courseId: courseId,
      nickname: nickname!.trim(),
      format: format,
      totalTimeInMilliseconds: waypoints.last.timestampInMillisecond - waypoints.first.timestampInMillisecond,
      gpsTrace: waypointsToString_(waypoints),
      punchTimes: punchTimes,
    );
  }

  @visibleForTesting
  static String waypointsToString_(List<GeodesicPoint> geodesicPoints) {
    const int baseTime = 0;
    String gpxString = "";
    for (GeodesicPoint geodesicPoint in geodesicPoints) {
      double latitude = geodesicPoint.latitude;
      double longitude = geodesicPoint.longitude;
      double altitude = geodesicPoint.altitude;
      int relativePunchTime = geodesicPoint.timestampInMillisecond - baseTime;
      gpxString += sprintf(_GPS_COORDINATE_TIMED_FORMAT, [latitude, longitude, relativePunchTime, altitude]);
    }
    return gpxString;
  }

  @visibleForTesting
  static List<GeodesicPoint> removeTrackPointsBeforeStartAndAfterEnd_(List<GeodesicPoint> waypoints, List<int> punchTimes) {
    var startIndex = 0;
    for (int i = 0; i < waypoints.length; i++) {
      if (waypoints[i].timestampInMillisecond >= punchTimes[0]) {
        startIndex = i;
        break;
      }
    }
    var endIndex = waypoints.length - 1;
    for (int i = waypoints.length - 1; i >= 0; i--) {
      if (waypoints[i].timestampInMillisecond <= punchTimes.last) {
        endIndex = i;
        break;
      }
    }
    return waypoints.sublist(startIndex, endIndex + 1);
  }

  @visibleForTesting
  static List<GeodesicPoint> changeAbsoluteTimeToRelativeTime_(List<GeodesicPoint> waypoints, List<int> punchTimes) {
    var startTime = waypoints[0].timestampInMillisecond;
    for (int i = 0; i < punchTimes.length; i++) {
      punchTimes[i] -= startTime;
    }
    for (var waypoint in waypoints) {
      waypoint.timestampInMillisecond -= startTime;
    }
    return waypoints;
  }

  @visibleForTesting
  static bool checkTrackIsInMapBound_(List<GeodesicPoint> waypoints, LatLonBox bounds) {
    int numberOfPointsOutOfBounds = 0;
    for (GeodesicPoint waypoint in waypoints) {
      if (waypoint.latitude > bounds.north || //
          waypoint.latitude < bounds.south ||
          waypoint.longitude > bounds.east ||
          waypoint.longitude < bounds.west) {
        numberOfPointsOutOfBounds++;
      }
    }
    return waypoints.length.toDouble() - numberOfPointsOutOfBounds.toDouble() > 0;
  }

  @visibleForTesting
  static List<int> extractPunchTimes_(final List<GeodesicPoint> waypoints, final List<Checkpoint> checkpoints, final int format) {
    void extractPunchTimesWithoutStartAndEnd(List<GeodesicPoint> waypoints, List<_Candidate> punchTimes, List<Checkpoint> checkpoints, int format) {
      // 1. General case without start and end
      var currentCheckpointToPunch = 1;
      for (var waypoint in waypoints) {
        for (int i = 1; i < punchTimes.length - 1; i++) {
          if (punchTimes[i].close) {
            continue;
          }
          var distance = GeodesicPoint.distanceBetweenInMeter(waypoint.latitude, waypoint.longitude, checkpoints[i].latitude, checkpoints[i].longitude);
          if (distance <= punchTimes[i].distance) {
            punchTimes[i].distance = distance;
            punchTimes[i].time = waypoint.timestampInMillisecond;
            if (format.isPresetOrder && i < currentCheckpointToPunch) {
              // In case of preset order, unpunch all next control points in case one has been punched by hazard
              for (int j = i + 1; j < punchTimes.length; j++) {
                punchTimes[j].time = -1;
                punchTimes[j].distance = _MAXIMUM_DISTANCE_TO_CP_IN_METERS;
                punchTimes[j].close = false;
              }
            }
          }
          // Next control point
          if (currentCheckpointToPunch != i && distance <= _MAXIMUM_DISTANCE_TO_CP_IN_METERS) {
            if (punchTimes[currentCheckpointToPunch].time > 0) {
              punchTimes[currentCheckpointToPunch].close = true;
            }
            currentCheckpointToPunch = i;
            // Checkpoints between current and i are supposed to be unpunched in preset order
          }
        }
      }
    }

    void extractStartPunchTime(List<_Candidate> punchTimes, List<GeodesicPoint> waypoints, List<Checkpoint> checkpoints) {
      const int thousandHours = 1000 * 60 * 60 * 1000;
      var firstPunchTime = thousandHours;
      for (int i = 1; i < punchTimes.length - 1; i++) {
        if (punchTimes[i].time > 0 && punchTimes[i].time < firstPunchTime) {
          firstPunchTime = punchTimes[i].time;
        }
      }
      if (firstPunchTime == thousandHours) {
        firstPunchTime = 0;
      }
      for (final waypoint in waypoints) {
        if (waypoint.timestampInMillisecond >= firstPunchTime) {
          break;
        }
        var distance = GeodesicPoint.distanceBetweenInMeter(waypoint.latitude, waypoint.longitude, checkpoints[0].latitude, checkpoints[0].longitude);
        if (distance <= punchTimes[0].distance) {
          punchTimes[0].distance = distance;
          punchTimes[0].time = waypoint.timestampInMillisecond;
          punchTimes[0].close = true;
        }
      }
      // At worst, use the first waypoint
      if (!punchTimes[0].close) {
        punchTimes[0].time = waypoints[0].timestampInMillisecond;
      }
    }

    void extractEndPunchTime(List<_Candidate> punchTimes, List<GeodesicPoint> waypoints, List<Checkpoint> checkpoints) {
      var lastPunchTime = 0;
      for (int i = 1; i < punchTimes.length - 1; i++) {
        if (punchTimes[i].time > 0 && punchTimes[i].time > lastPunchTime) {
          lastPunchTime = punchTimes[i].time;
        }
      }
      for (var waypoint in waypoints.reversed) {
        if (waypoint.timestampInMillisecond <= lastPunchTime) {
          break;
        }
        var distance = GeodesicPoint.distanceBetweenInMeter(waypoint.latitude, waypoint.longitude, checkpoints.last.latitude, checkpoints.last.longitude);
        if (distance <= punchTimes.last.distance) {
          punchTimes.last.distance = distance;
          punchTimes.last.time = waypoint.timestampInMillisecond;
          punchTimes.last.close = true;
        }
      }
      // At worst use the last point
      if (!punchTimes.last.close) {
        punchTimes.last.time = waypoints.last.timestampInMillisecond;
      }
    }

    final List<_Candidate> punchTimes = List.generate(checkpoints.length, (index) => _Candidate(), growable: false);
    extractPunchTimesWithoutStartAndEnd(waypoints, punchTimes, checkpoints, format);
    extractStartPunchTime(punchTimes, waypoints, checkpoints);
    extractEndPunchTime(punchTimes, waypoints, checkpoints);
    return punchTimes.map((punchTime) => punchTime.time).toList(growable: false);
  }

  @visibleForTesting
  static List<int> refinePunchTimesPosition_(List<GeodesicPoint> waypoints, List<Checkpoint> checkpoints, List<int> punchTimes) {
    const int maxTimeGapForChangeInMs = 5 * 1000;
    for (int i = 1; i < waypoints.length; i++) {
      for (int j = 1; j < punchTimes.length - 1; j++) {
        final punchTime = punchTimes[j];
        if (waypoints[i - 1].timestampInMillisecond < punchTime && //
            waypoints[i].timestampInMillisecond >= punchTime) {
          final checkpointLatitude = checkpoints[j].latitude;
          final checkpointLongitude = checkpoints[j].longitude;
          var distance = GeodesicPoint.distanceBetweenInMeter(
            waypoints[i].latitude,
            waypoints[i].longitude,
            checkpointLatitude,
            checkpointLongitude,
          );
          var visitTime = punchTime;
          for (int k = i + 1; k < waypoints.length; k++) {
            final waypoint = waypoints[k];
            final distance1 = GeodesicPoint.distanceBetweenInMeter(
              waypoint.latitude,
              waypoint.longitude,
              checkpointLatitude,
              checkpointLongitude,
            );
            if (distance1 < distance && //
                waypoint.timestampInMillisecond - punchTime <= maxTimeGapForChangeInMs) {
              distance = distance1;
              visitTime = waypoint.timestampInMillisecond;
            } else {
              break;
            }
          }
          punchTimes[j] = visitTime;
          break;
        }
      }
    }
    return punchTimes;
  }
}

class _Candidate {
  int time = -1;
  double distance = RouteData._MAXIMUM_DISTANCE_TO_CP_IN_METERS;
  bool close = false;
}
