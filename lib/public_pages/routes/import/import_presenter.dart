import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'import_model.dart';
import 'import_view.dart';
import 'route_data.dart';
import 'server_gateway.dart';

class ImportPresenter {
  final RouteListPagePresenter presenter;
  final ImportModel model = ImportModel();
  final ValueNotifier<int> inputController = ValueNotifier<int>(0);
  final ImportTabViewState view;

  ImportPresenter({
    required this.presenter,
    required this.view,
  });

  Future<void> sendToServer() async {
    try {
      RouteData routeData = RouteData.checkDataAndCreateRoute(
        courseId: presenter.courseId,
        nickname: model.nickname,
        format: model.format,
        waypoints: model.waypoints!,
        courseData: presenter.courseData!,
      );
      String? error = await ServerGateway.sendResultToServer(routeData);
      if (error == null) {
        model.reset();
        presenter.updateTrackList();
        view.displaySuccess();
      } else {
        view.displayError(error);
      }
    } catch (error) {
      view.displayError(error.toString());
    }
  }

  void displayError(Object errorMessage) => view.displayError(errorMessage.toString());

  List<List<double>> getCourseGeoLocation() => presenter.courseData!.courseGeoreference.latLngBounds();
}
