import 'dart:typed_data';

import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import 'file_format/fit_file_format.dart';
import 'file_format/gpx_file_format.dart';

abstract class TrackFileFormat {
  static TrackFileFormat? _decoder;

  static TrackFileFormat get decoder => _decoder!;

  factory TrackFileFormat.fromExtension(String fileExtension) {
    fileExtension = fileExtension.toLowerCase();
    if (fileExtension == 'gpx') {
      _decoder = GpxFileFormat();
    } else if (fileExtension == 'fit') {
      _decoder = FitFileFormat();
    }
    return _decoder!;
  }

  String? checkContent(Uint8List content);

  List<GeodesicPoint> extractWaypoints(Uint8List content);

  static bool isValidExtension(String? extension) {
    if (extension == null) {
      return false;
    }
    return ["fit", "gpx"].contains(extension.toLowerCase());
  }
}
