import 'dart:typed_data';

import 'package:vikazimut_website/fit_tool/fit_file.dart';
import 'package:vikazimut_website/fit_tool/fit_file_header.dart';
import 'package:vikazimut_website/fit_tool/profile/messages/record_message.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import '../track_file_format.dart';

class FitFileFormat implements TrackFileFormat {
  @override
  String? checkContent(Uint8List bytes) {
    String errorMessage = "import_gpx_server_bad_fit_file";
    try {
      var byteOffset = 0;
      final headerSize = bytes[0];
      final headerBytes = Uint8List.sublistView(bytes, byteOffset, byteOffset + headerSize);
      FitFileHeader.fromBytes(headerBytes);
      return null;
    } catch (_) {
      return errorMessage;
    }
  }

  @override
  List<GeodesicPoint> extractWaypoints(Uint8List bytes) {
    final fitFile = FitFile.fromBytes(bytes);
    List<GeodesicPoint> waypoints = [];
    var baseTime = -1;
    for (var record in fitFile.records) {
      final message = record.message;
      if (message is RecordMessage) {
        double? latitude = message.positionLat;
        double? longitude = message.positionLong;
        int? timestamp = message.timestamp;
        double? altitude = message.altitude;
        if (latitude != null && longitude != null && timestamp != null) {
          if (baseTime < 0) {
            baseTime = timestamp;
            timestamp = 0;
          } else {
            timestamp = timestamp - baseTime;
          }
          waypoints.add(GeodesicPoint(latitude, longitude, timestamp, altitude ?? 0));
        }
      }
    }
    return waypoints;
  }
}
