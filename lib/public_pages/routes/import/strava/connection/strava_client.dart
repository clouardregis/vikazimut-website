import 'session_manager.dart';
import '../data/repository_authentication.dart';

class StravaClient {
  late final SessionManager sessionManager;
  late final RepositoryAuthentication authentication;
  final String clientSecret;
  final String clientId;

  StravaClient({
    required this.clientSecret,
    required this.clientId,
    required String applicationName,
  }) {
    sessionManager = SessionManager();
    authentication = RepositoryAuthentication();
    sessionManager.initialize(
      secret: clientSecret,
      clientId: clientId,
      applicationName: applicationName,
    );
  }
}
