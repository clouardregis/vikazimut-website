import 'dart:async';

import 'authentication_response.dart';
import 'authentication_scopes.dart';
import 'local_storage.dart';

class SessionManager {
  late String secret;
  late String clientId;
  late String applicationName;

  TokenResponse? _currentToken;

  void initialize({
    required String secret,
    required String clientId,
    required String applicationName,
  }) {
    this.secret = secret;
    this.clientId = clientId;
    this.applicationName = applicationName;
  }

  Future<TokenResponse?> getToken() {
    var completer = Completer<TokenResponse?>();
    if (_currentToken != null) {
      completer.complete(_currentToken);
    } else {
      LocalStorageManager.getToken(applicationName: applicationName).then((storedValue) {
        if (storedValue != null) {
          _currentToken = storedValue;
        }
        completer.complete(_currentToken);
      });
    }
    return completer.future;
  }

  Future<void> setToken({
    required TokenResponse token,
    required List<AuthenticationScope> scopes,
  }) {
    _currentToken = token;
    return LocalStorageManager.saveToken(token, scopes, applicationName: applicationName).then((value) => _currentToken = token);
  }

  bool isTokenExpired(TokenResponse token) {
    DateTime expiresAt = DateTime.fromMillisecondsSinceEpoch(token.expiresAt * 1000);
    return DateTime.now().isAfter(expiresAt);
  }

  Future<void> logout() {
    _currentToken = null;
    return LocalStorageManager.deleteToken(applicationName: applicationName);
  }
}
