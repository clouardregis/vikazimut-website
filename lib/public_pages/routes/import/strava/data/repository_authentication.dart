import 'dart:async';

import 'package:flutter_web_auth_2/flutter_web_auth_2.dart';

import '../connection/authentication_response.dart';
import '../connection/authentication_scopes.dart';
import '../connection/model_fault.dart';
import '../connection/strava_client.dart';
import 'client.dart';

class RepositoryAuthentication {
  Future<TokenResponse> authenticate({
    required List<AuthenticationScope> scopes,
    required String redirectUrl,
    required String callbackUrlScheme,
    bool forceShowingApproval = false,
    required StravaClient client,
  }) async {
    var completer = Completer<TokenResponse>();
    var token = await client.sessionManager.getToken();
    if (token == null) {
      completer.complete(_completeAuthentication(
        scopes: scopes,
        forceShowingApproval: forceShowingApproval,
        redirectUrl: redirectUrl,
        callbackUrlScheme: callbackUrlScheme,
        client: client,
      ));
    } else {
      List<AuthenticationScope> oldScopes = AuthenticationScopeHelper.generateScopes(token.scopes ?? "");
      var isScopesAreSame = _compareScopes(oldScopes, scopes);
      if (isScopesAreSame) {
        if (client.sessionManager.isTokenExpired(token)) {
          _refreshAccessToken(client, client.sessionManager.clientId, client.sessionManager.secret, token.refreshToken).then((refreshResult) {
            client.sessionManager.setToken(scopes: scopes, token: TokenResponse(tokenType: token.tokenType, expiresAt: refreshResult.expiresAt, expiresIn: refreshResult.expiresIn, refreshToken: refreshResult.refreshToken, accessToken: refreshResult.accessToken)).then((value) => completer.complete(refreshResult));
          }).onError((error, stackTrace) {
            completer.completeError(error!, stackTrace);
          });
        } else {
          completer.complete(token);
        }
      } else {
        // Scopes have changed. we need a new token.
        completer.complete(_completeAuthentication(
          scopes: scopes,
          forceShowingApproval: forceShowingApproval,
          redirectUrl: redirectUrl,
          callbackUrlScheme: callbackUrlScheme,
          client: client,
        ));
      }
    }
    return completer.future;
  }

  Future<TokenResponse> _completeAuthentication({
    required List<AuthenticationScope> scopes,
    required bool forceShowingApproval,
    required String redirectUrl,
    required String callbackUrlScheme,
    required StravaClient client,
  }) {
    return _getStravaCode(
      redirectUrl: redirectUrl,
      scopes: scopes,
      forceShowingApproval: forceShowingApproval,
      callbackUrlScheme: callbackUrlScheme,
      client: client,
    ).then((code) {
      return _requestNewAccessToken(client, client.sessionManager.clientId, client.sessionManager.secret, code).then((token) async {
        await client.sessionManager.setToken(token: token, scopes: scopes);
        return token;
      });
    });
  }

  /// RedirectUrl works best when it is a custom scheme. For example: strava://auth
  ///
  /// If your redirectUrl is, for example, strava://auth then your callbackUrlScheme should be strava
  Future<String> _getStravaCode({
    required String redirectUrl,
    required List<AuthenticationScope> scopes,
    required bool forceShowingApproval,
    required String callbackUrlScheme,
    required StravaClient client,
  }) async {
    final Completer<String> completer = Completer<String>();
    final params = '?client_id=${client.sessionManager.clientId}&redirect_uri=$redirectUrl&response_type=code&approval_prompt=${forceShowingApproval ? "force" : "auto"}&scope=${AuthenticationScopeHelper.buildScopeString(scopes)}';
    var host = "https://www.strava.com/";
    const authorizationEndpoint = "oauth/authorize";
    final reqAuth = host + authorizationEndpoint + params;
    try {
      final result = await FlutterWebAuth2.authenticate(
        url: reqAuth,
        callbackUrlScheme: callbackUrlScheme,
      );
      final parsed = Uri.parse(result);

      final error = parsed.queryParameters['error'];
      final code = parsed.queryParameters['code'];
      if (error != null) {
        completer.completeError(Fault(errors: [], message: error));
      } else {
        completer.complete(code);
      }
    } catch (e) {
      completer.completeError(Fault(errors: [], message: e.toString()));
    }
    return completer.future;
  }

  Future<TokenResponse> _requestNewAccessToken(
    StravaClient client,
    String clientID,
    String secret,
    String code,
  ) {
    return ApiClient.postRequest(
        client: client,
        endPoint: "/v3/oauth/token",
        queryParameters: {
          "client_id": clientID,
          "client_secret": secret,
          "code": code,
          "grant_type": "authorization_code",
        },
        dataConstructor: (data) {
          var token = TokenResponse.fromJson(data);
          return token;
        });
  }

  Future<TokenResponse> _refreshAccessToken(StravaClient client, String clientID, String secret, String refreshToken) {
    return ApiClient.postRequest<TokenResponse>(
        client: client,
        endPoint: "/v3/oauth/token",
        queryParameters: {
          "client_id": clientID,
          "client_secret": secret,
          "grant_type": "refresh_token",
          "refresh_token": refreshToken,
        },
        dataConstructor: (data) => TokenResponse.fromJson(data));
  }

  bool _compareScopes(List<AuthenticationScope> left, List<AuthenticationScope> right) {
    if (left.length != right.length) {
      return false;
    }
    return left.every((element) => right.contains(element));
  }

  Future<void> deAuthorize({required StravaClient client}) async {
    var token = await client.sessionManager.getToken();

    var params = {"access_token": token?.accessToken ?? ""};
    return ApiClient.postRequest(
      client: client,
      baseUrl: "https://www.strava.com/",
      endPoint: "/oauth/deauthorize",
      queryParameters: params,
      dataConstructor: (data) => null,
    ).whenComplete(() => client.sessionManager.logout());
  }
}
