import '../connection/strava_client.dart';
import 'client.dart';
import 'model_run_activity.dart';

class RepositoryStream {
  Future<RunActivity> getActivityStreams(
    int activityId,
    String keys,
    StravaClient client,
  ) {
    return ApiClient.getRequest(
      endPoint: "/v3/activities/$activityId/streams",
      queryParameters: {"keys": keys, "key_by_type": true},
      dataConstructor: (data) => RunActivity.fromJson(Map<String, dynamic>.from(data)),
      client: client,
    );
  }
}
