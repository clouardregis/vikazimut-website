import 'dart:convert';

class DetailedAthlete {
  int id;
  String? username;
  String firstname;
  String lastname;

  DetailedAthlete({
    required this.id,
    required this.username,
    required this.firstname,
    required this.lastname,
  });

  factory DetailedAthlete.fromRawJson(String str) => DetailedAthlete.fromJson(json.decode(str));

  factory DetailedAthlete.fromJson(Map<String, dynamic> json) {
    return DetailedAthlete(
      id: json["id"],
      username: json["username"] ?? "",
      firstname: json["firstname"] ?? "",
      lastname: json["lastname"] ?? "",
    );
  }
}
