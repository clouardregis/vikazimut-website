class SummaryActivity {
  String? name;
  int? id;
  String? startDate;
  String? startDateLocal;
  List<double>? startLatlng;
  List<double>? endLatlng;

  SummaryActivity({
    this.name,
    this.id,
    this.startDate,
    this.startDateLocal,
    this.startLatlng,
    this.endLatlng,
  });

  SummaryActivity.fromJson(dynamic json) {
    name = json['name'];
    id = json['id'];
    startDate = json['start_date'];
    startDateLocal = json['start_date_local'];
    startLatlng = (json['start_latlng'] != null) ? json['start_latlng'].cast<double>() : [];
    endLatlng = (json['end_latlng'] != null) ? json['end_latlng'].cast<double>() : [];
  }
}
