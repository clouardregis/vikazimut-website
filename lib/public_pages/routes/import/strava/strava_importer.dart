import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/import/import_presenter.dart';
import 'package:vikazimut_website/public_pages/routes/import/strava/strava_dialog.dart';

import '../abstract_importer.dart';
import 'connection/authentication.dart';
import 'connection/authentication_scopes.dart';
import 'connection/strava_client.dart';
import 'data/model_run_activity.dart';
import 'data/model_summary_activity.dart';
import 'data/repository_activity.dart';
import 'data/repository_stream.dart';
import 'secret.dart';

class StravaImporter extends AbstractImporter {
  late final StravaClient _stravaClient;

  StravaImporter() {
    _stravaClient = StravaClient(
      clientSecret: clientPassword,
      clientId: clientId,
      applicationName: "Vikazimut",
    );
  }

  @override
  Future<List<GeodesicPoint>?> getRouteWaypoints(BuildContext context, ImportPresenter presenter) async {
    try {
      await Authentication(_stravaClient).authorize(
        client: _stravaClient,
        [AuthenticationScope.activity_read],
        "http://localhost:40357/auth.html",
      );
      Future<List<SummaryActivity>> activityStream = RepositoryActivity().listLoggedInAthleteActivities(
        page: 1,
        perPage: 30,
        client: _stravaClient,
      );
      Future<List<SummaryActivity>> filteredActivities = _filterActivitiesBasedOnPosition(
        activityStream,
        presenter.getCourseGeoLocation(),
      );
      if (!context.mounted) {
        _deAuthentication();
        return null;
      }
      SummaryActivity? activity = await StravaDialog(filteredActivities).execute(context);
      if (activity == null) {
        _deAuthentication();
        return null;
      }
      RunActivity runActivity = await RepositoryStream().getActivityStreams(
        activity.id!,
        "time,latlng,altitude,",
        _stravaClient,
      );
      List<GeodesicPoint> waypoints = extractGeodesicPointsFromRunActivity_(runActivity);
      _deAuthentication();
      return waypoints;
    } catch (_) {
      _deAuthentication();
      return null;
    }
  }

  Future<void> _deAuthentication() async {
    try {
      await Authentication(_stravaClient).deAuthorize();
    } catch (_) {}
  }

  @visibleForTesting
  List<GeodesicPoint> extractGeodesicPointsFromRunActivity_(RunActivity runActivity) {
    var positions = runActivity.positions!;
    var times = runActivity.times!;
    var altitudes = runActivity.altitudes!;
    List<GeodesicPoint> waypoints = [];
    for (int i = 0; i < positions.length; i++) {
      waypoints.add(GeodesicPoint(positions[i][0], positions[i][1], times[i] * 1000, altitudes[i]));
    }
    return waypoints;
  }

  static Future<List<SummaryActivity>> _filterActivitiesBasedOnPosition(
    Future<List<SummaryActivity>> activityStream,
    List<List<double>> coursePosition,
  ) async {
    final List<SummaryActivity> allActivities = await activityStream;
    final List<SummaryActivity> filteredActivities = [];
    final Area mapArea = Area(coursePosition[0], coursePosition[2]);
    for (final activity in allActivities) {
      final Area activityArea = Area(activity.startLatlng!, activity.endLatlng!);
      if (activityArea.intersects(mapArea) || mapArea.intersects(activityArea)) {
        filteredActivities.add(activity);
      }
    }
    return Future.value(filteredActivities);
  }
}

class Area {
  final List<double> northWest;
  final List<double> southEast;

  Area(this.northWest, this.southEast);

  bool intersects(Area area) {
    return area.northWest[1] < southEast[1] && //
        area.southEast[1] > northWest[1] &&
        area.northWest[0] > southEast[0] &&
        area.southEast[0] < northWest[0];
  }
}
