import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/utils/cryptography.dart';

import 'route_data.dart';

class ServerGateway {
  static Future<String?> sendResultToServer(RouteData route) async {
    String jsonMessage = jsonEncode(route);
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    final response = await http.post(
      Uri.parse(constants.SERVER_POST_GPX_URL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encryptedMessage,
    );
    if (response.statusCode == HttpStatus.ok) {
      return null;
    } else if (response.statusCode == HttpStatus.badRequest) {
      return L10n.getString("import_gpx_server_bad_request");
    } else {
      return L10n.getString("import_gpx_connection_error");
    }
  }
}
