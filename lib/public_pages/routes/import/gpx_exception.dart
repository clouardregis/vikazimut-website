class GpxException implements Exception {
  final String message;

  const GpxException(this.message);

  @override
  String toString() => message;
}
