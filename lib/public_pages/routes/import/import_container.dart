// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'abstract_importer.dart';
import 'file/track_file.dart';
import 'import_presenter.dart';

class ImportContainer extends StatelessWidget {
  final ImportPresenter presenter;

  const ImportContainer({
    required this.presenter,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.only(top: 5),
      child: SingleChildScrollView(
        child: ContainerDecoration(
          constraints: const BoxConstraints(maxWidth: 500),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  L10n.getString("track_import_title"),
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.displaySmall!.copyWith(color: kOrangeColor),
                ),
              ),
              const SizedBox(height: 20),
              _NicknameFormField(presenter),
              const SizedBox(height: 20),
              _FormatFormField(presenter),
              const SizedBox(height: 20),
              _TrackFromField(presenter),
              const SizedBox(height: 20),
              _SubmitButton(presenter),
            ],
          ),
        ),
      ),
    );
  }
}

class _NicknameFormField extends StatefulWidget {
  final ImportPresenter _presenter;

  const _NicknameFormField(this._presenter);

  @override
  State<_NicknameFormField> createState() => _NicknameFormFieldState();
}

class _NicknameFormFieldState extends State<_NicknameFormField> {
  bool _filled = false;
  late final TextEditingController _textController;

  @override
  void initState() {
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: true,
      controller: _textController,
      inputFormatters: [
        FilteringTextInputFormatter.allow(userIdentifierRegExp),
        LengthLimitingTextInputFormatter(userIdentifierLength),
      ],
      decoration: InputDecoration(
        icon: Icon(Icons.person, color: _filled ? kOrangeColor : kDisabledColor),
        labelText: L10n.getString("import_gpx_nickname_helper_text"),
        enabledBorder: (_filled) ? const HighlightedBorder() : null,
      ),
      keyboardType: TextInputType.name,
      onChanged: (String value) {
        if (value.isNotEmpty) {
          widget._presenter.model.nickname = value;
          widget._presenter.inputController.value++;
          setState(() {
            _filled = true;
          });
        } else {
          setState(() {
            _filled = false;
          });
        }
      },
      textInputAction: TextInputAction.next,
    );
  }
}

class _FormatFormField extends StatefulWidget {
  final ImportPresenter _presenter;

  const _FormatFormField(this._presenter);

  @override
  State<_FormatFormField> createState() => _FormatFormFieldState();
}

class _FormatFormFieldState extends State<_FormatFormField> {
  @override
  Widget build(BuildContext context) {
    return FormField<String>(
      builder: (FormFieldState<String> stateForm) {
        return InputDecorator(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8),
            icon: Icon(Icons.show_chart, color: hasValue() ? kOrangeColor : kDisabledColor),
            labelText: L10n.getString("import_gpx_route_format_helper_text"),
            floatingLabelStyle: TextStyle(color: hasValue() ? kOrangeColor : kDisabledColor),
            enabledBorder: hasValue() ? const HighlightedBorder() : null,
          ),
          child: Flex(
            mainAxisSize: MainAxisSize.min,
            direction: (ScreenHelper.isMobile(context)) ? Axis.vertical : Axis.horizontal,
            children: [
              Flexible(
                flex: 1,
                child: RadioListTile<int>(
                  activeColor: kOrangeColor,
                  title: Text(
                    L10n.getString("constant_route_preset"),
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  value: 0,
                  groupValue: widget._presenter.model.format,
                  onChanged: (int? value) {
                    setState(() {
                      widget._presenter.model.format = value!;
                      widget._presenter.inputController.value++;
                    });
                  },
                ),
              ),
              Flexible(
                flex: 1,
                child: RadioListTile<int>(
                  activeColor: kOrangeColor,
                  title: Text(
                    L10n.getString("constant_route_free"),
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  value: 1,
                  groupValue: widget._presenter.model.format,
                  onChanged: (int? value) {
                    setState(() {
                      widget._presenter.model.format = value!;
                      widget._presenter.inputController.value++;
                    });
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  bool hasValue() => widget._presenter.model.format >= 0;
}

class _SubmitButton extends StatefulWidget {
  final ImportPresenter _presenter;

  const _SubmitButton(this._presenter);

  @override
  State<_SubmitButton> createState() => _SubmitButtonState();
}

class _SubmitButtonState extends State<_SubmitButton> {
  bool _disabled = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget._presenter.inputController,
      builder: (BuildContext context, Widget? child) {
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: kOrangeColor,
            padding: const EdgeInsets.all(15),
            minimumSize: const Size(100, 36),
          ),
          onPressed: (_disabled || !widget._presenter.model.isValid)
              ? null
              : () async {
                  setState(() => _disabled = true);
                  await Future.delayed(const Duration(milliseconds: 100));
                  await widget._presenter.sendToServer();
                  setState(() => _disabled = false);
                },
          child: Text(
            L10n.getString("track_import_submit"),
            textAlign: TextAlign.center,
          ),
        );
      },
    );
  }
}

class _TrackFromField extends StatefulWidget {
  final ImportPresenter _presenter;

  const _TrackFromField(
    this._presenter,
  );

  @override
  State<_TrackFromField> createState() => _TrackFromFieldState();
}

class _TrackFromFieldState extends State<_TrackFromField> {
  bool _filled = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(Icons.upload_file, color: _filled ? kOrangeColor : kDisabledColor),
        const SizedBox(width: 9),
        Expanded(
          child: InputDecorator(
            decoration: InputDecoration(
              labelText: L10n.getString("import_gpx_route_file_helper_text"),
              floatingLabelStyle: TextStyle(color: _filled ? kOrangeColor : kDisabledColor),
              enabledBorder: _filled ? const HighlightedBorder() : null,
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Column(
                children: [
                  _ImportFromButton(
                    text: L10n.getString("import_gpx_file_button_label"),
                    backgroundColor: kOrangeColor,
                    onPressed: () => _getRoute(FileImporter()),
                  ),
                  // const SizedBox(height: 8),
                  // TODO Strava
                  // Text("----------- ${I18n.getString("import_gpx_file_separator_label")} ------------"),
                  // const SizedBox(height: 5),
                  // _ImportFromWebsiteButton(
                  //   image: "assets/images/btn_strava_connectwith_orange.png",
                  //   onPressed: () => getRoute(StravaImporter()),
                  // ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _getRoute(AbstractImporter fileImporter) async {
    List<GeodesicPoint>? waypoints = await fileImporter.getRouteWaypoints(
      context,
      widget._presenter,
    );
    if (waypoints != null) {
      widget._presenter.model.waypoints = waypoints;
      widget._presenter.inputController.value++;
      setState(() {
        _filled = true;
      });
    }
  }
}

class _ImportFromButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String? text;
  final String? image;
  final Color? backgroundColor;

  _ImportFromButton({
    this.text,
    this.image,
    this.backgroundColor,
    required this.onPressed,
  }) {
    assert(text != null || image != null);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 184, minHeight: 36),
      child: MaterialButton(
        padding: const EdgeInsets.all(0),
        onPressed: onPressed,
        color: backgroundColor,
        child: (text != null)
            ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  text!,
                ),
              )
            : Image.asset(image!),
      ),
    );
  }
}

class HighlightedBorder extends OutlineInputBorder {
  const HighlightedBorder() : super(borderSide: const BorderSide(width: 2, color: kOrangeColor));
}
