import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

import 'time_sheet_model.dart';

class TimeSheetPresenter {
  List<double>? _legStraightDistances;
  double _totalStraightLength = 0;
  List<Checkpoint>? _checkpoints;

  int get length => _checkpoints == null ? 0 : _checkpoints!.length;

  List<double> computeLegStraightDistances(List<Checkpoint> checkpoints) {
    _checkpoints = checkpoints;
    _legStraightDistances = [0];
    _totalStraightLength = 0;
    for (int i = 1; i < checkpoints.length; i++) {
      var distance = GeodesicPoint.distanceBetweenInMeter(
        checkpoints[i - 1].latitude,
        checkpoints[i - 1].longitude,
        checkpoints[i].latitude,
        checkpoints[i].longitude,
      );
      _legStraightDistances!.add(distance);
      _totalStraightLength += distance;
    }
    return _legStraightDistances!;
  }

  double getDistance(int i) => _legStraightDistances![i];

  String getCheckpointName(int i) => _checkpoints![i].id;

  double getActualRouteDistanceInMeters() => _totalStraightLength;

  static double computeOvercostPercentage(double actualDistance, double theoreticalDistance) {
    return TimeSheetModel.computeOvercostPercentage(actualDistance, theoreticalDistance);
  }

  double distanceBetween(int checkpoint1, int checkpoint2) {
    return GeodesicPoint.distanceBetweenInMeter(
      _checkpoints![checkpoint1].latitude,
      _checkpoints![checkpoint1].longitude,
      _checkpoints![checkpoint2].latitude,
      _checkpoints![checkpoint2].longitude,
    );
  }
}
