// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/public_pages/routes/time/preset_order_time_column_view.dart';

import 'free_order_header_column.dart';
import 'free_order_time_column_view.dart';
import 'preset_order_header_column.dart';
import 'settings_side_bar.dart';
import 'time_sheet_presenter.dart';

class TimeSheetTabView extends StatelessWidget {
  static const double DATA_ROW_HEIGHT = 40;
  static const double HEADER_ROW_HEIGHT = 45;

  final RouteListPagePresenter routePresenter;
  final TimeSheetPresenter tablePresenter = TimeSheetPresenter();
  final ScrollController horizontalScroll = ScrollController();
  final ScrollController verticalScroll = ScrollController();

  TimeSheetTabView({
    required this.routePresenter,
  });

  @override
  Widget build(BuildContext context) {
    List<GpsRoute> selectedTracks = routePresenter.selectedTracks;
    if (selectedTracks.isNotEmpty) {
      ValidationOrder routeFormat = selectedTracks[0].courseFormat;
      return Stack(
        alignment: Alignment.topRight,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: _buildTable(context, selectedTracks, routeFormat),
          ),
          SettingsSideBar(routePresenter),
        ],
      );
    } else {
      return Center(child: Text(L10n.getString("time_sheet_split_time_no_orienteer")));
    }
  }

  Widget _buildTable(BuildContext context, List<GpsRoute> selectedTracks, ValidationOrder routeFormat) {
    return Scrollbar(
      controller: verticalScroll,
      thumbVisibility: true,
      trackVisibility: true,
      child: Scrollbar(
        controller: horizontalScroll,
        thumbVisibility: true,
        trackVisibility: true,
        notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          controller: verticalScroll,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            controller: horizontalScroll,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10.0, right: 35),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (routeFormat.isPresetOrder)
                    PresetOrderHeaderColumn(
                      routePresenter,
                      tablePresenter,
                    )
                  else
                    FreeOrderHeaderColumn(
                      routePresenter,
                      tablePresenter,
                    ),
                  for (int i = 0; i < selectedTracks.length; i++)
                    if (selectedTracks[i].model.courseFormat.isFreeOrder)
                      FreeOrderTimeColumnView(
                        selectedTracks[i],
                        tablePresenter,
                      )
                    else
                      PresetOrderTimeColumnView(
                        selectedTracks[i],
                        tablePresenter,
                      ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
