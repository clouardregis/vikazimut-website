class TimeSheetModel {
  static double computeOvercostPercentage(double actualDistance, double theoreticalDistance) {
    if (theoreticalDistance == 0 || actualDistance - theoreticalDistance < 0) {
      return 0;
    } else {
      return 100 * (actualDistance - theoreticalDistance) / theoreticalDistance;
    }
  }
}
