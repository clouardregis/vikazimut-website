// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'time_column_helper.dart';
import 'time_sheet_presenter.dart';
import 'time_sheet_tab_view.dart';

class FreeOrderHeaderColumn extends StatelessWidget {
  final RouteListPagePresenter routePresenter;
  final TimeSheetPresenter tablePresenter;

  const FreeOrderHeaderColumn(
    this.routePresenter,
    this.tablePresenter,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 24),
        DataTable(
          border: TableBorder.all(
            style: BorderStyle.solid,
            width: 1,
            color: kOrangeColorDisabled,
          ),
          horizontalMargin: 10,
          columnSpacing: 5,
          headingRowHeight: TimeSheetTabView.HEADER_ROW_HEIGHT,
          dataRowMinHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
          dataRowMaxHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
          showBottomBorder: true,
          columns: [
            DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_leg_index"))),
          ],
          rows: _buildRows(Theme.of(context), routePresenter),
        ),
      ],
    );
  }

  List<DataRow> _buildRows(ThemeData theme, RouteListPagePresenter presenter) {
    if (presenter.courseData == null) {
      return [];
    }
    List<double> legDistances = tablePresenter.computeLegStraightDistances(presenter.courseData!.checkpoints);
    List<DataRow> rows = [];
    for (int i = 1; i < legDistances.length; i++) {
      List<DataCell> cells = [
        DataCell(Center(child: _buildControlNumberCell(legDistances.length, i, presenter.courseData!.checkpoints[i].id))),
      ];
      DataRow tableRow = DataRow(
        cells: cells,
        color: i.isEven ? WidgetStateProperty.all(theme.colorScheme.inversePrimary) : WidgetStateProperty.all(theme.colorScheme.surface),
      );
      rows.add(tableRow);
    }
    DataRow totalTableRow = DataRow(
      cells: [
        DataCell(TotalCell(child: TextCenteredBold(L10n.getString("time_sheet_total_row_title")))),
      ],
    );
    rows.insert(0, totalTableRow);
    return rows;
  }

  Widget _buildControlNumberCell(int numberOfLegs, int i, String id) {
    if (i < numberOfLegs - 1) {
      return Text(
        "$i",
        style: const TextStyle(fontWeight: FontWeight.bold),
      );
    } else {
      return TextCenteredBold(L10n.getString("time_sheet_checkpoint_finish"));
    }
  }
}
