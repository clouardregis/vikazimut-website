// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'time_column_helper.dart';
import 'time_sheet_presenter.dart';
import 'time_sheet_tab_view.dart';

class FreeOrderTimeColumnView extends StatelessWidget {
  final GpsRoute _selectedTrack;
  final TimeSheetPresenter _tablePresenter;

  const FreeOrderTimeColumnView(
    this._selectedTrack,
    this._tablePresenter,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        border: Border(
          left: BorderSide(width: 3.0, color: kOrangeColor),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.person, color: _selectedTrack.color),
              Text(
                _selectedTrack.nickname,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          DataTable(
            border: TableBorder.all(
              style: BorderStyle.solid,
              width: 1,
              color: kOrangeColor,
            ),
            horizontalMargin: 10,
            columnSpacing: 5,
            headingRowHeight: TimeSheetTabView.HEADER_ROW_HEIGHT,
            dataRowMaxHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
            dataRowMinHeight: TimeSheetTabView.DATA_ROW_HEIGHT,
            showBottomBorder: true,
            columns: [
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_checkpoint_index"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_length"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_time"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_distance"))),
              DataColumn(label: ColumnHeaderText(label: L10n.getString("time_sheet_split_pace"))),
            ],
            rows: _buildRows(Theme.of(context), _selectedTrack.model),
          ),
        ],
      ),
    );
  }

  List<DataRow> _buildRows(ThemeData theme, TrackModel track) {
    List<DataRow> rows = [];
    var totalActualDistance = 0.0;
    var totalElevation = 0.0;
    var totalTime = 0;
    var lastCheckpoint = track.getCheckpointIndex(0);
    var totalStraightDistance = 0.0;
    for (int legIndex = 1; legIndex < track.legCount; legIndex++) {
      int checkpoint = track.getCheckpointIndex(legIndex);
      if (checkpoint >= _tablePresenter.length) continue;
      String timeCell;
      final String cumulativeTimeCell;
      final String distanceCell;
      final String paceCell;
      final String legStraightDistanceCell;
      final Widget checkpointCell = _buildControlNumberCellValue(checkpoint, track.legCount, _tablePresenter.getCheckpointName(checkpoint));
      final punchTime = (legIndex < track.punchTimes.length) ? track.legs[legIndex].punchTime : -1;
      if (punchTime > 0) {
        final elapsedPunchTime = track.calculateLegDuration(legIndex);
        final legActualDistance = track.calculateLegActualDistance(legIndex);
        final legElevationGain = track.calculateLegElevationGain(legIndex);
        final legStraightDistance = _tablePresenter.distanceBetween(lastCheckpoint, checkpoint);
        totalStraightDistance += legStraightDistance;
        legStraightDistanceCell = L10n.formatNumber(legStraightDistance.toInt(), 2);
        timeCell = TimeColumnHelper.formatTime(elapsedPunchTime);
        cumulativeTimeCell = TimeColumnHelper.formatTime(punchTime);
        if (track.punchTimes[checkpoint].forced) timeCell += "^";
        distanceCell = TimeColumnHelper.distanceDataToString(legActualDistance, legStraightDistance, legElevationGain);
        final pace = TimeColumnHelper.calculatePaceInMillisecondPerKm(elapsedPunchTime, legStraightDistance);
        final speed = TimeColumnHelper.calculateSpeedInKmh(elapsedPunchTime, legActualDistance);
        paceCell = '${TimeColumnHelper.formatTime(pace)}\n${TimeColumnHelper.formatSpeed(speed)}';
        lastCheckpoint = checkpoint;
        totalActualDistance += legActualDistance;
        totalElevation += legElevationGain > 0 ? legElevationGain : 0;
        totalTime += elapsedPunchTime;
      } else {
        timeCell = "--:--";
        cumulativeTimeCell = "--:--";
        distanceCell = "- (-)";
        paceCell = "--:--";
        legStraightDistanceCell = "--";
      }
      DataRow tableRow = DataRow(
        cells: [
          DataCell(Center(child: checkpointCell)),
          DataCell(Center(child: TextCentered(legStraightDistanceCell))),
          DataCell(Center(child: TextCentered("$timeCell\n$cumulativeTimeCell"))),
          DataCell(Center(child: TextCentered(distanceCell))),
          DataCell(Center(child: TextCentered(paceCell))),
        ],
        color: legIndex.isEven ? WidgetStateProperty.all(theme.colorScheme.inversePrimary) : WidgetStateProperty.all(theme.colorScheme.surface),
      );
      rows.add(tableRow);
    }

    // Totals
    String totalTimeCell = TimeColumnHelper.formatTime(totalTime);
    String totalDistanceCell = TimeColumnHelper.distanceDataToString(totalActualDistance, totalStraightDistance, totalElevation);
    final pace = TimeColumnHelper.calculatePaceInMillisecondPerKm(totalTime, totalStraightDistance);
    final speed = TimeColumnHelper.calculateSpeedInKmh(totalTime, totalActualDistance);
    String totalPace = '${TimeColumnHelper.formatTime(pace)}\n${TimeColumnHelper.formatSpeed(speed)}';
    String totalTheoreticalDistance = L10n.formatNumber(totalStraightDistance.toInt(), 2);
    DataRow totalTableRow = DataRow(
      cells: [
        const DataCell(TotalCell(child: SizedBox(width: 0.0, height: 0.0))),
        DataCell(TotalCell(child: TextCenteredBold(totalTheoreticalDistance))),
        DataCell(TotalCell(child: TextCenteredBold(totalTimeCell))),
        DataCell(TotalCell(child: TextCenteredBold(totalDistanceCell))),
        DataCell(TotalCell(child: TextCenteredBold(totalPace))),
      ],
    );
    rows.insert(0, totalTableRow);
    return rows;
  }

  Widget _buildControlNumberCellValue(int i, int size, String id) {
    if (i < size - 1) {
      return Text.rich(
        TextSpan(
          children: [
            TextSpan(
              style: const TextStyle(fontWeight: FontWeight.bold),
              text: "$i",
            ),
            TextSpan(
              style: const TextStyle(fontSize: 11),
              text: " ($id)",
            ),
          ],
        ),
      );
    } else {
      return TextCenteredBold(L10n.getString("time_sheet_checkpoint_finish"));
    }
  }
}
