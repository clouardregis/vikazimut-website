import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/utils/time.dart';

class CsvConverter {
  static String convertResultToCsv(List<GpsRoute> selectedTracks, CourseData courseData) {
    // 1: Header orienteer names
    String csvContents = "";
    for (var i = 0; i < selectedTracks.length; ++i) {
      if (selectedTracks[i].model.courseFormat == ValidationOrder.PRESET_ORDER) {
        String mode = L10n.getString("track_export_result_preset_order");
        csvContents += ";;;";
        csvContents += "${selectedTracks[i].model.nickname};$mode;;";
      } else {
        String mode = L10n.getString("track_export_result_free_order");
        String score = L10n.getString("track_export_result_total_score");
        String runCount = L10n.getString("track_export_result_run_count");
        csvContents += "${selectedTracks[i].model.nickname};"
            "$mode;"
            "$score;"
            "${selectedTracks[i].model.score};"
            "$runCount;"
            "${selectedTracks[i].model.runCount};";
      }
    }
    csvContents += "\n";

    // 2. Column header
    for (var i = 0; i < selectedTracks.length; ++i) {
      if (selectedTracks[i].model.courseFormat.isPresetOrder) {
        csvContents += "${L10n.getString("track_export_result_column_checkpoint_index")};"
            "${L10n.getString("track_export_result_column_checkpoint_id")};"
            "${L10n.getString("track_export_result_column_length")};";
        csvContents += "${L10n.getString("track_export_result_column_punch_time")};"
            "${L10n.getString("track_export_result_column_distance")};"
            "${L10n.getString("track_export_result_column_elevation")};";
      } else {
        csvContents += "${L10n.getString("track_export_result_column_checkpoint_index")};"
            "${L10n.getString("track_export_result_column_checkpoint_id")};"
            "${L10n.getString("track_export_result_column_points")};"
            "${L10n.getString("track_export_result_column_punch_time")};"
            "${L10n.getString("track_export_result_column_distance")};"
            "${L10n.getString("track_export_result_column_elevation")};";
      }
    }
    csvContents += "\n";

    // 2: Rows
    var straightDistances = computeLegStraightDistances(courseData.checkpoints);
    for (var i = 1; i < courseData.checkpoints.length; ++i) {
      for (var j = 0; j < selectedTracks.length; ++j) {
        if (selectedTracks[j].model.courseFormat.isPresetOrder) {
          csvContents += "${courseData.checkpoints[i].index};${courseData.checkpoints[i].id};${straightDistances[i]};";
          var legDistance = selectedTracks[j].model.calculateLegActualDistance(i).toInt();
          var legElevation = selectedTracks[j].model.calculateLegElevationGain(i);
          var punchTime = selectedTracks[j].model.punchTimes[i].timestampInMillisecond;
          if (punchTime > 0) {
            csvContents += "${timeToStringWithMilliseconds(punchTime)};$legDistance;${legElevation.toInt()};";
          } else {
            csvContents += ";;";
          }
        } else {
          int checkpoint = selectedTracks[j].model.getCheckpointIndex(i);
          var punchTime = selectedTracks[j].model.punchTimes[checkpoint].timestampInMillisecond;
          if (punchTime > 0) {
            csvContents += "$checkpoint;${courseData.checkpoints[checkpoint].id};";
            csvContents += "${courseData.checkpoints[checkpoint].score};";
            var legDistance = selectedTracks[j].model.calculateLegActualDistance(i).toInt();
            var legElevation = selectedTracks[j].model.calculateLegElevationGain(i);
            csvContents += "${timeToStringWithMilliseconds(punchTime)};$legDistance;${legElevation.toInt()};";
          } else {
            csvContents += ";;;;;;";
          }
        }
      }
      csvContents += "\n";
    }
    return csvContents;
  }

  static List<int> computeLegStraightDistances(List<Checkpoint> checkpoints) {
    List<int> legStraightDistances = [0];
    for (int i = 1; i < checkpoints.length; i++) {
      var distance = GeodesicPoint.distanceBetweenInMeter(
        checkpoints[i - 1].latitude,
        checkpoints[i - 1].longitude,
        checkpoints[i].latitude,
        checkpoints[i].longitude,
      );
      legStraightDistances.add(distance.toInt());
    }
    return legStraightDistances;
  }
}
