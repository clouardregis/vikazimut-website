import 'package:flutter/material.dart';

class MyTabController extends TabController {
  int currentLeg = 0;

  MyTabController({required super.length, required super.vsync});

  void openTabWithLeg(int legIndex) {
    currentLeg = legIndex;
    animateTo(3);
  }
}
