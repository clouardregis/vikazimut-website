// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/html_utils.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_sidebar_widget.dart';

import '../route_list_page_presenter.dart';
import 'csv_writer.dart';
import 'key_performance_indicator/performance_processor.dart';

class SideBar extends StatefulWidget {
  const SideBar(this.routePresenter, this.performanceIndicators);

  final List<PerformanceIndicators> performanceIndicators;
  final RouteListPagePresenter routePresenter;

  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  @override
  Widget build(BuildContext context) {
    return CollapsibleSideBar(
      position: Position.RIGHT,
      isOpen: false,
      title: Text(
        L10n.getString("orienteering_performance_documentation_title"),
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
      minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
      padding: const EdgeInsets.all(10.0),
      children: [
        Text(
          L10n.getString("orienteering_performance_documentation_text"),
          style: TextStyle(fontSize: 14),
        ),
        CustomOutlinedButton(
          onPressed: () {
            downloadStringInFile("${widget.routePresenter.courseId}.csv", CsvWriter.convert(widget.routePresenter.selectedTracks, widget.performanceIndicators));
          },
          label: L10n.getString("orienteering_performance_export_button"),
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
        ),
      ],
    );
  }
}
