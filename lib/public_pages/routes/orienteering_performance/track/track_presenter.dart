import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_overlay_image_layer.dart';
import 'package:vikazimut_website/common/network_image.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

class TrackPresenter {
  Future<List<List<List<Object>>>> splitRouteIntoLegsAndAreas(
    GpsRoute track,
    List<PunchTime> punchTimes,
    List<Checkpoint> checkpoints,
    ValidationOrder courseFormat,
  ) async {
    final legs = track.model.legs;
    final List<List<GeodesicPoint>> legRoutes = [];
    for (int i = 1; i < legs.length; i++) {
      if (legs[i].punchTime >= 0) {
        legRoutes.add(legs[i].waypoints ?? []);
      }
    }
    List<List<List<GeodesicPoint>>> legAreaTracks = RouteHelper.splitLegIntoAreas(legRoutes);
    return [legRoutes, legAreaTracks];
  }

  LatLngBounds buildBounds(CourseGeoreference courseGeoreference) {
    final latLngBounds = courseGeoreference.latLngBounds();
    final LatLngBounds bounds = LatLngBounds(LatLng(latLngBounds[3][0], latLngBounds[3][1]), LatLng(latLngBounds[1][0], latLngBounds[1][1]));
    return bounds;
  }

  List<CustomBaseOverlayImage> loadOverlayImage(
    CourseGeoreference courseGeoreference,
    LatLngBounds bounds,
    IdentifiedUser? user,
  ) {
    final rotation = -courseGeoreference.bounds.rotation;
    return <CustomBaseOverlayImage>[
      CustomRotatedOverlayImage(
        bounds: bounds,
        opacity: 0.8,
        angleInDegree: rotation,
        imageProvider: loadNetworkImage(user, courseGeoreference.imageId),
      ),
    ];
  }

  List<TaggedColoredPolyline> buildPolylineFromSelectedLeg(
    List<RouteSegmentType> legRoutes,
    List<List<RouteSegmentType>> legAreaTracks,
    int currentLeg,
    RouteSegmentType completeRoute,
    Color color,
    Discipline? discipline,
  ) {
    final RouteSegmentType route;
    if (currentLeg != 0) {
      if (currentLeg > legRoutes.length) {
        route = [];
      } else {
        route = legRoutes[currentLeg - 1];
      }
    } else {
      route = completeRoute;
    }
    final List<LatLng> trackAsPoints = _trackAsGpsPositions(route);
    final List<int> areaAsColorIndices = _getAreasAsColor(route, legAreaTracks);
    List<int> allSpeedsAsColorIndices = _getAllSpeedsAsColor(discipline, route);
    return [
      TaggedColoredPolyline(
        points: trackAsPoints,
        punchPositions: [],
        pointColorIndices: allSpeedsAsColorIndices,
        borderColorIndices: areaAsColorIndices,
        color: color,
        strokeWidth: 5,
      )..selected = true
    ];
  }

  List<LatLng> _trackAsGpsPositions(RouteSegmentType track) => track.map((point) => LatLng(point.latitude, point.longitude)).toList();

  List<int> _getAreasAsColor(
    RouteSegmentType points,
    List<List<RouteSegmentType>> areas,
  ) {
    int computeColorArea(List<List<RouteSegmentType>> areas, GeodesicPoint startSegment, GeodesicPoint endSegment) {
      for (int leg = 0; leg < areas.length; leg++) {
        if (areas[leg].isEmpty) continue;
        for (int i = 0; i < 3; i++) {
          if (areas[leg][i].isEmpty) continue;
          for (int j = 0; j < areas[leg][i].length; j++) {
            if (startSegment.timestampInMillisecond == areas[leg][i][j].timestampInMillisecond) {
              return i;
            }
          }
        }
      }
      return 1;
    }

    const List<int> colors = [
      0xFF1186FF, // blue
      0x00FFFFFF, // yellow
      // 0xFFe81e63, // red
      0xFFFF00FF, // magenta
    ];
    List<int> coloredRoutes = [];
    for (int i = 0; i < points.length - 1; i++) {
      int k = computeColorArea(areas, points[i], points[i + 1]);
      coloredRoutes.add(colors[k]);
    }
    if (coloredRoutes.isNotEmpty) {
      coloredRoutes.add(colors[2]);
    }
    return coloredRoutes;
  }

  List<int> _getAllSpeedsAsColor(Discipline? discipline, RouteSegmentType route) {
    final punchTime = route.isEmpty? -1 : route.last.timestampInMillisecond;
    final GpsRoute track = GpsRoute(
        0,
        TrackModel(
          0,
          "",
          false,
          punchTime,
          route,
          [],
          ValidationOrder.PRESET_ORDER,
        ));
    return GpsRoute.getAllSpeedsAsColor(discipline, [track], legIndex: 0).first;
  }
}
