// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'global_grade_widget.dart';
import 'key_performance_indicator/area_score.dart';
import 'my_tab_controller.dart';
import 'radar_chart_widget.dart';

class GlobalPerformanceTab extends StatelessWidget {
  final AreaScore featureValues;
  final MyTabController tabController;
  final List<String> globalComments;

  const GlobalPerformanceTab({
    required this.featureValues,
    required this.globalComments,
    required this.tabController,
  });

  @override
  Widget build(BuildContext context) {
    final widgetSize = MediaQuery.of(context).size;
    final double screenHeight = math.min(widgetSize.width, widgetSize.height / 2 - 60);
    return Column(
      children: <Widget>[
        Container(
          constraints: BoxConstraints(maxHeight: 500),
          height: screenHeight,
          child: RadarChartWidget(
            featureValues: featureValues,
          ),
        ),
        GlobalGradeWidget(
          size: Size(100, 100),
          globalGrade: featureValues.computeAverage(),
          controlAttackGrade: featureValues.controlAttackReport.score,
          handrailGrade: featureValues.handrailReport.score,
          controlExitGrade: featureValues.controlExitReport.score,
        ),
      ],
    );
  }
}
