part of 'keypoint_indicator_processor.dart';

abstract class ControlKeypointIndicatorProcessor extends KeypointIndicatorProcessor {
  static const int _maxStopTimeInMs = 10 * 1000;
  static const double _maxOrientationDeviationSumInDeg = 135;
  static const double _minSpeedForMoveInKmh = 2;

  String getRelatedControl(int control1, int control2);

  @override
  AnalysisReport computeScoreAndReport(
    List<GeodesicPoint> trackZone,
    GeodesicPoint areaCenterLocation,
    int legIndex,
    int control1,
    int control2,
    double averageSpeedInKmh,
    Map<String, dynamic> log,
  ) {
    List<GeodesicPoint> croppedTrackFromDistance = cropRouteFromDistance(trackZone);
    List<GeodesicPoint> croppedTrackFromTime = cropRouteFromTime(trackZone);
    List<double> speedsEffortInKmh = RouteHelper.computeInstantaneousEffortSpeedsInKmh(croppedTrackFromTime);
    List<GeodesicPoint> segments = DouglasPeuckerAlgorithm.decimateTrackIntoLineSegments(croppedTrackFromDistance);
    final String control = getRelatedControl(control1, control2);
    Comment comments = Comment();

    int stopDurationInMs = computeStopDuration(speedsEffortInKmh, croppedTrackFromTime, comments, legIndex, control);
    log["Durée des arrêts (ms)"] = stopDurationInMs;

    double speedVariationScore = _computeSpeedVariationScore(speedsEffortInKmh, averageSpeedInKmh, comments, legIndex, control, log);
    log["Score variation de vitesse-effort (km/h)"] = speedVariationScore.toStringAsFixed(2);

    bool hasUTurn = detectUTurnsAndLoops(croppedTrackFromDistance, comments, legIndex, control);
    log["Présence demi-tours"] = hasUTurn;

    double orientationScore = computeOrientationScore(segments, areaCenterLocation, comments, legIndex, control, log);
    log["Orientation Score"] = orientationScore.toStringAsFixed(2);
    return computeFinalScore(stopDurationInMs, comments, hasUTurn, orientationScore, speedVariationScore);
  }

  int computeStopDuration(
    List<double> speedsEffortInKmh,
    List<GeodesicPoint> croppedTrackZone,
    Comment comments,
    int legIndex,
    String control,
  ) {
    int stopDurationInMs = KeypointIndicatorProcessor.calculateStopDurationInMs_(speedsEffortInKmh, croppedTrackZone, _minSpeedForMoveInKmh);
    if (stopDurationInMs > _maxStopTimeInMs) {
      comments.stop.add(LegComment(legIndex, control));
    }
    return stopDurationInMs;
  }

  double _computeSpeedVariationScore(
    List<double> speedsEffortInKmh,
    double averageSpeedInKmh,
    Comment comments,
    int legIndex,
    String control,
    Map<String, dynamic> log,
  ) {
    final List<double> accelerationInKmh2 = KeypointIndicatorProcessor.calculateAccelerationsInKmh2FromFiniteDifferences_(speedsEffortInKmh);
    double speedVariationScore = KeypointIndicatorProcessor.calculateSpeedVariationScore_(
      accelerationInKmh2,
      averageSpeedInKmh,
      log,
    );
    if (speedVariationScore < 5) {
      comments.speed.add(LegComment(legIndex, control));
    }
    return speedVariationScore;
  }

  bool detectUTurnsAndLoops(
    List<GeodesicPoint> croppedTrackZone,
    Comment comments,
    int legIndex,
    String control,
  ) {
    bool hasUTurn = KeypointIndicatorProcessor.detectUTurn(croppedTrackZone);
    if (hasUTurn) {
      comments.uTurn.add(LegComment(legIndex, control));
    }
    return hasUTurn;
  }

  double computeOrientationScore(
    List<GeodesicPoint> segments,
    GeodesicPoint areaCenterLocation,
    Comment comments,
    int legIndex,
    String control,
    Map<String, dynamic> log,
  ) {
    double orientationScore = _calculateOrientationVariationScore(
      segments,
      areaCenterLocation,
      _maxOrientationDeviationSumInDeg,
      log,
    );
    if (orientationScore < 5) {
      comments.direction.add(LegComment(legIndex, control));
    }
    return orientationScore;
  }

  AnalysisReport computeFinalScore(
    int stopDurationInMs,
    Comment comments,
    bool hasUTurn,
    double orientationScore,
    double speedVariationScore,
  ) {
    if (stopDurationInMs > _maxStopTimeInMs) {
      return AnalysisReport(score: 0, comments: comments);
    } else {
      double quotient;
      if (stopDurationInMs > _maxStopTimeInMs ~/ 2 + 1) {
        quotient = 2;
      } else if (stopDurationInMs > _maxStopTimeInMs ~/ 4 + 1) {
        quotient = 3 / 2;
      } else {
        quotient = 1;
      }
      if (hasUTurn) {
        orientationScore = 0;
      }
      final score = (speedVariationScore / 3 + 2 * orientationScore / 3) / quotient;
      return AnalysisReport(score: score, comments: comments);
    }
  }

  List<GeodesicPoint> cropRouteFromTime(List<GeodesicPoint> points);

  List<GeodesicPoint> cropRouteFromDistance(List<GeodesicPoint> points);

  double getWeightingSegmentLengthInM(List<GeodesicPoint> segments, int i);

  double _calculateOrientationVariationScore(
    List<GeodesicPoint> segments,
    GeodesicPoint checkpointLocation,
    double maxOrientationDeviationInDegree,
    Map<String, dynamic> log,
  ) {
    List<double> anglesInDeg = Vector2.computeAnglesBetweenSegmentsInDegree(segments);
    if (anglesInDeg.isEmpty) {
      // TODO L10n
      log["Déviation orientation totale (°)"] = 0;
      return 10;
    }

    double totalOrientationDeviationInDegree = 0;
    for (int i = 0; i < anglesInDeg.length; i++) {
      final double angle = anglesInDeg[i];
      final double segmentLength = getWeightingSegmentLengthInM(segments, i);
      totalOrientationDeviationInDegree += angle * angle * segmentLength;
    }
    totalOrientationDeviationInDegree /= RouteHelper.areaRadiusInM;
    log["Déviation orientation totale (°)"] = math.sqrt(totalOrientationDeviationInDegree).toStringAsFixed(2);
    final double score = 10.0 * (1.0 - totalOrientationDeviationInDegree / (maxOrientationDeviationInDegree * maxOrientationDeviationInDegree));
    return score.clamp(0.0, 10.0);
  }
}
