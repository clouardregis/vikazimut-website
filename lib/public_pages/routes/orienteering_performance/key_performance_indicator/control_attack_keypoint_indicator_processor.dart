part of 'keypoint_indicator_processor.dart';

class ControlAttackKeypointIndicatorProcessor extends ControlKeypointIndicatorProcessor {
  static const _distanceRemovedBeforeValidationInM = 10;
  static const _timeRemovedBeforeValidationInS = 2 * 1000;

  @override
  String getRelatedControl(int control1, int control2) => "$control2";

  @override
  List<GeodesicPoint> cropRouteFromDistance(List<GeodesicPoint> points) {
    return cropRouteFromDistance_(points, _distanceRemovedBeforeValidationInM);
  }

  @override
  List<GeodesicPoint> cropRouteFromTime(List<GeodesicPoint> points) {
    return cropRouteFromTime_(points, _timeRemovedBeforeValidationInS);
  }

  @visibleForTesting
  List<GeodesicPoint> cropRouteFromDistance_(List<GeodesicPoint> points, int maximumValidationDistanceInM) {
    var distance = 0.0;
    for (int i = points.length - 2; i >= 0; i--) {
      final point = points[i];
      final nextPoint = points[i + 1];
      final length = GeodesicPoint.distanceBetweenInMeter(nextPoint.latitude, nextPoint.longitude, point.latitude, point.longitude);
      distance += length;
      if (distance > maximumValidationDistanceInM) {
        var ratio = (maximumValidationDistanceInM - (distance - length)) / length;
        var time = nextPoint.timestampInMillisecond - point.timestampInMillisecond;
        final newPointTimestamp = nextPoint.timestampInMillisecond - (time * ratio).toInt();
        final newPoint = RouteHelper.createPointAtGivenTime(nextPoint, point, newPointTimestamp);
        return points.sublist(0, i + 1)..add(newPoint);
      } else if (distance == maximumValidationDistanceInM) {
        return points.sublist(0, i + i);
      }
    }
    return points;
  }

  @visibleForTesting
  List<GeodesicPoint> cropRouteFromTime_(List<GeodesicPoint> points, int timeRemovedBeforeValidationInS) {
    final lastPoint = points[points.length - 1];
    for (int i = points.length - 2; i >= 0; i--) {
      final point = points[i];
      if (lastPoint.timestampInMillisecond - point.timestampInMillisecond > timeRemovedBeforeValidationInS) {
        final previousPoint = points[i + 1];
        final newPointTimestamp = lastPoint.timestampInMillisecond - timeRemovedBeforeValidationInS;
        final newPoint = RouteHelper.createPointAtGivenTime(previousPoint, point, newPointTimestamp);
        return points.sublist(0, i + 1)..add(newPoint);
      } else if (lastPoint.timestampInMillisecond - point.timestampInMillisecond == timeRemovedBeforeValidationInS) {
        return points.sublist(0, i + 1);
      }
    }
    return points;
  }

  @override
  double getWeightingSegmentLengthInM(List<GeodesicPoint> segments, int i) {
    return segments[i + 1].distanceToInMeter(segments[i + 2]);
  }
}
