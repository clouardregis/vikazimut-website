import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

class Vector2 {
  final double x, y;

  Vector2(this.x, this.y);

  double dot(Vector2 other) {
    return x * other.x + y * other.y;
  }

  static double determinant(Vector2 vector1, Vector2 vector2) {
    return vector1.x * vector2.y - vector1.y * vector2.x;
  }

  static List<double> computeAnglesBetweenSegmentsInDegree(List<GeodesicPoint> segments) {
    List<double> angles = [];
    if (segments.length < 3) {
      return angles;
    }
    for (int i = 0; i < segments.length - 2; i++) {
      GeodesicPoint point1 = segments[i];
      GeodesicPoint point2 = segments[i + 1];
      GeodesicPoint point3 = segments[i + 2];

      double angleInDegrees = computeAngleBetween2SegmentsInDegree_(point1, point2, point3);
      angles.add(angleInDegrees);
    }
    return angles;
  }

  @visibleForTesting
  static double computeAngleBetween2SegmentsInDegree_(GeodesicPoint point1, GeodesicPoint point2, GeodesicPoint point3) {
    Vector2 vector1 = Vector2(point2.longitude - point1.longitude, point2.latitude - point1.latitude);
    Vector2 vector2 = Vector2(point3.longitude - point2.longitude, point3.latitude - point2.latitude);
    double dotProduct = vector1.dot(vector2);
    double determinant = Vector2.determinant(vector1, vector2);
    double angleInRadians = math.atan2(determinant, dotProduct);
    return angleInRadians * (180.0 / math.pi);
  }
}
