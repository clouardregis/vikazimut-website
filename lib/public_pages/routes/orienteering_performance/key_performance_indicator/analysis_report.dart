class AnalysisReport {
  final double score;
  final Comment? comments;

  AnalysisReport({
    this.score = 10.0,
    this.comments,
  });
}

class Comment {
  final List<LegComment> uTurn = [];
  final List<LegComment> speed = [];
  final List<LegComment> direction = [];
  final List<LegComment> stop = [];

  void add(Comment comment) {
    uTurn.addAll(comment.uTurn);
    speed.addAll(comment.speed);
    direction.addAll(comment.direction);
    stop.addAll(comment.stop);
  }
}

class LegComment {
  int legIndex;
  String legName;

  LegComment(this.legIndex, this.legName);
}
