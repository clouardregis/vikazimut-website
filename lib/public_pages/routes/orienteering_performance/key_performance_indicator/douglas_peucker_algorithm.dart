// coverage:ignore-file
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

class DouglasPeuckerAlgorithm {
  // Empirically estimated (represents 11,13 m on the terrain)
  static const double _EPSILON_IN_DEGREE = 0.0001;

  static List<GeodesicPoint> decimateTrackIntoLineSegments(List<GeodesicPoint> points) {
    return _recursiveDecimation(points);
  }

  static List<GeodesicPoint> _recursiveDecimation(List<GeodesicPoint> points) {
    if (points.length <= 3) {
      return points;
    }
    double maxDistance = 0;
    int indexOfMaximum = 0;
    for (int i = 1; i < points.length - 1; i++) {
      double distance = perpendicularDistanceToLine_(points[i], points.first, points.last);
      if (distance > maxDistance) {
        indexOfMaximum = i;
        maxDistance = distance;
      }
    }
    if (maxDistance > _EPSILON_IN_DEGREE) {
      List<GeodesicPoint> recPoints1 = _recursiveDecimation(points.sublist(0, indexOfMaximum + 1));
      List<GeodesicPoint> recPoints2 = _recursiveDecimation(points.sublist(indexOfMaximum, points.length));
      // Discard the end point since it is already on the recPoints2
      recPoints1 = recPoints1.sublist(0, recPoints1.length - 1);
      return [...recPoints1, ...recPoints2];
    } else {
      return [points.first, points.last];
    }
  }

  @visibleForTesting
  static double perpendicularDistanceToLine_(
    GeodesicPoint point,
    GeodesicPoint lineStart,
    GeodesicPoint lineEnd,
  ) {
    final x = point.longitude;
    final y = point.latitude;
    final x1 = lineStart.longitude;
    final y1 = lineStart.latitude;
    final x2 = lineEnd.longitude;
    final y2 = lineEnd.latitude;
    final num = ((y2 - y1) * x - (x2 - x1) * y + x2 * y1 - y2 * x1).abs();
    final den = sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    return num / den;
  }
}
