// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';

class GlobalGradeWidget extends StatelessWidget {
  final Size size;
  final double globalGrade;
  final double controlAttackGrade;
  final double handrailGrade;
  final double controlExitGrade;

  const GlobalGradeWidget({
    required this.size,
    required this.globalGrade,
    required this.controlAttackGrade,
    required this.handrailGrade,
    required this.controlExitGrade,
  });

  @override
  Widget build(BuildContext context) {
    Color color = getColorFromGrade_(globalGrade);
    return Column(
      children: [
        Text(
          L10n.getString("performance_tab_global_grade_title"),
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10.0),
        Stack(
          alignment: AlignmentDirectional.center,
          children: [
            SizedBox(
              height: size.height,
              width: size.width,
              child: CircularProgressIndicator(
                backgroundColor: Colors.grey.withValues(alpha: 0.4),
                valueColor: AlwaysStoppedAnimation<Color>(color),
                strokeCap: StrokeCap.round,
                strokeWidth: 15,
                value: globalGrade / 10.0,
              ),
            ),
            Text.rich(
              TextSpan(children: [
                TextSpan(text: globalGrade.toStringAsFixed(2), style: const TextStyle(fontSize: 26)),
                TextSpan(text: "/10", style: const TextStyle(fontSize: 14)),
              ]),
            ),
          ],
        ),
      ],
    );
  }

  @visibleForTesting
  static final Color redColor = Colors.red[800]!;
  @visibleForTesting
  static final Color orangeColor = Colors.orange[800]!;
  @visibleForTesting
  static final Color yellowColor = Colors.yellow[700]!;
  @visibleForTesting
  static final Color greenColor = Colors.green[500]!;

  @visibleForTesting
  static Color getColorFromGrade_(double grade) {
    if (grade < 3.0) {
      return redColor;
    } else if (grade <= 5.0) {
      return orangeColor;
    } else if (grade <= 7.0) {
      return yellowColor;
    } else {
      return greenColor;
    }
  }
}
