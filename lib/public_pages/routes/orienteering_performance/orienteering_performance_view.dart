// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/orienteering_performance_presenter.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';

import 'detailed_performance_view.dart';
import 'global_performance_tab.dart';
import 'key_performance_indicator/performance_processor.dart';
import 'my_tab_controller.dart';
import 'observation_tab.dart';
import 'side_bar.dart';
import 'track/track_widget.dart';

class OrienteeringPerformanceView extends StatefulWidget {
  final RouteListPagePresenter routePresenter;
  final Future<CourseData> courseInfo;

  const OrienteeringPerformanceView({
    required this.routePresenter,
    required this.courseInfo,
  });

  @override
  State<OrienteeringPerformanceView> createState() => _OrienteeringPerformanceViewState();
}

class _OrienteeringPerformanceViewState extends State<OrienteeringPerformanceView> with SingleTickerProviderStateMixin {
  late final ScrollController _scrollController;
  late final MyTabController _tabController;

  @override
  void initState() {
    _tabController = MyTabController(length: 4, vsync: this);
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.routePresenter.courseData!.courseGeoreference.discipline != Discipline.FORESTO) {
      return Center(
        child: Text(L10n.getString("global_performance_bad_discipline_error")),
      );
    } else if (widget.routePresenter.selectedTracks.isEmpty) {
      return Center(
        child: Text(L10n.getString("orienteer_profile_no_orienteer")),
      );
    } else {
      return FutureBuilder(
        future: OrienteeringPerformancePresenter.computeKeypointIndicators(
          widget.routePresenter.selectedTracks,
          widget.routePresenter.courseData!.checkpoints,
        ),
        builder: (BuildContext context, AsyncSnapshot<List<PerformanceIndicators>> snapshot) {
          if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
            final performanceIndicators = snapshot.data!;
            final height = MediaQuery.of(context).size.height;
            return Stack(
              alignment: Alignment.topRight,
              children: [
                Scrollbar(
                  controller: _scrollController,
                  thumbVisibility: true,
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: performanceIndicators.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        margin: EdgeInsets.all(8),
                        child: SizedBox(
                          height: height - 176,
                          child: DefaultTabController(
                            length: _tabController.length,
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 25, right: 8),
                                      child: Row(
                                        children: [
                                          Icon(Icons.person, color: widget.routePresenter.selectedTracks[index].color),
                                          Text(
                                            widget.routePresenter.selectedTracks[index].nickname,
                                            style: const TextStyle(fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      child: TabBar(
                                        controller: _tabController,
                                        labelPadding: EdgeInsets.all(2),
                                        tabs: [
                                          Tab(text: L10n.getString("performance_tab1_title"), height: 30),
                                          Tab(text: L10n.getString("performance_tab2_title"), height: 30),
                                          Tab(text: L10n.getString("performance_tab3_title"), height: 30),
                                          Tab(text: L10n.getString("performance_tab4_title"), height: 30),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: TabBarView(
                                    controller: _tabController,
                                    children: [
                                      GlobalPerformanceTab(
                                        featureValues: performanceIndicators[index].totalScore,
                                        globalComments: performanceIndicators[index].globalComments,
                                        tabController: _tabController,
                                      ),
                                      DetailedPerformanceView(
                                        performances: performanceIndicators[index].legPerformanceScores,
                                        tabController: _tabController,
                                      ),
                                      ObservationTab(
                                        featureValues: performanceIndicators[index].totalScore,
                                        globalComments: performanceIndicators[index].globalComments,
                                        tabController: _tabController,
                                      ),
                                      TrackView(
                                        presenter: widget.routePresenter,
                                        courseInfo: widget.courseInfo,
                                        track: widget.routePresenter.selectedTracks[index],
                                        tabController: _tabController,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                SideBar(widget.routePresenter, performanceIndicators),
              ],
            );
          } else if (snapshot.hasError) {
            return GlobalErrorWidget(snapshot.error.toString());
          } else {
            return Container();
          }
        },
      );
    }
  }
}
