// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'split_time_sheet_presenter.dart';
import 'split_time_sheet_tab_view.dart';
import 'time_cell.dart';

class PresetOrderTableView extends StatelessWidget {
  final RouteListPagePresenter _routePresenter;
  final SplitTimeSheetPresenter _tablePresenter;

  PresetOrderTableView(this._routePresenter, int thresholdErrorRate, this._tablePresenter) {
    _tablePresenter.buildTable(
      _routePresenter.selectedTracks,
      _routePresenter.courseData!.checkpoints,
      ValidationOrder.PRESET_ORDER,
      thresholdErrorRate,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_tablePresenter.isSelectedTracksEmpty) {
      return Container();
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: _buildCaption(context),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: DataTable(
            border: TableBorder.all(
              style: BorderStyle.solid,
              width: 1,
              color: kOrangeColorDisabled,
            ),
            horizontalMargin: 0,
            columnSpacing: 0,
            headingRowHeight: SplitTimeSheetTabView.HEADER_ROW_HEIGHT,
            dataRowMinHeight: SplitTimeSheetTabView.DATA_ROW_HEIGHT,
            dataRowMaxHeight: SplitTimeSheetTabView.DATA_ROW_HEIGHT,
            showBottomBorder: true,
            columns: _buildColumns(_tablePresenter.table),
            rows: _buildRows(context, _tablePresenter.table),
          ),
        ),
      ],
    );
  }

  Widget _buildCaption(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: constants.COLLAPSIBLE_MIN_WIDTH / 2),
          child: Text(L10n.getString("time_sheet_split_caption"), style: const TextStyle(fontSize: 12.0)),
        ),
        const SizedBox(width: 8),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(L10n.getString("time_sheet_split_caption_text"), style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color: kFirstPositionLegColor)),
                Text(L10n.getString("time_sheet_split_caption_first"), style: const TextStyle(fontSize: 14.0)),
              ],
            ),
            const SizedBox(width: 10),
            Row(
              children: [
                Text(L10n.getString("time_sheet_split_caption_text"), style: const TextStyle(fontSize: 14.0, color: Colors.black, backgroundColor: kMistakeLegColor)),
                Text(L10n.getString("time_sheet_split_caption_error"), style: const TextStyle(fontSize: 14.0)),
              ],
            ),
            const SizedBox(width: 10),
            Row(
              children: [
                Text(L10n.getString("time_sheet_split_caption_text"), style: const TextStyle(fontSize: 14.0, decoration: TextDecoration.underline)),
                Text(L10n.getString("time_sheet_split_caption_cheat"), style: const TextStyle(fontSize: 14.0)),
              ],
            ),
            const SizedBox(width: 10),
            Row(
              children: [
                Text(L10n.getString("time_sheet_split_caption_text"), style: const TextStyle(fontSize: 14.0, color: Colors.black, backgroundColor: kForbiddenPathColor)),
                Text(L10n.getString("time_sheet_split_caption_forbidden_path"), style: const TextStyle(fontSize: 14.0)),
              ],
            ),
          ],
        )
      ],
    );
  }

  List<DataColumn> _buildColumns(List<List<TimeCell>> table) {
    return List<DataColumn>.generate(
        table[0].length,
        (int i) => DataColumn(
              label: Expanded(
                child: Text(
                  table[0][i].firstText,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
        growable: false);
  }

  List<DataRow> _buildRows(BuildContext context, List<List<TimeCell>> table) {
    List<DataRow> rows = [];
    if (_tablePresenter.isSelectedTracksNotEmpty) {
      for (int i = 1; i < table.length; i++) {
        if (table[i][0].isNotEmpty) {
          List<DataCell> cells = [];
          for (int j = 0; j < table[i].length; j++) {
            Text text1 = _formatCellText(
              table[i][j].firstText,
              table[i][j].cumulativeTimeRank == 1,
              table[i][0].isCheater && j == 1,
              table[i][j].isMistakeMade,
              table[i][j].isUsedForbiddenPath,
            );
            Text text2 = _formatCellText(
              table[i][j].secondText,
              table[i][j].legTimeRank == 1,
              false,
              table[i][j].isMistakeMade,
              table[i][j].isUsedForbiddenPath,
            );
            cells.add(
              DataCell(
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  padding: const EdgeInsets.all(3.0),
                  alignment: (j == 1) ? Alignment.centerLeft : Alignment.center,
                  color: _getCellColor(table[i][j]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      text1,
                      text2,
                    ],
                  ),
                ),
              ),
            );
          }
          rows.add(DataRow(
            cells: cells,
            color: WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
          ));
        }
      }
    }
    return rows;
  }

  Color? _getCellColor(TimeCell cell) {
    if (cell.isMistakeMade) {
      return kMistakeLegColor;
    } else {
      if (cell.isUsedForbiddenPath) {
        return kForbiddenPathColor;
      } else {
        return null;
      }
    }
  }

  Text _formatCellText(
    String cellText,
    bool isBestTime,
    bool isCheater,
    bool isMadeMistake,
    bool isUsedForbiddenPath,
  ) {
    if (isBestTime) {
      if (isCheater && cellText != TimeCell.EMPTY_CELL) {
        return Text(
          cellText,
          style: const TextStyle(color: kFirstPositionLegColor, fontWeight: FontWeight.bold, decoration: TextDecoration.underline),
        );
      } else {
        return Text(
          cellText,
          style: const TextStyle(color: kFirstPositionLegColor, fontWeight: FontWeight.bold),
        );
      }
    } else {
      if (isCheater && cellText != TimeCell.EMPTY_CELL) {
        return Text(
          cellText,
          style: const TextStyle(decoration: TextDecoration.underline),
        );
      } else {
        if (isMadeMistake || isUsedForbiddenPath) {
          return Text(cellText, style: const TextStyle(color: Colors.black));
        } else {
          return Text(cellText);
        }
      }
    }
  }
}
