import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'cheater_helper.dart';
import 'forbidden_path_helper.dart';
import 'mistake_helper.dart';
import 'time_cell.dart';

const int FIRST_SPLIT_COLUMN_INDEX = 4;

class SplitTimeSheetModel {
  late final List<List<TimeCell>> _timeCells;
  late final List<int> _legDistances;

  List<List<TimeCell>> buildTable(
    List<GpsRoute> selectedTracks,
    List<Checkpoint> checkpoints,
    ValidationOrder format,
    int thresholdErrorRate,
  ) {
    bool isPresetOrder = format.isPresetOrder;
    _legDistances = computeLegStraightDistances_(checkpoints);
    selectedTracks.sort(((a, b) => a.model.totalTimeInMilliseconds.compareTo(b.model.totalTimeInMilliseconds)));
    int numberOfLegs = (isPresetOrder) ? FIRST_SPLIT_COLUMN_INDEX + _legDistances.length - 1 : FIRST_SPLIT_COLUMN_INDEX;
    int numberOfTracks = selectedTracks.length + 1;
    _timeCells = List.generate(numberOfTracks, (i) => List.generate(numberOfLegs, (int index) => TimeCell(), growable: false), growable: false);
    _buildTableHeader(isPresetOrder, selectedTracks);
    if (isPresetOrder) {
      _buildRankForEachControl(_timeCells);
      if (selectedTracks.length > 1) {
        MistakeHelper.findMistake(_timeCells, thresholdErrorRate);
        CheaterHelper.findCheater(_timeCells, checkpoints, selectedTracks);
        ForbiddenPathHelper.findForbiddenPath(_timeCells, checkpoints, selectedTracks);
      }
    }
    return _timeCells;
  }

  @visibleForTesting
  static List<int> computeLegStraightDistances_(List<Checkpoint> checkpoints) {
    List<int> legDistances = [0];
    for (int i = 1; i < checkpoints.length; i++) {
      var distance = GeodesicPoint.distanceBetweenInMeter(
        checkpoints[i - 1].latitude,
        checkpoints[i - 1].longitude,
        checkpoints[i].latitude,
        checkpoints[i].longitude,
      );
      legDistances.add(distance.toInt());
    }
    return legDistances;
  }

  void _buildTableHeader(bool isPresetOrder, List<GpsRoute> selectedTracks) {
    for (int row = 0; row < _timeCells.length; row++) {
      for (int column = 0; column < _timeCells[row].length; column++) {
        if (row == 0) {
          _buildHeaderCell(row, column, isPresetOrder);
        } else {
          GpsRoute track = selectedTracks[row - 1];
          if (column < FIRST_SPLIT_COLUMN_INDEX) {
            _buildIdentificationCell(row, column, track, selectedTracks);
          } else if (isPresetOrder) {
            _buildLegTimeCell(row, column, track);
          }
        }
      }
    }
  }

  void _buildHeaderCell(int row, int column, bool isPresetOrder) {
    if (column == 0) {
      _timeCells[row][column].setTitle(L10n.getString("time_sheet_split_rank"));
    } else if (column == 1) {
      _timeCells[row][column].setTitle(L10n.getString("time_sheet_split_nickname"));
    } else if (column == 2) {
      _timeCells[row][column].setTitle(L10n.getString("time_sheet_split_time"));
    } else if (column == 3) {
      _timeCells[row][column].setTitle(L10n.getString("time_sheet_split_time_behind"));
    } else if (column > 3 && isPresetOrder) {
      _buildControlNumberDistanceCell(row, column);
    }
  }

  void _buildControlNumberDistanceCell(int row, int column) {
    final int legIndex = column - FIRST_SPLIT_COLUMN_INDEX + 1;
    final String legIndexName;
    if (column < _timeCells[row].length - 1) {
      legIndexName = legIndex.toString();
    } else {
      legIndexName = L10n.getString("time_sheet_checkpoint_finish");
    }
    _timeCells[row][column].setLegIdentification(legIndexName, _legDistances[legIndex]);
  }

  void _buildIdentificationCell(int row, int column, GpsRoute gpsRoute, List<GpsRoute> selectedTracks) {
    if (column == 0) {
      _timeCells[row][0].setTitle(row.toString());
    } else if (column == 1) {
      _timeCells[row][1].setTitle(gpsRoute.model.nickname);
    } else if (column == 2) {
      _timeCells[row][2].setUniqueTime(gpsRoute.model.totalTimeInMilliseconds);
    } else {
      _buildTimeBehindCell(row, column, selectedTracks);
    }
  }

  void _buildTimeBehindCell(int row, int column, List<GpsRoute> selectedTracksForOrder) {
    if (row > 1) {
      var timeDiffWithBest = selectedTracksForOrder[row - 1].model.totalTimeInMilliseconds - selectedTracksForOrder[0].model.totalTimeInMilliseconds;
      var timeDiffWithPrevious = selectedTracksForOrder[row - 1].model.totalTimeInMilliseconds - selectedTracksForOrder[row - 2].model.totalTimeInMilliseconds;
      _timeCells[row][column].setTimes(timeDiffWithBest, timeDiffWithPrevious);
    } else {
      _timeCells[row][column].setUniqueTime(0);
    }
  }

  void _buildLegTimeCell(int row, int column, GpsRoute gpsRoute) {
    int index = column - FIRST_SPLIT_COLUMN_INDEX;
    int previousPunchTime;
    // Workaround: in case planner change course after the track has been done..
    if (index >= gpsRoute.model.punchTimes.length - 1) {
      _timeCells[row][column].setTimes(0, 0);
    } else {
      if (index == 0) {
        previousPunchTime = 0;
      } else {
        previousPunchTime = gpsRoute.model.punchTimes[index].timestampInMillisecond;
      }
      var time = gpsRoute.model.punchTimes[index + 1].timestampInMillisecond;
      var legTime = time - previousPunchTime;
      if (legTime <= 0) {
        legTime = 0;
        time = 0;
      }
      _timeCells[row][column].setTimes(time, legTime);
    }
  }

  void _buildRankForEachControl(List<List<TimeCell>> timeCells) {
    for (int column = FIRST_SPLIT_COLUMN_INDEX; column < timeCells[0].length; column++) {
      List<int> cumulativeTimeRanks = [];
      List<int> legTimeRanks = [];
      for (int row = 1; row < timeCells.length; row++) {
        if (timeCells[row][column].legTime != 0) {
          cumulativeTimeRanks.add(timeCells[row][column].cumulativeTime);
          legTimeRanks.add(timeCells[row][column].legTime);
        }
      }
      cumulativeTimeRanks.sort();
      legTimeRanks.sort();
      for (int row = 1; row < timeCells.length; row++) {
        timeCells[row][column].setTimesRank(
          1 + cumulativeTimeRanks.indexOf(timeCells[row][column].cumulativeTime),
          1 + legTimeRanks.indexOf(timeCells[row][column].legTime),
        );
      }
    }
  }

  static List<List<double>> calculateActualDistances(int numberOfLegs, List<GpsRoute> selectedTracks) {
    List<List<double>> distances = [];
    for (var i = 1; i < numberOfLegs; i++) {
      List<double> legDistance = [];
      for (GpsRoute track in selectedTracks) {
        legDistance.add(track.model.calculateLegActualDistance(i));
      }
      distances.add(legDistance);
    }
    return distances;
  }
}
