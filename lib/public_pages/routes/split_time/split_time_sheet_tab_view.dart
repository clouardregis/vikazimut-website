// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'free_order_table_view.dart';
import 'preset_order_table_view.dart';
import 'settings_side_bar.dart';
import 'split_time_sheet_presenter.dart';

const Color kFirstPositionLegColor = Color(0xFF2962FF);
const Color kMistakeLegColor = Color(0xfff8d7da);
const Color kForbiddenPathColor = Color(0xFfffcc00);

class SplitTimeSheetTabView extends StatelessWidget {
  static const double DATA_ROW_HEIGHT = 50;
  static const double HEADER_ROW_HEIGHT = 45;
  static const double FONT_SIZE = 15;
  static final SplitTimeSettings _settings = SplitTimeSettings();
  final ScrollController horizontalScroll = ScrollController();
  final ScrollController verticalScroll = ScrollController();

  final RouteListPagePresenter routePresenter;
  final SplitTimePresenter _presenter = SplitTimePresenter();

  SplitTimeSheetTabView({required this.routePresenter});

  @override
  Widget build(BuildContext context) {
    if (routePresenter.selectedTracks.isNotEmpty) {
      return ValueListenableBuilder<int>(
          valueListenable: _settings.thresholdErrorRate,
          builder: (BuildContext context, int thresholdErrorRate, Widget? child) {
            return Stack(
              alignment: Alignment.topRight,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: SizedBox(
                      height: double.infinity,
                      child: Scrollbar(
                        controller: verticalScroll,
                        thumbVisibility: true,
                        trackVisibility: true,
                        child: Scrollbar(
                          controller: horizontalScroll,
                          thumbVisibility: true,
                          trackVisibility: true,
                          notificationPredicate: (ScrollNotification notification) => notification.depth == 1,
                          child: SingleChildScrollView(
                            primary: false,
                            scrollDirection: Axis.vertical,
                            controller: verticalScroll,
                            child: ScrollConfiguration(
                              // Workaround to remove the second scrollbar below the table
                              behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                controller: horizontalScroll,
                                primary: false,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: PresetOrderTableView(routePresenter, thresholdErrorRate, _presenter.presetOrderPresenter),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FreeOrderTableView(routePresenter, thresholdErrorRate, _presenter.freeOrderPresenter),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SettingsSideBar(routePresenter, _settings, _presenter),
              ],
            );
          });
    } else {
      return Center(child: Text(L10n.getString("time_sheet_split_time_no_orienteer")));
    }
  }
}
