// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'split_time_sheet_presenter.dart';
import 'split_time_sheet_tab_view.dart';

class FreeOrderTableView extends StatelessWidget {
  final RouteListPagePresenter _routePresenter;
  final SplitTimeSheetPresenter _tablePresenter;

  FreeOrderTableView(this._routePresenter, int thresholdErrorRate, this._tablePresenter) {
    _tablePresenter.buildTable(
      _routePresenter.selectedTracks,
      _routePresenter.courseData!.checkpoints,
      ValidationOrder.FREE_ORDER,
      thresholdErrorRate,
    );
  }

  @override
  Widget build(BuildContext context) {
    if (_tablePresenter.isSelectedTracksEmpty) {
      return Container();
    }
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: DataTable(
        border: TableBorder.all(
          style: BorderStyle.solid,
          width: 1,
          color: kOrangeColorDisabled,
        ),
        horizontalMargin: 10,
        columnSpacing: 5,
        headingRowHeight: SplitTimeSheetTabView.HEADER_ROW_HEIGHT,
        dataRowMinHeight: SplitTimeSheetTabView.DATA_ROW_HEIGHT,
        dataRowMaxHeight: SplitTimeSheetTabView.DATA_ROW_HEIGHT,
        showBottomBorder: true,
        columns: _buildColumns(),
        rows: _buildRows(context, _routePresenter),
      ),
    );
  }

  List<DataColumn> _buildColumns() {
    return List<DataColumn>.generate(
        _tablePresenter.table[0].length,
        (int i) => DataColumn(
              label: Expanded(
                child: Text(
                  _tablePresenter.table[0][i].firstText,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
        growable: false);
  }

  List<DataRow> _buildRows(BuildContext context, RouteListPagePresenter presenter) {
    List<DataRow> rows = [];
    if (_tablePresenter.isSelectedTracksNotEmpty) {
      for (int row = 1; row < _tablePresenter.table.length; row++) {
        List<DataCell> cells = [];
        for (int column = 0; column < _tablePresenter.table[row].length; column++) {
          Widget contents = Padding(
            padding: const EdgeInsets.all(3.0),
            child: Text(
              _tablePresenter.table[row][column].firstText,
              textAlign: TextAlign.center,
            ),
          );
          if (column != 1) {
            contents = Center(
              child: contents,
            );
          }
          cells.add(DataCell(contents));
        }
        rows.add(DataRow(
          cells: cells,
          color: WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
        ));
      }
    }
    return rows;
  }
}
