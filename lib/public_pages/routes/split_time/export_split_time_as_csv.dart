import 'package:flutter/foundation.dart';

import 'split_time_sheet_presenter.dart';
import 'time_cell.dart';

class ExportSplitTimeAsCsv {
  static String convertToCsv(SplitTimePresenter presenter) {
    String csvContents = "";
    if (presenter.presetOrderPresenter.isSelectedTracksNotEmpty) {
      csvContents += _convertPresetOrderToCsv(presenter.presetOrderPresenter.table);
    }
    if (presenter.freeOrderPresenter.isSelectedTracksNotEmpty) {
      csvContents += _convertFreeOrderToCsv(presenter.freeOrderPresenter.table);
    }
    return csvContents;
  }

  static String _convertPresetOrderToCsv(List<List<TimeCell>> presetOrderTable) {
    String csvContents = "";
    // Header
    for (var i = 0; i < presetOrderTable[0].length; ++i) {
      csvContents += "${removeEndOfLine(presetOrderTable[0][i].firstText)};";
    }
    // Rows
    for (int i = 1; i < presetOrderTable.length; i++) {
      if (presetOrderTable[i][0].isNotEmpty) {
        csvContents += "\n";
        for (int j = 0; j < presetOrderTable[i].length; j++) {
          csvContents += "${presetOrderTable[i][j].firstText};";
        }
        csvContents += "\n";
        for (int j = 0; j < presetOrderTable[i].length; j++) {
          csvContents += "${presetOrderTable[i][j].secondText};";
        }
      }
    }
    csvContents += "\n";
    return csvContents;
  }

  static String _convertFreeOrderToCsv(List<List<TimeCell>> freeOrderTable) {
    String csvContents = "";
    // Header
    for (var i = 0; i < freeOrderTable[0].length; ++i) {
      csvContents += "${freeOrderTable[0][i].firstText};";
    }
    // Rows
    for (int row = 1; row < freeOrderTable.length; row++) {
      csvContents += "\n";
      for (int column = 0; column < freeOrderTable[row].length; column++) {
        csvContents += "${freeOrderTable[row][column].firstText};";
      }
    }
    return csvContents;
  }

  @visibleForTesting
  static String removeEndOfLine(String string) {
    return string.replaceAll("\n", " ");
  }
}
