import 'package:vikazimut_website/utils/time.dart';

class TimeCell {
  static const String EMPTY_CELL = "--:--";

  bool _isEmpty = true;
  String _headerText = "";
  String _secondText = "";
  int _cumulativeTime = -1;
  int _legTimes = -1;
  int _cumulativeTimeRank = -1;
  int _legTimeRank = -1;

  bool isMistakeMade = false;
  bool isCheater = false;
  bool isUsedForbiddenPath = false;

  bool get isNotEmpty => !_isEmpty;

  int get cumulativeTimeRank => _cumulativeTimeRank;

  int get legTimeRank => _legTimeRank;

  int get cumulativeTime => _cumulativeTime;

  int get legTime => _legTimes;

  String get firstText => _headerText;

  String get secondText => _secondText;

  void _reset() {
    _isEmpty = true;
    _headerText = "";
    _secondText = "";
    _cumulativeTime = 0;
    _legTimes = 0;
    _cumulativeTimeRank = 0;
    _legTimeRank = 0;
    isCheater = false;
    isMistakeMade = false;
    isUsedForbiddenPath = false;
  }

  void setTitle(String title) {
    _reset();
    _isEmpty = false;
    _headerText = title;
  }

  void setUniqueTime(int oneTime) {
    _reset();
    _isEmpty = false;
    _headerText = timeToString(oneTime, withHour: false);
  }

  void setLegIdentification(String legIndex, int legStraightDistance) {
    _reset();
    _isEmpty = false;
    _headerText = "$legIndex\n$legStraightDistance m";
  }

  void setTimes(int time, int intervalTime) {
    _reset();
    _isEmpty = false;
    _cumulativeTime = time;
    _legTimes = intervalTime;

    if (_cumulativeTime <= 0) {
      _headerText = EMPTY_CELL;
      _secondText = EMPTY_CELL;
      _legTimeRank = -1;
      _cumulativeTimeRank = -1;
    } else {
      _headerText = timeToString(_cumulativeTime, withHour: false);
      _secondText = timeToString(_legTimes, withHour: false);
    }
  }

  void setTimesRank(int cumulativeTimeRank, int legTimeRank) {
    _cumulativeTimeRank = cumulativeTimeRank;
    _legTimeRank = legTimeRank;
    if (_cumulativeTime <= 0) {
      _headerText = EMPTY_CELL;
      _secondText = EMPTY_CELL;
      _legTimeRank = -1;
      _cumulativeTimeRank = -1;
    } else {
      _headerText = "${timeToString(_cumulativeTime, withHour: false)} ($cumulativeTimeRank)";
      _secondText = "${timeToString(_legTimes, withHour: false)} ($legTimeRank)";
    }
  }
}
