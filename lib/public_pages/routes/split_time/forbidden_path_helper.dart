import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/utils/utils.dart';

import 'split_time_sheet_model.dart';
import 'time_cell.dart';

class ForbiddenPathHelper {
  static const int MIN_LEG_DISTANCE_IN_METER = 20;

  static void findForbiddenPath(List<List<TimeCell>> timeCells, List<Checkpoint> checkpoints, List<GpsRoute> selectedTracks) {
    List<List<double>> distances = SplitTimeSheetModel.calculateActualDistances(checkpoints.length, selectedTracks);
    for (int column = FIRST_SPLIT_COLUMN_INDEX; column < timeCells[0].length; column++) {
      List<double> legDistances = distances[column - FIRST_SPLIT_COLUMN_INDEX];
      final medianDistance = computeMedianValue(legDistances);
      if (medianDistance > MIN_LEG_DISTANCE_IN_METER) {
        for (int row = 1; row < timeCells.length; row++) {
          final timeCell = timeCells[row][column];
          timeCell.isUsedForbiddenPath = timeCell.legTime > 0 && legDistances[row - 1] < minDistance(legDistances, row - 1) ~/ 2;
        }
      }
    }
  }

  @visibleForTesting
  static double minDistance(List<double> tab, int index) {
    assert(tab.isNotEmpty && index >= 0 || index < tab.length);
    double minDistance = 10000000;
    for (var i = 0; i < tab.length; ++i) {
      if (i != index && tab[i] < minDistance) {
        minDistance = tab[i];
      }
    }
    return minDistance;
  }
}
