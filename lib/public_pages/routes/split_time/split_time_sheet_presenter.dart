import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

import 'split_time_sheet_model.dart';
import 'time_cell.dart';

class SplitTimePresenter {
  final SplitTimeSheetPresenter freeOrderPresenter = SplitTimeSheetPresenter();
  final SplitTimeSheetPresenter presetOrderPresenter = SplitTimeSheetPresenter();
}

class SplitTimeSheetPresenter {
  final List<GpsRoute> _filteredSelectedTracks = [];
  late List<List<TimeCell>> table;

  bool get isSelectedTracksNotEmpty => _filteredSelectedTracks.isNotEmpty;

  bool get isSelectedTracksEmpty => _filteredSelectedTracks.isEmpty;

  void buildTable(List<GpsRoute> selectedTracks, List<Checkpoint> checkpoints, RouteFormat format, int thresholdErrorRate) {
    _selectTracksOfCourseFormat(selectedTracks, format);
    table = SplitTimeSheetModel().buildTable(_filteredSelectedTracks, checkpoints, format, thresholdErrorRate);
  }

  void _selectTracksOfCourseFormat(List<GpsRoute> selectedTracks, RouteFormat format) {
    _filteredSelectedTracks.clear();
    for (int i = 0; i < selectedTracks.length; i++) {
      if (selectedTracks[i].model.courseFormat == format) {
        _filteredSelectedTracks.add(selectedTracks[i]);
      }
    }
  }
}
