import 'package:flutter/foundation.dart';

import 'split_time_sheet_model.dart';
import 'time_cell.dart';

class MistakeHelper {
  static void findMistake(List<List<TimeCell>> timeCells, int thresholdErrorRate) {
    if (timeCells.length < 2 || thresholdErrorRate == 0) {
      return;
    }
    // 1. For each leg, search best time
    List<int> bestTimes = [];
    for (int column = FIRST_SPLIT_COLUMN_INDEX; column < timeCells[0].length; column++) {
      bestTimes.add(bestTime(timeCells, column));
    }

    // 2. For each course, calculate the median of the gap
    //    with the best time of each leg
    List<double> errorMedians = [];
    for (int row = 1; row < timeCells.length; row++) {
      List<double> errorPercentages = [];
      for (int column = FIRST_SPLIT_COLUMN_INDEX; column < timeCells[row].length; column++) {
        final timeCell = timeCells[row][column];
        if (timeCell.legTimeRank != 1 && timeCell.legTime > 0) {
          final bestTime = bestTimes[column - FIRST_SPLIT_COLUMN_INDEX];
          errorPercentages.add((timeCell.legTime - bestTime) / bestTime);
        }
      }
      if (errorPercentages.isEmpty) {
        return;
      }
      errorPercentages.sort();
      int indexMedian = ((errorPercentages.length / 2.0) - 0.5).toInt();
      errorMedians.add(errorPercentages[indexMedian]);
    }

    // 3. Compute overcost errors for each cell
    final errorCoefficient = 1 + thresholdErrorRate / 100.0;
    for (int row = 1; row < timeCells.length; row++) {
      for (int column = FIRST_SPLIT_COLUMN_INDEX; column < timeCells[row].length; column++) {
        final timeCell = timeCells[row][column];
        if (timeCell.legTimeRank != 1 && timeCell.legTime > 0) {
          final overcostInPercentage = 1.0 + errorMedians[row - 1] * errorCoefficient;
          final bestTime = bestTimes[column - FIRST_SPLIT_COLUMN_INDEX];
          if (timeCell.legTime > bestTime * overcostInPercentage) {
            timeCell.isMistakeMade = true;
          }
        }
      }
    }
  }

  @visibleForTesting
  static int bestTime(List<List<TimeCell>> timeCells, int indexLeg) {
    for (int row = 1; row < timeCells.length; row++) {
      final TimeCell timeCell = timeCells[row][indexLeg];
      if (timeCell.legTimeRank == 1 && timeCell.legTime > 0) {
        return timeCell.legTime;
      }
    }
    // Should never reach this line
    return 0;
  }
}
