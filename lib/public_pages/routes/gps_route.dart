import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/flutter_map_layers/custom_map_tappable_polyline.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/leg.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/speed_color_handler.dart';

enum ValidationOrder {
  PRESET_ORDER,
  FREE_ORDER,
  UNSUPERVISED_PRESET_ORDER;

  static ValidationOrder toEnum(int? index) {
    if (index == null || index >= ValidationOrder.values.length) {
      return ValidationOrder.values[0];
    } else {
      return ValidationOrder.values[index];
    }
  }

  bool get isFreeOrder => this == FREE_ORDER;

  bool get isPresetOrder => this == PRESET_ORDER || this == UNSUPERVISED_PRESET_ORDER;

  bool equals(ValidationOrder order) {
    if (order.isFreeOrder) {
      return this == order;
    } else {
      return this == PRESET_ORDER || this == UNSUPERVISED_PRESET_ORDER;
    }
  }
}

extension IntExtensions on int {
  bool get isPresetOrder => this == ValidationOrder.PRESET_ORDER.index || this == ValidationOrder.UNSUPERVISED_PRESET_ORDER.index;

  bool get isFreeOrder => this == ValidationOrder.FREE_ORDER.index;
}

String courseFormatToL10nKey(int format) {
  return format == 1 ? "constant_route_free" : "constant_route_preset";
}

class GpsRoute {
  static const double MS_TO_KMH_CONVERSION = 3.6;

  final int id;
  final TrackModel model;
  int displayedFormat = 0;
  String hexColor = "00000000";

  GpsRoute(this.id, this.model);

  ValidationOrder get courseFormat => model.courseFormat;

  Color get color {
    String hexString = hexColor;
    return Color(int.parse(hexString, radix: 16));
  }

  String get nickname => model.nickname;

  String get time => model.getTime();

  String get filename => model.getFilename();

  List<GeodesicPoint> get route => model.route ?? [];

  List<Leg> get legs => model.legs;

  String getGpxContents() {
    const localHeader = "<gpx version=\"1.0\">";
    var contents = model.buildGpxTrace();
    if (contents.startsWith(localHeader)) {
      contents = contents.substring(localHeader.length);
      var header = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><gpx version=\"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"Vikazimut\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">";
      contents = header + contents;
    }
    return contents;
  }

  Future<void> loadRouteData(IdentifiedUser? user, List<Checkpoint> checkpoints) async {
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    try {
      final response = await http.get(
        Uri.parse("${constants.API_SERVER}/courses/track/${model.id}"),
        headers: headers,
      );
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        model.loadRoute(data, checkpoints);
      } else {
        model.route = null;
      }
    } catch (_) {
      model.route = null;
    }
  }

  Future<void> loadBestRouteData(IdentifiedUser? user, int courseId, List<Checkpoint> checkpoints) async {
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    try {
      final response = await http.get(
        Uri.parse("${constants.API_SERVER}/courses/create_superman_track/$courseId"),
        headers: headers,
      );
      if (response.statusCode == HttpStatus.ok) {
        var data = jsonDecode(response.body);
        model.loadModel(data["totalTime"], data["punchTimes"]);
        model.loadRoute(data["route"], checkpoints);
      } else {
        model.route = null;
      }
    } catch (_) {
      model.route = null;
    }
  }

  void deleteRouteData() {
    model.route = null;
  }

  static List<List<LatLng>> getAllTracksAsGpsPoints(
    List<GpsRoute> selectedTracks, {
    required int legIndex,
  }) {
    List<List<LatLng>> tracks = [];
    for (int i = 0; i < selectedTracks.length; i++) {
      final TrackModel model = selectedTracks[i].model;
      final List<GeodesicPoint>? waypoints;
      if (legIndex == 0) {
        waypoints = model.route;
      } else {
        waypoints = model.getWaypointsOfLeg(legIndex);
      }
      if (waypoints != null) {
        List<LatLng> points = [];
        for (int j = 0; j < waypoints.length; j++) {
          //       if (route[j].timestampInMillisecond >= limit.minTime && route[j].timestampInMillisecond <= limit.maxTime) {
          points.add(LatLng(waypoints[j].latitude, waypoints[j].longitude));
        }
        //     }
        tracks.add(points);
      }
    }
    return tracks;
  }

  static List<List<PunchPosition>> getAllPunchesAsPositions(List<GpsRoute> selectedTracks, {required int legIndex}) {
    if (legIndex > 0) {
      return List.generate(selectedTracks.length, (index) => []);
    } else {
      List<List<PunchPosition>> punchPositions = [];
      for (int i = 0; i < selectedTracks.length; i++) {
        List<GeodesicPoint> route = selectedTracks[i].route;
        if (route.isNotEmpty) {
          List<PunchTime> punchTimes = selectedTracks[i].model.punchTimes;
          List<PunchTime> punches = [];
          for (int k = 0; k < punchTimes.length; k++) {
            if (punchTimes[k].timestampInMillisecond >= 0) {
              punches.add(punchTimes[k]);
            }
          }
          if (punches.isEmpty) {
            // Should never happen -> defensive programming
            punchPositions.add([]);
            continue;
          }
          punches.sort((PunchTime a, PunchTime b) => a.timestampInMillisecond.compareTo(b.timestampInMillisecond));
          int k = 0;
          while (k < punches.length && punches[k].timestampInMillisecond < 0) {
            k++;
          }
          if (k < punchTimes.length) {
            List<PunchPosition> points = [];
            List<GeodesicPoint> route = selectedTracks[i].route;
            for (int j = 0; j < route.length; j++) {
              if (route[j].timestampInMillisecond >= punches[k].timestampInMillisecond) {
                if (j == 0 || route[j].timestampInMillisecond == punches[k].timestampInMillisecond) {
                  points.add(PunchPosition(LatLng(route[j].latitude, route[j].longitude), punches[k].forced));
                } else {
                  var lat1 = route[j - 1].latitude;
                  var lon1 = route[j - 1].longitude;
                  var t1 = route[j - 1].timestampInMillisecond;
                  var lat2 = route[j].latitude;
                  var lon2 = route[j].longitude;
                  var t2 = route[j].timestampInMillisecond;
                  var tx = punches[k].timestampInMillisecond;
                  var ratio = (tx - t1) / (t2 - t1);
                  var lat = ratio * (lat2 - lat1) + lat1;
                  var lon = ratio * (lon2 - lon1) + lon1;
                  points.add(PunchPosition(LatLng(lat, lon), punches[k].forced));
                }

                if (++k >= punches.length) {
                  break;
                }
              }
            }
            punchPositions.add(points);
          }
        }
      }
      return punchPositions;
    }
  }

  static List<List<int>> getAllSpeedsAsColor(
    Discipline? discipline,
    List<GpsRoute> selectedTracks, {
    required int legIndex,
  }) {
    var speedColorHandler = SpeedColorHandlerFactory.build(discipline);
    List<List<int>> allColoredRoutes = [];
    for (int i = 0; i < selectedTracks.length; i++) {
      final TrackModel model = selectedTracks[i].model;
      final List<GeodesicPoint>? waypoints;
      if (legIndex == 0) {
        waypoints = model.route;
      } else {
        waypoints = model.getWaypointsOfLeg(legIndex);
      }
      if (waypoints != null) {
        allColoredRoutes.add(_getRouteSpeedsAsColor(speedColorHandler, waypoints));
      }
    }
    return allColoredRoutes;
  }

  static List<int> _getRouteSpeedsAsColor(
    SpeedColorHandler speedColorHandler,
    List<GeodesicPoint> route,
  ) {
    const int filterHalfSize = 2;
    var speeds = filterSpeeds_(route, filterHalfSize);
    return colorizeSplitsFromSpeed_(speedColorHandler, speeds);
  }

  @visibleForTesting
  static List<int> colorizeSplitsFromSpeed_(
    SpeedColorHandler speedColorHandler,
    List<double> speeds,
  ) {
    return speeds.map((speed) => speedColorHandler.colorFromVelocity(speed)).toList();
  }

  @visibleForTesting
  static List<double> filterSpeeds_(List<GeodesicPoint> route, int filterHalfSize) {
    if (route.isEmpty) {
      return [];
    }
    List<double> speeds = [0];
    // Start of the track: no filtering
    for (int i = 1; i <= math.min(filterHalfSize + 1, route.length - 1); i++) {
      var distanceInMeter = GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
      var timeInMilliSecond = route[i].timestampInMillisecond - route[i - 1].timestampInMillisecond;
      final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMilliSecond);
      speeds.add(velocityInKmH);
    }

    if (route.length > 2 * filterHalfSize + 1) {
      // 1 Filter speeds using simple moving average
      double distanceInMeter = 0.0;
      for (int i = 1; i <= filterHalfSize * 2 + 1; i++) {
        distanceInMeter += GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
      }
      for (int i = filterHalfSize + 2; i < route.length - filterHalfSize - 1; i++) {
        final last = i + filterHalfSize;
        final first = i - filterHalfSize - 1;
        var distance0 = GeodesicPoint.distanceBetweenInMeter(route[first - 1].latitude, route[first - 1].longitude, route[first].latitude, route[first].longitude);
        var distance1 = GeodesicPoint.distanceBetweenInMeter(route[last - 1].latitude, route[last - 1].longitude, route[last].latitude, route[last].longitude);
        distanceInMeter += -distance0 + distance1;
        int timeInMs = route[last].timestampInMillisecond - route[first].timestampInMillisecond;
        final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMs);
        speeds.add(velocityInKmH);
      }
    }

    if (route.length > filterHalfSize + 1) {
      // End of the track: no filtering
      for (int i = math.max(route.length - filterHalfSize - 1, filterHalfSize + 1 + 1); i < route.length; i++) {
        var distanceInMeter = GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
        var timeInMilliSecond = route[i].timestampInMillisecond - route[i - 1].timestampInMillisecond;
        final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMilliSecond);
        speeds.add(velocityInKmH);
      }
    }
    return speeds;
  }

  @visibleForTesting
  static double computeVelocityInKmH_(double distanceInMeter, int timeInMilliSecond) {
    if (timeInMilliSecond == 0) {
      return 0;
    } else {
      return MS_TO_KMH_CONVERSION * 1000 * distanceInMeter / timeInMilliSecond;
    }
  }
}
