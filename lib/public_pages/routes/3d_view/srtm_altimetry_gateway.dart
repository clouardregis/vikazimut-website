import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:archive/archive.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/utils/utils.dart';

import 'course_3d_model.dart';
import 'image_size.dart';

class SrtmAltimetryGateway {
  static const String GATEWAY_URL = "${constants.SERVER}/data/dem-file/";
  static const int NUMBER_OF_ROWS_AND_COLUMNS_IN_FILE = 3601;

  static Future<Heightmap> fetchAltitudesForRegion(LatLonBox region, int orienteeringMapWidth, int orienteeringMapHeight) async {
    final double widthHeightRatio = orienteeringMapWidth / orienteeringMapHeight;
    final ImageSize altitudeMapSize = ImageSize(
      math.sqrt(Course3dModel.MAX_POINT_COUNT * Course3dModel.NUMBER_OF_PARTS * widthHeightRatio).floor(),
      math.sqrt((Course3dModel.MAX_POINT_COUNT * Course3dModel.NUMBER_OF_PARTS) / widthHeightRatio).floor(),
    );

    final double latN = region.north;
    final double latS = region.south;
    final double lonE = region.east;
    final double lonW = region.west;
    final deltaLat = (latN - latS) / altitudeMapSize.height;
    final deltaLon = (lonE - lonW) / altitudeMapSize.width;

    // Warning: Points are upside down: from north to south.
    final latitudeCenter = (latN + latS) / 2;
    final longitudeCenter = (lonE + lonW) / 2;
    final angleInRadian = deg2rad(region.rotation);
    final cosAngle = math.cos(angleInRadian);
    final sinAngle = math.sin(angleInRadian);
    List<_Position> points = [];
    for (var i = 0; i < altitudeMapSize.height; i++) {
      final latitude = latN - i * deltaLat;
      for (var j = 0; j < altitudeMapSize.width; j++) {
        var longitude = lonW + j * deltaLon;
        _Position point = _rotate(longitude, latitude, cosAngle, sinAngle, longitudeCenter, latitudeCenter);
        points.add(point);
      }
    }

    List<String> tileNames = getAllTileNamesCoveringRegion(region);
    List<Future<Uint8List?>> allRequestsToGateway = [];
    for (var tileName in tileNames) {
      allRequestsToGateway.add(_getAltitudeTileFromGateway(tileName));
    }
    List<Uint8List?> dataParts = await Future.wait<Uint8List?>(allRequestsToGateway);

    Map<String, ByteData> tiles = {};
    for (var i = 0; i < dataParts.length; ++i) {
      final Uint8List? mapData = dataParts[i];
      if (mapData == null) {
        return Heightmap([], const ImageSize(0, 0));
      }
      final ByteData data = ByteData.view(mapData.buffer);
      tiles[tileNames[i]] = data;
    }
    var data = _extractAltitudesFromTile(tiles, points);
    return Heightmap(data, altitudeMapSize);
  }

  static _Position _rotate(final double longitude, final double latitude, final double cosAngle, final double sinAngle, final double longitudeCenter, final double latitudeCenter) {
    final longitudeRotated = -(latitude - latitudeCenter) * sinAngle + (longitude - longitudeCenter) * cosAngle + longitudeCenter;
    final latitudeRotated = (latitude - latitudeCenter) * cosAngle + (longitude - longitudeCenter) * sinAngle + latitudeCenter;
    return _Position(longitude: longitudeRotated, latitude: latitudeRotated);
  }

  static Future<Uint8List?> _getAltitudeTileFromGateway(String mapName) async {
    final response = await http.get(Uri.parse("$GATEWAY_URL$mapName.SRTMGL1.hgt.zip"));
    try {
      if (response.statusCode == HttpStatus.ok) {
        final Archive archive = ZipDecoder().decodeBytes(response.bodyBytes);
        for (var file in archive.files) {
          if (file.isFile) {
            final extension = path.extension(file.name).toLowerCase();
            if (extension == ".hgt") {
              return file.content;
            }
          }
        }
        return null;
      } else {
        return null;
      }
    } catch (_) {
      return null;
    }
  }

  @visibleForTesting
  static double convertLatitudeToMapRowIndex(double latitude) => NUMBER_OF_ROWS_AND_COLUMNS_IN_FILE - 1.0 - (latitude - latitude.floor()).abs() * NUMBER_OF_ROWS_AND_COLUMNS_IN_FILE;

  @visibleForTesting
  static double convertLongitudeToMapColumnIndex(double longitude) => (longitude - longitude.floor()).abs() * NUMBER_OF_ROWS_AND_COLUMNS_IN_FILE;

  static List<double> _extractAltitudesFromTile(Map<String, ByteData> tiles, List<_Position> points) {
    final List<double> values = List.filled(points.length, 0, growable: false);
    for (var i = 0; i < points.length; ++i) {
      _Position point = points[i];
      final double col = convertLongitudeToMapColumnIndex(point.longitude);
      final double row = convertLatitudeToMapRowIndex(point.latitude);
      final ByteData data = _getTileData(tiles, point);
      values[i] = _getPixelValueFromAltitudeMap(data, col, row, NUMBER_OF_ROWS_AND_COLUMNS_IN_FILE);
    }
    return values;
  }

  static ByteData _getTileData(Map<String, ByteData> tiles, _Position point) {
    var tileName = getTileName(longitude: point.longitude, latitude: point.latitude);
    return tiles[tileName]!;
  }

  @visibleForTesting
  static double toInt16(ByteData data, int y, int x, int width) => data.getInt16((y * width + x) * 2, Endian.big).toDouble();

  static double _getPixelValueFromAltitudeMap(ByteData heightmap, double gx, double gy, int tileSize) {
    double value(ByteData heightMap, int y, int x) => toInt16(heightMap, y, x, tileSize);
    double linearInterpolation(double s, double e, double t) => s + (e - s) * t;
    double bilinearInterpolation(final double c00, double c10, double c01, double c11, double tx, double ty) => linearInterpolation(linearInterpolation(c00, c10, tx), linearInterpolation(c01, c11, tx), ty);

    final int gxi = gx.toInt();
    final int gyi = gy.toInt();
    int gxi1 = (gxi < tileSize - 1) ? gxi + 1 : gxi;
    int gyi1 = (gyi < tileSize - 1) ? gyi + 1 : gyi;
    final double c00 = value(heightmap, gyi, gxi);
    final double c10 = value(heightmap, gyi, gxi1);
    final double c01 = value(heightmap, gyi1, gxi);
    final double c11 = value(heightmap, gyi1, gxi1);
    return bilinearInterpolation(c00, c10, c01, c11, gx - gxi, gy - gyi);
  }

  @visibleForTesting
  static List<String> getAllTileNamesCoveringRegion(LatLonBox region) {
    final int latS = region.south.floor();
    final int latN = region.north.floor();
    final int lonE = region.east.floor();
    final int lonW = region.west.floor();

    List<String> tiles = [];
    for (var lat = latS; lat <= latN; ++lat) {
      for (var lon = lonW; lon <= lonE; ++lon) {
        tiles.add(getTileName(longitude: lon.toDouble() + 0.1, latitude: lat.toDouble() + 0.1));
      }
    }
    return tiles;
  }

  @visibleForTesting
  static String getTileName({required double longitude, required double latitude}) {
    String name;
    if (latitude >= 0) {
      name = sprintf("N%02d", [(latitude.toInt())]);
    } else {
      name = sprintf("S%02d", [1 - latitude.toInt()]);
    }
    if (longitude >= 0) {
      name += sprintf("E%03d", [longitude.toInt()]);
    } else {
      name += sprintf("W%03d", [1 - longitude.toInt()]);
    }
    return name;
  }
}

class _Position {
  final double longitude;
  final double latitude;

  _Position({required this.longitude, required this.latitude});
}
