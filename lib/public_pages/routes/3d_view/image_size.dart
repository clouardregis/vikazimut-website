import 'dart:math' as math;

class ImageSize {
  final int width;
  final int height;

  const ImageSize(this.width, this.height);

  static ImageSize calculateAspectRatio(int originalWidth, int originalHeight, int maxSideSize) {
    final double widthHeightRatio = originalWidth / originalHeight;
    if (originalWidth > originalHeight) {
      if (originalWidth < maxSideSize) {
        return ImageSize(originalWidth, originalHeight);
      }
      final int width = math.min(originalWidth, maxSideSize);
      final int height = width ~/ widthHeightRatio;
      return ImageSize(width, height);
    } else {
      final int height = math.min(originalHeight, maxSideSize);
      if (originalHeight < maxSideSize) {
        return ImageSize(originalWidth, originalHeight);
      }
      final int width = (height * widthHeightRatio).floor();
      return ImageSize(width, height);
    }
  }
}
