import 'dart:ui' as ui;

import 'package:image/image.dart' as img;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'course_3d_model.dart';
import 'course_3d_view.dart';

class Course3dPresenter {
  final Course3DViewState _course3dViewState;
  late final Heightmap _heightmap;
  late final img.Image _textureImage;
  int _currentTrackCount = -1;
  String _viewId = "1";

  Course3dPresenter(this._course3dViewState);

  String get viewId => _viewId;

  Future<String> createHtmlCode(RouteListPagePresenter presenter) async {
    _viewId = DateTime.now().millisecondsSinceEpoch.toString();
    _currentTrackCount = presenter.selectedTracks.length;

    _course3dViewState.updateProgressMessage("(1/5) ${L10n.getString("3dview_tab_progress_text_step1")}");
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    final ui.Image orienteeringMap = await Course3dModel.getOrienteeringMap(presenter.courseData!);

    _course3dViewState.updateProgressMessage("(2/5) ${L10n.getString("3dview_tab_progress_text_step2")}");
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    _heightmap = await Course3dModel.createHeightmap(presenter.courseData!, orienteeringMap);

    _course3dViewState.updateProgressMessage("(3/5) ${L10n.getString("3dview_tab_progress_text_step3")}");
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    _textureImage = await Course3dModel.createTextureImage(presenter.courseData!, orienteeringMap, _heightmap);

    _course3dViewState.updateProgressMessage("(4/5) ${L10n.getString("3dview_tab_progress_text_step4")}");
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    final img.Image textureWithTracks = await Course3dModel.addTrackOnTexture(presenter.courseData!, presenter.selectedTracks, _textureImage);

    _course3dViewState.updateProgressMessage("(5/5) ${L10n.getString("3dview_tab_progress_text_step5")}");
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    return Course3dModel.createWebGLContent(presenter.courseData!, _heightmap, textureWithTracks);
  }

  Future<String> refreshHtmlCodeWithNewTrackList(RouteListPagePresenter presenter) async {
    _viewId = DateTime.now().millisecondsSinceEpoch.toString();
    _currentTrackCount = presenter.selectedTracks.length;

    _course3dViewState.updateProgressMessage(L10n.getString("3dview_tab_progress_text_step6"));
    await Future.delayed(const Duration(milliseconds: 50)); // enable rotation of circular progress bar
    img.Image textureWithTracks = await Course3dModel.addTrackOnTexture(presenter.courseData!, presenter.selectedTracks, _textureImage);
    return Course3dModel.createWebGLContent(presenter.courseData!, _heightmap, textureWithTracks);
  }

  bool isNewTrackList(List<GpsRoute> selectedTracks) => selectedTracks.length != _currentTrackCount;
}
