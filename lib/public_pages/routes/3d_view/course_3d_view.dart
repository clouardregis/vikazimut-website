// coverage:ignore-file
import 'dart:js_interop';

import 'package:web/web.dart' as web;

import 'dart:ui_web' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/course_3d_presenter.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

class Course3DView extends StatefulWidget {
  final RouteListPagePresenter? routeListPresenter;

  const Course3DView({required this.routeListPresenter});

  @override
  Course3DViewState createState() => Course3DViewState();
}

class Course3DViewState extends State<Course3DView> {
  late final Course3dPresenter _presenter;
  late Future<String> _htmlCode;
  late final ValueNotifier<String> _progressMessage;

  @override
  void initState() {
    _presenter = Course3dPresenter(this);
    _progressMessage = ValueNotifier<String>("");
    _htmlCode = _presenter.createHtmlCode(widget.routeListPresenter!);
    super.initState();
  }

  @override
  void dispose() {
    _progressMessage.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_presenter.isNewTrackList(widget.routeListPresenter!.selectedTracks)) {
      _htmlCode = _presenter.refreshHtmlCodeWithNewTrackList(widget.routeListPresenter!);
    }
    return FutureBuilder<String>(
      future: _htmlCode,
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            // ignore: undefined_prefixed_name
            ui.platformViewRegistry.registerViewFactory(_presenter.viewId, (int viewId) {
              web.HTMLIFrameElement element = web.HTMLIFrameElement()
                ..srcdoc = (snapshot.data!).toJS
                ..style.width = '100%'
                ..style.height = '100%'
                ..style.border = 'none';
              return element;
            });

            return HtmlElementView(
              viewType: _presenter.viewId,
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    L10n.getString("3dview_tab_error_title"),
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(color: Colors.red),
                  ),
                  const SizedBox(height: 25),
                  Text(L10n.getString(snapshot.error.toString())),
                ],
              ),
            );
          }
        } else {
          return Center(
            child: SizedBox(
              width: 250,
              child: ValueListenableBuilder(
                valueListenable: _progressMessage,
                builder: (context, value, child) {
                  return Text(value.toString(), textAlign: TextAlign.center);
                },
              ),
            ),
          );
        }
      },
    );
  }

  void updateProgressMessage(String message) {
    _progressMessage.value = message;
  }
}
