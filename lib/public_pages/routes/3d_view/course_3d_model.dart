import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as img;
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'gps_track_painter.dart';
import 'ign_altimetry_gateway.dart';
import 'image_size.dart';
import 'srtm_altimetry_gateway.dart';
import 'textured_image.dart';
import 'webgl_content_template.dart';

class Course3dModel {
  static const int MAX_IMAGE_SIDE_SIZE = 2000;
  static const int MAX_POINT_COUNT = 5000;
  static const int NUMBER_OF_PARTS = 2;

  static const String SERVER_COURSE_LIST_URL = "${constants.SERVER}/data/courses";

  static Future<ui.Image> getOrienteeringMap(CourseData courseData) async {
    ui.Size imageSize = await Course3dModel._getImageSize(courseData.courseGeoreference.image);
    ImageSize newImageSize = ImageSize.calculateAspectRatio(imageSize.width.toInt(), imageSize.height.toInt(), Course3dModel.MAX_IMAGE_SIDE_SIZE);
    return Course3dModel._loadImage(courseData.courseGeoreference.image.image, newImageSize.width, newImageSize.height);
  }

  static Future<Heightmap> createHeightmap(CourseData courseData, final ui.Image orienteeringMap) async {
    Heightmap heightmap = await IgnAltimetryGateway.fetchAltitudesForRegion(
      courseData.courseGeoreference.bounds,
      orienteeringMap.width,
      orienteeringMap.height,
    );
    if (heightmap.data.isEmpty) {
      heightmap = await SrtmAltimetryGateway.fetchAltitudesForRegion(
        courseData.courseGeoreference.bounds,
        orienteeringMap.width,
        orienteeringMap.height,
      );
      if (heightmap.data.isEmpty) {
        return Future.error("3dview_tab_error_gateway_unavailable");
      }
    }
    final double minimumAltitude = heightmap.data.reduce(math.min);
    List<double> heightmapData = _normalizeAsUint16(heightmap.data, minimumAltitude);
    return Heightmap(heightmapData, heightmap.size);
  }

  static Future<img.Image> createTextureImage(CourseData courseData, final ui.Image orienteeringMapImage, Heightmap heightmap) async {
    ImageSize newOrienteeringMapSize = ImageSize.calculateAspectRatio(orienteeringMapImage.width, orienteeringMapImage.height, MAX_IMAGE_SIDE_SIZE);
    double resolutionInMeterPerPixel = computeResolutionInMeterPerPixel(courseData.courseGeoreference.bounds, heightmap.size);
    return await TexturedImage.build(orienteeringMapImage, heightmap.data, heightmap.size, resolutionInMeterPerPixel, newOrienteeringMapSize);
  }

  static Future<img.Image> addTrackOnTexture(CourseData courseData, List<GpsRoute> selectedTracks, img.Image textureImage) async {
    List<double> mapGeodesicCenter = [
      (courseData.courseGeoreference.bounds.east + courseData.courseGeoreference.bounds.west) / 2.0,
      (courseData.courseGeoreference.bounds.south + courseData.courseGeoreference.bounds.north) / 2.0,
    ];
    List<double> mapGeodesicSize = [
      courseData.courseGeoreference.bounds.east - courseData.courseGeoreference.bounds.west,
      courseData.courseGeoreference.bounds.north - courseData.courseGeoreference.bounds.south,
    ];
    img.Image textureCopy = textureImage.clone(noAnimation: true);
    await GpsTrackPainter.addTrackDataToTextureImage(
      selectedTracks,
      mapGeodesicCenter,
      mapGeodesicSize,
      courseData.courseGeoreference.bounds.rotation,
      textureCopy,
    );
    return textureCopy;
  }

  static Future<String> createWebGLContent(CourseData courseData, Heightmap heightmap, img.Image textureImage) async {
    Map<String, String> view3dAttributes = {};
    view3dAttributes["height_map_width"] = heightmap.size.width.toString();
    view3dAttributes["height_map_height"] = heightmap.size.height.toString();
    view3dAttributes["height_map_data"] = heightmap.data.toString();
    double resolutionInMeterPerPixel = computeResolutionInMeterPerPixel(courseData.courseGeoreference.bounds, heightmap.size);
    view3dAttributes["meters_per_pixel"] = resolutionInMeterPerPixel.toString();
    view3dAttributes["texture_width"] = textureImage.width.toString();
    view3dAttributes["texture_height"] = textureImage.height.toString();
    view3dAttributes["texture_data"] = _encodeAsBase64(textureImage.toUint8List());
    return replaceAttributeByValueInWebGLTemplate_(webGLContentTemplate, view3dAttributes);
  }

  @visibleForTesting
  static double computeResolutionInMeterPerPixel(LatLonBox bounds, ImageSize size) {
    int terrainWidthInMeter = computeTerrainWidthInMeter(bounds);
    return terrainWidthInMeter / size.width;
  }

  @visibleForTesting
  static int computeTerrainWidthInMeter(LatLonBox bounds) {
    double lonE = bounds.east;
    double lonW = bounds.west;
    double latN = bounds.north;
    return GeodesicPoint(latN, lonW).distanceToInMeter(GeodesicPoint(latN, lonE)).floor();
  }

  static String _encodeAsBase64(Uint8List data) => base64.encode(data);

  static Future<ui.Size> _getImageSize(Image image) async {
    var completer = Completer<Size>();
    ImageStreamListener? listener;
    ImageStream stream = image.image.resolve(const ImageConfiguration());
    listener = ImageStreamListener((ImageInfo info, _) => completer.complete(Size(info.image.width.toDouble(), info.image.height.toDouble())));
    stream.addListener(listener);
    var size = await completer.future;
    stream.removeListener(listener);
    return size;
  }

  static Future<ui.Image> _loadImage(ImageProvider imageProvider, int width, int height) async {
    var completer = Completer<ImageInfo>();
    ImageProvider resizedImageProvider = ResizeImage(imageProvider, width: width, height: height);
    ImageStream stream = resizedImageProvider.resolve(const ImageConfiguration());
    ImageStreamListener? listener;
    listener = ImageStreamListener((ImageInfo info, _) => completer.complete(info));
    stream.addListener(listener);
    ImageInfo imageInfo = await completer.future;
    stream.removeListener(listener);
    return imageInfo.image;
  }

  static List<double> _normalizeAsUint16(List<double> altitudes, double min) {
    return altitudes.map((i) => ((i - min) * 65535.0 / 9000.0)).toList();
  }

  @visibleForTesting
  static String replaceAttributeByValueInWebGLTemplate_(String template, Map<String, String> listOfParametersThreeJS) {
    List<String> parts = template.split("{%");
    StringBuffer result = StringBuffer(parts[0]);
    for (var i = 1; i < parts.length; ++i) {
      final part = parts[i];
      var indexOf = part.indexOf("%}");
      String key = part.substring(0, indexOf).trim();
      if (listOfParametersThreeJS.containsKey(key)) {
        result.write(listOfParametersThreeJS[key]!);
        result.write(part.substring(indexOf + 2));
      } else {
        result.write("{%");
        result.write(part);
      }
    }
    return result.toString();
  }
}

class Heightmap {
  final List<double> data;
  final ImageSize size;

  Heightmap(this.data, this.size);
}
