String webGLContentTemplate = r"""
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Vikazimut - 3D WebGL</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <style>
     <body {
        margin: 0;
        overflow: hidden;
     }
     .keys-widget {
	     	display: block;
	    	position: absolute;
	    	left: 40px;
	    	top: 10px;
    		background: rgba( 255, 255, 255, 0.5);
	    	border: none;
	    	border-radius: 1em;
	    	text-align: center;
	    	font-family: sans-serif;
	    	padding: 3px;
     }
     @media (max-width: 800px) {
      .keys-widget {
          display: none;
      }
     }
     .relief_div {
	      display: block;
	      position: absolute;
	      left: 40px;
	      bottom: 20px;
    	  background: rgba( 255, 255, 255, 0.5);
	      accent-color: #ff671f;
	      border: none;
	      border-radius: 1em;
	      padding: 3px 10px;
     }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/three@0.147.0/build/three.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/three@0.147.0/examples/js/controls/TrackballControls.js"></script>
</head>
<body>
<div id="webgl" ></div>
<div class="keys-widget">
  <big>Keys</big><br>
  &larr;, &rarr;<br>
  &uarr;, &darr;<br>
  +, -<br>
  v, h
</div>

<div class="relief_div">
  <input type="range" id="relief_slider" value="1" min="-3" max="10" name="range" onkeydown="event.preventDefault()">
  <span id="slider_value" value="x2" style="vertical-align: top;">200%</span>
</div>

<script type="text/javascript">
    const heightmapData = {% height_map_data %};
    const heightmapWidth = {% height_map_width %};
    const heightmapHeight = {% height_map_height %};
    const encodedTextureData = "{% texture_data %}";
    const textureWidth = {% texture_width %};
    const textureHeight = {% texture_height %};
    const resolutionInPixels = {% meters_per_pixel %};

    const scene = new THREE.Scene();
    scene.add(new THREE.AmbientLight(0xffffff));

    const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.set(0, -100, heightmapWidth);

    const renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
   
    const textureData = decodeBase64(encodedTextureData);

    const mesh1 = createTerrainTexture(heightmapData, heightmapWidth, heightmapHeight, textureData, textureWidth, textureHeight);
    const mesh2 = createMapBackside(heightmapData, heightmapWidth, heightmapHeight);

    document.getElementById('webgl').appendChild(renderer.domElement);
    const controls = new THREE.TrackballControls(camera, renderer.domElement);
    controls.rotateSpeed = 1.5;
    controls.zoomSpeed = 0.8;
    controls.panSpeed = 0.3;

    controls.staticMoving = false;
    controls.dynamicDampingFactor = 0.2;

    controls.minDistance = 10;
    controls.maxDistance = 500;

    setupKeyControls(mesh1, mesh2);

    window.focus();
    animate();

    function animate() {
        requestAnimationFrame(animate);
        controls.update();
        renderer.render(scene, camera);
    }

    function createGeometry(heightmapData, heightmapWidth, heightmapHeight) {
        const geometry = new THREE.PlaneGeometry(heightmapWidth, heightmapHeight, heightmapWidth - 1, heightmapHeight - 1);
        const resolution = resolutionInPixels / 2;
        const position = geometry.attributes.position;
        for (let i = 0; i < position.count; i++) {
            position.setZ(i, (heightmapData[i] / resolution) * 9000.0 / 65535.0);
        }
        return geometry;
    }

    function createTerrainTexture(heightmapData, heightmapWidth, heightmapHeight, textureData, textureWidth, textureHeight) {
        const geometry = createGeometry(heightmapData, heightmapWidth, heightmapHeight);
        const material = new THREE.MeshPhongMaterial();
        material.map = createTextureFromData(textureData, textureWidth, textureHeight);
        const mesh = new THREE.Mesh(geometry, material);
        scene.add(mesh);
        return mesh;
    }

    function createMapBackside(heightmapData, heightmapWidth, heightmapHeight) {
        const geometry = createGeometry(heightmapData, heightmapWidth, heightmapHeight);
        const material = new THREE.MeshBasicMaterial({color: 0xA6B1BB, wireframe: false});
        material.side = THREE.BackSide;
        const mesh = new THREE.Mesh(geometry, material);
        scene.add(mesh);
        return mesh;
    }

    function createTextureFromData(data, width, height) {
        const texture = new THREE.DataTexture(data, width, height);
        texture.flipY = true
        texture.needsUpdate = true;
        return texture;
    }

    function decodeBase64(encodedData) {
        return new Uint8Array(atob(encodedData).split("").map(c => c.charCodeAt(0)));
    }

    function setupKeyControls(mesh1, mesh2) {
      document.onkeydown = function(e) {
        switch (e.keyCode) {
          case 37:
            mesh1.rotateZ(-0.1);
            mesh2.rotateZ(-0.1);
            return;
          case 38:
            mesh1.rotateX(-0.1);
            mesh2.rotateX(-0.1);
            return;
          case 39:
            mesh1.rotateZ(0.1);
            mesh2.rotateZ(0.1);
            return;
          case 40:
            mesh1.rotateX(0.1);
            mesh2.rotateX(0.1);
            return;
        }
        switch (e.key) {
          case 'v':
            mesh1.position.set( 0, 0, 0 );
            mesh1.rotation.set( 0.8, 0, 0 );
            mesh1.scale.set( 1, 1, 1 );
            mesh2.position.set( 0, 0, 0 );
            mesh2.rotation.set( 0.8, 0, 0 );
            mesh2.scale.set( 1, 1, 1 );
            controls.reset();
            return;
          case 'h':
            mesh1.position.set( 0, 0, 0 );
            mesh1.rotation.set( -0.5, 0, 0 );
            mesh1.scale.set( 1, 1, 1 );
            mesh2.position.set( 0, 0, 0 );
            mesh2.rotation.set( -0.5, 0, 0 );
            mesh2.scale.set( 1, 1, 1 );
            controls.reset();
            return;
          case '+':
            camera.translateZ(-5);
            camera.lookAt(scene.position);
            return;
          case '-':
            camera.translateZ(+5);
            camera.lookAt(scene.position);
            return;
        }
      };
    }
    
    const input = document.querySelector("#relief_slider");
    const slider_value = document.querySelector("#slider_value");
    input.addEventListener("input", (event) => {
      const value = event.target.value;
      var heightScale;
      if (value > 0) {
	      heightScale = 1 + value * 0.5;
	      slider_value.innerHTML = value * 100 + "%";
      } else {
	      const scale = value * -1 + 1;
	      heightScale = 1 / (Math.pow(2, scale));
	      slider_value.innerHTML =  Math.floor(heightScale * 100)  + "%";
      } 
      mesh1.scale.set(1, 1, heightScale);
      mesh2.scale.set(1, 1, heightScale);
    });
  
</script>

</body>
</html>
""";
