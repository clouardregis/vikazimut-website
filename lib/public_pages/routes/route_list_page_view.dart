// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/course_info/course_info_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';
import 'package:vikazimut_website/utils/sidebar_widget/collapsible_or_rigid_sidebar.dart';

import '3d_view/course_3d_view.dart';
import 'animation/animation_tab_view.dart';
import 'export/export_tab_view.dart';
import 'gps_route.dart';
import 'graph/time_behind_leader_graph_view.dart';
import 'import/import_view.dart';
import 'orienteering_performance/orienteering_performance_view.dart';
import 'profile/profile_view.dart';
import 'route_list_page_presenter.dart';
import 'split_time/split_time_sheet_tab_view.dart';
import 'time/time_sheet_tab_view.dart';
import 'track/orienteer_tile.dart';
import 'track/track_tab_view.dart';

class RouteListPageView extends StatefulWidget {
  static const String routePath = '/course/tracks';
  final int courseId;

  RouteListPageView(args) : courseId = int.parse(args);

  @override
  State<RouteListPageView> createState() => RouteListPageViewState();
}

class RouteListPageViewState extends State<RouteListPageView> with TickerProviderStateMixin implements RouteListPageInterface {
  static const double MAX_WIDTH = 350;
  late Future<List<GpsRoute>> _trackList;
  late final Future<CourseData> _courseData;
  late final RouteListPagePresenter _routeListPagePresenter;

  @override
  void initState() {
    _routeListPagePresenter = RouteListPagePresenter(widget.courseId, this);
    _courseData = _routeListPagePresenter.loadCourseData();
    _trackList = _routeListPagePresenter.getTrackList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: FutureBuilder<CourseData>(
        future: _courseData,
        builder: (BuildContext context, AsyncSnapshot<CourseData> snapshotCourseData) {
          if (snapshotCourseData.hasError) {
            final errorText = L10n.translateIfPossible(snapshotCourseData.error.toString());
            return GlobalErrorWidget(errorText);
          } else if (snapshotCourseData.hasData) {
            return SizedBox(
              height: ScreenHelper.isDesktop(context) ? MediaQuery.of(context).size.height - 70 : 1000,
              child: CollapsibleOrRigidSidebar(
                isCollapsible: !ScreenHelper.isDesktop(context),
                sidebarTitle: Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Discipline.getIcon(snapshotCourseData.data!.courseGeoreference.discipline),
                          color: kOrangeColor,
                          size: 32,
                        ),
                        const SizedBox(width: 2),
                        Expanded(
                          child: OutlinedButton(
                            child: Text(
                              snapshotCourseData.data!.courseGeoreference.name,
                              style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: kOrangeColor),
                            ),
                            onPressed: () => GoRouter.of(context).push("${CourseInfoViewPage.routePath}/${snapshotCourseData.data!.courseGeoreference.imageId}"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    if (snapshotCourseData.data!.courseGeoreference.hiddenLevel == 1)
                      Container(
                        color: Colors.yellow,
                        child: Text(
                          L10n.getString("tracks.hidden"),
                          style: TextStyle(fontStyle: FontStyle.italic, color: Colors.black),
                        ),
                      ),
                    OutlinedButton(
                      onPressed: () async {
                        _routeListPagePresenter.unselectTracks();
                      },
                      child: Text(
                        L10n.getString("route_list_page_unselect_all"),
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                sidebarContents: [
                  FutureBuilder<List<GpsRoute>>(
                    future: _trackList,
                    builder: (BuildContext context, AsyncSnapshot<List<GpsRoute>> snapshotTrackList) {
                      if (snapshotTrackList.hasError) {
                        final errorText = L10n.translateIfPossible(snapshotTrackList.error.toString());
                        return GlobalErrorWidget(errorText);
                      } else if (snapshotTrackList.hasData) {
                        if (snapshotTrackList.data!.isEmpty) {
                          return _MessageContainer(
                            width: MAX_WIDTH,
                            message: L10n.getString("track_no_gps_track_found"),
                          );
                        } else {
                          return _TrackControlPanel(_routeListPagePresenter, snapshotCourseData.data!);
                        }
                      } else {
                        return _MessageContainer(
                          width: MAX_WIDTH,
                          message: L10n.getString("track.loading"),
                        );
                      }
                    },
                  ),
                ],
                contents: Container(
                  constraints: const BoxConstraints(minHeight: 500),
                  height: double.infinity,
                  child: DefaultTabController(
                    length: 10,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TabBar(
                          isScrollable: true,
                          tabs: [
                            Tab(text: L10n.getString("course_tab_trace_title")),
                            Tab(text: L10n.getString("course_tab_time_title")),
                            Tab(text: L10n.getString("course_tab_split_time_title")),
                            Tab(text: L10n.getString("course_tab_time_behind_leader_graph")),
                            Tab(text: L10n.getString("course_tab_orienteer_profile")),
                            Tab(text: L10n.getString("course_tab_analyze_trace_title")),
                            // Banner(
                            //   message: 'En travaux',
                            //   location: BannerLocation.topStart,
                            //   child: Tab(text: I18n.getString("course_tab_physical_performance_title")),
                            // ),
                            Tab(text: L10n.getString("course_tab_animation_title")),
                            Tab(text: L10n.getString("course_tab_3dview_title")),
                            Tab(text: L10n.getString("course_tab_import_title")),
                            Tab(text: L10n.getString("course_tab_export_title")),
                          ],
                        ),
                        Flexible(
                          child: TabBarView(
                            children: [
                              TrackTabView(presenter: _routeListPagePresenter, courseInfo: _courseData),
                              TimeSheetTabView(routePresenter: _routeListPagePresenter),
                              SplitTimeSheetTabView(routePresenter: _routeListPagePresenter),
                              TimeBehindLeaderGraphView(presenter: _routeListPagePresenter),
                              ProfileView(presenter: _routeListPagePresenter, courseInfo: _courseData),
                              OrienteeringPerformanceView(routePresenter: _routeListPagePresenter, courseInfo: _courseData),
                              // PhysicalPerformanceAnalysisView(routePresenter: _routeListPagePresenter, courseInfo: _courseData),
                              AnimationTabView(presenter: _routeListPagePresenter, courseData: _courseData),
                              Course3DView(routeListPresenter: _routeListPagePresenter),
                              ImportTabView(presenter: _routeListPagePresenter),
                              ExportTabView(presenter: _routeListPagePresenter),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  @override
  void updateTrackList() {
    setState(() {
      _trackList = _routeListPagePresenter.getTrackList();
    });
  }

  @override
  void updateTrackSelection() {
    setState(() {});
  }

  @override
  void displayToastMessage(String message, {Color? backgroundColor, Color? textColor}) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
            content: Text(
              message,
              style: TextStyle(color: textColor),
            ),
            backgroundColor: backgroundColor),
      );
  }
}

class _TrackControlPanel extends StatefulWidget {
  final RouteListPagePresenter routeListPagePresenter;
  final CourseData courseData;

  const _TrackControlPanel(this.routeListPagePresenter, this.courseData);

  @override
  State<_TrackControlPanel> createState() => _TrackControlPanelState();
}

class _TrackControlPanelState extends State<_TrackControlPanel> with SingleTickerProviderStateMixin {
  int _loading = -1;
  late final TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(_handleTabSelection);
    _tabController.index = _selectActiveTab();
    super.initState();
  }

  int _selectActiveTab() {
    int index;
    if (widget.courseData.courseGeoreference.format != null) {
      index = widget.courseData.courseGeoreference.format!.index;
      if (index == 0) {
        if (widget.routeListPagePresenter.presetOrderTrackList.isEmpty && widget.routeListPagePresenter.freeOrderTrackList.isNotEmpty) {
          index = 1;
        }
      } else {
        if (widget.routeListPagePresenter.freeOrderTrackList.isEmpty && widget.routeListPagePresenter.presetOrderTrackList.isNotEmpty) {
          index = 0;
        }
      }
    } else {
      index = widget.routeListPagePresenter.presetOrderTrackList.length > widget.routeListPagePresenter.freeOrderTrackList.length ? 0 : 1;
    }
    return index;
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    int incrementForPresetRank;
    if (widget.routeListPagePresenter.presetOrderTrackList.isNotEmpty && widget.routeListPagePresenter.presetOrderTrackList[0].id < 0) {
      incrementForPresetRank = 0;
    } else {
      incrementForPresetRank = 1;
    }
    return Padding(
      padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TabBar(
            controller: _tabController,
            isScrollable: false,
            labelColor: Colors.white,
            tabs: [
              Tab(child: Text("${L10n.getString("constant_route_preset")} (${widget.routeListPagePresenter.presetOrderTrackList.length > 1 ? widget.routeListPagePresenter.presetOrderTrackList.length - 1 : widget.routeListPagePresenter.presetOrderTrackList.length})", textAlign: TextAlign.center)),
              Tab(child: Text("${L10n.getString("constant_route_free")} (${widget.routeListPagePresenter.freeOrderTrackList.length})", textAlign: TextAlign.center)),
            ],
          ),
          if (_tabController.index == 0)
            Column(
              children: [
                for (int i = 0; i < widget.routeListPagePresenter.presetOrderTrackList.length; i++) _buildTrackItem(widget.routeListPagePresenter.presetOrderTrackList[i], i + incrementForPresetRank),
              ],
            )
          else
            Column(
              children: [
                for (int i = 0; i < widget.routeListPagePresenter.freeOrderTrackList.length; i++) _buildTrackItem(widget.routeListPagePresenter.freeOrderTrackList[i], i),
              ],
            ),
        ],
      ),
    );
  }

  Widget _buildTrackItem(GpsRoute track, int rank) {
    return GestureDetector(
      onTap: (_loading >= 0)
          ? null
          : () async {
              if (!mounted) return;
              setState(() {
                _loading = track.id;
              });
              await widget.routeListPagePresenter.onClick(track);
              if (!mounted) {
                _loading = -1;
                return;
              }
              setState(() {
                _loading = -1;
              });
            },
      child: Container(
        padding: const EdgeInsets.fromLTRB(2, 2, 0, 1),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: kOrangeColorDisabled),
        ),
        margin: const EdgeInsets.fromLTRB(5, 3, 5, 3),
        child: _buildOrienteerTile(track, rank, _loading),
      ),
    );
  }

  StatelessWidget _buildOrienteerTile(GpsRoute track, int rank, int loading) {
    if (track.id < 0) {
      return PresetOrderSupermanTile(track, rank, loading);
    } else if (track.courseFormat.isPresetOrder) {
      return PresetOrderOrienteerTile(track, rank, loading);
    } else {
      return FreeOrderOrienteerTile(track, rank, loading);
    }
  }
}

class _MessageContainer extends StatelessWidget {
  final double width;
  final String message;

  const _MessageContainer({
    required this.width,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Column(
        children: [
          Text(message, softWrap: true),
        ],
      ),
    );
  }
}
