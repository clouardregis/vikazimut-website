class ColorPicker {
  final List<String> _basicColors = [
    'ffe6194b',
    'ff4363d8',
    'ff3B9700',
    'fff032e6',
    'ff46f0f0',
    'ff0D98BA',
    'ffffef00',
    'ff145a32',
    'fff58231',
    'ff911eb4',
    'ff9a6324',
    'fffabebe',
    'ff0000FF',
    'ff777777',
    'ffbcf60c',
    'ff800000',
    'ff808000',
    'ff7900D7',
    'ff000075',
    'ffB0B0B0',
  ];

  String pickColor() {
    assert(_basicColors.isNotEmpty);
    return _basicColors.removeAt(0);
  }

  void restoreColor(String color) {
    _basicColors.insert(0, color);
  }
}
