import 'package:flutter/foundation.dart';

@immutable
class SpeedInKmH {
  final int speed;
  final double percent;

  const SpeedInKmH(this.speed, this.percent);
}
