// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

import 'profile_chart_widget.dart';
import 'profile_handler.dart';
import 'side_bar.dart';

class ProfileView extends StatelessWidget {
  final RouteListPagePresenter presenter;
  final Future<CourseData> courseInfo;
  final ScrollController _verticalScroll = ScrollController();

  ProfileView({required this.presenter, required this.courseInfo});

  @override
  Widget build(BuildContext context) {
    if (presenter.selectedTracks.isNotEmpty) {
      return FutureBuilder<CourseData>(
        future: courseInfo,
        builder: (BuildContext context, AsyncSnapshot<CourseData> snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return Stack(
              alignment: Alignment.topRight,
              children: [
                _buildProfilesWidget(
                  presenter.selectedTracks,
                  discipline: snapshot.data!.courseGeoreference.discipline,
                ),
                const SideBar(),
              ],
            );
          } else {
            return Container();
          }
        },
      );
    } else {
      return Center(
        child: Text(
          L10n.getString("orienteer_profile_no_orienteer"),
        ),
      );
    }
  }

  Widget _buildProfilesWidget(List<GpsRoute> selectedTracks, {required Discipline? discipline}) {
    ProfileHandler profileHandler = ProfileHandler(discipline);
    Future<List<ProfileData>> profiles = profileHandler.buildProfiles(selectedTracks);
    return FutureBuilder<List<ProfileData>>(
      future: profiles,
      builder: (BuildContext context, AsyncSnapshot<List<ProfileData>> snapshot) {
        if (snapshot.hasError) {
          return Text("Internal error while building profiles.. ${snapshot.error.toString()}");
        } else if (snapshot.hasData && snapshot.connectionState == ConnectionState.done) {
          return Scrollbar(
            controller: _verticalScroll,
            thumbVisibility: true,
            child: ListView.builder(
              controller: _verticalScroll,
              itemCount: snapshot.data!.length,
              itemBuilder: (context, i) {
                return ProfileChartWidget(
                  snapshot.data![i].nickname,
                  presenter.selectedTracks[i].color,
                  snapshot.data![i].histogram,
                  snapshot.data![i].profile,
                  discipline,
                  profileHandler,
                );
              },
            ),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }
}
