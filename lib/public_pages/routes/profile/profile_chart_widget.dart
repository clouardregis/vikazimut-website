// coverage:ignore-file
import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/constants.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

import 'decision_tree/decision_tree.dart';
import 'profile_handler.dart';
import 'rating_slider_widget.dart';
import 'speed_in_kmh.dart';

class ProfileChartWidget extends StatelessWidget {
  final String nickname;
  final Color trackColor;
  final List<SpeedInKmH> histogram;
  final OrienteerProfile profile;
  final Discipline? discipline;
  final ProfileHandler profileHandler;

  const ProfileChartWidget(
    this.nickname,
    this.trackColor,
    this.histogram,
    this.profile,
    this.discipline,
    this.profileHandler,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: COLLAPSIBLE_MIN_WIDTH),
          child: Row(
            children: [
              Icon(Icons.person, color: trackColor),
              Text(
                nickname,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Container(
          constraints: const BoxConstraints(maxWidth: 800),
          padding: const EdgeInsets.all(8.0),
          child: Card(
            margin: const EdgeInsets.only(left: 0, top: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.query_stats, color: kOrangeColor),
                            const SizedBox(width: 8),
                            Column(
                              children: [
                                Text(
                                  L10n.getString("orienteer_profile_widget_speed_histogram_title"),
                                  style: _titleTextStyle,
                                ),
                                Text(
                                  L10n.getString("orienteer_profile_widget_speed_histogram_note"),
                                  style: TextStyle(fontSize: 12, color: Colors.grey),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.all(2),
                          height: 300,
                          width: 700,
                          child: _buildChart(
                            context,
                            ProfileHandler.buildSpeedHistogramChart(
                              histogram,
                              discipline,
                              profileHandler.speedPerBin,
                            ),
                            profileHandler,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.military_tech, color: kOrangeColor),
                            const SizedBox(width: 8),
                            Text(L10n.getString("orienteer_profile_widget_estimated_level"), style: _titleTextStyle),
                          ],
                        ),
                        const SizedBox(height: 10),
                        SizedBox(
                          height: 55,
                          width: 700,
                          child: CustomPaint(
                            painter: RatingSliderWidget(rank: profile.orienteerGrade.index),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 15),
                  ],
                ),
                if (Discipline.isFootOrienteering(discipline))
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 15),
                      Padding(
                        padding: const EdgeInsets.only(left: COLLAPSIBLE_MIN_WIDTH),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.person, color: kOrangeColor),
                            Text(
                              L10n.getString("orienteer_profile_description_title"),
                              style: const TextStyle(fontWeight: FontWeight.bold, color: kOrangeColor),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          profile.characteristics ?? "",
                          style: _textStyleText,
                          strutStyle: const StrutStyle(leading: 0.3),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: COLLAPSIBLE_MIN_WIDTH),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(Icons.person, color: kOrangeColor),
                            Text(
                              L10n.getString("orienteer_profile_advice_title"),
                              style: const TextStyle(fontWeight: FontWeight.bold, color: kOrangeColor),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          profile.advices ?? "",
                          style: _textStyleText,
                          strutStyle: const StrutStyle(leading: 0.3),
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  static Widget _buildChart(
    BuildContext context,
    final List<charts.Series<SpeedInKmH, String>> seriesList,
    ProfileHandler profileHandler,
  ) {
    final charts.Color textColor = (Theme.of(context).brightness == Brightness.dark) ? charts.Color.white : charts.Color.black;
    final staticTicksXAxis = profileHandler.decisionTree.getTicksXAxis();
    final staticTicksYAxis = [for (var i = 0; i <= 40; i += 5) charts.TickSpec(i, style: charts.TextStyleSpec(color: textColor))];

    return charts.BarChart(
      seriesList,
      animate: false,
      defaultInteractions: false,
      defaultRenderer: charts.BarRendererConfig(
        cornerStrategy: const charts.ConstCornerStrategy(7),
      ),
      primaryMeasureAxis: charts.NumericAxisSpec(
        tickFormatterSpec: charts.BasicNumericTickFormatterSpec.fromNumberFormat(NumberFormat("### '%")),
        tickProviderSpec: charts.StaticNumericTickProviderSpec(staticTicksYAxis),
      ),
      domainAxis: charts.OrdinalAxisSpec(
        tickProviderSpec: charts.StaticOrdinalTickProviderSpec(staticTicksXAxis),
        renderSpec: charts.SmallTickRendererSpec(
          tickLengthPx: 10,
          labelOffsetFromAxisPx: 15,
          labelStyle: charts.TextStyleSpec(fontSize: 10, lineHeight: 1, color: textColor),
        ),
      ),
    );
  }
}

const TextStyle _titleTextStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: kOrangeColor);
const TextStyle _textStyleText = TextStyle(fontSize: 15);
