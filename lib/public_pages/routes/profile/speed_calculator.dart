class SpeedCalculator {
  static double computeSpeedInKmhWeightedByElevationGain(
      double distanceInMeter,
      int timeInMillisecond,
      double elevationInMeter,
      ) {
    final speed = computeSpeedInKmh(distanceInMeter, timeInMillisecond);
    final slopeInPercent = (distanceInMeter == 0) ? 1.0 : elevationInMeter * 100.0 / distanceInMeter;
    return speed * slopeAdjustment(slopeInPercent);
  }

  static double computeSpeedInKmh(
    double distanceInMeter,
    int timeInMillisecond,
  ) {
    if (timeInMillisecond == 0) {
      return 0;
    } else {
      return (distanceInMeter * 1000 * 3.6) / timeInMillisecond;
    }
  }

  static double slopeAdjustment(double slopeInPercent) {
    if (slopeInPercent < -30) {
      slopeInPercent = -30;
    }
    if (slopeInPercent > 20) {
      slopeInPercent = 20;
    }
    const double p5 = -0.000000375615554;
    const double p4 = -0.000001817902031;
    const double p3 = 0.001844793317856;
    const double p2 = 0.030229208603188;
    const double p1 = 1;

    final double v2 = slopeInPercent;
    final double v3 = slopeInPercent * slopeInPercent;
    final double v4 = v3 * slopeInPercent;
    final double v5 = v4 * slopeInPercent;
    return p5 * v5 - p4 * v4 + p3 * v3 + p2 * v2 + p1;
  }
}
