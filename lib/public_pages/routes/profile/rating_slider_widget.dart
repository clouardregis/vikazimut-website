// coverage:ignore-file
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

class RatingSliderWidget extends CustomPainter {
  static const textColor = kOrangeColor;
  final int rank;

  const RatingSliderWidget({required this.rank});

  @override
  void paint(Canvas canvas, Size size) {
    double fontSize = (size.width > 500) ? 14 : 12;
    final TextStyle textStyle = TextStyle(color: textColor, fontSize: fontSize);
    const xOffset = 15.0;
    const yOffset = 20.0;
    final width = size.width - 35.0;
    final List<Offset> tickCoordinates = [
      const Offset(xOffset, yOffset),
      Offset(xOffset + width / 9, yOffset),
      Offset(xOffset + width * (2 / 9), yOffset),
      Offset(xOffset + width * (3 / 9), yOffset),
      Offset(xOffset + width * (4 / 9), yOffset),
      Offset(xOffset + width * (5 / 9), yOffset),
      Offset(xOffset + width * (6 / 9), yOffset),
      Offset(xOffset + width * (7 / 9), yOffset),
      Offset(xOffset + width * (8 / 9), yOffset),
      Offset(xOffset + width, yOffset),
    ];

    final backgroundLinePaint = Paint()
      ..style = PaintingStyle.stroke
      ..color = kOrangeColorDark.withValues(alpha: 0.5)
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 6;
    canvas.drawLine(
      Offset(tickCoordinates[0].dx - 2, tickCoordinates[0].dy),
      Offset(tickCoordinates[tickCoordinates.length - 1].dx + 2, tickCoordinates[tickCoordinates.length - 1].dy),
      backgroundLinePaint,
    );

    final shadowLinePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1
      ..color = const Color(0xFF000000).withValues(alpha: 0.4);
    canvas.drawLine(
      Offset(tickCoordinates[0].dx - 2, tickCoordinates[0].dy - 3),
      Offset(tickCoordinates[tickCoordinates.length - 1].dx + 2, tickCoordinates[tickCoordinates.length - 1].dy - 3),
      shadowLinePaint,
    );

    final foregroundLinePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8
      ..strokeCap = StrokeCap.round
      ..color = textColor;
    canvas.drawLine(
      Offset(tickCoordinates[0].dx - 2, tickCoordinates[0].dy),
      Offset(tickCoordinates[rank].dx + 2, tickCoordinates[rank].dy),
      foregroundLinePaint,
    );

    final tickLinePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.5
      ..color = textColor;
    _drawTick(tickCoordinates[0], canvas, tickLinePaint);
    _drawTick(tickCoordinates[1], canvas, tickLinePaint);
    _drawTick(tickCoordinates[2], canvas, tickLinePaint);
    _drawTick(tickCoordinates[3], canvas, tickLinePaint);
    _drawTick(tickCoordinates[4], canvas, tickLinePaint);
    _drawTick(tickCoordinates[5], canvas, tickLinePaint);
    _drawTick(tickCoordinates[6], canvas, tickLinePaint);
    _drawTick(tickCoordinates[7], canvas, tickLinePaint);
    _drawTick(tickCoordinates[8], canvas, tickLinePaint);
    _drawTick(tickCoordinates[9], canvas, tickLinePaint);

    if (rank >= 0) {
      _changeSliderThumb(tickCoordinates[rank], canvas);
      _displayRankName(textStyle, rank, tickCoordinates[rank], canvas, size);
    }
  }

  void _drawTick(Offset tickCoordinates, Canvas canvas, Paint paint) {
    canvas.drawLine(Offset(tickCoordinates.dx, tickCoordinates.dy + 5), Offset(tickCoordinates.dx, tickCoordinates.dy + 10), paint);
  }

  void _drawTextPaint(String label, TextStyle textStyle, Offset tickCoordinates, Canvas canvas, Size size) {
    final textPainter = TextPainter(
      text: TextSpan(text: label, style: textStyle),
      textDirection: ui.TextDirection.ltr,
    );
    textPainter.layout();
    var x = tickCoordinates.dx - textPainter.width / 2;
    if (x < 0) {
      x = 5;
    } else if (x + textPainter.width + 5 > size.width) {
      x = size.width - textPainter.width - 5;
    }
    var y = tickCoordinates.dy + 15;
    textPainter.paint(canvas, Offset(x, y));
  }

  void _displayRankName(TextStyle textStyle, rank, Offset tickCoordinates, Canvas canvas, Size size) {
    switch (rank) {
      case 0:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_wood_one"), textStyle, tickCoordinates, canvas, size);
      case 1:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_wood_two"), textStyle, tickCoordinates, canvas, size);
      case 2:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_bronze_one"), textStyle, tickCoordinates, canvas, size);
      case 3:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_bronze_two"), textStyle, tickCoordinates, canvas, size);
      case 4:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_silver_one"), textStyle, tickCoordinates, canvas, size);
      case 5:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_silver_two"), textStyle, tickCoordinates, canvas, size);
      case 6:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_gold_one"), textStyle, tickCoordinates, canvas, size);
      case 7:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_gold_two"), textStyle, tickCoordinates, canvas, size);
      case 8:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_diamond_one"), textStyle, tickCoordinates, canvas, size);
      case 9:
        _drawTextPaint(L10n.getString("orienteer_profile_rank_diamond_two"), textStyle, tickCoordinates, canvas, size);
    }
  }

  void _changeSliderThumb(Offset offset, Canvas canvas) {
    const double sizeCircle = 10;

    Paint buttonBackground = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
    canvas.drawCircle(offset, sizeCircle, buttonBackground);

    Paint buttonSurroundPaint = Paint()
      ..color = kOrangeColor
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;
    canvas.drawCircle(offset, sizeCircle, buttonSurroundPaint);

    Paint buttonPaint = Paint()
      ..color = textColor
      ..style = PaintingStyle.fill;
    canvas.drawCircle(offset, sizeCircle - 2, buttonPaint);
  }

  @override
  bool shouldRepaint(covariant RatingSliderWidget oldDelegate) => rank != oldDelegate.rank;
}
