import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/profile/speed_calculator.dart';

import 'speed_in_kmh.dart';

class SpeedHistogram {
  static const int _HISTOGRAM_SIZE = 20;

  static List<SpeedInKmH> calculateSpeedHistogramFromWaypoints(
    List<GeodesicPoint> waypoints,
    int maxSpeed,
    int totalTimeInMilliseconds,
  ) {
    if (waypoints.isEmpty) {
      return [];
    }
    List<int> speedHistogram = List.filled(_HISTOGRAM_SIZE + 1, 0);
    const int filterHalfSize = 3;
    const int numberOfBadValues = 5; // Don't use the first 5 waypoints, they are usually bad waypoints
    var totalTime = computeHistogramBins_(
      waypoints,
      speedHistogram,
      filterHalfSize,
      maxSpeed,
      totalTimeInMilliseconds,
      numberOfBadValues,
    );
    return _computePercents(speedHistogram, totalTime);
  }

  /// Computes from filtered speeds using simple moving average
  @visibleForTesting
  static int computeHistogramBins_(
    List<GeodesicPoint> waypoints,
    List<int> speedHistogram,
    final int filterHalfSize,
    final int maxSpeed,
    final int totalTimeInMillisecond,
    final int numberOfBadValues,
  ) {
    double distanceBetweenInMeter(GeodesicPoint waypoint1, GeodesicPoint waypoint2) => GeodesicPoint.distanceBetweenInMeter(waypoint1.latitude, waypoint1.longitude, waypoint2.latitude, waypoint2.longitude);

    // If not enough points
    if (waypoints.length < 2 * filterHalfSize + 1 + numberOfBadValues) {
      return waypoints.isEmpty ? 0 : waypoints.last.timestampInMillisecond;
    }

    // 1. Speed filtering initialisation
    var filteredDistanceInMeter = 0.0;
    var filterSlopeSumInM = 0.0;
    for (int i = numberOfBadValues + 1; i <= math.min(waypoints.length - 1, numberOfBadValues + 2 * filterHalfSize + 1); i++) {
      filteredDistanceInMeter += distanceBetweenInMeter(waypoints[i - 1], waypoints[i]);
      filterSlopeSumInM += (waypoints[i].altitude - waypoints[i - 1].altitude) * distanceBetweenInMeter(waypoints[i - 1], waypoints[i]);
    }

    // 2. filter and compensation
    final int binSize = maxSpeed ~/ _HISTOGRAM_SIZE;
    int totalDurationInMillisecond = 0;
    for (int i = numberOfBadValues + filterHalfSize + 2; i < waypoints.length - filterHalfSize; i++) {
      // Filter
      final last = i + filterHalfSize;
      final first = i - filterHalfSize - 1;

      final distance0 = distanceBetweenInMeter(waypoints[first - 1], waypoints[first]);
      final distance1 = distanceBetweenInMeter(waypoints[last - 1], waypoints[last]);
      filteredDistanceInMeter += -distance0 + distance1;
      var slope0 = waypoints[first].altitude - waypoints[first - 1].altitude;
      var slope1 = waypoints[last].altitude - waypoints[last - 1].altitude;
      filterSlopeSumInM += -(slope0) * distance0 + (slope1) * distance1;

      final filteredDurationInMillisecond = waypoints[last].timestampInMillisecond - waypoints[first].timestampInMillisecond;
      if (totalDurationInMillisecond > totalTimeInMillisecond) {
        return totalDurationInMillisecond;
      }
      final double filteredSpeed = SpeedCalculator.computeSpeedInKmh(filteredDistanceInMeter, filteredDurationInMillisecond);
      // Compensate : speed-effort
      double slopeInPercent;
      if (filteredDistanceInMeter == 0) {
        slopeInPercent = 0.0;
      } else {
        final slopeInM = (filterHalfSize * 2 + 1) * filterSlopeSumInM / filteredDistanceInMeter;
        slopeInPercent = 100 * slopeInM / filteredDistanceInMeter;
      }
      final weighting = SpeedCalculator.slopeAdjustment(slopeInPercent);
      var speedInKmH = (filteredSpeed * weighting).toInt();
      if (speedInKmH > maxSpeed) {
        speedInKmH = maxSpeed;
      }
      final int numberOfMillisecondsWithThisSpeed = waypoints[i].timestampInMillisecond - waypoints[i - 1].timestampInMillisecond;
      totalDurationInMillisecond += numberOfMillisecondsWithThisSpeed;
      speedHistogram[speedInKmH ~/ binSize] += numberOfMillisecondsWithThisSpeed;
    }
    return totalDurationInMillisecond;
  }

  static List<SpeedInKmH> _computePercents(List<int> speedHistogram, int totalTime) {
    if (totalTime > 0) {
      final List<SpeedInKmH> data = [];
      for (int i = 0; i < speedHistogram.length; i++) {
        double percent = 100.0 * speedHistogram[i] / totalTime;
        data.add(SpeedInKmH(i, percent));
      }
      return data;
    } else {
      return List.generate(speedHistogram.length, (i) => SpeedInKmH(i, 0.0));
    }
  }
}
