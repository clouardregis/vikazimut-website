import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;

abstract class ProfileConstants {
  const ProfileConstants();

  List<charts.TickSpec<String>> getTicksXAxis();

  int get maxSpeed;

  int get speedPerBin;
}
