import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;

import 'profile_constants.dart';

class MtboConstants extends ProfileConstants {
  const MtboConstants();

  @override
  List<charts.TickSpec<String>> getTicksXAxis() {
    return <charts.TickSpec<String>>[
      const charts.TickSpec("0", label: "0\nkm/h"),
      const charts.TickSpec("5", label: "10\nkm/h"),
      const charts.TickSpec("10", label: "20\nkm/h"),
      const charts.TickSpec("15", label: "30\nkm/h"),
      const charts.TickSpec("20", label: "40\nkm/h"),
    ];
  }

  @override
  int get maxSpeed => 40;

  @override
  int get speedPerBin => 2;
}
