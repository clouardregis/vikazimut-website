import 'package:vikazimut_website/l10n/l10n.dart';

import '../orienteer_grades.dart';
import '../speed_in_kmh.dart';
import 'decision_tree.dart';
import 'footo_constants.dart';

class UrbanDecisionTree extends DecisionTree {
  UrbanDecisionTree() : super(const FootoConstants());

  @override
  OrienteerProfile calculateOrienteeringLevel(List<SpeedInKmH> data) {
    if (isNotEnoughData(data)) {
      return OrienteerProfile(OrienteerGrade.WOOD1);
    }
    double percent = computePercentOfStoppingPeriod(data);
    if (percent >= 15) {
      return _beginner1Profile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics1"))
        ..addAdvice(L10n.getString("profile_advice1"));
    } else if (percent >= 10) {
      return _beginner2Profile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics2"))
        ..addAdvice(L10n.getString("profile_advice1"));
    } else if (percent >= 5) {
      return _beginner3Profile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics3"))
        ..addAdvice(L10n.getString("profile_advice1"));
    } else {
      return _practitionerProfile(data)..addCharacteristic(L10n.getString("profile_characteristics4"));
    }
  }

  OrienteerProfile _beginner1Profile(List<SpeedInKmH> data) {
    final bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.WOOD1)
        ..addCharacteristic(L10n.getString("profile_characteristics5"))
        ..addAdvice(L10n.getString("profile_advice5"));
    } else {
      return OrienteerProfile(OrienteerGrade.WOOD2)..addCharacteristic(L10n.getString("profile_characteristics6"));
    }
  }

  OrienteerProfile _beginner2Profile(List<SpeedInKmH> data) {
    bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.WOOD2)
        ..addCharacteristic(L10n.getString("profile_characteristics5"))
        ..addAdvice(L10n.getString("profile_advice5"));
    } else {
      return OrienteerProfile(OrienteerGrade.BRONZE1)..addCharacteristic(L10n.getString("profile_characteristics6"));
    }
  }

  OrienteerProfile _beginner3Profile(List<SpeedInKmH> data) {
    final bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.BRONZE1)
        ..addCharacteristic(L10n.getString("profile_characteristics5"))
        ..addAdvice(L10n.getString("profile_advice5"));
    } else {
      return OrienteerProfile(OrienteerGrade.BRONZE2)..addCharacteristic(L10n.getString("profile_characteristics6"));
    }
  }

  OrienteerProfile _practitionerProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMinimumCumulatedSpeed(data, 10);
    if (speedInKmh < 8) {
      return _confirmedProfile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics7"))
        ..addAdvice(L10n.getString("profile_advice7"));
    } else {
      return _expert1Profile(data)..addCharacteristic(L10n.getString("profile_characteristics8"));
    }
  }

  OrienteerProfile _confirmedProfile(List<SpeedInKmH> data) {
    int numberOfClasses = computeHistogramWidth(data, 7);
    if (numberOfClasses >= 8) {
      return _rookieProfile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics9"))
        ..addAdvice(L10n.getString("profile_advice9"));
    } else {
      return _proficientProfile(data)..addCharacteristic(L10n.getString("profile_characteristics10"));
    }
  }

  OrienteerProfile _rookieProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMaximumPeak(data);
    if (speedInKmh < 6) {
      return OrienteerProfile(OrienteerGrade.BRONZE2)
        ..addCharacteristic(L10n.getString("profile_characteristics11"))
        ..addAdvice(L10n.getString("profile_advice5"))
        ..addAdvice(L10n.getString("profile_advice11"));
    } else if (speedInKmh < 12) {
      return OrienteerProfile(OrienteerGrade.SILVER1)..addCharacteristic(L10n.getString("profile_characteristics12"));
    } else {
      return OrienteerProfile(OrienteerGrade.SILVER2)..addCharacteristic(L10n.getString("profile_characteristics13"));
    }
  }

  OrienteerProfile _proficientProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMaximumPeak(data);
    if (speedInKmh < 10) {
      return OrienteerProfile(OrienteerGrade.SILVER1)
        ..addCharacteristic(L10n.getString("profile_characteristics12"))
        ..addAdvice(L10n.getString("profile_advice12"));
    } else if (speedInKmh < 14) {
      return OrienteerProfile(OrienteerGrade.SILVER2)..addCharacteristic(L10n.getString("profile_characteristics13"));
    } else {
      return OrienteerProfile(OrienteerGrade.GOLD1)..addCharacteristic(L10n.getString("profile_characteristics14"));
    }
  }

  OrienteerProfile _expert1Profile(List<SpeedInKmH> data) {
    int speedInKmh = computeMaximumCumulatedSpeed(data, 12);
    if (speedInKmh <= 14) {
      return _expertProfile(data)..addCharacteristic(L10n.getString("profile_characteristics18"));
    } else if (speedInKmh < 17) {
      return OrienteerProfile(OrienteerGrade.DIAMOND1)
        ..addCharacteristic(L10n.getString("profile_characteristics17"))
        ..addAdvice(L10n.getString("profile_advice17"));
    } else {
      return OrienteerProfile(OrienteerGrade.DIAMOND2)
        ..addCharacteristic(L10n.getString("profile_characteristics16"))
        ..addAdvice(L10n.getString("profile_advice16"));
    }
  }

  OrienteerProfile _expertProfile(List<SpeedInKmH> data) {
    int numberOfClasses = computeHistogramWidth(data, 7);
    if (numberOfClasses >= 8) {
      return _eliteProfile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics9"))
        ..addAdvice(L10n.getString("profile_advice9"));
    } else {
      return _championProfile(data)
        ..addCharacteristic(L10n.getString("profile_characteristics10"))
        ..addAdvice(L10n.getString("profile_advice17"));
    }
  }

  OrienteerProfile _eliteProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMaximumPeak(data);
    if (speedInKmh < 12) {
      return OrienteerProfile(OrienteerGrade.GOLD1)..addCharacteristic(L10n.getString("profile_characteristics12"));
    } else if (speedInKmh < 14) {
      return OrienteerProfile(OrienteerGrade.GOLD2)..addCharacteristic(L10n.getString("profile_characteristics13"));
    } else {
      return OrienteerProfile(OrienteerGrade.DIAMOND1)
        ..addCharacteristic(L10n.getString("profile_characteristics14"))
        ..addAdvice(L10n.getString("profile_advice17"));
    }
  }

  OrienteerProfile _championProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMaximumPeak(data);
    if (speedInKmh < 14) {
      return OrienteerProfile(OrienteerGrade.GOLD2)..addCharacteristic(L10n.getString("profile_characteristics13"));
    } else {
      return OrienteerProfile(OrienteerGrade.DIAMOND1)
        ..addCharacteristic(L10n.getString("profile_characteristics14"))
        ..addAdvice(L10n.getString("profile_advice17"));
    }
  }
}
