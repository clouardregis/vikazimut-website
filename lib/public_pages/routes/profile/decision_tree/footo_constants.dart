import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;

import 'profile_constants.dart';

class FootoConstants extends ProfileConstants {
  const FootoConstants();

  @override
  List<charts.TickSpec<String>> getTicksXAxis() {
    return <charts.TickSpec<String>>[
      const charts.TickSpec("0", label: "0\nkm/h"),
      const charts.TickSpec("5", label: "5\nkm/h"),
      const charts.TickSpec("10", label: "10\nkm/h"),
      const charts.TickSpec("15", label: "15\nkm/h"),
      const charts.TickSpec("20", label: "20\nkm/h"),
    ];
  }

  @override
  int get maxSpeed => 20;

  @override
  int get speedPerBin => 1;
}
