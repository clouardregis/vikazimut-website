import '../orienteer_grades.dart';
import '../speed_in_kmh.dart';
import 'decision_tree.dart';
import 'skio_constants.dart';

class SKIODecisionTree extends DecisionTree {
  SKIODecisionTree() : super(const SkioConstants());

  @override
  OrienteerProfile calculateOrienteeringLevel(List<SpeedInKmH> data) {
    if (isNotEnoughData(data)) {
      return OrienteerProfile(OrienteerGrade.WOOD1);
    }
    double percent = computePercentOfStoppingPeriod(data);
    if (percent >= 18) {
      return _beginner1Profile(data);
    } else if (percent >= 12) {
      return _beginner2Profile(data);
    } else if (percent >= 8) {
      return _beginner3Profile(data);
    } else {
      return _practitionerProfile(data);
    }
  }

  OrienteerProfile _beginner1Profile(List<SpeedInKmH> data) {
    final bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.WOOD1);
    } else {
      return OrienteerProfile(OrienteerGrade.WOOD2);
    }
  }

  OrienteerProfile _beginner2Profile(List<SpeedInKmH> data) {
    bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.WOOD2);
    } else {
      return OrienteerProfile(OrienteerGrade.BRONZE1);
    }
  }

  OrienteerProfile _beginner3Profile(List<SpeedInKmH> data) {
    final bool isRunningPeriod = isThereRunningPeriod(data);
    if (!isRunningPeriod) {
      return OrienteerProfile(OrienteerGrade.BRONZE1);
    } else {
      return OrienteerProfile(OrienteerGrade.BRONZE2);
    }
  }

  OrienteerProfile _practitionerProfile(List<SpeedInKmH> data) {
    int numberOfClasses = computeHistogramWidth(data, 7);
    if (numberOfClasses >= 8) {
      return _rookieProfile(data);
    } else {
      return _confirmedProfile(data);
    }
  }

  OrienteerProfile _rookieProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMinSpeedGreaterThan(data, 7);
    if (speedInKmh < 6) {
      return OrienteerProfile(OrienteerGrade.BRONZE2);
    } else if (speedInKmh <= 8) {
      return OrienteerProfile(OrienteerGrade.SILVER1);
    } else {
      return OrienteerProfile(OrienteerGrade.SILVER2);
    }
  }

  OrienteerProfile _confirmedProfile(List<SpeedInKmH> data) {
    double percent = computePercentOfStoppingPeriod(data);
    if (percent >= 5) {
      return _proficientProfile(data);
    } else {
      return _expertProfile(data);
    }
  }

  OrienteerProfile _proficientProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMinSpeedGreaterThan(data, 7);
    if (speedInKmh < 6) {
      return OrienteerProfile(OrienteerGrade.SILVER1);
    } else if (speedInKmh <= 8) {
      return OrienteerProfile(OrienteerGrade.SILVER2);
    } else {
      return OrienteerProfile(OrienteerGrade.GOLD1);
    }
  }

  OrienteerProfile _expertProfile(List<SpeedInKmH> data) {
    int numberOfClasses = computeHistogramWidth(data, 7);
    if (numberOfClasses >= 6) {
      return _eliteProfile(data);
    } else {
      return _championProfile(data);
    }
  }

  OrienteerProfile _eliteProfile(List<SpeedInKmH> data) {
    int speedInKmh = computeMinSpeedGreaterThan(data, 7);
    if (speedInKmh > 8) {
      return OrienteerProfile(OrienteerGrade.GOLD2);
    } else {
      return OrienteerProfile(OrienteerGrade.GOLD1);
    }
  }

  OrienteerProfile _championProfile(List<SpeedInKmH> data) {
    int speed = computeMaximumPeak(data);
    if (speed < 13) {
      return OrienteerProfile(OrienteerGrade.GOLD1);
    } else if (speed > 15) {
      return OrienteerProfile(OrienteerGrade.DIAMOND2);
    } else {
      return OrienteerProfile(OrienteerGrade.DIAMOND1);
    }
  }
}
