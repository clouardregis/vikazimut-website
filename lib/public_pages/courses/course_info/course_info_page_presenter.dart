import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'course_detail.dart';

class CourseInfoPagePresenter {
  IdentifiedUser? user;

  Future<CourseDetail?> loadCourseDetail(int courseId) async {
    user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    Map<String, String>? headers;
    if (accessToken != null) {
      headers = {
        HttpHeaders.authorizationHeader: "Bearer $accessToken",
      };
    }
    final response = await http.get(
      Uri.parse("${constants.API_SERVER}/courses/detail/$courseId"),
      headers: headers,
    );
    if (response.statusCode == HttpStatus.ok) {
      var data = jsonDecode(response.body);
      return Future.value(CourseDetail.fromJson(data));
    } else {
      final responseContent = jsonDecode(response.body);
      final String message;
      if (responseContent is Map && responseContent.containsKey("message")) {
        message = responseContent["message"];
      } else {
        message = responseContent;
      }
      return Future.error(message);
    }
  }
}
