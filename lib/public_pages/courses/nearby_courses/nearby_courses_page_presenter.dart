import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/constants.dart' as constants;

import 'nearby_course_model.dart';

class NearbyCoursesPagePresenter {
  static const int RADIUS_IN_M = 500;

  Future<List<NearbyCourseModel>> fetchMapList({required String latitude, required String longitude, int? courseId}) async {
    try {
      final response = await http.get(Uri.parse("${constants.API_SERVER}/routes/nearby/$RADIUS_IN_M-{$latitude}-{$longitude}"));
      if (response.statusCode == HttpStatus.ok) {
        return buildCourseList_(response.body, courseId);
      } else {
        return Future.error(response.body);
      }
    } catch (_) {
      return Future.error("Server connection error");
    }
  }

  @visibleForTesting
  List<NearbyCourseModel> buildCourseList_(String responseBody, int? courseId) {
    List<NearbyCourseModel> courses = _parseNearbyCourses(responseBody);
    courses.sort((a, b) => compareNearbyCourses(a, b, courseId));
    return courses;
  }

  static List<NearbyCourseModel> _parseNearbyCourses(String responseBody) {
    if (responseBody.isEmpty) {
      return [];
    }
    var parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<NearbyCourseModel>((json) => NearbyCourseModel.fromJson(json)).toList();
  }

  @visibleForTesting
  static int compareNearbyCourses(NearbyCourseModel a, NearbyCourseModel b, int? courseId) {
    if (a.id == courseId) return -1;
    if (b.id == courseId) return 1;
    if (a.distance == b.distance) return a.name.compareTo(b.name);
    return a.distance.compareTo(b.distance);
  }
}

class CourseList {
  List<NearbyCourseModel> selectedCourses;
  List<NearbyCourseModel> nearbyCourses;

  CourseList(this.selectedCourses, this.nearbyCourses);
}
