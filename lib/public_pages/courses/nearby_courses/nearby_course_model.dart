import 'package:flutter/foundation.dart';
import 'package:vikazimut_website/common/discipline.dart';

@immutable
class NearbyCourseModel {
  final int id;
  final String name;
  final String? club;
  final String? clubUrl;
  final bool touristic;
  final int tracks;
  final bool printable;
  final int distance;
  final Discipline? discipline;

  const NearbyCourseModel({
    required this.id,
    required this.name,
    required this.tracks,
    required this.distance,
    this.club,
    this.clubUrl,
    this.touristic = false,
    this.printable = false,
    this.discipline,
  });

  factory NearbyCourseModel.fromJson(Map<String, dynamic> json) {
    var touristic = json["touristic"] as int?;
    var printable = json["printable"] as bool?;
    var club = json["club"] as String?;
    if (club != null && club.isEmpty) {
      club = null;
    }
    var clubUrl = json["cluburl"] as String?;
    if (clubUrl != null && clubUrl.isEmpty) {
      clubUrl = null;
    }
    return NearbyCourseModel(
      id: json["id"] as int,
      name: json["name"] as String,
      tracks: json["tracks"] as int,
      distance: json["distance"] as int,
      club: club,
      clubUrl: clubUrl,
      touristic: touristic == null ? false : touristic == 1,
      printable: printable ?? false,
      discipline: Discipline.toEnum(json["discipline"] as int?),
    );
  }
}
