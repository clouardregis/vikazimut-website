// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/diacritic.dart';

class SearchField extends StatelessWidget {
  final ValueNotifier<List<SearchFieldListItem>> _courseNames;
  final Function(SearchFieldListItem option) _onSelected;

  const SearchField(this._courseNames, this._onSelected);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ValueListenableBuilder<List<SearchFieldListItem>>(
        valueListenable: _courseNames,
        builder: (context, value, child) {
          return Autocomplete<SearchFieldListItem>(
            fieldViewBuilder: (
              BuildContext context,
              TextEditingController textEditingController,
              FocusNode focusNode,
              VoidCallback onFieldSubmitted,
            ) {
              return TextFormField(
                cursorColor: kOrangeColor,
                controller: textEditingController,
                style: const TextStyle(color: kOrangeColor),
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: const BorderSide(
                      width: 2,
                      color: kOrangeColor,
                    ),
                  ),
                  labelText: L10n.getString("worldmap_search_hint_text"),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                ),
                focusNode: focusNode,
                onFieldSubmitted: (String value) {
                  onFieldSubmitted();
                },
              );
            },
            optionsViewBuilder: _fieldViewBuilder,
            optionsBuilder: (TextEditingValue textEditingValue) {
              if (textEditingValue.text == '') {
                return const Iterable<SearchFieldListItem>.empty();
              }
              var removeDiacritics2 = removeDiacritics(textEditingValue.text.toLowerCase());
              return _courseNames.value.where((SearchFieldListItem suggestion) {
                return suggestion._searchKey.contains(removeDiacritics2);
              });
            },
            displayStringForOption: (SearchFieldListItem item) => item.item,
            onSelected: (SearchFieldListItem selection) {
              _onSelected(selection);
              FocusManager.instance.primaryFocus?.unfocus();
            },
          );
        },
      ),
    );
  }

  static Widget _fieldViewBuilder(BuildContext _, AutocompleteOnSelected<SearchFieldListItem> onSelected, Iterable<SearchFieldListItem> options) {
    return Align(
      alignment: Alignment.topLeft,
      child: Material(
        elevation: 4.0,
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 280, maxWidth: 580),
          child: ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: options.length,
            itemBuilder: (BuildContext context, int index) {
              final SearchFieldListItem option = options.elementAt(index);
              return InkWell(
                onTap: () {
                  onSelected(option);
                },
                child: Builder(builder: (BuildContext context) {
                  final bool highlight = AutocompleteHighlightedOption.of(context) == index;
                  if (highlight) {
                    SchedulerBinding.instance.addPostFrameCallback((Duration timeStamp) {
                      Scrollable.ensureVisible(context, alignment: 0.5);
                    });
                  }
                  return Container(
                    color: highlight ? Theme.of(context).hoverColor : null,
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      option.item,
                      style: const TextStyle(color: kOrangeColor),
                    ),
                  );
                }),
              );
            },
          ),
        ),
      ),
    );
  }
}

class SearchFieldListItem {
  final String item;
  late final String _searchKey;

  SearchFieldListItem(this.item) {
    _searchKey = removeDiacritics(item).toLowerCase();
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) || other is SearchFieldListItem && runtimeType == other.runtimeType && _searchKey == other._searchKey;
  }

  @override
  int get hashCode => _searchKey.hashCode;
}
