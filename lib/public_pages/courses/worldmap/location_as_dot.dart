import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

class LocationAsDot {
  static const int _MAX_DISTANCE_IN_M = 80;

  final double latitude;
  final double longitude;
  final List<String> names;

  const LocationAsDot(this.latitude, this.longitude, this.names);

  bool isAtSamePlace(double latitude, double longitude) => GeodesicPoint.distanceBetweenInMeter(this.latitude, this.longitude, latitude, longitude) <= _MAX_DISTANCE_IN_M;

  void addName(String name) => names.add(name);

  @override
  String toString() => names.join("\n");
}
