// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:go_router/go_router.dart';
import 'package:latlong2/latlong.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/public_pages/courses/map_zoom.dart';
import 'package:vikazimut_website/public_pages/courses/nearby_courses/nearby_courses_page_view.dart';
import 'package:vikazimut_website/public_pages/model/course.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/openstreetmap_contribution_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'geolocation.dart';
import 'location_as_dot.dart';
import 'search_field_widget.dart';
import 'worldmap_page_presenter.dart';

class WorldMapPageView extends StatefulWidget {
  static const String routePath = '/worldmap';
  final String? latitude;
  final String? longitude;
  final String? zoom;

  const WorldMapPageView({this.latitude, this.longitude, this.zoom});

  @override
  WorldMapPageViewState createState() => WorldMapPageViewState();
}

class WorldMapPageViewState extends State<WorldMapPageView> with TickerProviderStateMixin {
  final WorldMapPagePresenter _worldMapPagePresenter = WorldMapPagePresenter();
  final ValueNotifier<int> _numberOfCourses = ValueNotifier<int>(0);
  final ValueNotifier<List<SearchFieldListItem>> _courseNames = ValueNotifier<List<SearchFieldListItem>>([]);
  late final Future<List<Course>> _courseLoader;
  late final MapZoomController _mapZoomController;
  late final Future<GeoLocation> _currentLocationLoader;
  late final PopupController _popupController;

  List<_CustomMarker>? _markers;
  String? _currentSelectedCourse;

  @override
  void initState() {
    _popupController = PopupController();
    if (widget.longitude != null && widget.latitude != null) {
      final latitude = double.parse(widget.latitude!);
      final longitude = double.parse(widget.longitude!);
      final zoom = double.parse(widget.zoom!);
      _currentLocationLoader = Future<GeoLocation>.value(GeoLocation(latitude: latitude, longitude: longitude, zoom: zoom));
    } else {
      _currentLocationLoader = _worldMapPagePresenter.getUserLocation();
    }
    _courseLoader = _worldMapPagePresenter.fetchCourseList();
    _mapZoomController = MapZoomController(this);
    _courseLoader.then((value) {
      if (mounted) {
        _numberOfCourses.value = value.length;
        _courseNames.value = value.reversed.map((e) => SearchFieldListItem(e.name)).toList();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _worldMapPagePresenter.saveUserLocation();
    _numberOfCourses.dispose();
    _courseNames.dispose();
    _popupController.dispose();
    _mapZoomController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Center(
        child: Column(
          children: [
            const SizedBox(height: 5),
            ScreenHelper.isMobile(context)
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildCourseNameWidget(),
                      Flexible(fit: FlexFit.loose, child: SearchField(_courseNames, _onSelected)),
                    ],
                  )
                : Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(constraints: const BoxConstraints(maxWidth: 500), child: SearchField(_courseNames, _onSelected)),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.center,
                          child: _buildCourseNameWidget(),
                        ),
                      ),
                      const Expanded(child: SizedBox()),
                      Tooltip(
                        message: L10n.getString("worldmap_page_url_tooltip"),
                        child: OutlinedButton(
                          onPressed: () {
                            String url = _worldMapPagePresenter.geLinkOfCurrentRegion(_mapZoomController, WorldMapPageView.routePath);
                            showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                content: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(L10n.getString("worldmap_page_url_tooltip")),
                                    Text(
                                      url,
                                      style: const TextStyle(fontStyle: FontStyle.italic),
                                    ),
                                  ],
                                ),
                                actions: [
                                  CustomPlainButton(
                                    onPressed: () {
                                      Clipboard.setData(ClipboardData(text: url));
                                      Navigator.pop(context);
                                    },
                                    label: L10n.getString("worldmap_page_url_button_copy"),
                                    backgroundColor: kOrangeColor,
                                  )
                                ],
                              ),
                            );
                          },
                          child: const Icon(Icons.link),
                        ),
                      ),
                    ],
                  ),
            FutureBuilder<dynamic>(
              future: Future.wait([_currentLocationLoader, _courseLoader]),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                } else if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
                  return Expanded(
                    child: _displayWorldMap(snapshot.data[0]!, snapshot.data[1]!),
                  );
                } else {
                  return const Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                      ],
                    ),
                  );
                }
              },
            ),
            Footer(),
          ],
        ),
      ),
    );
  }

  Future<void> _onSelected(SearchFieldListItem selection) async {
    _currentSelectedCourse = selection.item;
    final selectedCourseIndex = _worldMapPagePresenter.getSelectCourseIndex(selection.item);
    if (selectedCourseIndex >= 0) {
      if (_markers != null) {
        final _CustomMarker marker = _markers![selectedCourseIndex];
        _mapZoomController.mapController.move(marker.point, 18);
        _popupController.togglePopup(marker);
      }
    }
  }

  ValueListenableBuilder<int> _buildCourseNameWidget() {
    return ValueListenableBuilder<int>(
      valueListenable: _numberOfCourses,
      builder: (context, value, widget) {
        return Text(
          sprintf(value == 0 ? "" : L10n.getString("worldmap_title"), [value]),
          style: Theme.of(context).textTheme.displayMedium,
        );
      },
    );
  }

  Widget _displayWorldMap(GeoLocation userLocation, List<Course> courses) {
    final latitude = userLocation.latitude;
    final longitude = userLocation.longitude;
    _markers = _createMarkersFromCourses(courses);
    return Stack(
      alignment: Alignment.topRight,
      children: [
        PopupScope(
          popupController: _popupController,
          child: FlutterMap(
            mapController: _mapZoomController.mapController,
            options: MapOptions(
              minZoom: 4,
              maxZoom: 18,
              interactionOptions: InteractionOptions(
                cursorKeyboardRotationOptions: CursorKeyboardRotationOptions.disabled(),
                flags: InteractiveFlag.all & ~InteractiveFlag.rotate,
                scrollWheelVelocity: 0.002,
              ),
              initialCenter: LatLng(latitude, longitude),
              initialZoom: userLocation.zoom,
              onPositionChanged: (MapCamera camera, bool hasGesture) {
                _worldMapPagePresenter.userLocation = GeoLocation(
                  latitude: camera.center.latitude,
                  longitude: camera.center.longitude,
                  zoom: camera.zoom,
                );
              },
              onTap: (_, __) {
                _popupController.hideAllPopups();
              },
            ),
            children: [
              TileLayer(
                urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                userAgentPackageName: 'fr.vikazim.vikazimut',
                tileProvider: CancellableNetworkTileProvider(),
              ),
              MarkerClusterLayerWidget(
                options: MarkerClusterLayerOptions(
                  spiderfyCircleRadius: ScreenHelper.isMobile(context) ? 300 : 100,
                  maxClusterRadius: 45,
                  size: const Size(40, 40),
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(50),
                  maxZoom: 15,
                  showPolygon: false,
                  markers: _markers!,
                  onMarkerTap: (Marker marker) {
                    Course? course;
                    if (_currentSelectedCourse != null) {
                      course = WorldMapPagePresenter.getCourseFromName(courses, _currentSelectedCourse!);
                    }
                    if (course != null && course.latitude == marker.point.latitude && course.longitude == marker.point.longitude) {
                      context.go("${NearbyCoursesPageView.routePath}?lat=${marker.point.latitude}&lon=${marker.point.longitude}&id=${course.id}");
                    } else {
                      context.go("${NearbyCoursesPageView.routePath}?lat=${marker.point.latitude}&lon=${marker.point.longitude}");
                    }
                  },
                  popupOptions: PopupOptions(
                    popupSnap: PopupSnap.markerCenter,
                    popupController: _popupController,
                    popupBuilder: (_, __) {
                      return IgnorePointer(
                        child: Container(
                          height: 70,
                          width: 70,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.transparent,
                            border: Border.all(color: kOrangeColor, width: 5),
                          ),
                        ),
                      );
                    },
                  ),
                  builder: (BuildContext context, List<Marker> markers) {
                    final total = markers.fold(0, (sum, item) => sum + (item as _CustomMarker).count);
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: kGreenColor.withValues(alpha: 0.7),
                      ),
                      child: Center(
                        child: Text(
                          total.toString(),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    );
                  },
                ),
              ),
              ScaleLayerWidget(
                options: ScaleLayerPluginOption(
                  lineColor: Colors.blue,
                  lineWidth: 2,
                  textStyle: const TextStyle(color: Colors.blue, fontSize: 12),
                ),
              ),
              OpenStreetMapAttributionWidget(),
            ],
          ),
        ),
        Positioned(
          left: 5.0,
          top: 5.0,
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: kOrangeColorUltraLight,
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(width: 1, color: kCaptionColor),
                ),
                child: IconButton(
                  icon: const Icon(Icons.add, size: 24, color: Colors.black),
                  onPressed: () => _mapZoomController.zoomIn(),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: kOrangeColorUltraLight,
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(width: 1, color: kCaptionColor),
                ),
                child: IconButton(
                  icon: const Icon(Icons.remove, size: 24, color: Colors.black),
                  onPressed: () => _mapZoomController.zoomOut(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<_CustomMarker> _createMarkersFromCourses(List<Course> courses) {
    List<LocationAsDot> dots = _worldMapPagePresenter.getDots(courses);
    List<_CustomMarker> markers = [];
    const icon = Icon(
      Icons.location_on,
      color: kOrangeColor,
      size: 28,
    );
    for (int i = 0; i < dots.length; i++) {
      var marker = _CustomMarker(
        point: LatLng(dots[i].latitude, dots[i].longitude),
        child: Tooltip(
          waitDuration: const Duration(seconds: 1),
          message: dots[i].names.join("\n"),
          child: icon,
        ),
        count: dots[i].names.length,
      );
      markers.add(marker);
    }
    return markers;
  }
}

class _CustomMarker extends Marker {
  final int count;

  const _CustomMarker({
    required super.point,
    required super.child,
    required this.count,
  });
}
