// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/public_pages/home/home_page_view.dart';
import 'package:vikazimut_website/public_pages/login/login_page_view.dart';
import 'package:vikazimut_website/authentication/authentication_helper.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';

class UserAvatar extends StatelessWidget {
  final IdentifiedUser? user;

  const UserAvatar({required this.user});

  @override
  Widget build(BuildContext context) {
    Color foreground;
    Color background;
    if (user == null) {
      foreground = Theme.of(context).appBarTheme.foregroundColor!;
      background = Theme.of(context).appBarTheme.backgroundColor!;
    } else {
      foreground = Theme.of(context).appBarTheme.backgroundColor!;
      background = Theme.of(context).appBarTheme.foregroundColor!;
    }
    return Ink(
      decoration: BoxDecoration(
        color: foreground,
        shape: BoxShape.circle,
      ),
      padding: const EdgeInsets.all(2),
      child: InkWell(
        child: CircleAvatar(
          radius: 20,
          backgroundColor: background,
          child: _generateAvatar(user, foreground),
        ),
        onTap: () {
          if (user == null) {
            GoRouter.of(context).go(LoginPageView.routePath);
          } else {
            AuthenticationHelper.logout();
            GoRouter.of(context).go(HomePageView.routePath);
          }
        },
      ),
    );
  }

  Widget _generateAvatar(IdentifiedUser? user, Color color) {
    return (user != null)
        ? Text(
            _getUserInitials(user.username),
            style: TextStyle(
              fontSize: 20,
              color: color,
              fontWeight: FontWeight.bold,
            ),
          )
        : Icon(
            Icons.person_outline,
            color: color,
            size: 30,
          );
  }

  String _getUserInitials(String username) {
    List<String> names = username.split(" ");
    String initials = "";
    int numWords = names.length;
    numWords = (numWords < 2) ? numWords : 2;

    for (var i = 0; i < numWords; i++) {
      initials += names[i][0];
    }
    return initials.toUpperCase();
  }
}
