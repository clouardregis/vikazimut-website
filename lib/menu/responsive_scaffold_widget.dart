// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/l10n/language_cubit.dart';
import 'package:vikazimut_website/l10n/language_entity.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/theme/theme_cubit.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'custom_app_bar.dart';
import 'custom_scaffold.dart';
import 'header_item.dart';
import 'menus.dart' as menu;
import 'user_avatar.dart';

class ResponsiveScaffoldWidget extends StatefulWidget {
  final Widget body;

  const ResponsiveScaffoldWidget({
    required this.body,
  });

  @override
  State<ResponsiveScaffoldWidget> createState() => _ResponsiveScaffoldWidgetState();
}

class _ResponsiveScaffoldWidgetState extends State<ResponsiveScaffoldWidget> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();
  static IdentifiedUser? _user;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<IdentifiedUser?>(
      future: IdentifiedUser.getUser(),
      initialData: _user,
      builder: (context, snapshot) {
        _user = snapshot.data;
        return CustomScaffold(
          key: _key,
          appBar: (ScreenHelper.isMobile(context)) ? _appBarMobile(context, _user) : _appBarDesktop(context, _user),
          endDrawer: (ScreenHelper.isMobile(context)) ? _drawerMobile(context, _user) : _drawerDesktop(context, _user),
          body: widget.body,
        );
      },
    );
  }

  AppBar _appBarMobile(BuildContext context, IdentifiedUser? user) {
    return AppBar(
      leading: IconButton(
        icon: AppTheme.isDarkMode(context) ? const Icon(Icons.wb_sunny_outlined) : const Icon(Icons.nightlight_outlined),
        onPressed: () => context.read<ThemeCubit>().switchTheme(),
      ),
      flexibleSpace: const SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: CustomAppBarBackground(Colors.white),
      ),
      actions: [
        Container(
          padding: const EdgeInsets.all(8.0),
          height: 60.0,
          alignment: Alignment.center,
          child: Text(
            user?.username ?? "",
            style: TextStyle(color: Theme.of(context).appBarTheme.foregroundColor, fontSize: 24),
          ),
        ),
      ],
    );
  }

  AppBar _appBarDesktop(BuildContext context, IdentifiedUser? user) {
    List<HeaderItem> actionItems = menu.publicActionItems;
    return AppBar(
      leading: IconButton(
        icon: AppTheme.isDarkMode(context) ? const Icon(Icons.sunny) : const Icon(Icons.nightlight_outlined),
        onPressed: context.read<ThemeCubit>().switchTheme,
      ),
      automaticallyImplyLeading: false,
      elevation: 2.0,
      shadowColor: Colors.white,
      flexibleSpace: const SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: CustomAppBarBackground(Colors.white),
      ),
      actions: [
        for (int i = 0; i < actionItems.length; i++) _buildAppBarActions(context, actionItems[i], fontSize: 16),
        _buildDropDownButton(menu.helpActionItems, fontSize: 16),
        const SizedBox(width: 6),
        const _LanguageDropDownMenu(),
        const SizedBox(width: 6),
        UserAvatar(user: user),
      ],
    );
  }

  Widget? _drawerDesktop(BuildContext context, IdentifiedUser? user) {
    if (user == null) {
      return null;
    }
    List<HeaderItem> actionItems = _buildDesktopDrawerMenuFrom(user);
    return Drawer(
      child: Material(
        color: Theme.of(context).appBarTheme.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 25.0, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              for (int i = 0; i < actionItems.length; i++) _buildListTile(context, actionItems[i], fontSize: 18),
            ],
          ),
        ),
      ),
    );
  }

  Widget _drawerMobile(BuildContext context, IdentifiedUser? user) {
    List<HeaderItem> actionItems = _buildMobileDrawerMenuFrom(user);
    return Drawer(
      child: Material(
        color: Theme.of(context).appBarTheme.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 5.0, 0, 0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (int i = 0; i < actionItems.length; i++) _buildListTile(context, actionItems[i], fontSize: 18),
                _buildDropDownButton(menu.helpActionItems, fontSize: 18),
                const Divider(
                  thickness: 1,
                  color: kOrangeColor,
                ),
                const _LanguageDropDownMenu(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildListTile(BuildContext context, HeaderItem item, {required double fontSize}) {
    if (item.isEmpty) {
      return const Divider(thickness: 1);
    } else {
      return _CustomListTile(
        leading: Icon(
          item.icon,
          color: Theme.of(context).appBarTheme.foregroundColor,
        ),
        title: Text(
          L10n.getString(item.title!),
          style: TextStyle(
            fontSize: fontSize,
            color: Theme.of(context).appBarTheme.foregroundColor,
          ),
        ),
        onTap: () {
          item.onTap!(context);
          Navigator.pop(context);
        },
      );
    }
  }

  Widget _buildAppBarActions(BuildContext context, HeaderItem item, {required double fontSize}) {
    return SizedBox(
      height: 60.0,
      child: MaterialButton(
        onPressed: () => item.onTap?.call(context),
        onLongPress: () => item.onLongTap!(context),
        child: Text(
          L10n.getString(item.title!),
          style: TextStyle(
            fontSize: 16,
            color: Theme.of(context).appBarTheme.foregroundColor,
          ),
        ),
      ),
    );
  }

  List<HeaderItem> _buildDesktopDrawerMenuFrom(IdentifiedUser user) {
    if (user.admin) {
      return menu.plannerActionItems + [HeaderItem.empty()] + menu.adminActionItems;
    } else {
      return menu.plannerActionItems;
    }
  }

  List<HeaderItem> _buildMobileDrawerMenuFrom(IdentifiedUser? user) {
    if (user == null) {
      return menu.publicActionItems + menu.loginActionItems;
    } else if (user.admin) {
      return menu.publicActionItems + [HeaderItem.empty()] + menu.plannerActionItems + [HeaderItem.empty()] + menu.adminActionItems + menu.logoutActionItems;
    } else {
      return menu.publicActionItems + [HeaderItem.empty()] + menu.plannerActionItems + menu.logoutActionItems;
    }
  }

  Widget _buildDropDownButton(List<HeaderItem> items, {required double fontSize}) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.all(8.0),
      height: 60.0,
      child: PopupMenuButton(
        tooltip: "",
        surfaceTintColor: Theme.of(context).colorScheme.surface,
        color: Theme.of(context).colorScheme.surface,
        child: Text(
          L10n.getString("menu_help"),
          style: TextStyle(fontSize: fontSize, color: Theme.of(context).appBarTheme.foregroundColor),
        ),
        itemBuilder: (BuildContext context) => <PopupMenuEntry>[
          for (var item in items)
            PopupMenuItem(
              child: ListTile(
                leading: Icon(
                  item.icon,
                  color: kOrangeColor,
                ),
                title: Text(L10n.getString(item.title!), style: const TextStyle(color: kOrangeColor)),
              ),
              onTap: () => item.onTap!(context),
            ),
        ],
      ),
    );
  }
}

class _LanguageDropDownMenu extends StatefulWidget {
  const _LanguageDropDownMenu();

  @override
  _LanguageDropDownMenuState createState() => _LanguageDropDownMenuState();
}

class _LanguageDropDownMenuState extends State<_LanguageDropDownMenu> {
  int _value = 0;

  @override
  void initState() {
    _value = Language.getCurrentLanguageIndex();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(5.0, 10, 5, 10),
      padding: const EdgeInsets.only(left: 5.0, right: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(color: Theme.of(context).appBarTheme.foregroundColor!),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<int>(
          elevation: 0,
          style: Theme.of(context).textTheme.labelLarge,
          dropdownColor: Theme.of(context).colorScheme.surface,
          value: _value,
          iconEnabledColor: kOrangeColor,
          items: L10n.locales.entries
              .map<DropdownMenuItem<int>>(
                (locale) => DropdownMenuItem<int>(
                  value: locale.value.index,
                  child: Text(
                    locale.value.language,
                    style: const TextStyle(color: kOrangeColor),
                  ),
                ),
              )
              .toList(),
          onChanged: (value) {
            setState(() {
              if (value != null) {
                _value = value;
                context.read<LanguageCubit>().changeLanguage(Language.getCode(_value));
              }
            });
            if (!ScreenHelper.isDesktop(context)) {
              Navigator.pop(context);
            }
          },
        ),
      ),
    );
  }
}

class _CustomListTile extends ListTile {
  const _CustomListTile({
    required Widget title,
    required Widget leading,
    required GestureTapCallback onTap,
  }) : super(
          title: title,
          leading: leading,
          onTap: onTap,
          visualDensity: const VisualDensity(vertical: -4),
          textColor: kOrangeColor,
        );
}
