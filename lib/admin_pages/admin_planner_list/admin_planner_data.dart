import 'dart:convert';

import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/utils/diacritic.dart';

class AdminPlannerData {
  final int id;
  String username;
  final int courseCount;
  String email;
  final int modifiedDateInMillisecond;
  final int createdDateInMillisecond;
  String? lastName;
  String? firstName;
  String? phone;
  String? languageCode;

  AdminPlannerData({
    this.id = 0,
    this.username = '',
    this.email = '',
    this.lastName = '',
    this.firstName = '',
    this.courseCount = 0,
    this.modifiedDateInMillisecond = -1,
    this.createdDateInMillisecond = -1,
  });

  factory AdminPlannerData.fromJson(Map<String, dynamic> json) {
    return AdminPlannerData(
      id: json["id"] as int,
      username: json["username"] as String,
      email: json["email"] as String,
      courseCount: json["courseCount"] as int,
      modifiedDateInMillisecond: json["modifiedDate"] as int,
      createdDateInMillisecond: json["createdDate"] as int,
    );
  }

  dynamic toJson() {
    return json.encode({
      "username": username,
      "email": email,
      "phone": phone,
      "lastname": lastName,
      "firstname": firstName,
      "message": L10n.getStringFromLanguage(languageCode!, "mailer_create_user_message"),
    });
  }

  static int sortOnUsername(bool ascending, AdminPlannerData a, AdminPlannerData b) {
    if (ascending) {
      return a.username.toLowerCase().toRawString().compareTo(b.username.toLowerCase().toRawString());
    } else {
      return b.username.toLowerCase().toRawString().compareTo(a.username.toLowerCase().toRawString());
    }
  }

  static int sortOnCourseCount(bool ascending, AdminPlannerData a, AdminPlannerData b) {
    if (ascending) {
      return a.courseCount.compareTo(b.courseCount);
    } else {
      return b.courseCount.compareTo(a.courseCount);
    }
  }

  static int sortOnDate(bool ascending, int date1, int date2) {
    if (ascending) {
      return date1.compareTo(date2);
    } else {
      return date2.compareTo(date1);
    }
  }

  static int sortOnModificationDate(bool ascending, AdminPlannerData a, AdminPlannerData b) {
    return sortOnDate(ascending, a.modifiedDateInMillisecond, b.modifiedDateInMillisecond);
  }

  static int sortOnCreationDate(bool ascending, AdminPlannerData a, AdminPlannerData b) {
    return sortOnDate(ascending, a.createdDateInMillisecond, b.createdDateInMillisecond);
  }
}
