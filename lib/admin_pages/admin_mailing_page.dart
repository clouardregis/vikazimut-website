// coverage:ignore-file
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/authentication/unauthorized_exception.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/error_container.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class AdminMailingPage extends StatefulWidget {
  static const String routePath = '/admin/mailing';

  const AdminMailingPage();

  @override
  State<AdminMailingPage> createState() => AdminMailingPageState();
}

class AdminMailingPageState extends State<AdminMailingPage> {
  final TextEditingController _subjectController = TextEditingController();
  final TextEditingController _messageController = TextEditingController();
  final ValueNotifier<String> _formController = ValueNotifier<String>("");
  final _AdminMailingPresenter _mailingPresenter = _AdminMailingPresenter();
  String? _failureMessage;
  bool _isMailSent = false;

  @override
  void dispose() {
    _subjectController.dispose();
    _messageController.dispose();
    _formController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: ScreenHelper.isDesktop(context) ? const EdgeInsets.all(0) : const EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Column(
              children: [
                ContainerDecoration(
                  constraints: const BoxConstraints(maxWidth: 700),
                  child: AutofillGroup(
                    child: Column(
                      children: [
                        Text(
                          L10n.getString("mailing_page_title"),
                          style: Theme.of(context).textTheme.displayMedium,
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 30),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            L10n.getString("mailing_page_subject_label"),
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        TextField(
                          autofocus: true,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(100),
                          ],
                          controller: _subjectController,
                          onChanged: (value) => _formController.value = "1$value",
                        ),
                        const SizedBox(height: 15),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            L10n.getString("mailing_page_message_label"),
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        TextFormField(
                          textInputAction: TextInputAction.newline,
                          autocorrect: true,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(500),
                          ],
                          maxLength: 500,
                          controller: _messageController,
                          keyboardType: TextInputType.multiline,
                          maxLines: 10,
                          onChanged: (value) => _formController.value = "2$value",
                        ),
                        const SizedBox(height: 15),
                        AnimatedBuilder(
                          animation: _formController,
                          builder: (_, __) => SizedBox(
                            width: double.infinity,
                            child: CustomPlainButton(
                              onPressed: _subjectController.text.isNotEmpty && _messageController.text.isNotEmpty
                                  ? () async {
                                      String errorMessage = await _mailingPresenter._sendLoginRequest(context, _subjectController.text, _messageController.text);
                                      if (errorMessage.isEmpty) {
                                        _failureMessage = null;
                                        setState(() {
                                          _isMailSent = true;
                                        });
                                        await Future.delayed(const Duration(seconds: 2));
                                        setState(() {
                                          _isMailSent = false;
                                          _subjectController.clear();
                                          _messageController.clear();
                                        });
                                      } else {
                                        setState(() {
                                          _failureMessage = L10n.getString(errorMessage);
                                        });
                                      }
                                    }
                                  : null,
                              backgroundColor: (_isMailSent) ? kSuccessColor : kOrangeColor,
                              label: (_isMailSent) ? L10n.getString("mailing_page_mail_sent") : L10n.getString("mailing_page_submit_button_label"),
                            ),
                          ),
                        ),
                        if (_failureMessage != null) ErrorContainer(_failureMessage!),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 40),
                Footer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _AdminMailingPresenter {
  final _AdminMailingGateway _model = _AdminMailingGateway();

  Future<String> _sendLoginRequest(BuildContext context, String subject, String message) async {
    try {
      return await _model._sendMailing(subject, message);
    } on UnauthorizedException {
      GoRouter.of(context).replace(AdminMailingPage.routePath);
      return Future.error("");
    }
  }
}

class _AdminMailingGateway {
  static const POST_URL = '${constants.API_SERVER}/admin/mailing';

  Future<String> _sendMailing(String subject, String message) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return throw UnauthorizedException();
    } else {
      try {
        final response = await http.post(
          Uri.parse(POST_URL),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
          body: jsonEncode({"subject": subject, "message": message}),
        );
        if (response.statusCode == HttpStatus.ok) {
          return "";
        } else {
          return jsonDecode(response.body);
        }
      } catch (_) {
        return "Server connection error";
      }
    }
  }
}
