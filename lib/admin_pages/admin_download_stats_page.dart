import 'package:vikazimut_website/constants.dart' as constants;
import 'package:vikazimut_website/common/download_stats/download_stats_page.dart';

class AdminDownloadStatsPage extends DownloadStatsPage {
  static const String routePath = '/admin/download-stats';
  static const URL = '${constants.API_SERVER}/admin/download_stats';
  static int currentDisplayedRowIndex = 0;

  const AdminDownloadStatsPage() : super(routePath, URL, true);
}
