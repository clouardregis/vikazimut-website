import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/admin_pages/admin_download_stats_page.dart';
import 'package:vikazimut_website/admin_pages/admin_event_list_page.dart';
import 'package:vikazimut_website/admin_pages/admin_mailing_page.dart';
import 'package:vikazimut_website/admin_pages/admin_planner_list/admin_planner_list_view.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/planner_pages/my_partners/detail/community_course_missing_checkpoints_page.dart';
import 'package:vikazimut_website/planner_pages/my_partners/detail/community_course_stats_view.dart';
import 'package:vikazimut_website/planner_pages/my_partners/list/community_list_page_view.dart';
import 'package:vikazimut_website/planner_pages/home/planner_doc_page/planner_doc_page.dart';
import 'package:vikazimut_website/planner_pages/home/planner_home_view.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/planner_course_add_view.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_delete_tracks_page.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/planner_course_edit_view.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_list_page.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_missing_checkpoints_page.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_preview_page.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_qrcode_page/planner_course_qrcode_page.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_participants/planner_event_participant_view.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_course_page/planner_event_course_view.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_detail_page/planner_event_detail_page.dart';
import 'package:vikazimut_website/planner_pages/my_events/planner_event_list_view.dart';
import 'package:vikazimut_website/planner_pages/planner_download_stats_view.dart';
import 'package:vikazimut_website/planner_pages/profile/planner_profile_edit_view.dart';
import 'package:vikazimut_website/planner_pages/profile/planner_profile_page.dart';
import 'package:vikazimut_website/public_pages/courses/course_info/course_info_page_view.dart';
import 'package:vikazimut_website/public_pages/courses/nearby_courses/nearby_courses_page_view.dart';
import 'package:vikazimut_website/public_pages/courses/worldmap/worldmap_page_view.dart';
import 'package:vikazimut_website/public_pages/events/event_list_page_view.dart';
import 'package:vikazimut_website/public_pages/events/event_page_view.dart';
import 'package:vikazimut_website/public_pages/home/home_page_view.dart';
import 'package:vikazimut_website/public_pages/live_tracking/live_tracking_page.dart';
import 'package:vikazimut_website/public_pages/login/login_page_view.dart';
import 'package:vikazimut_website/public_pages/login/forgotten_password_view.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/url_routes/invalid_url_page.dart';

import 'not_found_page.dart';

class RouteGenerator {
  static final List<GoRoute> publicRoutes = [
    GoRoute(
      path: '/',
      pageBuilder: (context, state) => const NoTransitionPage(child: WorldMapPageView()),
    ),
    GoRoute(
      path: HomePageView.routePath,
      pageBuilder: (context, state) => NoTransitionPage(child: HomePageView()),
    ),
    GoRoute(
      path: HomePageView.aboutUsRoutePath,
      pageBuilder: (context, state) => NoTransitionPage(child: HomePageView(true)),
    ),
    GoRoute(
      path: WorldMapPageView.routePath,
      pageBuilder: (context, state) => NoTransitionPage(
        child: WorldMapPageView(
          latitude: state.uri.queryParameters['lat'],
          longitude: state.uri.queryParameters['lon'],
          zoom: state.uri.queryParameters['zoom'],
        ),
      ),
    ),
    GoRoute(
      path: "${CourseInfoViewPage.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: _isInteger(state.pathParameters['id']) ? CourseInfoViewPage(state.pathParameters['id']) : const InvalidUrlPage()),
    ),
    GoRoute(
      path: "${RouteListPageView.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(
        child: _isInteger(state.pathParameters['id']) ? RouteListPageView(state.pathParameters['id']) : const InvalidUrlPage(),
      ),
    ),
    GoRoute(
      path: NearbyCoursesPageView.routePath,
      pageBuilder: (context, state) => NoTransitionPage(
          child: _isDouble(state.uri.queryParameters['lat']) && _isDouble(state.uri.queryParameters['lon'])
              ? NearbyCoursesPageView(
                  latitude: state.uri.queryParameters['lat']!,
                  longitude: state.uri.queryParameters['lon']!,
                  courseId: state.uri.queryParameters['id'],
                )
              : const InvalidUrlPage()),
    ),
    GoRoute(
      path: EventListPageView.routePath,
      pageBuilder: (context, state) => NoTransitionPage(child: EventListPageView()),
    ),
    GoRoute(
      path: "${EventPageView.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: _isInteger(state.pathParameters['id']) ? EventPageView(state.pathParameters['id']) : const InvalidUrlPage()),
    ),
    GoRoute(
      path: ResetPasswordView.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: ResetPasswordView()),
    ),
    GoRoute(
      path: LoginPageView.routePath,
      pageBuilder: (context, state) => NoTransitionPage(child: LoginPageView(redirectTo: state.pathParameters['redirectTo'])),
    ),
    GoRoute(
      path: "${LiveTrackingPage.routePathPublic}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: _isInteger(state.pathParameters['id']) ? LiveTrackingPage(state.pathParameters['id'], false) : const InvalidUrlPage()),
    ),
    GoRoute(
      path: CommunityCourseStatsView.routePath,
      pageBuilder: (context, state) {
        String url = state.uri.query;
        const String TOKEN_PATTERN = "token=";
        var indexOf = url.indexOf(TOKEN_PATTERN);
        String token = url.substring(indexOf + TOKEN_PATTERN.length);
        return NoTransitionPage(child: CommunityCourseStatsView(token: token));
      },
    ),
    GoRoute(
      path: CommunityCourseMissingCheckpointsPage.routePath,
      pageBuilder: (BuildContext context, GoRouterState state) {
        var courseId = state.uri.queryParameters['course_id'];
        var token = state.uri.queryParameters['token'];
        return NoTransitionPage(child: CommunityCourseMissingCheckpointsPage(courseId: int.parse(courseId!), token: token!));
      },
    ),
  ];

  static final List<GoRoute> plannerRoutes = [
    GoRoute(
      path: '/planner',
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerHomeView()),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: PlannerHomeView.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerHomeView()),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: PlannerDocPage.routePath,
      pageBuilder: (context, state) => NoTransitionPage(child: PlannerDocPage(section: state.uri.queryParameters['section'])),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: PlannerDownloadStatsPage.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerDownloadStatsPage()),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCourseListPage.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: PlannerCourseListPage(state.pathParameters['id'])),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerEventListView.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: PlannerEventListView(state.pathParameters['id'])),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerProfilePage.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: PlannerProfilePage(state.pathParameters['id'])),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: PlannerCourseAddPage.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerCourseAddPage()),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerProfileEditView.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerProfileEditView(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCoursePreviewPage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerCoursePreviewPage(state.pathParameters['id'], state.uri.queryParameters['reload']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCourseDeleteTracksPage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerCourseDeleteTracksPage(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCourseEditPage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerCourseEditPage(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCourseQrCodePage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerCourseQrCodePage(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerCourseMissingCheckpointsPage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerCourseMissingCheckpointsPage(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerEventDetailPage.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerEventDetailPage(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerEventCourseView.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerEventCourseView(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${PlannerEventParticipantView.routePath}/:id",
      pageBuilder: (context, state) {
        if (_isInteger(state.pathParameters['id'])) {
          return NoTransitionPage(child: PlannerEventParticipantView(state.pathParameters['id']));
        } else {
          return const NoTransitionPage(child: InvalidUrlPage());
        }
      },
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${CommunityListPageView.routePath}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: CommunityListPageView(state.pathParameters['id'])),
      redirect: _checkPlannerAuthentication,
    ),
    GoRoute(
      path: "${LiveTrackingPage.routePathPlanner}/:id",
      pageBuilder: (context, state) => NoTransitionPage(child: LiveTrackingPage(state.pathParameters['id'], true)),
      redirect: _checkPlannerAuthentication,
    ),
  ];

  static final List<GoRoute> adminRoutes = [
    GoRoute(
      path: '/admin',
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerHomeView()),
      redirect: _checkAdminAuthentication,
    ),
    GoRoute(
      path: PlannerHomeView.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: PlannerHomeView()),
      redirect: _checkAdminAuthentication,
    ),
    GoRoute(
      path: AdminDownloadStatsPage.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: AdminDownloadStatsPage()),
      redirect: _checkAdminAuthentication,
    ),
    GoRoute(
      path: AdminPlannerListView.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: AdminPlannerListView()),
      redirect: _checkAdminAuthentication,
    ),
    GoRoute(
      path: AdminEventListPage.routePath,
      pageBuilder: (context, state) => NoTransitionPage(child: AdminEventListPage(null)),
      redirect: _checkAdminAuthentication,
    ),
    GoRoute(
      path: AdminMailingPage.routePath,
      pageBuilder: (context, state) => const NoTransitionPage(child: AdminMailingPage()),
    ),
  ];

  static final router = GoRouter(
    initialLocation: "/",
    errorBuilder: (context, state) => const NotFoundPage(),
    routes: [
      GoRoute(
        path: '/404',
        pageBuilder: (BuildContext context, GoRouterState state) {
          return const NoTransitionPage(child: NotFoundPage());
        },
      ),
      ...publicRoutes,
      ...plannerRoutes,
      ...adminRoutes,
    ],
  );

  static bool _isInteger(String? parameter) {
    if (parameter == null) return false;
    return int.tryParse(parameter) != null;
  }

  static bool _isDouble(String? parameter) {
    if (parameter == null) return false;
    return double.tryParse(parameter) != null;
  }
}

Future<String?>? _checkAdminAuthentication(BuildContext context, GoRouterState state) async {
  if (!await IdentifiedUser.isAuthenticatedAsAdmin()) {
    return LoginPageView.routePath;
  } else {
    return null;
  }
}

Future<String?>? _checkPlannerAuthentication(BuildContext context, GoRouterState state) async {
  if (!await IdentifiedUser.isAuthenticated()) {
    return LoginPageView.routePath;
  } else {
    return null;
  }
}
