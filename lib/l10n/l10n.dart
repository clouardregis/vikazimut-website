import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';

import 'en.dart';
import 'fr.dart';
import 'language_entity.dart';
import 'pt.dart';

final globalKeyContext = GlobalKey<ScaffoldMessengerState>();

class L10n {
  static const DEFAULT_LOCALE = "en";
  static Locale _currentLocale = const Locale(DEFAULT_LOCALE);
  static late Map<String, String> _localizedString;
  static Map<String, Localization> locales = {
    "en": const Localization(index: 0, code: "en", language: "English", strings: en),
    "fr": const Localization(index: 1, code: "fr", language: "Français", strings: fr),
    "pt": const Localization(index: 2, code: "pt", language: "Português", strings: pt),
  };

  static Locale get locale => _currentLocale;

  L10n._();

  static const LocalizationsDelegate<L10n> delegate = _AppLocalizationDelegate();

  static L10n of(BuildContext context) => Localizations.of<L10n>(context, L10n)!;

  static Future<L10n> _load(Locale locale) async {
    _currentLocale = locale;
    L10n translations = L10n._();
    Map<String, String>? localizedStrings = locales[locale.languageCode]?.strings;
    _localizedString = localizedStrings!;
    return translations;
  }

  static String getString(String key, [var parameter]) {
    // Keep the following instruction for test purpose
    if (globalKeyContext.currentContext == null) return key;
    return of(globalKeyContext.currentContext!)._getString(key, parameter);
  }

  String _getString(String key, [var parameter]) {
    if (_localizedString[key] == null) {
      return '** $key not found';
    }
    if (parameter != null) {
      if (parameter is List) {
        return sprintf(_localizedString[key]!, parameter);
      } else {
        return sprintf(_localizedString[key]!, [parameter]);
      }
    } else {
      return _localizedString[key]!;
    }
  }

  static String formatNumber(num number, int length) {
    NumberFormat formatter;
    if (length <= 1) {
      formatter = NumberFormat('#,##0.#', _currentLocale.languageCode);
    } else {
      formatter = NumberFormat('#,##0.##', _currentLocale.languageCode);
    }
    return formatter.format(number);
  }

  static String getStringFromLanguage(String languageCode, String key) {
    Map<String, String>? localStrings = locales[languageCode]?.strings;
    return localStrings![key]!;
  }

  static String translateIfPossible(String? errorText) {
    if (errorText == null) {
      return "";
    }
    if (_localizedString[errorText] == null) {
      return errorText;
    }
    return getString(errorText);
  }
}

class _AppLocalizationDelegate extends LocalizationsDelegate<L10n> {
  const _AppLocalizationDelegate();

  @override
  bool isSupported(Locale locale) {
    return Language.getSupportedLocales().contains(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate old) => false;

  @override
  Future<L10n> load(Locale locale) async => L10n._load(locale);
}

class Localization {
  final int index;
  final Map<String, String> strings;
  final String code;
  final String language;

  const Localization({required this.index, required this.strings, required this.code, required this.language});
}
