import 'package:flutter/material.dart';

import 'l10n.dart';

class Language {
  const Language._();

  static List<Locale> getSupportedLocales() {
    return L10n.locales.entries.map((MapEntry<String, Localization> e) => Locale(e.value.code)).toList();
  }

  static int getCurrentLanguageIndex() {
    final languageCode = L10n.locale.languageCode;
    for (var key in L10n.locales.keys) {
      if (languageCode == L10n.locales[key]!.code) {
        return L10n.locales[key]!.index;
      }
    }
    return 0;
  }

  static String getCode(int index) {
    for (var key in L10n.locales.keys) {
      if (L10n.locales[key]!.index == index) {
        return L10n.locales[key]!.code;
      }
    }
    return L10n.DEFAULT_LOCALE;
  }
}
