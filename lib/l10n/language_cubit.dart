import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut_website/constants.dart' as constants;

class LanguageCubit extends Cubit<Locale?> {
  LanguageCubit(super.initialLocale);

  void changeLanguage(String languageCode) async {
    emit(Locale(languageCode));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(constants.KEY_LANGUAGE, languageCode);
  }
}
