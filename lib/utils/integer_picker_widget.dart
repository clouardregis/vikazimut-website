// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class IntegerPickerWidget extends StatefulWidget {
  final int initialValue;
  final ValueChanged<int> onChanged;

  const IntegerPickerWidget({required this.initialValue, required this.onChanged});

  @override
  State<IntegerPickerWidget> createState() => _IntegerPickerWidgetState();
}

class _IntegerPickerWidgetState extends State<IntegerPickerWidget> {
  @override
  void initState() {
    _scrollController.text = "${widget.initialValue}";
    super.initState();
  }

  final TextEditingController _scrollController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenHelper.isDesktop(context) ? 60 : 84,
      foregroundDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.blueGrey,
          width: 2.0,
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextFormField(
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                contentPadding: ScreenHelper.isDesktop(context) ? const EdgeInsets.symmetric(vertical: 12, horizontal: 10) : const EdgeInsets.symmetric(vertical: 25.0, horizontal: 10.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
              controller: _scrollController,
              keyboardType: const TextInputType.numberWithOptions(
                decimal: false,
                signed: true,
              ),
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly,
              ],
            ),
          ),
          SizedBox(
            width: ScreenHelper.isDesktop(context) ? 30.0 : 36.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 0.5),
                    ),
                  ),
                  child: InkWell(
                    child: Icon(
                      Icons.arrow_drop_up,
                      size: ScreenHelper.isDesktop(context) ? 18.0 : 30.0,
                    ),
                    onTap: () {
                      int currentValue = int.parse(_scrollController.text);
                      setState(() {
                        currentValue++;
                        _scrollController.text = (currentValue).toString();
                        widget.onChanged.call(currentValue);
                      });
                    },
                  ),
                ),
                InkWell(
                  child: Icon(
                    Icons.arrow_drop_down,
                    size: ScreenHelper.isDesktop(context) ? 18.0 : 30.0,
                  ),
                  onTap: () {
                    int currentValue = int.parse(_scrollController.text);
                    if (currentValue >= 0) {
                      setState(() {
                        currentValue--;
                        _scrollController.text = (currentValue > 0 ? currentValue : 0).toString();
                        widget.onChanged.call(currentValue);
                      });
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
