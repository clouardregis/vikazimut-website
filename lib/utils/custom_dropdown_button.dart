// coverage:ignore-file
import 'package:flutter/material.dart';

class CustomDropdownButton<T> extends StatelessWidget {
  final double? height;
  final double? width;
  final List<DropdownMenuItem<T>>? items;
  final ValueChanged<T?>? onChanged;
  final T? value;
  final Widget? hintMessage;

  const CustomDropdownButton({
    required this.value,
    required this.items,
    required this.onChanged,
    this.width,
    this.height,
    this.hintMessage,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: const EdgeInsets.all(2),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: value == null ? Theme.of(context).disabledColor : Theme.of(context).primaryColor),
          borderRadius: BorderRadius.circular(10),
          boxShadow: const <BoxShadow>[
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.57),
              blurRadius: 5,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
          child: DropdownButton<T>(
            hint: hintMessage,
            value: value,
            onChanged: onChanged,
            underline: Container(),
            isExpanded: true,
            items: items,

            isDense: true,
          ),
        ),
      ),
    );
  }
}
