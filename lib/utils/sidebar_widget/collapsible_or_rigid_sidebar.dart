// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'collapsible_sidebar_widget.dart';
import 'rigid_sidebar_widget.dart';

class CollapsibleOrRigidSidebar extends StatelessWidget {
  final Widget sidebarTitle;
  final List<Widget> sidebarContents;
  final Widget contents;
  final bool isCollapsible;

  const CollapsibleOrRigidSidebar({
    required this.isCollapsible,
    required this.sidebarContents,
    required this.sidebarTitle,
    required this.contents,
  });

  @override
  Widget build(BuildContext context) {
    if (isCollapsible) {
      return Stack(
        children: [
          contents,
          CollapsibleSideBar(
            title: sidebarTitle,
            minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
            maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
            children: sidebarContents,
          ),
        ],
      );
    } else {
      return Flex(
        direction: Axis.horizontal,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RigidSideBar(
            title: sidebarTitle,
            minWidth: constants.COLLAPSIBLE_MIN_WIDTH,
            maxWidth: constants.COLLAPSIBLE_MAX_WIDTH,
            children: sidebarContents,
          ),
          Expanded(child: contents),
        ],
      );
    }
  }
}
