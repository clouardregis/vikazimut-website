// coverage:ignore-file
import 'package:flutter/material.dart';

enum Position { LEFT, RIGHT }

class RigidSideBar extends StatefulWidget {
  final Widget title;
  final List<Widget> children;
  final double minWidth;
  final double maxWidth;
  final EdgeInsetsGeometry? padding;

  const RigidSideBar({
    required this.title,
    required this.children,
    required this.minWidth,
    required this.maxWidth,
    this.padding,
  });

  @override
  RigidSideBarSideBarState createState() => RigidSideBarSideBarState();
}

class RigidSideBarSideBarState extends State<RigidSideBar> with SingleTickerProviderStateMixin {
  late final ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.maxWidth,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withValues(alpha: 0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3),
          ),
        ],
        color: Theme.of(context).scaffoldBackgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: widget.title,
          ),
          Expanded(
            child: Scrollbar(
              thumbVisibility: true,
              controller: _scrollController,
              child: ListView(
                padding: widget.padding,
                controller: _scrollController,
                shrinkWrap: true,
                children: widget.children,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
