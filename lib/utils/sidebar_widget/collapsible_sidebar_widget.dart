// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:vikazimut_website/theme/theme.dart';

enum Position { LEFT, RIGHT }

class CollapsibleSideBar extends StatefulWidget {
  final Widget title;
  final List<Widget> children;
  final double minWidth;
  final double maxWidth;
  final EdgeInsetsGeometry? padding;
  final Position position;
  final bool isOpen;

  const CollapsibleSideBar({
    required this.title,
    required this.children,
    required this.minWidth,
    required this.maxWidth,
    this.padding,
    this.isOpen = true,
    this.position = Position.LEFT,
  });

  @override
  CollapsibleSideBarState createState() => CollapsibleSideBarState();
}

class CollapsibleSideBarState extends State<CollapsibleSideBar> with SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  late AnimationController _animationController;
  late Animation<double> _widthAnimation;
  bool _isCollapsed = false;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 200));
    if (widget.isOpen) {
      _isCollapsed = true;
    }
    _widthAnimation = Tween<double>(begin: widget.minWidth, end: widget.maxWidth).animate(_animationController);
    SchedulerBinding.instance.addPostFrameCallback((_) {
      if (widget.isOpen) _animationController.forward();
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) => Material(
        type: MaterialType.transparency,
        child: Ink(
          color: Colors.white.withValues(alpha: 0.1), // Tips to avoid bad redraw effect of the collapsible widget
          width: _widthAnimation.value,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: (widget.position == Position.LEFT)
                ? [
                    _buildContentWidget(),
                    _buildButtonBar(),
                  ]
                : [
                    _buildButtonBar(),
                    _buildContentWidget(),
                  ],
          ),
        ),
      ),
    );
  }

  Flexible _buildContentWidget() {
    return Flexible(
      child: _widthAnimation.value == widget.maxWidth
          ? Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withValues(alpha: 0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: const Offset(0, 3),
                  ),
                ],
                color: Theme.of(context).scaffoldBackgroundColor,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: widget.title,
                  ),
                  Expanded(
                    child: ScrollConfiguration(
                      behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
                      child: ListView(
                        padding: widget.padding,
                        controller: _scrollController,
                        shrinkWrap: true,
                        children: widget.children,
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Container(
              color: Theme.of(context).scaffoldBackgroundColor,
            ),
    );
  }

  Widget _buildButtonBar() {
    return Align(
      alignment: widget.position == Position.LEFT ? const Alignment(0, -1) : const Alignment(0, 0.7),
      child: GestureDetector(
        onTap: () {
          setState(() {
            _isCollapsed = !_isCollapsed;
            _isCollapsed ? _animationController.forward() : _animationController.reverse();
          });
        },
        child: ClipPath(
          clipper: widget.position == Position.LEFT ? CustomLeftMenuClipper() : CustomRightMenuClipper(),
          child: Container(
            width: 35,
            height: 110,
            color: kOrangeColor,
            alignment: Alignment.centerLeft,
            child: AnimatedIcon(
              progress: _animationController.view,
              icon: AnimatedIcons.menu_close,
              color: kOrangeColorUltraLight,
              size: 25,
            ),
          ),
        ),
      ),
    );
  }
}

class CustomRightMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(width, 0);
    path.quadraticBezierTo(width, 8, width - 10, 16);
    path.quadraticBezierTo(1, height / 2 - 20, 0, height / 2);
    path.quadraticBezierTo(1, height / 2 + 20, width - 10, height - 16);
    path.quadraticBezierTo(width, height - 8, width, height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class CustomLeftMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;

    final width = size.width;
    final height = size.height;

    Path path = Path();
    path.moveTo(0, 0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width - 1, height / 2 - 20, width, height / 2);
    path.quadraticBezierTo(width - 1, height / 2 + 20, 10, height - 16);
    path.quadraticBezierTo(0, height - 8, 0, height);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
