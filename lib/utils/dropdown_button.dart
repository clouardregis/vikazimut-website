// coverage:ignore-file
import 'package:flutter/material.dart';

import 'screen_helper.dart';

enum SampleItem { itemOne, itemTwo, itemThree }

class DropDownButton extends StatelessWidget {
  final Function(int? value) onChanged;
  final String buttonText;
  final List<String> itemTexts;
  final Color backgroundColor;
  final Color foregroundColor;
  final bool dense;

  const DropDownButton({
    super.key,
    required this.onChanged,
    required this.buttonText,
    required this.itemTexts,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.dense,
  });

  @override
  Widget build(BuildContext context) {
    double verticalPadding;
    if (dense) {
      verticalPadding = 4;
    } else if (ScreenHelper.isDesktop(context)) {
      verticalPadding = 6;
    } else {
      verticalPadding = 4;
    }
    return PopupMenuButton<int>(
      constraints: const BoxConstraints(
        minWidth: 2.0 * 56.0,
        maxWidth: 5.0 * 60.0,
      ),
      onSelected: onChanged,
      color: backgroundColor,
      tooltip: "",
      itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
        for (int i = 0; i < itemTexts.length; i++)
          PopupMenuItem<int>(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            value: i,
            child: Text(
              itemTexts[i],
              style: TextStyle(color: foregroundColor),
            ),
          ),
      ],
      child: Container(
        padding: EdgeInsets.symmetric(vertical: verticalPadding, horizontal: 15),
        decoration: ShapeDecoration(
          color: backgroundColor,
          shape: const StadiumBorder(),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              buttonText,
              textAlign: TextAlign.center,
              style: TextStyle(color: foregroundColor, fontSize: 16, fontWeight: FontWeight.w500),
            ),
            const SizedBox(width: 5),
            Icon(
              Icons.arrow_drop_down,
              color: foregroundColor,
            ),
          ],
        ),
      ),
    );
  }
}
