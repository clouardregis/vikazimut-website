// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

class HelpWidget extends StatelessWidget {
  final String title;
  final String message;

  const HelpWidget(this.title, this.message);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title, style: const TextStyle(color: kOrangeColor)),
      content: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 800),
          child: Text(
            message,
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            L10n.getString("ok_message"),
            style: const TextStyle(color: kOrangeColor),
          ),
        ),
      ],
    );
  }
}
