// coverage:ignore-file
library;

import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';

class DurationPicker extends StatefulWidget {
  final ValueChanged<int> onChanged;
  final int initialDurationInMs;
  final String labelText;

  const DurationPicker({required this.labelText, required this.initialDurationInMs, required this.onChanged});

  @override
  State<DurationPicker> createState() => _DurationPickerState();
}

class _DurationPickerState extends State<DurationPicker> {
  late int _hours;
  late int _minutes;
  late int _seconds;

  @override
  void initState() {
    _hours = getHoursFromMilliseconds(widget.initialDurationInMs);
    _minutes = getMinutesFromMilliseconds(widget.initialDurationInMs);
    _seconds = getSecondsFromMilliseconds(widget.initialDurationInMs);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.labelText),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DropdownButton<int>(
              value: _hours,
              items: List<DropdownMenuItem<int>>.generate(24, (int i) => DropdownMenuItem<int>(value: i, child: Text(sprintf("%02d", [i])))),
              onChanged: (int? value) {
                setState(() {
                  _hours = value!;
                  int duration = _convertToMilliseconds(_hours, _minutes, _seconds);
                  widget.onChanged.call(duration);
                });
              },
            ),
            const Text(" : ", style: TextStyle(fontFamily: 'RobotoMono', ),),
            DropdownButton<int>(
              value: _minutes,
              items: List<DropdownMenuItem<int>>.generate(60, (int i) => DropdownMenuItem<int>(value: i, child: Text(sprintf("%02d", [i])))),
              onChanged: (int? value) {
                setState(() {
                  _minutes = value!;
                  int duration = _convertToMilliseconds(_hours, _minutes, _seconds);
                  widget.onChanged.call(duration);
                });
              },
            ),
            const Text(" : ", style: TextStyle(fontFamily: 'RobotoMono', ),),
            DropdownButton<int>(
              value: _seconds,
              items: List<DropdownMenuItem<int>>.generate(60, (int i) => DropdownMenuItem<int>(value: i, child: Text(sprintf("%02d", [i])))),
              onChanged: (int? value) {
                setState(() {
                  _seconds = value!;
                  int duration = _convertToMilliseconds(_hours, _minutes, _seconds);
                  widget.onChanged.call(duration);
                });
              },
            ),
          ],
        ),
      ],
    );
  }
}

int _convertToMilliseconds(int hours, int minutes, int seconds) {
  return (((hours * 60 + minutes) * 60) + seconds) * 1000;
}

@visibleForTesting
int getHoursFromMilliseconds(int time) {
  return (time / 1000) ~/ 3600;
}

@visibleForTesting
int getMinutesFromMilliseconds(int time) {
  int h = (time / 1000) ~/ 3600;
  return (((time ~/ 1000) - h * 3600)) ~/ 60;
}

@visibleForTesting
int getSecondsFromMilliseconds(int time) {
  return (time ~/ 1000) % 60;
}
