// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

class ContainerDecoration extends StatelessWidget {
  final Widget child;
  final BoxConstraints? constraints;

  const ContainerDecoration({
    required this.child,
    this.constraints,
  });

  @override
  Widget build(BuildContext context) {
    if (ScreenHelper.isMobile(context)) {
      return Container(
        padding: const EdgeInsets.all(5),
        child: child,
      );
    } else {
      return Container(
        padding: const EdgeInsets.all(8),
        constraints: constraints,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withValues(alpha: 0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        alignment: Alignment.topCenter,
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 5),
        child: child,
      );
    }
  }
}
