import 'package:flutter/widgets.dart';

class VikazimutIcons {
  VikazimutIcons._();

  static const _fontFamily = 'Vikazimut';

  static const IconData skiing_nordic = IconData(0xf7ca, fontFamily: _fontFamily, fontPackage: null);
}
