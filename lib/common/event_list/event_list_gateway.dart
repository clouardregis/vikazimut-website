import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'event_list_model.dart';
import 'event_list_page.dart';

class EventListGateway {
  static const String DELETE_URL = '${constants.API_SERVER}/planner/my-event/delete';
  static const String POST_URL = '${constants.API_SERVER}/planner/my-event/create';

  static Future<List<EventData>> fetchEvents(String url, int plannerId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return Future.error("Access token error");
    } else {
      try {
        final response = await http.get(
          Uri.parse(sprintf(url, [plannerId])),
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $accessToken",
          },
        );
        if (response.statusCode == HttpStatus.ok) {
          return (json.decode(response.body) as List).map((data) => EventData.fromJson(data)).toList();
        } else {
          return Future.error(response.body);
        }
      } catch (_) {
        return Future.error("Server connection error");
      }
    }
  }

  static Future<http.Response> deleteEvent(int id) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      final response = await http.delete(
        Uri.parse("$DELETE_URL/$id"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }

  static Future<http.Response> addCourse(EventFormData eventData) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      String jsonMessage = jsonEncode({"name": eventData.name, "type": eventData.type});
      final response = await http.post(
        Uri.parse(POST_URL),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
        body: jsonMessage,
      );
      return response;
    }
  }
}
