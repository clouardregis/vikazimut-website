part of 'event_list_page.dart';

class AddCourseFormDialog {
  final TextEditingController _nameController = TextEditingController();
  final EventFormData _eventData = EventFormData();

  Future<EventFormData?> execute(BuildContext context) async {
    return await showDialog<EventFormData?>(
      context: context,
      builder: (BuildContext context) => _buildAddEventForm(context),
    );
  }

  Widget _buildAddEventForm(BuildContext context) {
    return AlertDialog(
      title: Text(
        L10n.getString("planner_event_add_page_title"),
        style: const TextStyle(color: kOrangeColor),
      ),
      content: SizedBox(
        width: 550,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: TextField(
                autofocus: true,
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: L10n.getString("planner_event_add_page_event_name_label"),
                ),
                keyboardType: TextInputType.text,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(50),
                  FilteringTextInputFormatter.allow(userIdentifierRegExp),
                ],
                onChanged: (String value) => _eventData.name = value,
              ),
            ),
            const SizedBox(height: 18),
            Text(L10n.getString("planner_event_add_page_event_type_label")),
            StatefulBuilder(builder: (BuildContext context, StateSetter dropDownState) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  CustomDropdownButton<int>(
                    value: _eventData.type,
                    onChanged: (int? newValue) {
                      dropDownState(() {
                        _eventData.type = newValue!;
                      });
                    },
                    items: _dropdownTypeItems,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  _displayTypeExplanation(_eventData.type),
                ],
              );
            }),
          ],
        ),
      ),
      actions: <Widget>[
        AnimatedBuilder(
          animation: _nameController,
          builder: (_, __) => ElevatedButton(
            onPressed: (_eventData.name != null && _eventData.name!.trim().isNotEmpty)
                ? () {
                    _eventData.name = _eventData.name!.trim();
                    Navigator.of(context).pop(_eventData);
                  }
                : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: kOrangeColor,
              padding: const EdgeInsets.all(15),
            ),
            child: Text(L10n.getString("planner_event_add_page_add_button")),
          ),
        ),
        CustomOutlinedButton(
          foregroundColor: kOrangeColor,
          backgroundColor: kOrangeColor,
          onPressed: () => Navigator.of(context).pop(null),
          label: L10n.getString("button_cancel_label"),
        ),
      ],
    );
  }

  List<DropdownMenuItem<int>> get _dropdownTypeItems {
    List<DropdownMenuItem<int>> menuItems = [
      DropdownMenuItem(value: 0, child: Text(L10n.getString("planner_event_add_page_event_type0_label"))),
      DropdownMenuItem(value: 1, child: Text(L10n.getString("planner_event_add_page_event_type1_label"))),
      DropdownMenuItem(value: 2, child: Text(L10n.getString("planner_event_add_page_event_type2_label"))),
    ];
    return menuItems;
  }

  Text _displayTypeExplanation(int selectedEventType) {
    switch (selectedEventType) {
      case 0:
        return Text(L10n.getString("planner_event_add_page_event_type0_text"));
      case 1:
        return Text(L10n.getString("planner_event_add_page_event_type1_text"));
      default:
        return Text(L10n.getString("planner_event_add_page_event_type2_text"));
    }
  }
}

class EventFormData {
  String? name;
  int type = 0;
}
