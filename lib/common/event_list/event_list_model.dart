import 'package:flutter/foundation.dart';

@immutable
class EventData {
  final int id;
  final String name;

  const EventData({
    required this.id,
    required this.name,
  });

  factory EventData.fromJson(Map<String, dynamic> json) {
    return EventData(
      id: json['id'] as int,
      name: json['name'] as String,
    );
  }
}
