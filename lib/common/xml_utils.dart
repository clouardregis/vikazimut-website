String convertXmlTagsToLowerCase(String xmlText) {
  final convertedXmlText = xmlText.replaceAllMapped(RegExp(r'</*[\w:]+'), (match) {
    return match.group(0)!.toLowerCase();
  });
  return convertedXmlText;
}
