import 'package:flutter/material.dart';
import 'package:vikazimut_website/utils/vikazimut_icons.dart';

enum Discipline {
  URBANO,
  FORESTO,
  MTBO,
  SKIO;

  static Discipline? toEnum(int? index) {
    if (index == null) {
      return null;
    } else {
      return Discipline.values[index];
    }
  }

  static bool isFootOrienteering(Discipline? discipline) {
    return (discipline == null || discipline == Discipline.URBANO || discipline == Discipline.FORESTO);
  }

  static bool isMTBOrienteering(Discipline? discipline) {
    return (discipline != null && discipline == Discipline.MTBO);
  }

  static bool isSKIOrienteering(Discipline? discipline) {
    return (discipline != null && discipline == Discipline.SKIO);
  }

  static IconData getIcon(Discipline? discipline) {
    if (discipline == null) {
      return Icons.directions_run;
    } else if (discipline == SKIO) {
      return VikazimutIcons.skiing_nordic;
    } else if (discipline == MTBO) {
      return Icons.directions_bike;
    } else {
      return Icons.directions_run;
    }
  }
}
