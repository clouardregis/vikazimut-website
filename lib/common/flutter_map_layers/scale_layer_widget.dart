// coverage:ignore-file
import 'dart:math' as math;
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/utils/utils.dart';

part 'scale_bar_utils.dart';

/// Source https://github.com/kimlet/flutter_map/tree/master/example/lib
class ScaleLayerPluginOption {
  TextStyle? textStyle;
  Color lineColor;
  double lineWidth;

  ScaleLayerPluginOption({
    this.textStyle,
    this.lineColor = Colors.white,
    this.lineWidth = 2,
  });
}

class ScaleLayerWidget extends StatelessWidget {
  final ScaleLayerPluginOption options;
  final scale = [
    25000000,
    15000000,
    8000000,
    4000000,
    2000000,
    1000000,
    500000,
    250000,
    100000,
    50000,
    25000,
    15000,
    8000,
    4000,
    2000,
    1000,
    500,
    250,
    100,
    50,
    25,
    10,
    5,
  ];

  ScaleLayerWidget({super.key, required this.options});

  @override
  Widget build(BuildContext context) {
    final MapCamera map = MapCamera.maybeOf(context)!;
    final Point<double> startInPixels = map.project(map.center);
    final double terrainDistance = scale[math.max(0, math.min(20, map.zoom.round() + 2))].toDouble();
    final LatLng targetPoint = calculateEndingGeodesicCoordinates(map.center, 90, terrainDistance);
    final Point<double> endInPixels = map.project(targetPoint);
    final String displayedDistance = terrainDistance > 999 ? '${(terrainDistance / 1000).toStringAsFixed(0)} km' : '${terrainDistance.toStringAsFixed(0)} m';
    final double widthInPixels = endInPixels.x - startInPixels.x;
    return Align(
      alignment: Alignment.bottomLeft,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints bc) {
          return Container(
            padding: const EdgeInsets.only(left: 10),
            width: 100,
            height: 30,
            child: CustomPaint(
              painter: _ScalePainter(
                widthInPixels,
                displayedDistance,
                lineColor: options.lineColor,
                lineWidth: options.lineWidth,
                textStyle: options.textStyle,
              ),
            ),
          );
        },
      ),
    );
  }
}

class _ScalePainter extends CustomPainter {
  final double width;
  final String text;
  final TextStyle? textStyle;
  final double? lineWidth;
  final Color? lineColor;

  const _ScalePainter(this.width, this.text, {this.textStyle, this.lineWidth, this.lineColor});

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    final paint = Paint()
      ..color = lineColor!
      ..strokeCap = StrokeCap.square
      ..strokeWidth = lineWidth!;

    const sizeForStartEnd = 4;
    var paddingTop = 0.0;

    final textSpan = TextSpan(style: textStyle, text: text);
    final textPainter = TextPainter(text: textSpan, textDirection: TextDirection.ltr)..layout();
    textPainter.paint(canvas, Offset(width / 2 - textPainter.width / 2 + 0.0, paddingTop));
    paddingTop += textPainter.height;
    final p1 = Offset(0.0, sizeForStartEnd + paddingTop);
    final p2 = Offset(0.0 + width, sizeForStartEnd + paddingTop);

    canvas.drawLine(Offset(0.0, paddingTop), Offset(0.0, sizeForStartEnd + paddingTop), paint);
    final middleX = width / 2 + 0.0 - lineWidth! / 2;
    canvas.drawLine(Offset(middleX, paddingTop + sizeForStartEnd / 2), Offset(middleX, sizeForStartEnd + paddingTop), paint);
    canvas.drawLine(Offset(width + 0.0, paddingTop), Offset(width + 0.0, sizeForStartEnd + paddingTop), paint);
    canvas.drawLine(p1, p2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
