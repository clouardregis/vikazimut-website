// coverage:ignore-file
import 'dart:math' as math;
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
// ignore: implementation_imports
import 'package:flutter_map/src/misc/offsets.dart';
import 'package:latlong2/latlong.dart';

// Source https://pub.dev/packages/flutter_map_tappable_polyline

class CustomTappablePolylineLayer extends StatelessWidget {
  final List<TaggedColoredPolyline> polylines;
  final void Function(List<TaggedColoredPolyline>, List<int> segments, TapUpDetails tapPosition)? onTap;
  final double pointerDistanceTolerance;

  const CustomTappablePolylineLayer({
    required this.polylines,
    required this.onTap,
    this.pointerDistanceTolerance = 10,
  });

  @override
  Widget build(BuildContext context) {
    final mapCamera = MapCamera.maybeOf(context)!;
    final mapController = MapController.maybeOf(context)!;
    final mapOptions = MapOptions.maybeOf(context)!;
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints box) {
        final size = Size(box.maxWidth, box.maxHeight);
        for (final polyline in polylines) {
          polyline.punchPoints.clear();
          for (var i = 0; i < polyline.punchPositions.length; ++i) {
            final position = polyline.punchPositions[i];
            var pos = mapCamera.project(position.position);
            pos = pos * mapCamera.getZoomScale(mapCamera.zoom, mapCamera.zoom) - mapCamera.pixelOrigin.toDoublePoint();
            polyline.punchPoints.add(Offset(pos.x.toDouble(), pos.y.toDouble()));
          }
        }
        return GestureDetector(
          onDoubleTapDown: (TapDownDetails details) {
            _zoomMap(details, context, mapCamera, mapController);
          },
          onTapUp: (TapUpDetails details) {
            _forwardCallToMapOptions(details, context, mapCamera, mapOptions, mapController);
            _handlePolylineTap(details, onTap, mapCamera);
          },
          child: CustomPaint(
            painter: _MyPolylinePainter(polylines, mapCamera),
            size: size,
          ),
        );
      },
    );
  }

  void _handlePolylineTap(TapUpDetails details, Function? onTap, MapCamera map) {
    // We might hit close to multiple polylines. We will therefore keep a reference to these in this map.
    Map<double, List<TaggedColoredPolyline>> polylineCandidates = {};
    Map<double, List<int>> segmentCandidates = {};

    // Calculating taps in between points on the polyline. We
    // iterate over all the segments in the polyline to find any
    // matches with the tapped point within the
    // pointerDistanceTolerance.
    for (Polyline currentPolyline in polylines) {
      for (var j = 0; j < currentPolyline.points.length - 1; j++) {
        // We consider the points point1, point2 and tap points in a triangle
        final position1 = currentPolyline.points[j];
        final point1 = _getOffset(position1, map);
        final position2 = currentPolyline.points[j + 1];
        final point2 = _getOffset(position2, map);
        final tap = details.localPosition;

        // To determine if we have tapped in between two po ints, we
        // calculate the length from the tapped point to the line
        // created by point1, point2. If this distance is shorter
        // than the specified threshold, we have detected a tap
        // between two points.
        //
        // We start by calculating the length of all the sides using pythagoras.
        final a = _distance(point1, point2);
        final b = _distance(point1, tap);
        final c = _distance(point2, tap);

        // To find the height when we only know the lengths of the sides, we can use Herons formula to get the Area.
        final semiPerimeter = (a + b + c) / 2.0;
        final triangleArea = math.sqrt(semiPerimeter * (semiPerimeter - a) * (semiPerimeter - b) * (semiPerimeter - c));

        // We can then finally calculate the length from the tapped point onto the line created by point1, point2.
        // Area of triangles is half the area of a rectangle
        // area = 1/2 base * height -> height = (2 * area) / base
        final height = (2 * triangleArea) / a;

        // We're not there yet - We need to satisfy the edge case
        // where the perpendicular line from the tapped point onto
        // the line created by point1, point2 (called point D) is
        // outside of the segment point1, point2. We need
        // to check if the length from D to the original segment
        // (point1, point2) is less than the threshold.
        final hypotenuse = math.max(b, c);
        final newTriangleBase = math.sqrt((hypotenuse * hypotenuse) - (height * height));
        final lengthDToOriginalSegment = newTriangleBase - a;
        if (height < pointerDistanceTolerance && lengthDToOriginalSegment < pointerDistanceTolerance) {
          var minimum = math.min(height, lengthDToOriginalSegment);
          polylineCandidates[minimum] ??= <TaggedColoredPolyline>[];
          polylineCandidates[minimum]!.add(currentPolyline as TaggedColoredPolyline);
          segmentCandidates[minimum] ??= <int>[];
          segmentCandidates[minimum]!.add(j);
        }
      }
    }

    if (polylineCandidates.isNotEmpty) {
      // We look up in the map of distances to the tap, and choose the shortest one.
      final closestToTapKey = polylineCandidates.keys.reduce(math.min);
      onTap!(polylineCandidates[closestToTapKey], segmentCandidates[closestToTapKey], details);
    }
  }

  void _forwardCallToMapOptions(TapUpDetails details, BuildContext context, MapCamera map, MapOptions mapOptions, MapController controller) {
    final position = _offsetToLatLng(details.localPosition, context.size!.width, context.size!.height, map, controller);
    final tapPosition = TapPosition(details.globalPosition, details.localPosition);
    // Forward the onTap call to map.options so that we won't break onTap
    if (mapOptions.onTap != null) {
      mapOptions.onTap!(tapPosition, position);
    }
  }

  double _distance(Offset point1, Offset point2) {
    final distanceX = (point1.dx - point2.dx).abs();
    final distanceY = (point1.dy - point2.dy).abs();
    return math.sqrt((distanceX * distanceX) + (distanceY * distanceY));
  }

  void _zoomMap(TapDownDetails details, BuildContext context, MapCamera map, MapController controller) {
    final newCenter = _offsetToLatLng(details.localPosition, context.size!.width, context.size!.height, map, controller);
    controller.move(newCenter, map.zoom + 0.5);
  }

  LatLng _offsetToLatLng(Offset offset, double width, double height, MapCamera map, MapController mapController) {
    final localPoint = Point(offset.dx, offset.dy);
    final localPointCenterDistance = Point((width / 2) - localPoint.x, (height / 2) - localPoint.y);
    final mapCenter = map.project(map.center);
    final point = mapCenter - localPointCenterDistance;
    return map.unproject(point);
  }

  _getOffset(LatLng point, MapCamera map) {
    return map.getOffsetFromOrigin(point);
  }
}

class _MyPolylinePainter extends CustomPainter {
  final List<TaggedColoredPolyline> _polylines;
  final MapCamera _mapCamera;

  _MyPolylinePainter(this._polylines, this._mapCamera);

  @override
  void paint(Canvas canvas, Size size) {
    if (_polylines.isEmpty) {
      return;
    }
    final drawingRectangle = Offset.zero & size;
    canvas.clipRect(drawingRectangle);

    Paint? borderPaint;
    for (final polyline in _polylines) {
      if (polyline.borderStrokeWidth > 0.0) {
        borderPaint = Paint()
          ..style = PaintingStyle.stroke
          ..color = polyline.borderColor
          ..strokeCap = StrokeCap.round
          ..strokeJoin = StrokeJoin.round
          ..blendMode = BlendMode.srcOver;
      }
      final paint = Paint()
        ..strokeCap = StrokeCap.round
        ..strokeJoin = StrokeJoin.round
        ..blendMode = BlendMode.srcOver
        ..color = polyline.color;

      paint.style = PaintingStyle.stroke;
      canvas.saveLayer(drawingRectangle, Paint());

      double strokeWidth = polyline.strokeWidth;
      final origin = _mapCamera.project(_mapCamera.center).toOffset() - _mapCamera.size.toOffset() / 2;

      var offsets = getOffsets(_mapCamera, origin, polyline.points);
      if (polyline.borderColorIndices != null) {
        paint.strokeWidth = polyline.borderStrokeWidth * 5 + strokeWidth;
        _paintColoredLine(canvas, offsets, polyline.borderColorIndices!, paint);
      }
      if (borderPaint != null) {
        borderPaint.strokeWidth = polyline.borderStrokeWidth + strokeWidth;
        _paintFlatLine(canvas, offsets, borderPaint);
      }
      paint.strokeWidth = strokeWidth;
      if (polyline.selected) {
        final pointColorIndices = polyline.pointColorIndices;
        paint.strokeWidth = strokeWidth;
        _paintColoredLine(canvas, offsets, pointColorIndices, paint);
      } else {
        _paintFlatLine(canvas, offsets, paint);
      }
      _drawPunchPositions(polyline, canvas);
      canvas.restore();
    }
  }

  void _drawPunchPositions(TaggedColoredPolyline taggedColoredPolyline, ui.Canvas canvas) {
    final validationCirclePaint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;
    final validationCircleOutlinePaint = Paint()
      ..style = PaintingStyle.stroke
      ..color = Colors.black
      ..strokeWidth = 2
      ..blendMode = BlendMode.srcOver;
    for (int i = 0; i < taggedColoredPolyline.punchPoints.length; i++) {
      const double radius = 4;
      final offset = taggedColoredPolyline.punchPoints[i];
      canvas.drawCircle(offset, radius, validationCirclePaint);
      canvas.drawCircle(offset, radius, validationCircleOutlinePaint);
      final forced = taggedColoredPolyline.punchPositions[i].forced;
      if (forced) {
        canvas.drawLine(Offset(offset.dx - radius * 0.707, offset.dy - radius * 0.707), Offset(offset.dx + radius * 0.707, offset.dy + radius * 0.707), validationCircleOutlinePaint);
        canvas.drawLine(Offset(offset.dx - radius * 0.707, offset.dy + radius * 0.707), Offset(offset.dx + radius * 0.707, offset.dy - radius * 0.707), validationCircleOutlinePaint);
      }
    }
  }

  void _paintFlatLine(Canvas canvas, List<Offset> offsets, Paint paint) {
    if (offsets.isNotEmpty) {
      final path = ui.Path()..moveTo(offsets[0].dx, offsets[0].dy);
      for (var offset in offsets) {
        path.lineTo(offset.dx, offset.dy);
      }
      canvas.drawPath(path, paint);
    }
  }

  void _paintColoredLine(Canvas canvas, List<Offset> offsets, List<int> colors, Paint paint) {
    final List<_Line> lines = [];
    for (int i = 1; i < colors.length; i++) {
      lines.add(_Line(offsets[i - 1], offsets[i], colors[i]));
    }
    lines.sort((a, b) => b.colorIndex.compareTo(a.colorIndex));
    for (final line in lines) {
      paint.color = Color(line.colorIndex);
      canvas.drawLine(line.start, line.end, paint);
    }
  }

  @override
  bool shouldRepaint(covariant _MyPolylinePainter oldDelegate) => _polylines != oldDelegate._polylines || _mapCamera != oldDelegate._mapCamera;
}

class _Line {
  final Offset start;
  final Offset end;
  final int colorIndex;

  const _Line(this.start, this.end, this.colorIndex);
}

class TaggedColoredPolyline extends Polyline {
  final String? tag;
  final List<int> pointColorIndices;
  final List<int>? borderColorIndices;
  final List<PunchPosition> punchPositions;
  final List<Offset> punchPoints = [];
  bool selected = false;

  TaggedColoredPolyline({
    this.tag,
    required points,
    required this.punchPositions,
    super.strokeWidth = 1.0,
    super.color,
    super.borderStrokeWidth = 2.0,
    super.borderColor = Colors.black,
    required this.pointColorIndices,
    this.borderColorIndices,
  }) : super(points: points) {
    assert(pointColorIndices.isEmpty || points.length == pointColorIndices.length);
  }

  bool setToggleSelected() {
    selected = !selected;
    return selected;
  }
}

class PunchPosition {
  LatLng position;
  bool forced;

  PunchPosition(this.position, this.forced);
}
