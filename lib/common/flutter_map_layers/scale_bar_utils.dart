/// Source https://github.com/kimlet/flutter_map/tree/master/example/lib

part of 'scale_layer_widget.dart';

LatLng calculateEndingGeodesicCoordinates(LatLng start, double startBearing, double distance) {
  const int RADIUS_EARTH_METERS = 6378137;

  final mSemiMajorAxis = RADIUS_EARTH_METERS; //WGS84 major axis
  final mSemiMinorAxis = (1.0 - 1.0 / 298.257223563) * RADIUS_EARTH_METERS;
  final mFlattening = 1.0 / 298.257223563;

  final a = mSemiMajorAxis;
  final b = mSemiMinorAxis;
  final aSquared = a * a;
  final bSquared = b * b;
  final f = mFlattening;
  final phi1 = deg2rad(start.latitude);
  final alpha1 = deg2rad(startBearing);
  final cosAlpha1 = math.cos(alpha1);
  final sinAlpha1 = math.sin(alpha1);
  final s = distance;
  final tanU1 = (1.0 - f) * math.tan(phi1);
  final cosU1 = 1.0 / math.sqrt(1.0 + tanU1 * tanU1);
  final sinU1 = tanU1 * cosU1;

  // eq. 1
  final sigma1 = math.atan2(tanU1, cosAlpha1);

  // eq. 2
  final sinAlpha = cosU1 * sinAlpha1;

  final sin2Alpha = sinAlpha * sinAlpha;
  final cos2Alpha = 1 - sin2Alpha;
  final uSquared = cos2Alpha * (aSquared - bSquared) / bSquared;

  // eq. 3
  final A = 1 + (uSquared / 16384) * (4096 + uSquared * (-768 + uSquared * (320 - 175 * uSquared)));

  // eq. 4
  final B = (uSquared / 1024) * (256 + uSquared * (-128 + uSquared * (74 - 47 * uSquared)));

  // iterate until there is a negligible change in sigma
  final sOverbA = s / (b * A);
  var sigma = sOverbA;
  var prevSigma = sOverbA;
  for (;;) {
    // eq. 5
    final sigmaM2 = 2.0 * sigma1 + sigma;
    final cosSigmaM2 = math.cos(sigmaM2);
    final cos2SigmaM2 = cosSigmaM2 * cosSigmaM2;
    final sinSigma = math.sin(sigma);
    final cosSigma = math.cos(sigma);

    // eq. 6
    final deltaSigma = B * sinSigma * (cosSigmaM2 + (B / 4.0) * (cosSigma * (-1 + 2 * cos2SigmaM2) - (B / 6.0) * cosSigmaM2 * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM2)));

    // eq. 7
    sigma = sOverbA + deltaSigma;

    // break after converging to tolerance
    if ((sigma - prevSigma).abs() < 0.0000000000001) {
      break;
    }

    prevSigma = sigma;
  }

  final sigmaM2 = 2.0 * sigma1 + sigma;
  final cosSigmaM2 = math.cos(sigmaM2);
  final cos2SigmaM2 = cosSigmaM2 * cosSigmaM2;

  final cosSigma = math.cos(sigma);
  final sinSigma = math.sin(sigma);

  // eq. 8
  final phi2 = math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1.0 - f) * math.sqrt(sin2Alpha + math.pow(sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1, 2.0)));

  // eq. 9
  // This fixes the pole crossing defect spotted by Matt Feemster. When a
  // path passes a pole and essentially crosses a line of latitude twice -
  // once in each direction - the longitude calculation got messed up.
  // Using
  // atan2 instead of atan fixes the defect. The change is in the next 3
  // lines.
  // double tanLambda = sinSigma * sinAlpha1 / (cosU1 * cosSigma - sinU1 *
  // sinSigma * cosAlpha1);
  // double lambda = Math.atan(tanLambda);
  final lambda = math.atan2(sinSigma * sinAlpha1, (cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1));

  // eq. 10
  final C = (f / 16) * cos2Alpha * (4 + f * (4 - 3 * cos2Alpha));

  // eq. 11
  final L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cosSigmaM2 + C * cosSigma * (-1 + 2 * cos2SigmaM2)));

  // build result
  var latitude = rad2deg(phi2);
  var longitude = start.longitude + rad2deg(L);

  latitude = latitude < -90 ? -90 : latitude;
  latitude = latitude > 90 ? 90 : latitude;
  longitude = longitude < -180 ? -180 : longitude;
  longitude = longitude > 180 ? 180 : longitude;
  return LatLng(latitude, longitude);
}
