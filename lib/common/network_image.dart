import 'dart:io';

import 'package:flutter/material.dart';
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/constants.dart' as constants;

NetworkImage loadNetworkImage(IdentifiedUser? user, String imageId, [bool reload = false]) {
  var accessToken = user?.accessToken;
  Map<String, String>? headers;
  if (accessToken != null) {
    headers = {
      HttpHeaders.authorizationHeader: "Bearer $accessToken",
    };
  }
  String url = "${constants.API_SERVER}/courses/image/$imageId";
  if (reload) {
    url += "?${DateTime.now().millisecondsSinceEpoch}";
  }
  return NetworkImage(url, headers: headers);
}
