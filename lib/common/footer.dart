import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sprintf/sprintf.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/theme/theme.dart';

const isRunningWithWasm = bool.fromEnvironment('dart.tool.dart2wasm');

class Footer extends StatefulWidget {
  @override
  State<Footer> createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  String _versionNumber = "";

  @override
  void initState() {
    getVersionNumber();
    super.initState();
  }

  Future<void> getVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    if (mounted) {
      setState(() {
        _versionNumber = packageInfo.version;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildUi(context, MediaQuery.of(context).size.width * .8),
    );
  }

  Widget _buildUi(BuildContext context, double width) {
    return Center(
      child: Column(
        children: [
          const Divider(
            thickness: 2,
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5.0),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    style: const TextStyle(color: kOrangeColor, fontSize: 11),
                    text: '\u00a9 ${sprintf(L10n.getString("footer_part1"), [_versionNumber, isRunningWithWasm? 'WebAssembly': 'JavaScript'])}',
                  ),
                  TextSpan(
                    style: const TextStyle(color: kOrangeColorDisabled, fontSize: 11),
                    text: L10n.getString("footer_part2"),
                    recognizer: TapGestureRecognizer()..onTap = () => launchUrl(Uri.parse(L10n.getString("footer_part2_url"))),
                  ),
                  TextSpan(
                    style: const TextStyle(color: kOrangeColor, fontSize: 11),
                    text: L10n.getString("footer_part3"),
                  ),
                  TextSpan(
                    style: const TextStyle(color: kOrangeColorDisabled, fontSize: 11),
                    text: L10n.getString("footer_part4"),
                    recognizer: TapGestureRecognizer()..onTap = () => launchUrl(Uri.parse(L10n.getString("footer_part4_url"))),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
