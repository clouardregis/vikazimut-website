import 'dart:io';
import 'dart:js_interop';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:web/web.dart' as web;

void downloadFromUrl(
  String url,
  String filename, {
  String Function(String content)? prettyPrinter,
}) async {
  var user = await IdentifiedUser.getUser();
  var accessToken = user?.accessToken;
  Map<String, String>? headers;
  if (accessToken != null) {
    headers = {
      HttpHeaders.authorizationHeader: "Bearer $accessToken",
    };
  }

  final response = await http.get(Uri.parse(url), headers: headers);
  if (response.statusCode == 200) {
    Uint8List bytes;
    if (prettyPrinter != null) {
      String xml = String.fromCharCodes(response.bodyBytes);
      xml = prettyPrinter(xml);
      bytes = Uint8List.fromList(xml.codeUnits);
    } else {
      bytes = response.bodyBytes;
    }

    final JSArrayBuffer data = Uint8List.fromList(bytes).buffer.toJS;
    final web.Blob blob;
    if (response.headers[HttpHeaders.contentTypeHeader] == null) {
      blob = web.Blob([data].toJS);
    } else {
      blob = web.Blob([data].toJS, web.BlobPropertyBag(type: response.headers[HttpHeaders.contentTypeHeader]!));
    }
    final String url = web.URL.createObjectURL(blob);
    final web.HTMLAnchorElement anchor = web.HTMLAnchorElement();
    anchor.href = url;
    anchor.download = filename;
    anchor.click();
    anchor.remove();
  }
}
