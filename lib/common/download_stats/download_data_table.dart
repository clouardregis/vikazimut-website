import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_preview_page.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_view.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/alert_dialog.dart';
import 'package:vikazimut_website/utils/custom_button.dart';
import 'package:vikazimut_website/utils/diacritic.dart';
import 'package:vikazimut_website/utils/time.dart' as time_utils;

import 'download_stats_model.dart';
import 'download_stats_presenter.dart';

class DownloadDataTable extends StatefulWidget {
  DownloadDataTable(
    this.presenter,
    this.items,
  );

  final GlobalKey<PaginatedDataTableState> tableKey = GlobalKey<PaginatedDataTableState>();
  final DownloadStatsPagePresenter presenter;
  final List<DownloadStatsModel> items;

  @override
  State<DownloadDataTable> createState() => DownloadDataTableState();
}

class DownloadDataTableState extends State<DownloadDataTable> {
  _TableRowData? _courseDataSource;
  final TextEditingController _searchFieldController = TextEditingController();
  late List<DownloadStatsModel> _initialCourseList;
  static String _currentSearchUsername = "";
  static int? _sortColumnIndex;

  static bool _sortAscending = false;

  late final List<Map<String, dynamic>> tableColumnHeaders;

  @override
  void initState() {
    super.initState();
    _initialCourseList = widget.items.toList(growable: false);
    _searchFieldController.text = _currentSearchUsername;
    if (_sortColumnIndex != null) {
      _sortCourseList(_sortColumnIndex!, _sortAscending);
    }
    _courseDataSource = _TableRowData(_initialCourseList, _currentSearchUsername, context: context, presenter: widget.presenter);
    tableColumnHeaders = [
      {"label": L10n.getString("admin_download_stats_page_table_column1"), "sortable": false, "numeric": false},
      {"label": L10n.getString("admin_download_stats_page_table_column3"), "sortable": true, "numeric": true},
      {"label": L10n.getString("admin_download_stats_page_table_column2"), "sortable": true, "numeric": true},
      {"label": L10n.getString("admin_download_stats_page_table_column5"), "sortable": true, "numeric": true},
      {"label": L10n.getString("admin_download_stats_page_table_column4"), "sortable": false, "numeric": true},
      if (widget.presenter.isAdmin) {"label": "", "sortable": false, "numeric": false},
    ];
  }

  @override
  void didUpdateWidget(covariant DownloadDataTable oldWidget) {
    if (widget.items != oldWidget.items) {
      _initialCourseList = widget.items.toList(growable: false);
      if (_sortColumnIndex != null) {
        _sortCourseList(_sortColumnIndex!, _sortAscending);
      }
      _courseDataSource = _TableRowData(_initialCourseList, _currentSearchUsername, context: context, presenter: widget.presenter);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final int rowPerPage = (height - 360) ~/ 45;
    return PaginatedDataTable(
      key: widget.tableKey,
      source: _courseDataSource!,
      initialFirstRowIndex: DownloadStatsPagePresenter.currentDisplayedRowIndex,
      dataRowMaxHeight: 45,
      dataRowMinHeight: 45,
      columnSpacing: 10.0,
      horizontalMargin: 0,
      rowsPerPage: rowPerPage,
      showFirstLastButtons: true,
      showEmptyRows: false,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
      showCheckboxColumn: false,
      arrowHeadColor: kOrangeColor,
      onPageChanged: (int value) => DownloadStatsPagePresenter.currentDisplayedRowIndex = value,
      header: Padding(
        padding: const EdgeInsets.all(3),
        child: TextField(
          controller: _searchFieldController,
          style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: const BorderSide(
                width: 2,
                color: kOrangeColor,
              ),
            ),
            labelText: L10n.getString("admin_download_stats_page_hint_text"),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
          ),
          onChanged: (searchText) {
            setState(() {
              _currentSearchUsername = searchText;
              widget.tableKey.currentState!.pageTo(0);
              _courseDataSource = _TableRowData(_initialCourseList, searchText, context: context, presenter: widget.presenter);
            });
          },
        ),
      ),
      columns: tableColumnHeaders
          .map(
            (Map<String, dynamic> tableColumnHeader) => DataColumn(
              label: Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    tableColumnHeader["label"],
                    style: const TextStyle(fontSize: 16, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              numeric: tableColumnHeader["numeric"],
              onSort: (tableColumnHeader["sortable"])
                  ? (columnIndex, ascending) {
                      setState(() {
                        widget.tableKey.currentState!.pageTo(0);
                        if (columnIndex >= 1 && columnIndex <= 3) {
                          if (_sortColumnIndex != columnIndex) {
                            _sortColumnIndex = columnIndex;
                          } else {
                            _sortAscending = ascending;
                          }
                          _sortCourseList(columnIndex, _sortAscending);
                        }
                      });
                    }
                  : null,
            ),
          )
          .toList(),
    );
  }

  void _sortCourseList(int columnIndex, bool ascending) {
    _initialCourseList.sort((DownloadStatsModel a, DownloadStatsModel b) {
      if (columnIndex == 1) {
        if (ascending) {
          return a.createdAtInMilliseconds.compareTo(b.createdAtInMilliseconds);
        } else {
          return b.createdAtInMilliseconds.compareTo(a.createdAtInMilliseconds);
        }
      } else if (columnIndex == 2) {
        if (ascending) {
          return a.downloadCount.compareTo(b.downloadCount);
        } else {
          return b.downloadCount.compareTo(a.downloadCount);
        }
      } else {
        if (ascending) {
          return a.downloadPerMonth.compareTo(b.downloadPerMonth);
        } else {
          return b.downloadPerMonth.compareTo(a.downloadPerMonth);
        }
      }
    });

    setState(() {
      _courseDataSource = _TableRowData(_initialCourseList, _currentSearchUsername, context: context, presenter: widget.presenter);
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }
}

class _TableRowData extends DataTableSource {
  _TableRowData(
    List<DownloadStatsModel> plannerList,
    String searchText, {
    required this.context,
    required this.presenter,
  }) {
    if (searchText.isNotEmpty) {
      var rawSearchText = searchText.toLowerCase().toRawString();
      _courses = plannerList.where((element) => element.name.toLowerCase().toRawString().contains(rawSearchText)).toList();
    } else {
      _courses = plannerList;
    }
  }

  late final List<DownloadStatsModel> _courses;
  final BuildContext context;
  final DownloadStatsPagePresenter presenter;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _courses.length;

  @override
  int get selectedRowCount => 0;

  @override
  DataRow getRow(int index) {
    var course = _courses[index];
    return DataRow(
      color: index.isEven ? WidgetStateProperty.all(Theme.of(context).colorScheme.inversePrimary) : WidgetStateProperty.all(Theme.of(context).colorScheme.surface),
      cells: [
        DataCell(
          TextButton(
            onPressed: () {
              GoRouter.of(context).go("${PlannerCoursePreviewPage.routePath}/${course.id}");
            },
            child: Row(
              children: [
                Icon(
                  Discipline.getIcon(course.discipline),
                  color: kOrangeColor,
                ),
                const SizedBox(width: 15),
                Text(
                  course.name,
                  style: TextStyle(color: Theme.of(context).colorScheme.tertiary),
                ),
                if (course.locked)
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(Icons.lock_outlined, color: kDangerColor),
                  ),
                if (course.closed)
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(Icons.alarm, color: kWarningColor),
                  ),
                if (course.touristic)
                  const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Icon(Icons.quiz_outlined, color: kSuccessColor),
                  ),
              ],
            ),
          ),
        ),
        DataCell(
          Center(child: Text(time_utils.getLocalizedDate(course.createdAtInMilliseconds))),
        ),
        DataCell(
          Center(child: Text('${course.downloadCount}')),
        ),
        DataCell(
          Center(child: Text(L10n.formatNumber(course.downloadPerMonth, 2))),
        ),
        DataCell(
          Center(
            child: SizedBox(
              width: 80,
              child: ElevatedButton(
                onPressed: () {
                  GoRouter.of(context).go("${RouteListPageView.routePath}/${course.id}");
                },
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all(kWarningColor),
                  padding: WidgetStateProperty.all<EdgeInsets?>(EdgeInsets.zero),
                ),
                child: Text(
                  '${course.traceCount}',
                  style: const TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
        ),
        if (presenter.isAdmin)
          DataCell(
            Center(
              child: CustomPlainButton(
                label: L10n.getString("planner_course_list_page_button_delete"),
                backgroundColor: kDangerColor,
                onPressed: () => showBinaryQuestionAlertDialog(
                  context: context,
                  title: L10n.getString("planner_course_list_page_delete_title"),
                  message: L10n.getString("planner_course_list_page_delete_message", [
                    course.name,
                  ]),
                  okMessage: L10n.getString("planner_course_list_page_delete_ok_message"),
                  noMessage: L10n.getString("planner_course_list_page_delete_no_message"),
                  action: () {
                    presenter.deleteCourse(course.id, course.name);
                  },
                ),
              ),
            ),
          ),
      ],
    );
  }
}
