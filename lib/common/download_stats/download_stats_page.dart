// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vikazimut_website/common/footer.dart';
import 'package:vikazimut_website/l10n/l10n.dart';
import 'package:vikazimut_website/menu/responsive_scaffold_widget.dart';
import 'package:vikazimut_website/theme/theme.dart';
import 'package:vikazimut_website/utils/container_decoration.dart';
import 'package:vikazimut_website/utils/global_error_widget.dart';
import 'package:vikazimut_website/utils/screen_helper.dart';

import 'download_data_table.dart';
import 'download_stats_model.dart';
import 'download_stats_presenter.dart';

abstract class DownloadStatsPage extends StatefulWidget {
  final String _routePath;
  final String _courseListUrl;
  final bool _isAdmin;

  const DownloadStatsPage(this._routePath, this._courseListUrl, this._isAdmin);

  @override
  State<DownloadStatsPage> createState() => DownloadStatsPageState();
}

class DownloadStatsPageState extends State<DownloadStatsPage> {
  late final DownloadStatsPagePresenter _presenter;
  late Future<List<DownloadStatsModel>> _courses;
  final ScrollController _verticalScrollController = ScrollController();

  @override
  void initState() {
    _presenter = DownloadStatsPagePresenter(this, widget._isAdmin);
    _courses = _presenter.fetchCourses(widget._routePath, widget._courseListUrl);
    super.initState();
  }

  @override
  void dispose() {
    _verticalScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffoldWidget(
      body: Column(
        children: [
          Flexible(
            child: ContainerDecoration(
              constraints: const BoxConstraints(maxWidth: ScreenHelper.desktopMinWidth),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      L10n.getString("admin_download_stats_page_title"),
                      style: Theme.of(context).textTheme.displayMedium,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Expanded(
                    child: FutureBuilder<List<DownloadStatsModel>>(
                      future: _courses,
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return GlobalErrorWidget(snapshot.error.toString());
                        } else if (snapshot.hasData) {
                          final courses = snapshot.data!.reversed.toList();
                          return Scrollbar(
                            controller: _verticalScrollController,
                            thumbVisibility: true,
                            interactive: true,
                            child: SingleChildScrollView(
                              controller: _verticalScrollController,
                              scrollDirection: Axis.vertical,
                              child: SizedBox(
                                width: ScreenHelper.isMobile(context) ? ScreenHelper.mobileMaxWidth : ScreenHelper.tabletMaxWidth - 20,
                                child: Theme(
                                  data: Theme.of(context).copyWith(
                                    textTheme: const TextTheme(bodySmall: TextStyle(color: kOrangeColor)),
                                    cardTheme: CardTheme(color: Theme.of(context).scaffoldBackgroundColor),
                                    iconTheme: const IconThemeData(color: Colors.white),
                                  ),
                                  child: DownloadDataTable(_presenter, courses),
                                ),
                              ),
                            ),
                          );
                        } else {
                          return const Center(child: CircularProgressIndicator());
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (!ScreenHelper.isMobile(context)) const SizedBox(height: 8),
          Footer(),
        ],
      ),
    );
  }

  void redirectTo(String routePath) => GoRouter.of(context).replace(routePath);

  void displayToast(String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
  }

  void update() {
    setState(() {
      _courses = _presenter.fetchCourses(widget._routePath, widget._courseListUrl);
    });
  }
}
