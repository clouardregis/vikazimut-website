import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/l10n/l10n.dart';

import 'download_stats_gateway.dart';
import 'download_stats_model.dart';
import 'download_stats_page.dart';

class DownloadStatsPagePresenter {
  static int currentDisplayedRowIndex = 0;
  final DownloadStatsPageState _view;
  final bool isAdmin;

  DownloadStatsPagePresenter(this._view, this.isAdmin);

  Future<List<DownloadStatsModel>> fetchCourses(
    String routePath,
    String url,
  ) async {
    try {
      return DownloadStatsGateway.fetchCourses(url);
    } catch (_) {
      _view.redirectTo(routePath);
      return Future.error("Access token error");
    }
  }

  Future<void> deleteCourse(int courseId, String courseName) async {
    http.Response response = await DownloadStatsGateway.deleteCourse(courseId);
    if (response.statusCode == HttpStatus.ok) {
      _view.displayToast(L10n.getString("delete_course_success", [courseName]));
      _view.update();
    } else {
      String message = L10n.getString("delete_course_error1", [courseName]);
      _view.displayToast("$message ${jsonDecode(response.body)}");
    }
  }
}
