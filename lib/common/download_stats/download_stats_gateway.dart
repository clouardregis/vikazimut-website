import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:vikazimut_website/authentication/identifier_user.dart';
import 'package:vikazimut_website/authentication/unauthorized_exception.dart';
import 'package:vikazimut_website/constants.dart' as constants;

import 'download_stats_model.dart';

class DownloadStatsGateway {
  static Future<List<DownloadStatsModel>> fetchCourses(String url) async {
    IdentifiedUser? user = await IdentifiedUser.getUser();
    String? accessToken = user?.accessToken;
    if (accessToken == null) {
      throw UnauthorizedException();
    } else {
      final response = await http.get(
        Uri.parse(url),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        return (json.decode(response.body) as List).map((data) => DownloadStatsModel.fromJson(data)).toList();
      } else {
        return Future.error(response.body);
      }
    }
  }

  static Future<http.Response> deleteCourse(int courseId) async {
    var user = await IdentifiedUser.getUser();
    var accessToken = user?.accessToken;
    if (accessToken == null) {
      return http.Response("Access token error", HttpStatus.internalServerError);
    } else {
      const String URL = '${constants.API_SERVER}/planner/my-course/delete-course';
      final response = await http.delete(
        Uri.parse("$URL/$courseId"),
        headers: {
          HttpHeaders.authorizationHeader: "Bearer $accessToken",
        },
      );
      return response;
    }
  }
}
