import 'dart:typed_data';

import '../../base_type.dart';
import '../../data_message.dart';
import '../../definition_message.dart';
import '../../errors.dart';
import '../../field.dart';
import '../../sub_field.dart';
import '../profile_type.dart';
import 'common_fields.dart';

// ignore_for_file: constant_identifier_names

class LapMessage extends DataMessage {
  LapMessage({
    super.definitionMessage,
    super.developerFields,
    int localId = 0,
    Endian endian = Endian.little,
  })  : growable = definitionMessage == null,
        super(
          name: LapMessage.NAME,
          globalId: LapMessage.ID,
          localId: definitionMessage?.localId ?? localId,
          endian: definitionMessage?.endian ?? endian,
          fields: [
            MessageIndexField(size: definitionMessage?.getFieldDefinition(MessageIndexField.ID)?.size ?? 0, growable: definitionMessage == null),
            TimestampField(size: definitionMessage?.getFieldDefinition(TimestampField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEventField(size: definitionMessage?.getFieldDefinition(LapEventField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEventTypeField(size: definitionMessage?.getFieldDefinition(LapEventTypeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapStartTimeField(size: definitionMessage?.getFieldDefinition(LapStartTimeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapStartPositionLatField(size: definitionMessage?.getFieldDefinition(LapStartPositionLatField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapStartPositionLongField(size: definitionMessage?.getFieldDefinition(LapStartPositionLongField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEndPositionLatField(size: definitionMessage?.getFieldDefinition(LapEndPositionLatField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEndPositionLongField(size: definitionMessage?.getFieldDefinition(LapEndPositionLongField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalElapsedTimeField(size: definitionMessage?.getFieldDefinition(LapTotalElapsedTimeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalTimerTimeField(size: definitionMessage?.getFieldDefinition(LapTotalTimerTimeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalDistanceField(size: definitionMessage?.getFieldDefinition(LapTotalDistanceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalCyclesField(size: definitionMessage?.getFieldDefinition(LapTotalCyclesField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalCaloriesField(size: definitionMessage?.getFieldDefinition(LapTotalCaloriesField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalFatCaloriesField(size: definitionMessage?.getFieldDefinition(LapTotalFatCaloriesField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgSpeedField(size: definitionMessage?.getFieldDefinition(LapAvgSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxSpeedField(size: definitionMessage?.getFieldDefinition(LapMaxSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgHeartRateField(size: definitionMessage?.getFieldDefinition(LapAvgHeartRateField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxHeartRateField(size: definitionMessage?.getFieldDefinition(LapMaxHeartRateField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgCadenceField(size: definitionMessage?.getFieldDefinition(LapAvgCadenceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxCadenceField(size: definitionMessage?.getFieldDefinition(LapMaxCadenceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgPowerField(size: definitionMessage?.getFieldDefinition(LapAvgPowerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxPowerField(size: definitionMessage?.getFieldDefinition(LapMaxPowerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalAscentField(size: definitionMessage?.getFieldDefinition(LapTotalAscentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalDescentField(size: definitionMessage?.getFieldDefinition(LapTotalDescentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapIntensityField(size: definitionMessage?.getFieldDefinition(LapIntensityField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapLapTriggerField(size: definitionMessage?.getFieldDefinition(LapLapTriggerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapSportField(size: definitionMessage?.getFieldDefinition(LapSportField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEventGroupField(size: definitionMessage?.getFieldDefinition(LapEventGroupField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapNumLengthsField(size: definitionMessage?.getFieldDefinition(LapNumLengthsField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapNormalizedPowerField(size: definitionMessage?.getFieldDefinition(LapNormalizedPowerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapLeftRightBalanceField(size: definitionMessage?.getFieldDefinition(LapLeftRightBalanceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapFirstLengthIndexField(size: definitionMessage?.getFieldDefinition(LapFirstLengthIndexField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgStrokeDistanceField(size: definitionMessage?.getFieldDefinition(LapAvgStrokeDistanceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapSwimStrokeField(size: definitionMessage?.getFieldDefinition(LapSwimStrokeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapSubSportField(size: definitionMessage?.getFieldDefinition(LapSubSportField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapNumActiveLengthsField(size: definitionMessage?.getFieldDefinition(LapNumActiveLengthsField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalWorkField(size: definitionMessage?.getFieldDefinition(LapTotalWorkField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgAltitudeField(size: definitionMessage?.getFieldDefinition(LapAvgAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxAltitudeField(size: definitionMessage?.getFieldDefinition(LapMaxAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapGpsAccuracyField(size: definitionMessage?.getFieldDefinition(LapGpsAccuracyField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgGradeField(size: definitionMessage?.getFieldDefinition(LapAvgGradeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgPosGradeField(size: definitionMessage?.getFieldDefinition(LapAvgPosGradeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgNegGradeField(size: definitionMessage?.getFieldDefinition(LapAvgNegGradeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxPosGradeField(size: definitionMessage?.getFieldDefinition(LapMaxPosGradeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxNegGradeField(size: definitionMessage?.getFieldDefinition(LapMaxNegGradeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgTemperatureField(size: definitionMessage?.getFieldDefinition(LapAvgTemperatureField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxTemperatureField(size: definitionMessage?.getFieldDefinition(LapMaxTemperatureField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalMovingTimeField(size: definitionMessage?.getFieldDefinition(LapTotalMovingTimeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgPosVerticalSpeedField(size: definitionMessage?.getFieldDefinition(LapAvgPosVerticalSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgNegVerticalSpeedField(size: definitionMessage?.getFieldDefinition(LapAvgNegVerticalSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxPosVerticalSpeedField(size: definitionMessage?.getFieldDefinition(LapMaxPosVerticalSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxNegVerticalSpeedField(size: definitionMessage?.getFieldDefinition(LapMaxNegVerticalSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTimeInHrZoneField(size: definitionMessage?.getFieldDefinition(LapTimeInHrZoneField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTimeInSpeedZoneField(size: definitionMessage?.getFieldDefinition(LapTimeInSpeedZoneField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTimeInCadenceZoneField(size: definitionMessage?.getFieldDefinition(LapTimeInCadenceZoneField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTimeInPowerZoneField(size: definitionMessage?.getFieldDefinition(LapTimeInPowerZoneField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapRepetitionNumField(size: definitionMessage?.getFieldDefinition(LapRepetitionNumField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMinAltitudeField(size: definitionMessage?.getFieldDefinition(LapMinAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMinHeartRateField(size: definitionMessage?.getFieldDefinition(LapMinHeartRateField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapWorkoutStepIndexField(size: definitionMessage?.getFieldDefinition(LapWorkoutStepIndexField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapOpponentScoreField(size: definitionMessage?.getFieldDefinition(LapOpponentScoreField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapStrokeCountField(size: definitionMessage?.getFieldDefinition(LapStrokeCountField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapZoneCountField(size: definitionMessage?.getFieldDefinition(LapZoneCountField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgVerticalOscillationField(size: definitionMessage?.getFieldDefinition(LapAvgVerticalOscillationField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgStanceTimePercentField(size: definitionMessage?.getFieldDefinition(LapAvgStanceTimePercentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgStanceTimeField(size: definitionMessage?.getFieldDefinition(LapAvgStanceTimeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgFractionalCadenceField(size: definitionMessage?.getFieldDefinition(LapAvgFractionalCadenceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxFractionalCadenceField(size: definitionMessage?.getFieldDefinition(LapMaxFractionalCadenceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalFractionalCyclesField(size: definitionMessage?.getFieldDefinition(LapTotalFractionalCyclesField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapPlayerScoreField(size: definitionMessage?.getFieldDefinition(LapPlayerScoreField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgTotalHemoglobinConcField(size: definitionMessage?.getFieldDefinition(LapAvgTotalHemoglobinConcField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMinTotalHemoglobinConcField(size: definitionMessage?.getFieldDefinition(LapMinTotalHemoglobinConcField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxTotalHemoglobinConcField(size: definitionMessage?.getFieldDefinition(LapMaxTotalHemoglobinConcField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgSaturatedHemoglobinPercentField(size: definitionMessage?.getFieldDefinition(LapAvgSaturatedHemoglobinPercentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMinSaturatedHemoglobinPercentField(size: definitionMessage?.getFieldDefinition(LapMinSaturatedHemoglobinPercentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxSaturatedHemoglobinPercentField(size: definitionMessage?.getFieldDefinition(LapMaxSaturatedHemoglobinPercentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLeftTorqueEffectivenessField(size: definitionMessage?.getFieldDefinition(LapAvgLeftTorqueEffectivenessField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgRightTorqueEffectivenessField(size: definitionMessage?.getFieldDefinition(LapAvgRightTorqueEffectivenessField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLeftPedalSmoothnessField(size: definitionMessage?.getFieldDefinition(LapAvgLeftPedalSmoothnessField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgRightPedalSmoothnessField(size: definitionMessage?.getFieldDefinition(LapAvgRightPedalSmoothnessField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgCombinedPedalSmoothnessField(size: definitionMessage?.getFieldDefinition(LapAvgCombinedPedalSmoothnessField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTimeStandingField(size: definitionMessage?.getFieldDefinition(LapTimeStandingField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapStandCountField(size: definitionMessage?.getFieldDefinition(LapStandCountField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLeftPcoField(size: definitionMessage?.getFieldDefinition(LapAvgLeftPcoField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgRightPcoField(size: definitionMessage?.getFieldDefinition(LapAvgRightPcoField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLeftPowerPhaseField(size: definitionMessage?.getFieldDefinition(LapAvgLeftPowerPhaseField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLeftPowerPhasePeakField(size: definitionMessage?.getFieldDefinition(LapAvgLeftPowerPhasePeakField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgRightPowerPhaseField(size: definitionMessage?.getFieldDefinition(LapAvgRightPowerPhaseField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgRightPowerPhasePeakField(size: definitionMessage?.getFieldDefinition(LapAvgRightPowerPhasePeakField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgPowerPositionField(size: definitionMessage?.getFieldDefinition(LapAvgPowerPositionField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxPowerPositionField(size: definitionMessage?.getFieldDefinition(LapMaxPowerPositionField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgCadencePositionField(size: definitionMessage?.getFieldDefinition(LapAvgCadencePositionField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxCadencePositionField(size: definitionMessage?.getFieldDefinition(LapMaxCadencePositionField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEnhancedAvgSpeedField(size: definitionMessage?.getFieldDefinition(LapEnhancedAvgSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEnhancedMaxSpeedField(size: definitionMessage?.getFieldDefinition(LapEnhancedMaxSpeedField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEnhancedAvgAltitudeField(size: definitionMessage?.getFieldDefinition(LapEnhancedAvgAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEnhancedMinAltitudeField(size: definitionMessage?.getFieldDefinition(LapEnhancedMinAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapEnhancedMaxAltitudeField(size: definitionMessage?.getFieldDefinition(LapEnhancedMaxAltitudeField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgLevMotorPowerField(size: definitionMessage?.getFieldDefinition(LapAvgLevMotorPowerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxLevMotorPowerField(size: definitionMessage?.getFieldDefinition(LapMaxLevMotorPowerField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapLevBatteryConsumptionField(size: definitionMessage?.getFieldDefinition(LapLevBatteryConsumptionField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgVerticalRatioField(size: definitionMessage?.getFieldDefinition(LapAvgVerticalRatioField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgStanceTimeBalanceField(size: definitionMessage?.getFieldDefinition(LapAvgStanceTimeBalanceField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgStepLengthField(size: definitionMessage?.getFieldDefinition(LapAvgStepLengthField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgVamField(size: definitionMessage?.getFieldDefinition(LapAvgVamField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalGritField(size: definitionMessage?.getFieldDefinition(LapTotalGritField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalFlowField(size: definitionMessage?.getFieldDefinition(LapTotalFlowField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapJumpCountField(size: definitionMessage?.getFieldDefinition(LapJumpCountField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgGritField(size: definitionMessage?.getFieldDefinition(LapAvgGritField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgFlowField(size: definitionMessage?.getFieldDefinition(LapAvgFlowField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalFractionalAscentField(size: definitionMessage?.getFieldDefinition(LapTotalFractionalAscentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapTotalFractionalDescentField(size: definitionMessage?.getFieldDefinition(LapTotalFractionalDescentField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapAvgCoreTemperatureField(size: definitionMessage?.getFieldDefinition(LapAvgCoreTemperatureField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMinCoreTemperatureField(size: definitionMessage?.getFieldDefinition(LapMinCoreTemperatureField.ID)?.size ?? 0, growable: definitionMessage == null),
            LapMaxCoreTemperatureField(size: definitionMessage?.getFieldDefinition(LapMaxCoreTemperatureField.ID)?.size ?? 0, growable: definitionMessage == null),
          ],
        );

  /// The Global ID of the message. In the FIT documentation this is referred to as the "Global Message Number".
  static const ID = 19;
  static const NAME = 'lap';

  final bool growable;

  /// Returns an instance of LapMessage from a bytes list.
  static LapMessage fromBytes(DefinitionMessage definitionMessage, Uint8List bytes) {
    final message = LapMessage(definitionMessage: definitionMessage);
    message.readFromBytes(bytes);
    return message;
  }

  /// Returns the value of the messageIndex field. Returns null if the field is not defined in the message.
  int? get messageIndex {
    final field = getField(MessageIndexField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the messageIndex field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set messageIndex(int? value) {
    final field = getField(MessageIndexField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timestamp field in  milliseconds since January 1st, 1970 at 00:00:00 UTC
  int? get timestamp {
    final field = getField(TimestampField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the timestamp field. [value] is milliseconds since January 1st, 1970 at 00:00:00 UTC. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timestamp(int? value) {
    final field = getField(TimestampField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the event field. Returns null if the field is not defined in the message.
  Event? get event {
    final field = getField(LapEventField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return EventExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the event field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set event(Event? value) {
    final field = getField(LapEventField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the eventType field. Returns null if the field is not defined in the message.
  EventType? get eventType {
    final field = getField(LapEventTypeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return EventTypeExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the eventType field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set eventType(EventType? value) {
    final field = getField(LapEventTypeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timestamp field in  milliseconds since January 1st, 1970 at 00:00:00 UTC
  int? get startTime {
    final field = getField(LapStartTimeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the timestamp field. [value] is milliseconds since January 1st, 1970 at 00:00:00 UTC. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set startTime(int? value) {
    final field = getField(LapStartTimeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the startPositionLat field. Returns null if the field is not defined in the message.
  double? get startPositionLat {
    final field = getField(LapStartPositionLatField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the startPositionLat field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set startPositionLat(double? value) {
    final field = getField(LapStartPositionLatField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the startPositionLong field. Returns null if the field is not defined in the message.
  double? get startPositionLong {
    final field = getField(LapStartPositionLongField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the startPositionLong field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set startPositionLong(double? value) {
    final field = getField(LapStartPositionLongField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the endPositionLat field. Returns null if the field is not defined in the message.
  double? get endPositionLat {
    final field = getField(LapEndPositionLatField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the endPositionLat field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set endPositionLat(double? value) {
    final field = getField(LapEndPositionLatField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the endPositionLong field. Returns null if the field is not defined in the message.
  double? get endPositionLong {
    final field = getField(LapEndPositionLongField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the endPositionLong field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set endPositionLong(double? value) {
    final field = getField(LapEndPositionLongField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalElapsedTime field. Returns null if the field is not defined in the message.
  double? get totalElapsedTime {
    final field = getField(LapTotalElapsedTimeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalElapsedTime field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalElapsedTime(double? value) {
    final field = getField(LapTotalElapsedTimeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalTimerTime field. Returns null if the field is not defined in the message.
  double? get totalTimerTime {
    final field = getField(LapTotalTimerTimeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalTimerTime field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalTimerTime(double? value) {
    final field = getField(LapTotalTimerTimeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalDistance field. Returns null if the field is not defined in the message.
  double? get totalDistance {
    final field = getField(LapTotalDistanceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalDistance field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalDistance(double? value) {
    final field = getField(LapTotalDistanceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalCycles field. Returns null if the field is not defined in the message.
  int? get totalCycles {
    final field = getField(LapTotalCyclesField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalCycles field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalCycles(int? value) {
    final field = getField(LapTotalCyclesField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Get the value of the subfield totalStrides
  int? get totalStrides {
    final field = getField(LapTotalCyclesField.ID);
    final typeField = getField(LapSportField.ID);

    final isSubFieldValid = typeField != null && [1, 11].contains(typeField.getValue());
    if (field != null && field.isValid() && isSubFieldValid) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalCycles subfield with [value]. Throws [FieldNotDefinedError] if the subfield is not defined in the message.
  set totalStrides(int? value) {
    final field = getField(LapTotalCyclesField.ID);
    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Get the value of the subfield totalStrokes
  int? get totalStrokes {
    final field = getField(LapTotalCyclesField.ID);
    final typeField = getField(LapSportField.ID);

    final isSubFieldValid = typeField != null && [2, 5, 15, 37].contains(typeField.getValue());
    if (field != null && field.isValid() && isSubFieldValid) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalCycles subfield with [value]. Throws [FieldNotDefinedError] if the subfield is not defined in the message.
  set totalStrokes(int? value) {
    final field = getField(LapTotalCyclesField.ID);
    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalCalories field. Returns null if the field is not defined in the message.
  int? get totalCalories {
    final field = getField(LapTotalCaloriesField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalCalories field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalCalories(int? value) {
    final field = getField(LapTotalCaloriesField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalFatCalories field. Returns null if the field is not defined in the message.
  int? get totalFatCalories {
    final field = getField(LapTotalFatCaloriesField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalFatCalories field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalFatCalories(int? value) {
    final field = getField(LapTotalFatCaloriesField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgSpeed field. Returns null if the field is not defined in the message.
  double? get avgSpeed {
    final field = getField(LapAvgSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgSpeed(double? value) {
    final field = getField(LapAvgSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxSpeed field. Returns null if the field is not defined in the message.
  double? get maxSpeed {
    final field = getField(LapMaxSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxSpeed(double? value) {
    final field = getField(LapMaxSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgHeartRate field. Returns null if the field is not defined in the message.
  int? get avgHeartRate {
    final field = getField(LapAvgHeartRateField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgHeartRate field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgHeartRate(int? value) {
    final field = getField(LapAvgHeartRateField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxHeartRate field. Returns null if the field is not defined in the message.
  int? get maxHeartRate {
    final field = getField(LapMaxHeartRateField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxHeartRate field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxHeartRate(int? value) {
    final field = getField(LapMaxHeartRateField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgCadence field. Returns null if the field is not defined in the message.
  int? get avgCadence {
    final field = getField(LapAvgCadenceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgCadence field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgCadence(int? value) {
    final field = getField(LapAvgCadenceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Get the value of the subfield avgRunningCadence
  int? get avgRunningCadence {
    final field = getField(LapAvgCadenceField.ID);
    final typeField = getField(LapSportField.ID);

    final isSubFieldValid = typeField != null && [1].contains(typeField.getValue());
    if (field != null && field.isValid() && isSubFieldValid) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgCadence subfield with [value]. Throws [FieldNotDefinedError] if the subfield is not defined in the message.
  set avgRunningCadence(int? value) {
    final field = getField(LapAvgCadenceField.ID);
    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxCadence field. Returns null if the field is not defined in the message.
  int? get maxCadence {
    final field = getField(LapMaxCadenceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxCadence field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxCadence(int? value) {
    final field = getField(LapMaxCadenceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Get the value of the subfield maxRunningCadence
  int? get maxRunningCadence {
    final field = getField(LapMaxCadenceField.ID);
    final typeField = getField(LapSportField.ID);

    final isSubFieldValid = typeField != null && [1].contains(typeField.getValue());
    if (field != null && field.isValid() && isSubFieldValid) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxCadence subfield with [value]. Throws [FieldNotDefinedError] if the subfield is not defined in the message.
  set maxRunningCadence(int? value) {
    final field = getField(LapMaxCadenceField.ID);
    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgPower field. Returns null if the field is not defined in the message.
  int? get avgPower {
    final field = getField(LapAvgPowerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgPower field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgPower(int? value) {
    final field = getField(LapAvgPowerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxPower field. Returns null if the field is not defined in the message.
  int? get maxPower {
    final field = getField(LapMaxPowerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxPower field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxPower(int? value) {
    final field = getField(LapMaxPowerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalAscent field. Returns null if the field is not defined in the message.
  int? get totalAscent {
    final field = getField(LapTotalAscentField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalAscent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalAscent(int? value) {
    final field = getField(LapTotalAscentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalDescent field. Returns null if the field is not defined in the message.
  int? get totalDescent {
    final field = getField(LapTotalDescentField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalDescent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalDescent(int? value) {
    final field = getField(LapTotalDescentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the intensity field. Returns null if the field is not defined in the message.
  Intensity? get intensity {
    final field = getField(LapIntensityField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return IntensityExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the intensity field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set intensity(Intensity? value) {
    final field = getField(LapIntensityField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the lapTrigger field. Returns null if the field is not defined in the message.
  LapTrigger? get lapTrigger {
    final field = getField(LapLapTriggerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return LapTriggerExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the lapTrigger field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set lapTrigger(LapTrigger? value) {
    final field = getField(LapLapTriggerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the sport field. Returns null if the field is not defined in the message.
  Sport? get sport {
    final field = getField(LapSportField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return SportExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the sport field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set sport(Sport? value) {
    final field = getField(LapSportField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the eventGroup field. Returns null if the field is not defined in the message.
  int? get eventGroup {
    final field = getField(LapEventGroupField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the eventGroup field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set eventGroup(int? value) {
    final field = getField(LapEventGroupField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the numLengths field. Returns null if the field is not defined in the message.
  int? get numLengths {
    final field = getField(LapNumLengthsField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the numLengths field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set numLengths(int? value) {
    final field = getField(LapNumLengthsField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the normalizedPower field. Returns null if the field is not defined in the message.
  int? get normalizedPower {
    final field = getField(LapNormalizedPowerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the normalizedPower field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set normalizedPower(int? value) {
    final field = getField(LapNormalizedPowerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the leftRightBalance field. Returns null if the field is not defined in the message.
  int? get leftRightBalance {
    final field = getField(LapLeftRightBalanceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the leftRightBalance field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set leftRightBalance(int? value) {
    final field = getField(LapLeftRightBalanceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the firstLengthIndex field. Returns null if the field is not defined in the message.
  int? get firstLengthIndex {
    final field = getField(LapFirstLengthIndexField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the firstLengthIndex field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set firstLengthIndex(int? value) {
    final field = getField(LapFirstLengthIndexField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgStrokeDistance field. Returns null if the field is not defined in the message.
  double? get avgStrokeDistance {
    final field = getField(LapAvgStrokeDistanceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgStrokeDistance field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgStrokeDistance(double? value) {
    final field = getField(LapAvgStrokeDistanceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the swimStroke field. Returns null if the field is not defined in the message.
  SwimStroke? get swimStroke {
    final field = getField(LapSwimStrokeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return SwimStrokeExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the swimStroke field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set swimStroke(SwimStroke? value) {
    final field = getField(LapSwimStrokeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the subSport field. Returns null if the field is not defined in the message.
  SubSport? get subSport {
    final field = getField(LapSubSportField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      final value = field.getValue(subField: subField);
      if (value == null) {
        return null;
      }
      return SubSportExt.fromValue(value);
    } else {
      return null;
    }
  }

  /// Sets the subSport field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set subSport(SubSport? value) {
    final field = getField(LapSubSportField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value.value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the numActiveLengths field. Returns null if the field is not defined in the message.
  int? get numActiveLengths {
    final field = getField(LapNumActiveLengthsField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the numActiveLengths field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set numActiveLengths(int? value) {
    final field = getField(LapNumActiveLengthsField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalWork field. Returns null if the field is not defined in the message.
  int? get totalWork {
    final field = getField(LapTotalWorkField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalWork field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalWork(int? value) {
    final field = getField(LapTotalWorkField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgAltitude field. Returns null if the field is not defined in the message.
  double? get avgAltitude {
    final field = getField(LapAvgAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgAltitude(double? value) {
    final field = getField(LapAvgAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxAltitude field. Returns null if the field is not defined in the message.
  double? get maxAltitude {
    final field = getField(LapMaxAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxAltitude(double? value) {
    final field = getField(LapMaxAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the gpsAccuracy field. Returns null if the field is not defined in the message.
  int? get gpsAccuracy {
    final field = getField(LapGpsAccuracyField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the gpsAccuracy field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set gpsAccuracy(int? value) {
    final field = getField(LapGpsAccuracyField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgGrade field. Returns null if the field is not defined in the message.
  double? get avgGrade {
    final field = getField(LapAvgGradeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgGrade field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgGrade(double? value) {
    final field = getField(LapAvgGradeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgPosGrade field. Returns null if the field is not defined in the message.
  double? get avgPosGrade {
    final field = getField(LapAvgPosGradeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgPosGrade field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgPosGrade(double? value) {
    final field = getField(LapAvgPosGradeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgNegGrade field. Returns null if the field is not defined in the message.
  double? get avgNegGrade {
    final field = getField(LapAvgNegGradeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgNegGrade field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgNegGrade(double? value) {
    final field = getField(LapAvgNegGradeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxPosGrade field. Returns null if the field is not defined in the message.
  double? get maxPosGrade {
    final field = getField(LapMaxPosGradeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxPosGrade field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxPosGrade(double? value) {
    final field = getField(LapMaxPosGradeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxNegGrade field. Returns null if the field is not defined in the message.
  double? get maxNegGrade {
    final field = getField(LapMaxNegGradeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxNegGrade field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxNegGrade(double? value) {
    final field = getField(LapMaxNegGradeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgTemperature field. Returns null if the field is not defined in the message.
  int? get avgTemperature {
    final field = getField(LapAvgTemperatureField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgTemperature field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgTemperature(int? value) {
    final field = getField(LapAvgTemperatureField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxTemperature field. Returns null if the field is not defined in the message.
  int? get maxTemperature {
    final field = getField(LapMaxTemperatureField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxTemperature field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxTemperature(int? value) {
    final field = getField(LapMaxTemperatureField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalMovingTime field. Returns null if the field is not defined in the message.
  double? get totalMovingTime {
    final field = getField(LapTotalMovingTimeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalMovingTime field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalMovingTime(double? value) {
    final field = getField(LapTotalMovingTimeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgPosVerticalSpeed field. Returns null if the field is not defined in the message.
  double? get avgPosVerticalSpeed {
    final field = getField(LapAvgPosVerticalSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgPosVerticalSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgPosVerticalSpeed(double? value) {
    final field = getField(LapAvgPosVerticalSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgNegVerticalSpeed field. Returns null if the field is not defined in the message.
  double? get avgNegVerticalSpeed {
    final field = getField(LapAvgNegVerticalSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgNegVerticalSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgNegVerticalSpeed(double? value) {
    final field = getField(LapAvgNegVerticalSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxPosVerticalSpeed field. Returns null if the field is not defined in the message.
  double? get maxPosVerticalSpeed {
    final field = getField(LapMaxPosVerticalSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxPosVerticalSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxPosVerticalSpeed(double? value) {
    final field = getField(LapMaxPosVerticalSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxNegVerticalSpeed field. Returns null if the field is not defined in the message.
  double? get maxNegVerticalSpeed {
    final field = getField(LapMaxNegVerticalSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxNegVerticalSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxNegVerticalSpeed(double? value) {
    final field = getField(LapMaxNegVerticalSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timeInHrZone field. Returns null if the field is not defined in the message.
  List<double>? get timeInHrZone {
    final field = getField(LapTimeInHrZoneField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the timeInHrZone field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timeInHrZone(List<double>? value) {
    final field = getField(LapTimeInHrZoneField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timeInSpeedZone field. Returns null if the field is not defined in the message.
  List<double>? get timeInSpeedZone {
    final field = getField(LapTimeInSpeedZoneField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the timeInSpeedZone field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timeInSpeedZone(List<double>? value) {
    final field = getField(LapTimeInSpeedZoneField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timeInCadenceZone field. Returns null if the field is not defined in the message.
  List<double>? get timeInCadenceZone {
    final field = getField(LapTimeInCadenceZoneField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the timeInCadenceZone field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timeInCadenceZone(List<double>? value) {
    final field = getField(LapTimeInCadenceZoneField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timeInPowerZone field. Returns null if the field is not defined in the message.
  List<double>? get timeInPowerZone {
    final field = getField(LapTimeInPowerZoneField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the timeInPowerZone field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timeInPowerZone(List<double>? value) {
    final field = getField(LapTimeInPowerZoneField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the repetitionNum field. Returns null if the field is not defined in the message.
  int? get repetitionNum {
    final field = getField(LapRepetitionNumField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the repetitionNum field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set repetitionNum(int? value) {
    final field = getField(LapRepetitionNumField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the minAltitude field. Returns null if the field is not defined in the message.
  double? get minAltitude {
    final field = getField(LapMinAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the minAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set minAltitude(double? value) {
    final field = getField(LapMinAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the minHeartRate field. Returns null if the field is not defined in the message.
  int? get minHeartRate {
    final field = getField(LapMinHeartRateField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the minHeartRate field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set minHeartRate(int? value) {
    final field = getField(LapMinHeartRateField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the workoutStepIndex field. Returns null if the field is not defined in the message.
  int? get workoutStepIndex {
    final field = getField(LapWorkoutStepIndexField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the workoutStepIndex field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set workoutStepIndex(int? value) {
    final field = getField(LapWorkoutStepIndexField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the opponentScore field. Returns null if the field is not defined in the message.
  int? get opponentScore {
    final field = getField(LapOpponentScoreField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the opponentScore field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set opponentScore(int? value) {
    final field = getField(LapOpponentScoreField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the strokeCount field. Returns null if the field is not defined in the message.
  List<int>? get strokeCount {
    final field = getField(LapStrokeCountField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the strokeCount field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set strokeCount(List<int>? value) {
    final field = getField(LapStrokeCountField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the zoneCount field. Returns null if the field is not defined in the message.
  List<int>? get zoneCount {
    final field = getField(LapZoneCountField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the zoneCount field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set zoneCount(List<int>? value) {
    final field = getField(LapZoneCountField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgVerticalOscillation field. Returns null if the field is not defined in the message.
  double? get avgVerticalOscillation {
    final field = getField(LapAvgVerticalOscillationField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgVerticalOscillation field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgVerticalOscillation(double? value) {
    final field = getField(LapAvgVerticalOscillationField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgStanceTimePercent field. Returns null if the field is not defined in the message.
  double? get avgStanceTimePercent {
    final field = getField(LapAvgStanceTimePercentField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgStanceTimePercent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgStanceTimePercent(double? value) {
    final field = getField(LapAvgStanceTimePercentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgStanceTime field. Returns null if the field is not defined in the message.
  double? get avgStanceTime {
    final field = getField(LapAvgStanceTimeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgStanceTime field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgStanceTime(double? value) {
    final field = getField(LapAvgStanceTimeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgFractionalCadence field. Returns null if the field is not defined in the message.
  double? get avgFractionalCadence {
    final field = getField(LapAvgFractionalCadenceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgFractionalCadence field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgFractionalCadence(double? value) {
    final field = getField(LapAvgFractionalCadenceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxFractionalCadence field. Returns null if the field is not defined in the message.
  double? get maxFractionalCadence {
    final field = getField(LapMaxFractionalCadenceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxFractionalCadence field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxFractionalCadence(double? value) {
    final field = getField(LapMaxFractionalCadenceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalFractionalCycles field. Returns null if the field is not defined in the message.
  double? get totalFractionalCycles {
    final field = getField(LapTotalFractionalCyclesField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalFractionalCycles field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalFractionalCycles(double? value) {
    final field = getField(LapTotalFractionalCyclesField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the playerScore field. Returns null if the field is not defined in the message.
  int? get playerScore {
    final field = getField(LapPlayerScoreField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the playerScore field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set playerScore(int? value) {
    final field = getField(LapPlayerScoreField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgTotalHemoglobinConc field. Returns null if the field is not defined in the message.
  List<double>? get avgTotalHemoglobinConc {
    final field = getField(LapAvgTotalHemoglobinConcField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgTotalHemoglobinConc field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgTotalHemoglobinConc(List<double>? value) {
    final field = getField(LapAvgTotalHemoglobinConcField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the minTotalHemoglobinConc field. Returns null if the field is not defined in the message.
  List<double>? get minTotalHemoglobinConc {
    final field = getField(LapMinTotalHemoglobinConcField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the minTotalHemoglobinConc field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set minTotalHemoglobinConc(List<double>? value) {
    final field = getField(LapMinTotalHemoglobinConcField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxTotalHemoglobinConc field. Returns null if the field is not defined in the message.
  List<double>? get maxTotalHemoglobinConc {
    final field = getField(LapMaxTotalHemoglobinConcField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the maxTotalHemoglobinConc field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxTotalHemoglobinConc(List<double>? value) {
    final field = getField(LapMaxTotalHemoglobinConcField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgSaturatedHemoglobinPercent field. Returns null if the field is not defined in the message.
  List<double>? get avgSaturatedHemoglobinPercent {
    final field = getField(LapAvgSaturatedHemoglobinPercentField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgSaturatedHemoglobinPercent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgSaturatedHemoglobinPercent(List<double>? value) {
    final field = getField(LapAvgSaturatedHemoglobinPercentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the minSaturatedHemoglobinPercent field. Returns null if the field is not defined in the message.
  List<double>? get minSaturatedHemoglobinPercent {
    final field = getField(LapMinSaturatedHemoglobinPercentField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the minSaturatedHemoglobinPercent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set minSaturatedHemoglobinPercent(List<double>? value) {
    final field = getField(LapMinSaturatedHemoglobinPercentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxSaturatedHemoglobinPercent field. Returns null if the field is not defined in the message.
  List<double>? get maxSaturatedHemoglobinPercent {
    final field = getField(LapMaxSaturatedHemoglobinPercentField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the maxSaturatedHemoglobinPercent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxSaturatedHemoglobinPercent(List<double>? value) {
    final field = getField(LapMaxSaturatedHemoglobinPercentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLeftTorqueEffectiveness field. Returns null if the field is not defined in the message.
  double? get avgLeftTorqueEffectiveness {
    final field = getField(LapAvgLeftTorqueEffectivenessField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgLeftTorqueEffectiveness field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLeftTorqueEffectiveness(double? value) {
    final field = getField(LapAvgLeftTorqueEffectivenessField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgRightTorqueEffectiveness field. Returns null if the field is not defined in the message.
  double? get avgRightTorqueEffectiveness {
    final field = getField(LapAvgRightTorqueEffectivenessField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgRightTorqueEffectiveness field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgRightTorqueEffectiveness(double? value) {
    final field = getField(LapAvgRightTorqueEffectivenessField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLeftPedalSmoothness field. Returns null if the field is not defined in the message.
  double? get avgLeftPedalSmoothness {
    final field = getField(LapAvgLeftPedalSmoothnessField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgLeftPedalSmoothness field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLeftPedalSmoothness(double? value) {
    final field = getField(LapAvgLeftPedalSmoothnessField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgRightPedalSmoothness field. Returns null if the field is not defined in the message.
  double? get avgRightPedalSmoothness {
    final field = getField(LapAvgRightPedalSmoothnessField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgRightPedalSmoothness field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgRightPedalSmoothness(double? value) {
    final field = getField(LapAvgRightPedalSmoothnessField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgCombinedPedalSmoothness field. Returns null if the field is not defined in the message.
  double? get avgCombinedPedalSmoothness {
    final field = getField(LapAvgCombinedPedalSmoothnessField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgCombinedPedalSmoothness field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgCombinedPedalSmoothness(double? value) {
    final field = getField(LapAvgCombinedPedalSmoothnessField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the timeStanding field. Returns null if the field is not defined in the message.
  double? get timeStanding {
    final field = getField(LapTimeStandingField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the timeStanding field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set timeStanding(double? value) {
    final field = getField(LapTimeStandingField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the standCount field. Returns null if the field is not defined in the message.
  int? get standCount {
    final field = getField(LapStandCountField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the standCount field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set standCount(int? value) {
    final field = getField(LapStandCountField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLeftPco field. Returns null if the field is not defined in the message.
  int? get avgLeftPco {
    final field = getField(LapAvgLeftPcoField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgLeftPco field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLeftPco(int? value) {
    final field = getField(LapAvgLeftPcoField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgRightPco field. Returns null if the field is not defined in the message.
  int? get avgRightPco {
    final field = getField(LapAvgRightPcoField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgRightPco field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgRightPco(int? value) {
    final field = getField(LapAvgRightPcoField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLeftPowerPhase field. Returns null if the field is not defined in the message.
  List<double>? get avgLeftPowerPhase {
    final field = getField(LapAvgLeftPowerPhaseField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgLeftPowerPhase field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLeftPowerPhase(List<double>? value) {
    final field = getField(LapAvgLeftPowerPhaseField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLeftPowerPhasePeak field. Returns null if the field is not defined in the message.
  List<double>? get avgLeftPowerPhasePeak {
    final field = getField(LapAvgLeftPowerPhasePeakField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgLeftPowerPhasePeak field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLeftPowerPhasePeak(List<double>? value) {
    final field = getField(LapAvgLeftPowerPhasePeakField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgRightPowerPhase field. Returns null if the field is not defined in the message.
  List<double>? get avgRightPowerPhase {
    final field = getField(LapAvgRightPowerPhaseField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgRightPowerPhase field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgRightPowerPhase(List<double>? value) {
    final field = getField(LapAvgRightPowerPhaseField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgRightPowerPhasePeak field. Returns null if the field is not defined in the message.
  List<double>? get avgRightPowerPhasePeak {
    final field = getField(LapAvgRightPowerPhasePeakField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgRightPowerPhasePeak field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgRightPowerPhasePeak(List<double>? value) {
    final field = getField(LapAvgRightPowerPhasePeakField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgPowerPosition field. Returns null if the field is not defined in the message.
  List<int>? get avgPowerPosition {
    final field = getField(LapAvgPowerPositionField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgPowerPosition field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgPowerPosition(List<int>? value) {
    final field = getField(LapAvgPowerPositionField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxPowerPosition field. Returns null if the field is not defined in the message.
  List<int>? get maxPowerPosition {
    final field = getField(LapMaxPowerPositionField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the maxPowerPosition field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxPowerPosition(List<int>? value) {
    final field = getField(LapMaxPowerPositionField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgCadencePosition field. Returns null if the field is not defined in the message.
  List<int>? get avgCadencePosition {
    final field = getField(LapAvgCadencePositionField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the avgCadencePosition field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgCadencePosition(List<int>? value) {
    final field = getField(LapAvgCadencePositionField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxCadencePosition field. Returns null if the field is not defined in the message.
  List<int>? get maxCadencePosition {
    final field = getField(LapMaxCadencePositionField.ID);
    if (field != null && field.isValid()) {
      return field.getValues();
    } else {
      return null;
    }
  }

  /// Sets the maxCadencePosition field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxCadencePosition(List<int>? value) {
    final field = getField(LapMaxCadencePositionField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        final values = value;
        field.setValues(values);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the enhancedAvgSpeed field. Returns null if the field is not defined in the message.
  double? get enhancedAvgSpeed {
    final field = getField(LapEnhancedAvgSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the enhancedAvgSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set enhancedAvgSpeed(double? value) {
    final field = getField(LapEnhancedAvgSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the enhancedMaxSpeed field. Returns null if the field is not defined in the message.
  double? get enhancedMaxSpeed {
    final field = getField(LapEnhancedMaxSpeedField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the enhancedMaxSpeed field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set enhancedMaxSpeed(double? value) {
    final field = getField(LapEnhancedMaxSpeedField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the enhancedAvgAltitude field. Returns null if the field is not defined in the message.
  double? get enhancedAvgAltitude {
    final field = getField(LapEnhancedAvgAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the enhancedAvgAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set enhancedAvgAltitude(double? value) {
    final field = getField(LapEnhancedAvgAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the enhancedMinAltitude field. Returns null if the field is not defined in the message.
  double? get enhancedMinAltitude {
    final field = getField(LapEnhancedMinAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the enhancedMinAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set enhancedMinAltitude(double? value) {
    final field = getField(LapEnhancedMinAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the enhancedMaxAltitude field. Returns null if the field is not defined in the message.
  double? get enhancedMaxAltitude {
    final field = getField(LapEnhancedMaxAltitudeField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the enhancedMaxAltitude field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set enhancedMaxAltitude(double? value) {
    final field = getField(LapEnhancedMaxAltitudeField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgLevMotorPower field. Returns null if the field is not defined in the message.
  int? get avgLevMotorPower {
    final field = getField(LapAvgLevMotorPowerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgLevMotorPower field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgLevMotorPower(int? value) {
    final field = getField(LapAvgLevMotorPowerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxLevMotorPower field. Returns null if the field is not defined in the message.
  int? get maxLevMotorPower {
    final field = getField(LapMaxLevMotorPowerField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxLevMotorPower field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxLevMotorPower(int? value) {
    final field = getField(LapMaxLevMotorPowerField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the levBatteryConsumption field. Returns null if the field is not defined in the message.
  double? get levBatteryConsumption {
    final field = getField(LapLevBatteryConsumptionField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the levBatteryConsumption field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set levBatteryConsumption(double? value) {
    final field = getField(LapLevBatteryConsumptionField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgVerticalRatio field. Returns null if the field is not defined in the message.
  double? get avgVerticalRatio {
    final field = getField(LapAvgVerticalRatioField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgVerticalRatio field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgVerticalRatio(double? value) {
    final field = getField(LapAvgVerticalRatioField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgStanceTimeBalance field. Returns null if the field is not defined in the message.
  double? get avgStanceTimeBalance {
    final field = getField(LapAvgStanceTimeBalanceField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgStanceTimeBalance field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgStanceTimeBalance(double? value) {
    final field = getField(LapAvgStanceTimeBalanceField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgStepLength field. Returns null if the field is not defined in the message.
  double? get avgStepLength {
    final field = getField(LapAvgStepLengthField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgStepLength field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgStepLength(double? value) {
    final field = getField(LapAvgStepLengthField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgVam field. Returns null if the field is not defined in the message.
  double? get avgVam {
    final field = getField(LapAvgVamField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgVam field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgVam(double? value) {
    final field = getField(LapAvgVamField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalGrit field. Returns null if the field is not defined in the message.
  int? get totalGrit {
    final field = getField(LapTotalGritField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalGrit field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalGrit(int? value) {
    final field = getField(LapTotalGritField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalFlow field. Returns null if the field is not defined in the message.
  int? get totalFlow {
    final field = getField(LapTotalFlowField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalFlow field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalFlow(int? value) {
    final field = getField(LapTotalFlowField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the jumpCount field. Returns null if the field is not defined in the message.
  int? get jumpCount {
    final field = getField(LapJumpCountField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the jumpCount field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set jumpCount(int? value) {
    final field = getField(LapJumpCountField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgGrit field. Returns null if the field is not defined in the message.
  int? get avgGrit {
    final field = getField(LapAvgGritField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgGrit field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgGrit(int? value) {
    final field = getField(LapAvgGritField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgFlow field. Returns null if the field is not defined in the message.
  int? get avgFlow {
    final field = getField(LapAvgFlowField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgFlow field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgFlow(int? value) {
    final field = getField(LapAvgFlowField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalFractionalAscent field. Returns null if the field is not defined in the message.
  double? get totalFractionalAscent {
    final field = getField(LapTotalFractionalAscentField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalFractionalAscent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalFractionalAscent(double? value) {
    final field = getField(LapTotalFractionalAscentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the totalFractionalDescent field. Returns null if the field is not defined in the message.
  double? get totalFractionalDescent {
    final field = getField(LapTotalFractionalDescentField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the totalFractionalDescent field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set totalFractionalDescent(double? value) {
    final field = getField(LapTotalFractionalDescentField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the avgCoreTemperature field. Returns null if the field is not defined in the message.
  double? get avgCoreTemperature {
    final field = getField(LapAvgCoreTemperatureField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the avgCoreTemperature field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set avgCoreTemperature(double? value) {
    final field = getField(LapAvgCoreTemperatureField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the minCoreTemperature field. Returns null if the field is not defined in the message.
  double? get minCoreTemperature {
    final field = getField(LapMinCoreTemperatureField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the minCoreTemperature field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set minCoreTemperature(double? value) {
    final field = getField(LapMinCoreTemperatureField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }

  /// Returns the value of the maxCoreTemperature field. Returns null if the field is not defined in the message.
  double? get maxCoreTemperature {
    final field = getField(LapMaxCoreTemperatureField.ID);
    if (field != null && field.isValid()) {
      var subField = field.getValidSubField(fields);
      return field.getValue(subField: subField);
    } else {
      return null;
    }
  }

  /// Sets the maxCoreTemperature field with [value]. Throws [FieldNotDefinedError] if the field is not defined in the message.
  set maxCoreTemperature(double? value) {
    final field = getField(LapMaxCoreTemperatureField.ID);

    if (field != null) {
      if (value == null) {
        field.clear();
      } else {
        var subField = field.getValidSubField(fields);
        field.setValue(0, value, subField);
      }
    } else {
      throw FieldNotDefinedError(field!.name);
    }
  }
}

class LapEventField extends Field {
  LapEventField({super.size, super.growable = true}) : super(name: 'event', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 0;
}

class LapEventTypeField extends Field {
  LapEventTypeField({super.size = 0, super.growable = true}) : super(name: 'event_type', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 1;
}

class LapStartTimeField extends Field {
  LapStartTimeField({super.size = 0, super.growable = true}) : super(name: 'start_time', id: ID, type: BaseType.UINT32, offset: -631065600000, scale: 0.001, units: 'ms', mainTypeName: 'date_time', subFields: []);

  static const ID = 2;
}

class LapStartPositionLatField extends Field {
  LapStartPositionLatField({super.size = 0, super.growable = true}) : super(name: 'start_position_lat', id: ID, type: BaseType.SINT32, offset: 0, scale: 11930464.711111112, units: 'degrees', mainTypeName: 'sint32', subFields: []);

  static const ID = 3;
}

class LapStartPositionLongField extends Field {
  LapStartPositionLongField({super.size = 0, super.growable = true}) : super(name: 'start_position_long', id: ID, type: BaseType.SINT32, offset: 0, scale: 11930464.711111112, units: 'degrees', mainTypeName: 'sint32', subFields: []);

  static const ID = 4;
}

class LapEndPositionLatField extends Field {
  LapEndPositionLatField({super.size = 0, super.growable = true}) : super(name: 'end_position_lat', id: ID, type: BaseType.SINT32, offset: 0, scale: 11930464.711111112, units: 'degrees', mainTypeName: 'sint32', subFields: []);

  static const ID = 5;
}

class LapEndPositionLongField extends Field {
  LapEndPositionLongField({super.size = 0, super.growable = true}) : super(name: 'end_position_long', id: ID, type: BaseType.SINT32, offset: 0, scale: 11930464.711111112, units: 'degrees', mainTypeName: 'sint32', subFields: []);

  static const ID = 6;
}

class LapTotalElapsedTimeField extends Field {
  LapTotalElapsedTimeField({super.size = 0, super.growable = true}) : super(name: 'total_elapsed_time', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 7;
}

class LapTotalTimerTimeField extends Field {
  LapTotalTimerTimeField({super.size = 0, super.growable = true}) : super(name: 'total_timer_time', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 8;
}

class LapTotalDistanceField extends Field {
  LapTotalDistanceField({super.size = 0, super.growable = true}) : super(name: 'total_distance', id: ID, type: BaseType.UINT32, offset: 0, scale: 100, units: 'm', mainTypeName: 'uint32', subFields: []);

  static const ID = 9;
}

class LapTotalCyclesField extends Field {
  LapTotalCyclesField({super.size = 0, super.growable = true})
      : super(name: 'total_cycles', id: ID, type: BaseType.UINT32, offset: 0, scale: 1, units: 'cycles', mainTypeName: 'uint32', subFields: [
          SubField(name: 'total_strides', type: BaseType.UINT32, scale: 1, offset: 0, units: 'strides', referenceMap: {
            LapSportField.ID: [1, 11]
          }),
          SubField(name: 'total_strokes', type: BaseType.UINT32, scale: 1, offset: 0, units: 'strokes', referenceMap: {
            LapSportField.ID: [2, 5, 15, 37]
          })
        ]);

  static const ID = 10;
}

class LapTotalCaloriesField extends Field {
  LapTotalCaloriesField({super.size = 0, super.growable = true}) : super(name: 'total_calories', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'kcal', mainTypeName: 'uint16', subFields: []);

  static const ID = 11;
}

class LapTotalFatCaloriesField extends Field {
  LapTotalFatCaloriesField({super.size = 0, super.growable = true}) : super(name: 'total_fat_calories', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'kcal', mainTypeName: 'uint16', subFields: []);

  static const ID = 12;
}

class LapAvgSpeedField extends Field {
  LapAvgSpeedField({super.size = 0, super.growable = true}) : super(name: 'avg_speed', id: ID, type: BaseType.UINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'uint16', subFields: []);

  static const ID = 13;
}

class LapMaxSpeedField extends Field {
  LapMaxSpeedField({super.size = 0, super.growable = true}) : super(name: 'max_speed', id: ID, type: BaseType.UINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'uint16', subFields: []);

  static const ID = 14;
}

class LapAvgHeartRateField extends Field {
  LapAvgHeartRateField({super.size = 0, super.growable = true}) : super(name: 'avg_heart_rate', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'bpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 15;
}

class LapMaxHeartRateField extends Field {
  LapMaxHeartRateField({super.size = 0, super.growable = true}) : super(name: 'max_heart_rate', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'bpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 16;
}

class LapAvgCadenceField extends Field {
  LapAvgCadenceField({super.size = 0, super.growable = true})
      : super(name: 'avg_cadence', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'rpm', mainTypeName: 'uint8', subFields: [
          SubField(name: 'avg_running_cadence', type: BaseType.UINT8, scale: 1, offset: 0, units: 'strides/min', referenceMap: {
            LapSportField.ID: [1]
          })
        ]);

  static const ID = 17;
}

class LapMaxCadenceField extends Field {
  LapMaxCadenceField({super.size = 0, super.growable = true})
      : super(name: 'max_cadence', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'rpm', mainTypeName: 'uint8', subFields: [
          SubField(name: 'max_running_cadence', type: BaseType.UINT8, scale: 1, offset: 0, units: 'strides/min', referenceMap: {
            LapSportField.ID: [1]
          })
        ]);

  static const ID = 18;
}

class LapAvgPowerField extends Field {
  LapAvgPowerField({super.size = 0, super.growable = true}) : super(name: 'avg_power', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 19;
}

class LapMaxPowerField extends Field {
  LapMaxPowerField({super.size = 0, super.growable = true}) : super(name: 'max_power', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 20;
}

class LapTotalAscentField extends Field {
  LapTotalAscentField({super.size = 0, super.growable = true}) : super(name: 'total_ascent', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 21;
}

class LapTotalDescentField extends Field {
  LapTotalDescentField({super.size = 0, super.growable = true}) : super(name: 'total_descent', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 22;
}

class LapIntensityField extends Field {
  LapIntensityField({super.size = 0, super.growable = true}) : super(name: 'intensity', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 23;
}

class LapLapTriggerField extends Field {
  LapLapTriggerField({super.size = 0, super.growable = true}) : super(name: 'lap_trigger', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 24;
}

class LapSportField extends Field {
  LapSportField({super.size = 0, super.growable = true}) : super(name: 'sport', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 25;
}

class LapEventGroupField extends Field {
  LapEventGroupField({super.size = 0, super.growable = true}) : super(name: 'event_group', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, subFields: []);

  static const ID = 26;
}

class LapNumLengthsField extends Field {
  LapNumLengthsField({super.size = 0, super.growable = true}) : super(name: 'num_lengths', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'lengths', mainTypeName: 'uint16', subFields: []);

  static const ID = 32;
}

class LapNormalizedPowerField extends Field {
  LapNormalizedPowerField({super.size = 0, super.growable = true}) : super(name: 'normalized_power', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 33;
}

class LapLeftRightBalanceField extends Field {
  LapLeftRightBalanceField({super.size = 0, super.growable = true}) : super(name: 'left_right_balance', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 34;
}

class LapFirstLengthIndexField extends Field {
  LapFirstLengthIndexField({super.size = 0, super.growable = true}) : super(name: 'first_length_index', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 35;
}

class LapAvgStrokeDistanceField extends Field {
  LapAvgStrokeDistanceField({super.size = 0, super.growable = true}) : super(name: 'avg_stroke_distance', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 37;
}

class LapSwimStrokeField extends Field {
  LapSwimStrokeField({super.size = 0, super.growable = true}) : super(name: 'swim_stroke', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 38;
}

class LapSubSportField extends Field {
  LapSubSportField({super.size = 0, super.growable = true}) : super(name: 'sub_sport', id: ID, type: BaseType.ENUM, offset: 0, scale: 1, subFields: []);

  static const ID = 39;
}

class LapNumActiveLengthsField extends Field {
  LapNumActiveLengthsField({super.size = 0, super.growable = true}) : super(name: 'num_active_lengths', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'lengths', mainTypeName: 'uint16', subFields: []);

  static const ID = 40;
}

class LapTotalWorkField extends Field {
  LapTotalWorkField({super.size = 0, super.growable = true}) : super(name: 'total_work', id: ID, type: BaseType.UINT32, offset: 0, scale: 1, units: 'J', mainTypeName: 'uint32', subFields: []);

  static const ID = 41;
}

class LapAvgAltitudeField extends Field {
  LapAvgAltitudeField({super.size = 0, super.growable = true}) : super(name: 'avg_altitude', id: ID, type: BaseType.UINT16, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 42;
}

class LapMaxAltitudeField extends Field {
  LapMaxAltitudeField({super.size = 0, super.growable = true}) : super(name: 'max_altitude', id: ID, type: BaseType.UINT16, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 43;
}

class LapGpsAccuracyField extends Field {
  LapGpsAccuracyField({super.size = 0, super.growable = true}) : super(name: 'gps_accuracy', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'm', mainTypeName: 'uint8', subFields: []);

  static const ID = 44;
}

class LapAvgGradeField extends Field {
  LapAvgGradeField({super.size = 0, super.growable = true}) : super(name: 'avg_grade', id: ID, type: BaseType.SINT16, offset: 0, scale: 100, units: '%', mainTypeName: 'sint16', subFields: []);

  static const ID = 45;
}

class LapAvgPosGradeField extends Field {
  LapAvgPosGradeField({super.size = 0, super.growable = true}) : super(name: 'avg_pos_grade', id: ID, type: BaseType.SINT16, offset: 0, scale: 100, units: '%', mainTypeName: 'sint16', subFields: []);

  static const ID = 46;
}

class LapAvgNegGradeField extends Field {
  LapAvgNegGradeField({super.size = 0, super.growable = true}) : super(name: 'avg_neg_grade', id: ID, type: BaseType.SINT16, offset: 0, scale: 100, units: '%', mainTypeName: 'sint16', subFields: []);

  static const ID = 47;
}

class LapMaxPosGradeField extends Field {
  LapMaxPosGradeField({super.size = 0, super.growable = true}) : super(name: 'max_pos_grade', id: ID, type: BaseType.SINT16, offset: 0, scale: 100, units: '%', mainTypeName: 'sint16', subFields: []);

  static const ID = 48;
}

class LapMaxNegGradeField extends Field {
  LapMaxNegGradeField({super.size = 0, super.growable = true}) : super(name: 'max_neg_grade', id: ID, type: BaseType.SINT16, offset: 0, scale: 100, units: '%', mainTypeName: 'sint16', subFields: []);

  static const ID = 49;
}

class LapAvgTemperatureField extends Field {
  LapAvgTemperatureField({super.size = 0, super.growable = true}) : super(name: 'avg_temperature', id: ID, type: BaseType.SINT8, offset: 0, scale: 1, units: 'C', mainTypeName: 'sint8', subFields: []);

  static const ID = 50;
}

class LapMaxTemperatureField extends Field {
  LapMaxTemperatureField({super.size = 0, super.growable = true}) : super(name: 'max_temperature', id: ID, type: BaseType.SINT8, offset: 0, scale: 1, units: 'C', mainTypeName: 'sint8', subFields: []);

  static const ID = 51;
}

class LapTotalMovingTimeField extends Field {
  LapTotalMovingTimeField({super.size = 0, super.growable = true}) : super(name: 'total_moving_time', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 52;
}

class LapAvgPosVerticalSpeedField extends Field {
  LapAvgPosVerticalSpeedField({super.size = 0, super.growable = true}) : super(name: 'avg_pos_vertical_speed', id: ID, type: BaseType.SINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'sint16', subFields: []);

  static const ID = 53;
}

class LapAvgNegVerticalSpeedField extends Field {
  LapAvgNegVerticalSpeedField({super.size = 0, super.growable = true}) : super(name: 'avg_neg_vertical_speed', id: ID, type: BaseType.SINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'sint16', subFields: []);

  static const ID = 54;
}

class LapMaxPosVerticalSpeedField extends Field {
  LapMaxPosVerticalSpeedField({super.size = 0, super.growable = true}) : super(name: 'max_pos_vertical_speed', id: ID, type: BaseType.SINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'sint16', subFields: []);

  static const ID = 55;
}

class LapMaxNegVerticalSpeedField extends Field {
  LapMaxNegVerticalSpeedField({super.size = 0, super.growable = true}) : super(name: 'max_neg_vertical_speed', id: ID, type: BaseType.SINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'sint16', subFields: []);

  static const ID = 56;
}

class LapTimeInHrZoneField extends Field {
  LapTimeInHrZoneField({super.size = 0, super.growable = true}) : super(name: 'time_in_hr_zone', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 57;
}

class LapTimeInSpeedZoneField extends Field {
  LapTimeInSpeedZoneField({super.size = 0, super.growable = true}) : super(name: 'time_in_speed_zone', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 58;
}

class LapTimeInCadenceZoneField extends Field {
  LapTimeInCadenceZoneField({super.size = 0, super.growable = true}) : super(name: 'time_in_cadence_zone', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 59;
}

class LapTimeInPowerZoneField extends Field {
  LapTimeInPowerZoneField({super.size = 0, super.growable = true}) : super(name: 'time_in_power_zone', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 60;
}

class LapRepetitionNumField extends Field {
  LapRepetitionNumField({super.size = 0, super.growable = true}) : super(name: 'repetition_num', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 61;
}

class LapMinAltitudeField extends Field {
  LapMinAltitudeField({super.size = 0, super.growable = true}) : super(name: 'min_altitude', id: ID, type: BaseType.UINT16, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint16', subFields: []);

  static const ID = 62;
}

class LapMinHeartRateField extends Field {
  LapMinHeartRateField({super.size = 0, super.growable = true}) : super(name: 'min_heart_rate', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'bpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 63;
}

class LapWorkoutStepIndexField extends Field {
  LapWorkoutStepIndexField({super.size = 0, super.growable = true}) : super(name: 'wkt_step_index', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 71;
}

class LapOpponentScoreField extends Field {
  LapOpponentScoreField({super.size = 0, super.growable = true}) : super(name: 'opponent_score', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 74;
}

class LapStrokeCountField extends Field {
  LapStrokeCountField({super.size = 0, super.growable = true}) : super(name: 'stroke_count', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'counts', mainTypeName: 'uint16', subFields: []);

  static const ID = 75;
}

class LapZoneCountField extends Field {
  LapZoneCountField({super.size = 0, super.growable = true}) : super(name: 'zone_count', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'counts', mainTypeName: 'uint16', subFields: []);

  static const ID = 76;
}

class LapAvgVerticalOscillationField extends Field {
  LapAvgVerticalOscillationField({super.size = 0, super.growable = true}) : super(name: 'avg_vertical_oscillation', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: 'mm', mainTypeName: 'uint16', subFields: []);

  static const ID = 77;
}

class LapAvgStanceTimePercentField extends Field {
  LapAvgStanceTimePercentField({super.size = 0, super.growable = true}) : super(name: 'avg_stance_time_percent', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'percent', mainTypeName: 'uint16', subFields: []);

  static const ID = 78;
}

class LapAvgStanceTimeField extends Field {
  LapAvgStanceTimeField({super.size = 0, super.growable = true}) : super(name: 'avg_stance_time', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: 'ms', mainTypeName: 'uint16', subFields: []);

  static const ID = 79;
}

class LapAvgFractionalCadenceField extends Field {
  LapAvgFractionalCadenceField({super.size = 0, super.growable = true}) : super(name: 'avg_fractional_cadence', id: ID, type: BaseType.UINT8, offset: 0, scale: 128, units: 'rpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 80;
}

class LapMaxFractionalCadenceField extends Field {
  LapMaxFractionalCadenceField({super.size = 0, super.growable = true}) : super(name: 'max_fractional_cadence', id: ID, type: BaseType.UINT8, offset: 0, scale: 128, units: 'rpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 81;
}

class LapTotalFractionalCyclesField extends Field {
  LapTotalFractionalCyclesField({super.size = 0, super.growable = true}) : super(name: 'total_fractional_cycles', id: ID, type: BaseType.UINT8, offset: 0, scale: 128, units: 'cycles', mainTypeName: 'uint8', subFields: []);

  static const ID = 82;
}

class LapPlayerScoreField extends Field {
  LapPlayerScoreField({super.size = 0, super.growable = true}) : super(name: 'player_score', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 83;
}

class LapAvgTotalHemoglobinConcField extends Field {
  LapAvgTotalHemoglobinConcField({super.size = 0, super.growable = true}) : super(name: 'avg_total_hemoglobin_conc', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'g/dL', mainTypeName: 'uint16', subFields: []);

  static const ID = 84;
}

class LapMinTotalHemoglobinConcField extends Field {
  LapMinTotalHemoglobinConcField({super.size = 0, super.growable = true}) : super(name: 'min_total_hemoglobin_conc', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'g/dL', mainTypeName: 'uint16', subFields: []);

  static const ID = 85;
}

class LapMaxTotalHemoglobinConcField extends Field {
  LapMaxTotalHemoglobinConcField({super.size = 0, super.growable = true}) : super(name: 'max_total_hemoglobin_conc', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'g/dL', mainTypeName: 'uint16', subFields: []);

  static const ID = 86;
}

class LapAvgSaturatedHemoglobinPercentField extends Field {
  LapAvgSaturatedHemoglobinPercentField({super.size = 0, super.growable = true}) : super(name: 'avg_saturated_hemoglobin_percent', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: '%', mainTypeName: 'uint16', subFields: []);

  static const ID = 87;
}

class LapMinSaturatedHemoglobinPercentField extends Field {
  LapMinSaturatedHemoglobinPercentField({super.size = 0, super.growable = true}) : super(name: 'min_saturated_hemoglobin_percent', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: '%', mainTypeName: 'uint16', subFields: []);

  static const ID = 88;
}

class LapMaxSaturatedHemoglobinPercentField extends Field {
  LapMaxSaturatedHemoglobinPercentField({super.size = 0, super.growable = true}) : super(name: 'max_saturated_hemoglobin_percent', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: '%', mainTypeName: 'uint16', subFields: []);

  static const ID = 89;
}

class LapAvgLeftTorqueEffectivenessField extends Field {
  LapAvgLeftTorqueEffectivenessField({super.size = 0, super.growable = true}) : super(name: 'avg_left_torque_effectiveness', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 91;
}

class LapAvgRightTorqueEffectivenessField extends Field {
  LapAvgRightTorqueEffectivenessField({super.size = 0, super.growable = true}) : super(name: 'avg_right_torque_effectiveness', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 92;
}

class LapAvgLeftPedalSmoothnessField extends Field {
  LapAvgLeftPedalSmoothnessField({super.size = 0, super.growable = true}) : super(name: 'avg_left_pedal_smoothness', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 93;
}

class LapAvgRightPedalSmoothnessField extends Field {
  LapAvgRightPedalSmoothnessField({super.size = 0, super.growable = true}) : super(name: 'avg_right_pedal_smoothness', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 94;
}

class LapAvgCombinedPedalSmoothnessField extends Field {
  LapAvgCombinedPedalSmoothnessField({super.size = 0, super.growable = true}) : super(name: 'avg_combined_pedal_smoothness', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 95;
}

class LapTimeStandingField extends Field {
  LapTimeStandingField({super.size = 0, super.growable = true}) : super(name: 'time_standing', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 's', mainTypeName: 'uint32', subFields: []);

  static const ID = 98;
}

class LapStandCountField extends Field {
  LapStandCountField({super.size = 0, super.growable = true}) : super(name: 'stand_count', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 99;
}

class LapAvgLeftPcoField extends Field {
  LapAvgLeftPcoField({super.size = 0, super.growable = true}) : super(name: 'avg_left_pco', id: ID, type: BaseType.SINT8, offset: 0, scale: 1, units: 'mm', mainTypeName: 'sint8', subFields: []);

  static const ID = 100;
}

class LapAvgRightPcoField extends Field {
  LapAvgRightPcoField({super.size = 0, super.growable = true}) : super(name: 'avg_right_pco', id: ID, type: BaseType.SINT8, offset: 0, scale: 1, units: 'mm', mainTypeName: 'sint8', subFields: []);

  static const ID = 101;
}

class LapAvgLeftPowerPhaseField extends Field {
  LapAvgLeftPowerPhaseField({super.size = 0, super.growable = true}) : super(name: 'avg_left_power_phase', id: ID, type: BaseType.UINT8, offset: 0, scale: 0.7111111, units: 'degrees', mainTypeName: 'uint8', subFields: []);

  static const ID = 102;
}

class LapAvgLeftPowerPhasePeakField extends Field {
  LapAvgLeftPowerPhasePeakField({super.size = 0, super.growable = true}) : super(name: 'avg_left_power_phase_peak', id: ID, type: BaseType.UINT8, offset: 0, scale: 0.7111111, units: 'degrees', mainTypeName: 'uint8', subFields: []);

  static const ID = 103;
}

class LapAvgRightPowerPhaseField extends Field {
  LapAvgRightPowerPhaseField({super.size = 0, super.growable = true}) : super(name: 'avg_right_power_phase', id: ID, type: BaseType.UINT8, offset: 0, scale: 0.7111111, units: 'degrees', mainTypeName: 'uint8', subFields: []);

  static const ID = 104;
}

class LapAvgRightPowerPhasePeakField extends Field {
  LapAvgRightPowerPhasePeakField({super.size = 0, super.growable = true}) : super(name: 'avg_right_power_phase_peak', id: ID, type: BaseType.UINT8, offset: 0, scale: 0.7111111, units: 'degrees', mainTypeName: 'uint8', subFields: []);

  static const ID = 105;
}

class LapAvgPowerPositionField extends Field {
  LapAvgPowerPositionField({super.size = 0, super.growable = true}) : super(name: 'avg_power_position', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 106;
}

class LapMaxPowerPositionField extends Field {
  LapMaxPowerPositionField({super.size = 0, super.growable = true}) : super(name: 'max_power_position', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 107;
}

class LapAvgCadencePositionField extends Field {
  LapAvgCadencePositionField({super.size = 0, super.growable = true}) : super(name: 'avg_cadence_position', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'rpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 108;
}

class LapMaxCadencePositionField extends Field {
  LapMaxCadencePositionField({super.size = 0, super.growable = true}) : super(name: 'max_cadence_position', id: ID, type: BaseType.UINT8, offset: 0, scale: 1, units: 'rpm', mainTypeName: 'uint8', subFields: []);

  static const ID = 109;
}

class LapEnhancedAvgSpeedField extends Field {
  LapEnhancedAvgSpeedField({super.size = 0, super.growable = true}) : super(name: 'enhanced_avg_speed', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'uint32', subFields: []);

  static const ID = 110;
}

class LapEnhancedMaxSpeedField extends Field {
  LapEnhancedMaxSpeedField({super.size = 0, super.growable = true}) : super(name: 'enhanced_max_speed', id: ID, type: BaseType.UINT32, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'uint32', subFields: []);

  static const ID = 111;
}

class LapEnhancedAvgAltitudeField extends Field {
  LapEnhancedAvgAltitudeField({super.size = 0, super.growable = true}) : super(name: 'enhanced_avg_altitude', id: ID, type: BaseType.UINT32, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint32', subFields: []);

  static const ID = 112;
}

class LapEnhancedMinAltitudeField extends Field {
  LapEnhancedMinAltitudeField({super.size = 0, super.growable = true}) : super(name: 'enhanced_min_altitude', id: ID, type: BaseType.UINT32, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint32', subFields: []);

  static const ID = 113;
}

class LapEnhancedMaxAltitudeField extends Field {
  LapEnhancedMaxAltitudeField({super.size = 0, super.growable = true}) : super(name: 'enhanced_max_altitude', id: ID, type: BaseType.UINT32, offset: 500, scale: 5, units: 'm', mainTypeName: 'uint32', subFields: []);

  static const ID = 114;
}

class LapAvgLevMotorPowerField extends Field {
  LapAvgLevMotorPowerField({super.size = 0, super.growable = true}) : super(name: 'avg_lev_motor_power', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 115;
}

class LapMaxLevMotorPowerField extends Field {
  LapMaxLevMotorPowerField({super.size = 0, super.growable = true}) : super(name: 'max_lev_motor_power', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, units: 'watts', mainTypeName: 'uint16', subFields: []);

  static const ID = 116;
}

class LapLevBatteryConsumptionField extends Field {
  LapLevBatteryConsumptionField({super.size = 0, super.growable = true}) : super(name: 'lev_battery_consumption', id: ID, type: BaseType.UINT8, offset: 0, scale: 2, units: 'percent', mainTypeName: 'uint8', subFields: []);

  static const ID = 117;
}

class LapAvgVerticalRatioField extends Field {
  LapAvgVerticalRatioField({super.size = 0, super.growable = true}) : super(name: 'avg_vertical_ratio', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'percent', mainTypeName: 'uint16', subFields: []);

  static const ID = 118;
}

class LapAvgStanceTimeBalanceField extends Field {
  LapAvgStanceTimeBalanceField({super.size = 0, super.growable = true}) : super(name: 'avg_stance_time_balance', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'percent', mainTypeName: 'uint16', subFields: []);

  static const ID = 119;
}

class LapAvgStepLengthField extends Field {
  LapAvgStepLengthField({super.size = 0, super.growable = true}) : super(name: 'avg_step_length', id: ID, type: BaseType.UINT16, offset: 0, scale: 10, units: 'mm', mainTypeName: 'uint16', subFields: []);

  static const ID = 120;
}

class LapAvgVamField extends Field {
  LapAvgVamField({super.size = 0, super.growable = true}) : super(name: 'avg_vam', id: ID, type: BaseType.UINT16, offset: 0, scale: 1000, units: 'm/s', mainTypeName: 'uint16', subFields: []);

  static const ID = 121;
}

class LapTotalGritField extends Field {
  LapTotalGritField({super.size = 0, super.growable = true}) : super(name: 'total_grit', id: ID, type: BaseType.FLOAT32, offset: 0, scale: 1, units: 'kGrit', mainTypeName: 'float32', subFields: []);

  static const ID = 149;
}

class LapTotalFlowField extends Field {
  LapTotalFlowField({super.size = 0, super.growable = true}) : super(name: 'total_flow', id: ID, type: BaseType.FLOAT32, offset: 0, scale: 1, units: 'Flow', mainTypeName: 'float32', subFields: []);

  static const ID = 150;
}

class LapJumpCountField extends Field {
  LapJumpCountField({super.size = 0, super.growable = true}) : super(name: 'jump_count', id: ID, type: BaseType.UINT16, offset: 0, scale: 1, subFields: []);

  static const ID = 151;
}

class LapAvgGritField extends Field {
  LapAvgGritField({super.size = 0, super.growable = true}) : super(name: 'avg_grit', id: ID, type: BaseType.FLOAT32, offset: 0, scale: 1, units: 'kGrit', mainTypeName: 'float32', subFields: []);

  static const ID = 153;
}

class LapAvgFlowField extends Field {
  LapAvgFlowField({super.size = 0, super.growable = true}) : super(name: 'avg_flow', id: ID, type: BaseType.FLOAT32, offset: 0, scale: 1, units: 'Flow', mainTypeName: 'float32', subFields: []);

  static const ID = 154;
}

class LapTotalFractionalAscentField extends Field {
  LapTotalFractionalAscentField({super.size = 0, super.growable = true}) : super(name: 'total_fractional_ascent', id: ID, type: BaseType.UINT8, offset: 0, scale: 100, units: 'm', mainTypeName: 'uint8', subFields: []);

  static const ID = 156;
}

class LapTotalFractionalDescentField extends Field {
  LapTotalFractionalDescentField({super.size = 0, super.growable = true}) : super(name: 'total_fractional_descent', id: ID, type: BaseType.UINT8, offset: 0, scale: 100, units: 'm', mainTypeName: 'uint8', subFields: []);

  static const ID = 157;
}

class LapAvgCoreTemperatureField extends Field {
  LapAvgCoreTemperatureField({super.size = 0, super.growable = true}) : super(name: 'avg_core_temperature', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'C', mainTypeName: 'uint16', subFields: []);

  static const ID = 158;
}

class LapMinCoreTemperatureField extends Field {
  LapMinCoreTemperatureField({super.size = 0, super.growable = true}) : super(name: 'min_core_temperature', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'C', mainTypeName: 'uint16', subFields: []);

  static const ID = 159;
}

class LapMaxCoreTemperatureField extends Field {
  LapMaxCoreTemperatureField({super.size = 0, super.growable = true}) : super(name: 'max_core_temperature', id: ID, type: BaseType.UINT16, offset: 0, scale: 100, units: 'C', mainTypeName: 'uint16', subFields: []);

  static const ID = 160;
}
