// ignore_for_file: constant_identifier_names
import 'data_message.dart';
import 'field.dart';

class GenericMessage extends DataMessage {
  GenericMessage({
    required super.definitionMessage,
    super.developerFields,
  }) : super(
          name: GenericMessage.NAME,
          globalId: definitionMessage!.globalId,
          localId: definitionMessage.localId,
          endian: definitionMessage.endian,
          fields: definitionMessage.fieldDefinitions.map<Field>((fieldDefinition) => Field.fromFieldDefinition(fieldDefinition)).toList(),
        );

  static const NAME = 'generic';
}
