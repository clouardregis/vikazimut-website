import 'dart:typed_data';

import 'package:collection/collection.dart';

import 'base_type.dart';
import 'definition_message.dart';
import 'developer_field.dart';
import 'fit_file_header.dart';
import 'profile/messages/field_description_message.dart';
import 'record.dart';
import 'utils/crc.dart';

Function listEqual = const ListEquality().equals;

class FitFile {
  final FitFileHeader header;
  final List<Record> records;
  int? crc; //crc16 of header and records

  FitFile(this.header, this.records, this.crc);

  static FitFile fromBytes(Uint8List bytes, {checkCrc = true, checkRecords = false}) {
    var byteOffset = 0;
    var crc = 0;

    final headerSize = bytes[0];
    final headerBytes = Uint8List.sublistView(bytes, byteOffset, byteOffset + headerSize);
    final header = FitFileHeader.fromBytes(headerBytes);
    crc = crc16(headerBytes, initial: crc);
    byteOffset += header.size;

    final records = <Record>[];
    final definitionMessageMap = <int, DefinitionMessage>{};
    final developerFieldsById = <int, Map<int, DeveloperField>>{};

    var recordBytesRemainingCount = header.recordsSize;
    while (recordBytesRemainingCount > 0) {
      final remainingBytes = Uint8List.sublistView(bytes, byteOffset);

      final record = Record.fromBytes(definitionMessageMap, remainingBytes, developerFieldsById: developerFieldsById);

      if (record.isDefinition) {
        definitionMessageMap[record.localId] = record.message as DefinitionMessage;
      } else if (record.message is FieldDescriptionMessage) {
        final message = record.message as FieldDescriptionMessage;

        final developerField = DeveloperField(
          developerDataIndex: message.developerDataIndex ?? 0,
          id: message.fieldDefinitionNumber ?? 0,
          name: message.name,
          type: BaseTypeExtension.fromValue(message.fitBaseTypeId ?? 0),
          scale: message.scale?.toDouble(),
          offset: message.offset?.toDouble(),
          units: message.units ?? '',
        );
        if (developerFieldsById[developerField.developerDataIndex] == null) {
          developerFieldsById[developerField.developerDataIndex] = {};
        }
        developerFieldsById[developerField.developerDataIndex]![developerField.id] = developerField;
      }

      records.add(record);

      final definitionMessage = definitionMessageMap[record.localId];
      final definedSize = record.definedSize(definitionMessage!);

      crc = crc16(Uint8List.sublistView(bytes, byteOffset, byteOffset + definedSize), initial: crc);

      recordBytesRemainingCount -= definedSize;
      byteOffset += definedSize;
    }

    final byteData = ByteData.sublistView(bytes, byteOffset, byteOffset + 2);
    final fileCrc = byteData.getUint16(0, Endian.little);

    if (crc != fileCrc) {
      final message = 'Calculated crc (0x${crc.toRadixString(16)}) does match crc in file (0x${fileCrc.toRadixString(16)}).';
      if (checkCrc) {
        throw Exception(message);
      }
    }

    return FitFile(header, records, crc);
  }
}
