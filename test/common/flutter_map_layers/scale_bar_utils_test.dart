import 'package:flutter_test/flutter_test.dart';
import 'package:latlong2/latlong.dart';
import 'package:vikazimut_website/common/flutter_map_layers/scale_layer_widget.dart';

void main() {
  test('test calculateEndingGeodesicCoordinates', () {
    LatLng start = const LatLng(49.040315, -0.431608);
    double startBearing = 90;
    double distance = 10000;
    var actual = calculateEndingGeodesicCoordinates(start, startBearing, distance);
    expect(actual.latitude, moreOrLessEquals(49.04023, epsilon: 0.00001));
    expect(actual.longitude, moreOrLessEquals(-0.294833040, epsilon: 0.000001));
  });
}
