import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/download_stats/download_stats_model.dart';

void main() {
  test('test compute download per month when less than 1 month', () {
    double actual = DownloadStatsModel.computeDownloadPerMonth_(10, 1);
    expect(actual, 10);
  });

  test('test compute download per month when exactly 1 month', () {
    double actual = DownloadStatsModel.computeDownloadPerMonth_(10, DownloadStatsModel.MONTH_IN_MS - 1);
    expect(actual, 10);
  });

  test('test compute download per month when 2 months', () {
    double actual = DownloadStatsModel.computeDownloadPerMonth_(10, 2 * DownloadStatsModel.MONTH_IN_MS - 1);
    expect(actual, 5);
  });
}
