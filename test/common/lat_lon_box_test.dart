import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';

void main() {
  test('test calculateFourQuadrilateralCorners without rotation', () {
    var latLonBox = const LatLonBox(north: 10, south: -10, east: 10, west: -10, rotation: 0);
    List<List<double>> expected = [
      [10.0, -10.0],
      [10.0, 10.0],
      [-10.0, 10.0],
      [-10.0, -10.0],
    ];
    var actual = latLonBox.calculateFourQuadrilateralCorners();
    expect(actual, expected);
  });

  test('test calculateFourQuadrilateralCorners with rotation', () {
    var latLonBox = const LatLonBox(north: 10, south: -10, east: 10, west: -10, rotation: 45);
    List<List<double>> expected = [
      [10.0, -10.0],
      [10.0, 10.0],
      [-10.0, 10.0],
      [-10.0, -10.0],
    ];
    var actual = latLonBox.calculateFourQuadrilateralCorners();
    expect(actual, expected);
  });
}
