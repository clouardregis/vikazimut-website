import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/xml_utils.dart';

void main() {
  test('test convert XML tags to lowercase nominal case', () {
    var actual = convertXmlTagsToLowerCase("<Essai><UnAutre></UnAutre></Essai>");
    expect(actual, "<essai><unautre></unautre></essai>");
  });

  test('test convert XML tags to lowercase with package name', () {
    var actual = convertXmlTagsToLowerCase("<gx:LatLonQuad>");
    expect(actual, "<gx:latlonquad>");
  });

  test('test convert XML tags to lowercase with attribute', () {
    var actual = convertXmlTagsToLowerCase('<GroundOverlay id="PNG">');
    expect(actual, '<groundoverlay id="PNG">');
  });

  test('test convert XML tags to lowercase when empty', () {
    var actual = convertXmlTagsToLowerCase("");
    expect(actual, "");
  });
}
