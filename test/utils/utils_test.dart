import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/utils/utils.dart';

void main() {
  test('Test compute median value when even list', () {
    List<double> tab = [3, 5, 1, 2, 4];
    var distanceMedian = computeMedianValue(tab);
    expect(distanceMedian, 3);
  });

  test('Test compute median value when odd list', () {
    List<double> tab = [3, 4, 1, 2];
    var distanceMedian = computeMedianValue(tab);
    expect(distanceMedian, 2);
  });

  test('Test compute median value one value', () {
    List<double> tab = [1];
    var distanceMedian = computeMedianValue(tab);
    expect(distanceMedian, 1);
  });
}
