import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/utils/duration_picker.dart';

void main() {
  test('Test getSecondsFromMilliseconds 1', () {
    final actual = getSecondsFromMilliseconds(60 * 1000);
    expect(actual, 0);
  });

  test('test getSecondsFromMilliseconds 2', () {
    final actual = getSecondsFromMilliseconds(((3 * 60) + 10) * 1000);
    expect(actual, 10);
  });

  test('Test getMinutesFromMilliseconds 1', () {
    final actual = getMinutesFromMilliseconds(((3 * 60) + 10) * 1000);
    expect(actual, 3);
  });

  test('Test getMinutesFromMilliseconds 2', () {
    final actual = getMinutesFromMilliseconds(((60 * 60) + 10) * 1000);
    expect(actual, 0);
  });

  test('Test getMinutesFromMilliseconds 3', () {
    final actual = getMinutesFromMilliseconds(10 * 1000);
    expect(actual, 0);
  });

  test('Test getHoursFromMilliseconds 1', () {
    final actual = getHoursFromMilliseconds(((3 * 60) + 10) * 1000);
    expect(actual, 0);
  });

  test('Test getHoursFromMilliseconds 2', () {
    final actual = getHoursFromMilliseconds(((60 * 60) + 10) * 1000);
    expect(actual, 1);
  });
}
