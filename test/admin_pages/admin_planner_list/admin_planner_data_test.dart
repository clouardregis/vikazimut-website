import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/admin_pages/admin_planner_list/admin_planner_data.dart';

void main() {
  test('test sort on username ascending -1', () {
    AdminPlannerData a = AdminPlannerData(username: "a");
    AdminPlannerData b = AdminPlannerData(username: "b");
    var actual = AdminPlannerData.sortOnUsername(true, a, b);
    expect(actual, -1);
  });
  test('test sort on username ascending 0', () {
    AdminPlannerData a = AdminPlannerData(username: "a");
    AdminPlannerData b = AdminPlannerData(username: "a");
    var actual = AdminPlannerData.sortOnUsername(true, a, b);
    expect(actual, 0);
  });
  test('test sort on username ascending 1', () {
    AdminPlannerData a = AdminPlannerData(username: "b");
    AdminPlannerData b = AdminPlannerData(username: "a");
    var actual = AdminPlannerData.sortOnUsername(true, a, b);
    expect(actual, 1);
  });
  test('test sort on username descending -1', () {
    AdminPlannerData a = AdminPlannerData(username: "a");
    AdminPlannerData b = AdminPlannerData(username: "b");
    var actual = AdminPlannerData.sortOnUsername(false, a, b);
    expect(actual, 1);
  });
  test('test sort on username descending 0', () {
    AdminPlannerData a = AdminPlannerData(username: "a");
    AdminPlannerData b = AdminPlannerData(username: "a");
    var actual = AdminPlannerData.sortOnUsername(false, a, b);
    expect(actual, 0);
  });
  test('test sort on username descending 1', () {
    AdminPlannerData a = AdminPlannerData(username: "b");
    AdminPlannerData b = AdminPlannerData(username: "a");
    var actual = AdminPlannerData.sortOnUsername(false, a, b);
    expect(actual, -1);
  });

  test('test sort on courseCount ascending -1', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 1);
    AdminPlannerData b = AdminPlannerData(courseCount: 2);
    var actual = AdminPlannerData.sortOnCourseCount(true, a, b);
    expect(actual, -1);
  });
  test('test sort on courseCount ascending 0', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 1);
    AdminPlannerData b = AdminPlannerData(courseCount: 1);
    var actual = AdminPlannerData.sortOnCourseCount(true, a, b);
    expect(actual, 0);
  });
  test('test sort on courseCount ascending 1', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 2);
    AdminPlannerData b = AdminPlannerData(courseCount: 1);
    var actual = AdminPlannerData.sortOnCourseCount(true, a, b);
    expect(actual, 1);
  });

  test('test sort on courseCount descending -1', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 1);
    AdminPlannerData b = AdminPlannerData(courseCount: 2);
    var actual = AdminPlannerData.sortOnCourseCount(false, a, b);
    expect(actual, 1);
  });
  test('test sort on courseCount descending 0', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 1);
    AdminPlannerData b = AdminPlannerData(courseCount: 1);
    var actual = AdminPlannerData.sortOnCourseCount(false, a, b);
    expect(actual, 0);
  });
  test('test sort on courseCount descending 1', () {
    AdminPlannerData a = AdminPlannerData(courseCount: 2);
    AdminPlannerData b = AdminPlannerData(courseCount: 1);
    var actual = AdminPlannerData.sortOnCourseCount(false, a, b);
    expect(actual, -1);
  });

  test('test sort on modifiedDate ascending -1', () {
    var actual = AdminPlannerData.sortOnDate(true, 1, 2);
    expect(actual, -1);
  });
  test('test sort on modifiedDate ascending 0', () {
    var actual = AdminPlannerData.sortOnDate(true, 1, 1);
    expect(actual, 0);
  });
  test('test sort on modifiedDate ascending 1', () {
    var actual = AdminPlannerData.sortOnDate(true, 2, 1);
    expect(actual, 1);
  });

  test('test sort on modifiedDate descending -1', () {
    var actual = AdminPlannerData.sortOnDate(false, 1, 2);
    expect(actual, 1);
  });
  test('test sort on modifiedDate descending 0', () {
    var actual = AdminPlannerData.sortOnDate(false, 1, 1);
    expect(actual, 0);
  });
  test('test sort on modifiedDate descending 1', () {
    var actual = AdminPlannerData.sortOnDate(false, 2, 1);
    expect(actual, -1);
  });
}
