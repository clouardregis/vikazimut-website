library;

// test with: flutter test --platform chrome
import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/animation/animation_handler.dart';

void main() {
  test('Test get leg from current time', () {
    List<int> timeBarriers = [0, 100, 200, 300, 400];
    var actual = AnimationHandler.getLegFromCurrentTime(110, timeBarriers);
    expect(actual, 2);
  });
}




