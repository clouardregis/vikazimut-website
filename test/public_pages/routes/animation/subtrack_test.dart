import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/routes/animation/subtrack.dart';

void main() {
  test('Test punched location when start point', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(10),
      const PunchTime(20),
      const PunchTime(30),
      const PunchTime(40),
    ];
    var actual = SubTrack.isPunchLocation(0, punchTimes, 10);
    expect(actual, true);
  });

  test('Test punched location when point not punched', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(10),
      const PunchTime(-1),
      const PunchTime(30),
      const PunchTime(40),
    ];
    var actual = SubTrack.isPunchLocation(20, punchTimes, 5);
    expect(actual, false);
  });

  test('Test punched location when true at last point', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(10),
      const PunchTime(20),
      const PunchTime(30),
      const PunchTime(40),
    ];
    var actual = SubTrack.isPunchLocation(40, punchTimes, 5);
    expect(actual, true);
  });

  test('Test punched location when between two checkpoints', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(10),
      const PunchTime(20),
      const PunchTime(30),
      const PunchTime(40),
    ];
    var actual = SubTrack.isPunchLocation(25, punchTimes, 2);
    expect(actual, false);
  });

  test('Get sublist when empty', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.getSubList_(route, 0, 0);
    expect(actual, []);
  });

  test('Get sublist when all points', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.getSubList_(route, 3982, 2);
    expect(actual.length, 3);
  });

  test('Get sublist even when index > length', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.getSubList_(route, 0, 10);
    expect(actual.length, 0);
  });

  test('Get sublist when all legs', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.getSubList_(route, 8007, 2);
    expect(actual.length, route.length);
  });

  test('Test find current GPS point when start', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.findCurrentGPSPoint_(route, 0);
    expect(actual, 0);
  });

  test('Test find current GPS point when before GPS time', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.findCurrentGPSPoint_(route, 3981);
    expect(actual, 1);
  });

  test('Test find current GPS point when equals GPS time', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.findCurrentGPSPoint_(route, 3982);
    expect(actual, 1);
  });

  test('Test find current GPS point when after GPS time', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.findCurrentGPSPoint_(route, 3983);
    expect(actual, 2);
  });

  test('Test find current GPS point when nd', () {
    List<GeodesicPoint> route = _getFakeRoute();
    var actual = SubTrack.findCurrentGPSPoint_(route, 8007);
    expect(actual, 2);
  });
}

List<GeodesicPoint> _getFakeRoute() {
  return [
    GeodesicPoint(49.278524, -0.425337, 02005, 90.5),
    GeodesicPoint(49.278511, -0.425321, 03982, 89.9),
    GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
  ];
}
