import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/time/time_column_helper.dart';

void main() async {
  test('Test computeSpeed', () {
    var actual = TimeColumnHelper.calculateSpeedInKmh(1000, 1);
    expect(actual, 3.6);
  });

  test('Test computePaceInMillisecondPerKm', () {
    var actual = TimeColumnHelper.calculatePaceInMillisecondPerKm(1000, 1);
    expect(actual, 1000 * 1000 / 1);
  });

  test('Test distanceDataToString', () {
    var actual = TimeColumnHelper.distanceDataToString(20, 10, 30);
    expect(actual,  '20 (+30)\n+100%');
  });
}
