import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/time/csv_converter.dart';

void main() async {
  test('Test computeLegStraightDistances', () {
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 0, 0, ""),
      const Checkpoint(1, "1", 1, 0, ""),
      const Checkpoint(2, "2", 1, 1, ""),
      const Checkpoint(2, "2", 0, 1, ""),
    ];

    List<int> actual = CsvConverter.computeLegStraightDistances(checkpoints);
    expect(actual.length, 4);
    expect(actual[0], 0);
    expect(actual[1], 111319);
    expect(actual[2], 111302);
    expect(actual[3], 111319);
  });
}
