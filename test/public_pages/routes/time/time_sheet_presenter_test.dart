import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/time/time_sheet_presenter.dart';

void main() async {
  test('Test computeLegDistances', () {
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 0, 0, "Control"),
      const Checkpoint(1, "1", 1, 1, "Control"),
      const Checkpoint(2, "2", 2, 2, "Control"),
      const Checkpoint(3, "3", 3, 3, "Control"),
    ];
    TimeSheetPresenter presenter = TimeSheetPresenter();
    List<double> distances = presenter.computeLegStraightDistances(checkpoints);
    List<int> actual = List.filled(distances.length, 0);
    for (int i = 0; i <distances.length; i++) {
      actual[i] = distances[i].floor();
    }
    List<int> expected = [0, 157425, 157401, 157353];
    expect(actual, expected);
  });

  test('Test computeLegDistances with only 1 point', () {
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 0, 0, "Control"),
    ];
    TimeSheetPresenter presenter = TimeSheetPresenter();
    List<double> actual = presenter.computeLegStraightDistances(checkpoints);
    List<double> expected = [0];
    expect(actual, expected);
  });
}
