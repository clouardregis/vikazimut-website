import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/cheater_helper.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/time_cell.dart';

void main() async {
  test('Test isGreaterThanOneMinuteForAllFourPunchTimes false', () {
    var actual = CheaterHelper.isGreaterThanOneMinuteForAllFourPunchTimes_(0, 1, 0, 1, 1 * 60 * 1000);
    expect(actual, false);
  });

  test('Test isGreaterThanOneMinuteForAllFourPunchTimes true', () {
    var actual = CheaterHelper.isGreaterThanOneMinuteForAllFourPunchTimes_(0, 60 * 1000 + 1, 0, 1, 1 * 60 * 1000);
    expect(actual, true);
  });

  test('Test isGreaterThanOneMinuteForAllFourPunchTimes true', () {
    var actual = CheaterHelper.isGreaterThanOneMinuteForAllFourPunchTimes_(0, 1, 0, 60 * 1000 + 1, 5 * 1000);
    expect(actual, true);
  });

  test('Test isLessThanFiveSecondBetweenFourSuccessivePunchTimes 1', () {
    var actual = CheaterHelper.isLessThanFiveSecondBetweenFourSuccessivePunchTimes_(0, 1, 2, 3, 0, 1, 2, 3, 5 * 1000);
    expect(actual, true);
  });

  test('Test isLessThanFiveSecondBetweenFourSuccessivePunchTimes 2', () {
    var actual = CheaterHelper.isLessThanFiveSecondBetweenFourSuccessivePunchTimes_(0, 1, 2, 3, 0, 1, 2, 5003, 5 * 1000);
    expect(actual, false);
  });

  test('Test isLessThanFiveSecondBetweenFourSuccessivePunchTimes 3', () {
    var actual = CheaterHelper.isLessThanFiveSecondBetweenFourSuccessivePunchTimes_(0, 1, 5002, 3, 0, 1, 2, 3, 5 * 1000);
    expect(actual, false);
  });

  test('Test findCheater when  false', () {
    List<List<TimeCell>> timeCells = [
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(1, 1), TimeCell()..setTimes(2, 1), TimeCell()..setTimes(6, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(1, 1), TimeCell()..setTimes(2, 1), TimeCell()..setTimes(6, 1)],
    ];
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(0, "Jean Moulin", false, 49000, checkpoints1, punchTimes1, ValidationOrder.FREE_ORDER,);
    TrackModel track2 = TrackModel(0, "Charles De Gaulle", false, 65000, checkpoints2, punchTimes2, ValidationOrder.FREE_ORDER);
    List<GpsRoute> selectedTracks = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    CheaterHelper.findCheater(timeCells, checkpoints, selectedTracks);
    expect(timeCells[1][0].isCheater, false);
    expect(timeCells[2][0].isCheater, false);
  });

  test('Test findCheater when  true', () {
    List<List<TimeCell>> timeCells = [
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
    ];
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(0, "Jean Moulin", false, 49000, checkpoints1, punchTimes1, ValidationOrder.FREE_ORDER);
    TrackModel track2 = TrackModel(0, "Charles De Gaulle", false, 65000, checkpoints2, punchTimes2, ValidationOrder.FREE_ORDER);
    List<GpsRoute> selectedTracks = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    CheaterHelper.findCheater(timeCells, checkpoints, selectedTracks);
    expect(timeCells[1][0].isCheater, true);
    expect(timeCells[2][0].isCheater, true);
  });

  test('Test findCheater when some true', () {
    List<List<TimeCell>> timeCells = [
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(30000, 1), TimeCell()..setTimes(40000, 1), TimeCell()..setTimes(60001, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
    ];
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(0, "Jean Moulin", false, 49000, checkpoints1, punchTimes1, ValidationOrder.FREE_ORDER);
    TrackModel track2 = TrackModel(0, "Charles De Gaulle", false, 65000, checkpoints2, punchTimes2, ValidationOrder.FREE_ORDER);
    List<GpsRoute> selectedTracks = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    CheaterHelper.findCheater(timeCells, checkpoints, selectedTracks);
    expect(timeCells[1][0].isCheater, true);
    expect(timeCells[2][0].isCheater, false);
    expect(timeCells[3][0].isCheater, true);
  });

  test('Test findCheater when all true', () {
    List<List<TimeCell>> timeCells = [
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
      [TimeCell(), TimeCell(), TimeCell(), TimeCell(), TimeCell()..setTimes(0, 0), TimeCell()..setTimes(10000, 1), TimeCell()..setTimes(20000, 1), TimeCell()..setTimes(60001, 1)],
    ];
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(0, "Jean Moulin", false, 49000, checkpoints1, punchTimes1, ValidationOrder.FREE_ORDER);
    TrackModel track2 = TrackModel(0, "Charles De Gaulle", false, 65000, checkpoints2, punchTimes2, ValidationOrder.FREE_ORDER);
    List<GpsRoute> selectedTracks = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    CheaterHelper.findCheater(timeCells, checkpoints, selectedTracks);
    expect(timeCells[1][0].isCheater, true);
    expect(timeCells[3][0].isCheater, true);
    expect(timeCells[3][0].isCheater, true);
  });
}
