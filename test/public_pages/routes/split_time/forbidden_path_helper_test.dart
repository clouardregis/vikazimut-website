import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/forbidden_path_helper.dart';

void main() async {
  test('Test isForbiddenPath 1', () {
    List<double> tab = [4, 1, 5];
    int index = 1;
    var actual = ForbiddenPathHelper.minDistance(tab, index);
    expect(actual, 4);
  });

  test('Test isForbiddenPath 2', () {
    List<double> tab = [4, 2, 5];
    int index = 0;
    var actual = ForbiddenPathHelper.minDistance(tab, index);
    expect(actual, 2);
  });
}
