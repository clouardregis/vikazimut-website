import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/leg.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/split_time_sheet_model.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/split_time_sheet_presenter.dart';
import 'package:vikazimut_website/public_pages/routes/split_time/time_cell.dart';

void main() async {
  test('Test calculate actual Distances', () {
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
      legActualDistances: const [6, 10, 12],
    );
    track1.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 19000), Leg(2, punchTime: 30000)];
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
      legActualDistances: const [8, 16, 32],
    );
    track2.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 9000), Leg(2, punchTime: 35000)];
    List<GpsRoute> selectedTracks = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<List<double>> distances = SplitTimeSheetModel.calculateActualDistances(3, selectedTracks);
    var actual = (distances.length == 3 - 1 && distances[0].length == selectedTracks.length);
    expect(actual, true);
  });

  test('Test of build table in free order', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.FREE_ORDER,
      legActualDistances: const [6, 12, 24],
    );
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.FREE_ORDER,
      legActualDistances: const [8, 16, 32],
    );
    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    SplitTimeSheetPresenter splitTimeSheetPresenter = SplitTimeSheetPresenter();
    splitTimeSheetPresenter.buildTable(gpsRoutes, checkpoints, ValidationOrder.FREE_ORDER, 0);
    List<List<TimeCell>> actual = splitTimeSheetPresenter.table;

    expect(actual[1][3].cumulativeTime, 0);
    expect(actual[1][3].legTime, 0);
  });

  test('Test build table in preset order', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
      legActualDistances: const [6, 12, 24],
    );
    track1.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 19000), Leg(2, punchTime: 30000)];
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
      legActualDistances: const [8, 16, 32],
    );
    track2.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 9000), Leg(2, punchTime: 35000)];

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    SplitTimeSheetPresenter splitTimeSheetPresenter = SplitTimeSheetPresenter();
    splitTimeSheetPresenter.buildTable(gpsRoutes, checkpoints, ValidationOrder.PRESET_ORDER, 0);
    List<List<TimeCell>> actual = splitTimeSheetPresenter.table;

    expect(actual[1][3].cumulativeTime, 0);
    expect(actual[1][3].legTime, 0);
    expect(actual[1][4].cumulativeTime, 19000);
  });

  test('Test build table in unsupervised preset order', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    List<GeodesicPoint> checkpoints1 = [GeodesicPoint(46.672474, 0.367607), GeodesicPoint(46.67253, 0.36757), GeodesicPoint(46.672671, 0.367486)];
    List<GeodesicPoint> checkpoints2 = [GeodesicPoint(46.672544, 0.367676), GeodesicPoint(46.672675, 0.367637), GeodesicPoint(46.672784, 0.367585)];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.UNSUPERVISED_PRESET_ORDER,
      legActualDistances: const [6, 12, 24],
    );
    track1.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 19000), Leg(2, punchTime: 30000)];
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.UNSUPERVISED_PRESET_ORDER,
      legActualDistances: const [8, 16, 32],
    );
    track2.legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 9000), Leg(2, punchTime: 35000)];

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];
    SplitTimeSheetPresenter splitTimeSheetPresenter = SplitTimeSheetPresenter();
    splitTimeSheetPresenter.buildTable(gpsRoutes, checkpoints, ValidationOrder.PRESET_ORDER, 0);
    List<List<TimeCell>> actual = splitTimeSheetPresenter.table;
    expect(actual[1][3].cumulativeTime, 0);
    expect(actual[1][3].legTime, 0);
    expect(actual[1][4].cumulativeTime, 19000);
  });

  test('Test computeLegStraightDistances', () {
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 0, 0, ""),
      const Checkpoint(1, "1", 1, 0, ""),
      const Checkpoint(2, "2", 1, 1, ""),
      const Checkpoint(2, "2", 0, 1, ""),
    ];

    List<int> actual = SplitTimeSheetModel.computeLegStraightDistances_(checkpoints);
    expect(actual.length, 4);
    expect(actual[0], 0);
    expect(actual[1], 111319);
    expect(actual[2], 111302);
    expect(actual[3], 111319);
  });
}
