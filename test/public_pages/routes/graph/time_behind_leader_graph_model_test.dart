import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/graph/graph_data.dart';
import 'package:vikazimut_website/public_pages/routes/graph/settings_side_bar.dart';
import 'package:vikazimut_website/public_pages/routes/graph/time_behind_leader_graph_model.dart';

void main() async {
  test('Test build table in preset order', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<GeodesicPoint> checkpoints2 = [
      GeodesicPoint(46.672544, 0.367676),
      GeodesicPoint(46.672675, 0.367637),
      GeodesicPoint(46.672784, 0.367585),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
    );
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
    );

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2,", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    GraphData table = TimeBehindLeaderGraphModel.buildTable(gpsRoutes, checkpoints, GraphType.CUMULATIVE);

    expect(table.legDistance[0], 0);
    expect(table.legDistance[1], 87);
    expect(table.legDistance[2], 49);

    expect(table.data[0][0], 0);
    expect(table.data[0][1], 19000);
    expect(table.data[0][2], 30000);

    expect(table.data[1][0], 0);
    expect(table.data[1][1], 9000);
    expect(table.data[1][2], 35000);
  });

  test('Test method calculateTimeDiff when cumulative', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<GeodesicPoint> checkpoints2 = [
      GeodesicPoint(46.672544, 0.367676),
      GeodesicPoint(46.672675, 0.367637),
      GeodesicPoint(46.672784, 0.367585),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
    );
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
    );

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    GraphData table = TimeBehindLeaderGraphModel.buildTable(gpsRoutes, checkpoints, GraphType.CUMULATIVE);
    int actual = TimeBehindLeaderGraphModel.calculateMaxTimeDifference(table);
    expect(actual, 10000);
  });

  test('Test method calculateTimeDiff when leg', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<GeodesicPoint> checkpoints2 = [
      GeodesicPoint(46.672544, 0.367676),
      GeodesicPoint(46.672675, 0.367637),
      GeodesicPoint(46.672784, 0.367585),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
    );
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
    );

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    GraphData table = TimeBehindLeaderGraphModel.buildTable(gpsRoutes, checkpoints, GraphType.LEG);
    int actual = TimeBehindLeaderGraphModel.calculateMaxTimeDifference(table);
    expect(actual, 15000);
  });

  test('Test method getTotalDistance', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<GeodesicPoint> checkpoints2 = [
      GeodesicPoint(46.672544, 0.367676),
      GeodesicPoint(46.672675, 0.367637),
      GeodesicPoint(46.672784, 0.367585),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(
      0,
      "Jean",
      false,
      49000,
      checkpoints1,
      punchTimes1,
      ValidationOrder.PRESET_ORDER,
    );
    TrackModel track2 = TrackModel(
      0,
      "Charles De Gaulle",
      false,
      65000,
      checkpoints2,
      punchTimes2,
      ValidationOrder.PRESET_ORDER,
    );

    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    GraphData table = TimeBehindLeaderGraphModel.buildTable(gpsRoutes, checkpoints, GraphType.CUMULATIVE);

    expect(TimeBehindLeaderGraphModel.getTotalDistance(table), 136);
  });

  test('Test method indexOfLeaderInTheColumn', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<GeodesicPoint> checkpoints2 = [
      GeodesicPoint(46.672544, 0.367676),
      GeodesicPoint(46.672675, 0.367637),
      GeodesicPoint(46.672784, 0.367585),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    List<PunchTime> punchTimes2 = [const PunchTime(0), const PunchTime(9000), const PunchTime(35000)];
    TrackModel track1 = TrackModel(0, "Jean", false, 49000, checkpoints1, punchTimes1, ValidationOrder.PRESET_ORDER);
    TrackModel track2 = TrackModel(0, "Charles De Gaulle", false, 65000, checkpoints2, punchTimes2, ValidationOrder.PRESET_ORDER);
    List<GpsRoute> gpsRoutes = [GpsRoute(0, track1), GpsRoute(1, track2)];
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 46.67244157716873, 0.3677583234146477, "Control"),
      const Checkpoint(1, "1", 46.673197262305344, 0.36744944208704905, "Control"),
      const Checkpoint(2, "2", 46.67364596148423, 0.36743222275931514, "Control"),
    ];

    GraphData table = TimeBehindLeaderGraphModel.buildTable(gpsRoutes, checkpoints, GraphType.CUMULATIVE);

    expect(TimeBehindLeaderGraphModel.indexOfLeaderInTheColumn(table.data, 1), 1);
  });

  test('Test method calculateGraphWidthFromCPCount with 1 checkpoint', () {
    var width = TimeBehindLeaderGraphModel.calculateGraphWidthFromCheckpointCount(1);
    expect(width, 570);
  });

  test('Test method calculateGraphWidthFromCPCount with 100 checkpoints', () {
    var width = TimeBehindLeaderGraphModel.calculateGraphWidthFromCheckpointCount(100);
    expect(width, 100 * TimeBehindLeaderGraphModel.MIN_WIDTH_BETWEEN_CHECKPOINT);
  });

  test('Test method containMissingControls false', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(19000), const PunchTime(30000)];
    TrackModel track1 = TrackModel(0, "Jean", false, 49000, checkpoints1, punchTimes1, ValidationOrder.PRESET_ORDER);
    var gpsRoute = GpsRoute(0, track1);
    var actual = TimeBehindLeaderGraphModel.containMissingControls_(gpsRoute);
    expect(actual, false);
  });

  test('Test method containMissingControls true', () {
    List<GeodesicPoint> checkpoints1 = [
      GeodesicPoint(46.672474, 0.367607),
      GeodesicPoint(46.67253, 0.36757),
      GeodesicPoint(46.672671, 0.367486),
    ];
    List<PunchTime> punchTimes1 = [const PunchTime(0), const PunchTime(-1), const PunchTime(30000)];
    TrackModel track1 = TrackModel(0, "Jean", false, 49000, checkpoints1, punchTimes1, ValidationOrder.PRESET_ORDER);
    var gpsRoute = GpsRoute(0, track1);
    var actual = TimeBehindLeaderGraphModel.containMissingControls_(gpsRoute);
    expect(actual, true);
  });

  test('Test method getTotalDistance when zero', () {
    var table = GraphData(10, 20);
    var actual = TimeBehindLeaderGraphModel.getTotalDistance(table);
    expect(actual, 0);
  });

  test('Test method getTotalDistance when 1 to 10', () {
    var table = GraphData(10, 20);
    // THe first distance = 0
    for (int i = 0; i < table.legDistance.length; i++) {
      table.legDistance[i] = i;
    }
    var actual = TimeBehindLeaderGraphModel.getTotalDistance(table);
    expect(actual, 9 * 10 / 2);
  });

  test('Test method indexOfLeaderInTheColumn ', () {
    List<List<int>> timeCell = [
      [2],
      [1],
      [4]
    ];
    var actual = TimeBehindLeaderGraphModel.indexOfLeaderInTheColumn(timeCell, 0);
    expect(actual, 1);
  });

  test('Test computeLegStraightDistances', () {
    List<Checkpoint> checkpoints = [
      const Checkpoint(0, "0", 0, 0, ""),
      const Checkpoint(1, "1", 1, 0, ""),
      const Checkpoint(2, "2", 1, 1, ""),
      const Checkpoint(2, "2", 0, 1, ""),
    ];

    List<int> actual = TimeBehindLeaderGraphModel.computeLegStraightDistances_(checkpoints);
    expect(actual.length, 4);
    expect(actual[0], 0);
    expect(actual[1], 111319);
    expect(actual[2], 111302);
    expect(actual[3], 111319);
  });
}
