import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/profile/speed_histogram.dart';

void main() {
  const int MAX_INT = 13148730000;
  test('Test computeHistogramBins with no waypoint', () {
    List<GeodesicPoint> waypoints = List.filled(5, GeodesicPoint(0, 0, 0, 0));
    List<int> speedHistogram = List.filled(5, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 3, 20, MAX_INT, 1);
    expect(totalTime, 0);
  });

  test('Test computeHistogramBins for constant speed and no elevation values', () {
    // Add 3 fake waypoints at the beginning and at the end (due the low pass filter border)
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0), // Not used
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.00001, 1000, 0),
      GeodesicPoint(0.00002, 0.00002, 2000, 0),
      GeodesicPoint(0.00003, 0.00003, 3000, 0),
      GeodesicPoint(0.00004, 0.00004, 4000, 0),
      GeodesicPoint(0.00005, 0.00005, 5000, 0),
      GeodesicPoint(0.00006, 0.00006, 6000, 0),
      GeodesicPoint(0.00007, 0.00007, 7000, 0),
      GeodesicPoint(0.00008, 0.00008, 8000, 0),
      GeodesicPoint(0.00009, 0.00009, 9000, 0),
      GeodesicPoint(0.00010, 0.00010, 10000, 0),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 1);
    expect(totalTime, 7000);
    expect(speedHistogram[0], 0);
    expect(speedHistogram[5], 7000);
  });

  test('Test computeHistogramBins with continuous acceleration (distance) and no elevation', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.000001, 0.000001, 1000, 0),
      GeodesicPoint(0.000002, 0.000002, 2000, 0),
      GeodesicPoint(0.000004, 0.000004, 3000, 0),
      GeodesicPoint(0.000007, 0.000007, 4000, 0),
      GeodesicPoint(0.000011, 0.000011, 5000, 0),
      GeodesicPoint(0.000016, 0.000016, 6000, 0),
      GeodesicPoint(0.000022, 0.000022, 7000, 0),
      GeodesicPoint(0.000029, 0.000029, 8000, 0),
      GeodesicPoint(0.000037, 0.000037, 9000, 0),
      GeodesicPoint(0.000046, 0.000046, 10000, 0),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 0);
    expect(totalTime, 7000);
    expect(speedHistogram[0], 0);
    expect(speedHistogram[1], 2000);
    expect(speedHistogram[2], 2000);
    expect(speedHistogram[3], 2000);
    expect(speedHistogram[4], 1000);
  });

  test('Test computeHistogramBins with continuous acceleration (time) and no elevation', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.00001, 100),
      GeodesicPoint(0.00002, 0.00002, 1000),
      GeodesicPoint(0.00003, 0.00003, 1800),
      GeodesicPoint(0.00004, 0.00004, 2500),
      GeodesicPoint(0.00005, 0.00005, 3100),
      GeodesicPoint(0.00006, 0.00006, 3600),
      GeodesicPoint(0.00007, 0.00007, 4000),
      GeodesicPoint(0.00008, 0.00008, 4300),
      GeodesicPoint(0.00009, 0.00009, 4500),
      GeodesicPoint(0.00010, 0.00010, 4600),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 0);
    expect(totalTime, 3500);
    expect(speedHistogram[0], 0);
    expect(speedHistogram[7], 800);
    expect(speedHistogram[8], 700);
    expect(speedHistogram[9], 600);
    expect(speedHistogram[11], 500);
    expect(speedHistogram[14], 400);
    expect(speedHistogram[18], 300);
    expect(speedHistogram[20], 200);
  });

  test('Test computeHistogramBins with one peak and no elevation', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0), // Not used
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.00001, 1000),
      GeodesicPoint(0.00002, 0.00002, 2000),
      GeodesicPoint(0.00003, 0.00003, 3000),
      GeodesicPoint(0.00004, 0.00004, 4000),
      GeodesicPoint(0.00008, 0.00008, 5000),
      GeodesicPoint(0.00004, 0.00004, 6000),
      GeodesicPoint(0.00003, 0.00003, 7000),
      GeodesicPoint(0.00002, 0.00002, 8000),
      GeodesicPoint(0.00001, 0.00001, 9000),
      GeodesicPoint(0.00000, 0.00000, 10000),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 0);
    expect(totalTime, 8000);
    expect(speedHistogram[5], 4000);
    expect(speedHistogram[11], 2000);
    expect(speedHistogram[17], 2000);
  });

  test('Test computeHistogramBins with one pit and no elevation', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0), // Not used
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00002, 0.00002, 1000),
      GeodesicPoint(0.00004, 0.00004, 2000),
      GeodesicPoint(0.00006, 0.00006, 3000),
      GeodesicPoint(0.00008, 0.00008, 4000),
      GeodesicPoint(0.00009, 0.00009, 5000),
      GeodesicPoint(0.00008, 0.00008, 6000),
      GeodesicPoint(0.00006, 0.00006, 7000),
      GeodesicPoint(0.00004, 0.00004, 8000),
      GeodesicPoint(0.00002, 0.00002, 9000),
      GeodesicPoint(0.00000, 0.00000, 10000),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 0);
    expect(totalTime, 8000);
    expect(speedHistogram[7], 2000);
    expect(speedHistogram[9], 2000);
    expect(speedHistogram[11], 4000);
  });

  test('Test computeHistogramBins for constant speed and elevation values', () {
    // Add 3 fake waypoints at the beginning and at the end (due the low pass filter border)
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0, 0), // Not used
      GeodesicPoint(0, 0, 0, 1),
      GeodesicPoint(0.00001, 0.00001, 1000, 2),
      GeodesicPoint(0.00002, 0.00002, 2000, 3),
      GeodesicPoint(0.00003, 0.00003, 3000, 4),
      GeodesicPoint(0.00004, 0.00004, 4000, 5),
      GeodesicPoint(0.00005, 0.00005, 5000, 6),
      GeodesicPoint(0.00006, 0.00006, 6000, 7),
      GeodesicPoint(0.00007, 0.00007, 7000, 8),
      GeodesicPoint(0.00008, 0.00008, 8000, 9),
      GeodesicPoint(0.00009, 0.00009, 9000, 10),
      GeodesicPoint(0.00010, 0.00010, 10000, 11),
    ];
    List<int> speedHistogram = List.filled(21, 0);
    int totalTime = SpeedHistogram.computeHistogramBins_(waypoints, speedHistogram, 1, 20, MAX_INT, 1);
    expect(totalTime, 7000);
    expect(speedHistogram[0], 0);
    expect(speedHistogram[13], 7000);
  });
}
