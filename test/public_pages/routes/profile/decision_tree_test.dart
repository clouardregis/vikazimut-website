import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/profile/decision_tree/decision_tree.dart';
import 'package:vikazimut_website/public_pages/routes/profile/speed_in_kmh.dart';

void main() {
  test('Test computePercentOfZeroSpeeds case 0', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 0),
      SpeedInKmH(1, 0),
      SpeedInKmH(2, 0),
      SpeedInKmH(3, 100),
    ];
    double value = computePercentOfStoppingPeriod(data);
    expect(value, 0);
  });

  test('Test computePercentOfZeroSpeeds case nominal', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 10),
      SpeedInKmH(1, 20),
      SpeedInKmH(2, 30),
      SpeedInKmH(3, 40),
    ];
    double value = computePercentOfStoppingPeriod(data);
    expect(value, 60);
  });

  test('Test computeMaximumPeak when nominal', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 30),
      SpeedInKmH(1, 18),
      SpeedInKmH(2, 19),
      SpeedInKmH(3, 0),
      SpeedInKmH(4, 20),
      SpeedInKmH(5, 20),
      SpeedInKmH(6, 21),
      SpeedInKmH(7, 13),
      SpeedInKmH(8, 14),
      SpeedInKmH(9, 20),
      SpeedInKmH(10, 20),
      SpeedInKmH(11, 10),
      SpeedInKmH(12, 7),
      SpeedInKmH(13, 7),
      SpeedInKmH(14, 7),
    ];
    int value = computeMaximumPeak(data);
    expect(value, 6);
  });

  test('Test computeMinimumCumulatedSpeed', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 1),
      SpeedInKmH(1, 5),
      SpeedInKmH(2, 2),
      SpeedInKmH(3, 2),
      SpeedInKmH(4, 2),
      SpeedInKmH(5, 7),
    ];
    int value = computeMinimumCumulatedSpeed(data, 7);
    expect(value, 2);
  });

  test('Test computeMaximumCumulatedSpeed', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 0),
      SpeedInKmH(1, 1),
      SpeedInKmH(2, 10),
      SpeedInKmH(3, 1),
      SpeedInKmH(4, 5),
      SpeedInKmH(5, 1),
    ];
    int value = computeMaximumCumulatedSpeed(data, 7);
    expect(value, 3);
  });

  test('Test computerHistogramWidth', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(3, 7),
      SpeedInKmH(4, 7),
      SpeedInKmH(5, 7),
      SpeedInKmH(6, 7),
      SpeedInKmH(7, 7),
      SpeedInKmH(8, 7),
      SpeedInKmH(9, 0),
      SpeedInKmH(10, 0),
      SpeedInKmH(11, 7),
      SpeedInKmH(12, 7),
      SpeedInKmH(13, 7),
    ];
    int value = computeHistogramWidth(data, 7);
    expect(value, 7);
  });

  test('Test computeMeanSpeed simple case', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(14, 33),
      SpeedInKmH(15, 34),
      SpeedInKmH(16, 33),
    ];
    double value = computeMeanSpeed(data);
    expect(value, 15);
  });

  test('Test computeMeanSpeed toy case', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(14, 25),
      SpeedInKmH(15, 50),
      SpeedInKmH(16, 25),
    ];
    double value = computeMeanSpeed(data);
    expect(value, 15);
  });

  test('Test computeStandardDeviation toy case', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(2, 5),
      SpeedInKmH(3, 10),
      SpeedInKmH(4, 15),
      SpeedInKmH(5, 35),
      SpeedInKmH(6, 20),
      SpeedInKmH(7, 15),
    ];
    double mean = computeMeanSpeed(data);
    expect(mean, 5.0);
    double stdev = computeStandardDeviation(data, mean);
    expect(stdev, moreOrLessEquals(1.34, epsilon: 0.01));
  });

  test('Test computeStandardDeviation toy case', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 10),
      SpeedInKmH(1, 14),
      SpeedInKmH(2, 19),
      SpeedInKmH(3, 29),
      SpeedInKmH(4, 23),
      SpeedInKmH(5, 5),
    ];
    double mean = computeMeanSpeed(data);
    expect(mean, 2.56);
    double stdev = computeStandardDeviation(data, mean);
    expect(stdev, moreOrLessEquals(1.365, epsilon: 0.01));
  });

  test('Test lastBinGreaterThan when no one', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 1),
      SpeedInKmH(1, 3),
      SpeedInKmH(2, 5),
    ];
    int speed = lastBinGreaterThan(data, 5);
    expect(speed, 0);
  });

  test('Test lastBinGreaterThan when one', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 1),
      SpeedInKmH(1, 3),
      SpeedInKmH(2, 5),
      SpeedInKmH(3, 6),
      SpeedInKmH(4, 5),
    ];
    int speed = lastBinGreaterThan(data, 5);
    expect(speed, 3);
  });

  test('Test lastBinGreaterThan when one', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 1),
      SpeedInKmH(1, 3),
      SpeedInKmH(2, 5),
      SpeedInKmH(3, 6),
      SpeedInKmH(4, 6),
      SpeedInKmH(5, 6),
      SpeedInKmH(6, 5),
    ];
    int speed = lastBinGreaterThan(data, 5);
    expect(speed, (5));
  });

  test('Test empty when true', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 0),
      SpeedInKmH(1, 0),
      SpeedInKmH(2, 0),
      SpeedInKmH(3, 0),
      SpeedInKmH(4, 0),
      SpeedInKmH(5, 0),
      SpeedInKmH(6, 0),
    ];
    bool actual = isNotEnoughData(data);
    expect(actual, true);
  });

  test('Test empty when false', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 0),
      SpeedInKmH(1, 0),
      SpeedInKmH(2, 0),
      SpeedInKmH(3, 1),
      SpeedInKmH(4, 0),
      SpeedInKmH(5, 0),
      SpeedInKmH(6, 0),
    ];
    bool actual = isNotEnoughData(data);
    expect(actual, false);
  });

  test('Test computeMinSpeedGreaterThan7Percent', () {
    const List<SpeedInKmH> data = [
      SpeedInKmH(0, 10),
      SpeedInKmH(1, 10),
      SpeedInKmH(2, 0),
      SpeedInKmH(3, 2),
      SpeedInKmH(4, 2),
      SpeedInKmH(5, 7),
    ];
    int value = computeMinSpeedGreaterThan(data, 7);
    expect(value, 5);
  });
}
