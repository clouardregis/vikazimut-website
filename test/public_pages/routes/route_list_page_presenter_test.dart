import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

void main() {
  test('Test compare tracks case PRESET & time > ', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);

    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.comparePresetOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test compare tracks case PRESET & times < ', () {
    var model1 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);

    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.comparePresetOrderTracks_(track1, track2);
    expect(actual, -1);
  });

  test('Test compare tracks case PRESET & times = ', () {
    var model1 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);

    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.comparePresetOrderTracks_(track1, track2);
    expect(actual, 0);
  });

  test('Test compare tracks case equals PRESET & MP <', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 5, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.comparePresetOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test compare tracks case equals PRESET & MP >', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 5, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.comparePresetOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test compare tracks case FREE equals', () {
    var model1 = TrackModel(0, "nickname", false, 10, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 10, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 1);

    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, 0);
  });

  test('Test compare tracks case FREE & score <', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 5, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test compare tracks case FREE & score >', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 5, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, -1);
  });

  test('Test compare tracks case FREE & runs <', () {
    var model1 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    var model2 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test compare tracks case FREE & runs >', () {
    var model1 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 1);
    var model2 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, -1);
  });

  test('Test compare tracks case FREE & times <', () {
    var model1 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    var model2 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, -1);
  });

  test('Test compare tracks case FREE & times >', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    var model2 = TrackModel(0, "nickname", false, 1, [], [], ValidationOrder.FREE_ORDER, score: 10, runCount: 2);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    var actual = RouteListPagePresenter.compareFreeOrderTracks_(track1, track2);
    expect(actual, 1);
  });

  test('Test selected tracks without Superman when null', () {
    List<GpsRoute> tracks = [];
    var actual = RouteListPagePresenter.selectedTracksWithoutSuperman_(tracks);
    expect(0, actual.length);
  });

  test('Test selected tracks without Superman with superman', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 5, runCount: 1);
    var model2 = TrackModel(1, "nickname", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(1, model2);
    List<GpsRoute> tracks = [track1, track2];
    var actual = RouteListPagePresenter.selectedTracksWithoutSuperman_(tracks);
    expect(2, actual.length);
  });

  test('Test selected tracks without Superman without superman', () {
    var model1 = TrackModel(0, "nickname", false, 2, [], [], ValidationOrder.PRESET_ORDER, score: 5, runCount: 1);
    var model2 = TrackModel(1, "Superman", false, 1, [], [], ValidationOrder.PRESET_ORDER, score: 10, runCount: 1);
    model2.isCompleted = false;
    GpsRoute track1 = GpsRoute(0, model1);
    GpsRoute track2 = GpsRoute(-1000, model2);
    List<GpsRoute> tracks = [track1, track2];
    var actual = RouteListPagePresenter.selectedTracksWithoutSuperman_(tracks);
    expect(1, actual.length);
  });
}
