import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/douglas_peucker_algorithm.dart';

void main() {
  test('Test perpendicularDistanceToLine when on the line', () {
    GeodesicPoint startLine = GeodesicPoint(0, 0);
    GeodesicPoint endLine = GeodesicPoint(2, 2);
    GeodesicPoint point = GeodesicPoint(1, 1);
    var actual = DouglasPeuckerAlgorithm.perpendicularDistanceToLine_(point, startLine, endLine);
    expect(0, actual);
  });

  test('Test perpendicularDistanceToLine when above the line', () {
    GeodesicPoint startLine = GeodesicPoint(0, 0);
    GeodesicPoint endLine = GeodesicPoint(0, 2);
    GeodesicPoint point = GeodesicPoint(1, 1);
    var actual = DouglasPeuckerAlgorithm.perpendicularDistanceToLine_(point, startLine, endLine);
    expect(1, actual);
  });

  test('Test perpendicularDistanceToLine when below the line', () {
    GeodesicPoint startLine = GeodesicPoint(0, 0);
    GeodesicPoint endLine = GeodesicPoint(2, 0);
    GeodesicPoint point = GeodesicPoint(1, 1);
    var actual = DouglasPeuckerAlgorithm.perpendicularDistanceToLine_(point, startLine, endLine);
    expect(1, actual);
  });
}
