import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/analysis_report.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/area_score.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/performance_processor.dart';

void main() {
  test('Test sortCheckpointsByPunchTimes when preset order', () {
    const List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 0, 0, "Start"),
      Checkpoint(1, "1", 0, 1, "control"),
      Checkpoint(2, "2", 1, 1, "control"),
      Checkpoint(3, "3", 1, 1, "Finish"),
    ];
    const List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(1),
      PunchTime(2),
      PunchTime(3),
    ];

    final actualCheckpoints = PerformanceProcessor.sortCheckpointsByPunchTimes_(checkpoints, punchTimes);
    expect(actualCheckpoints.length, 4);
    expect(actualCheckpoints[0].id, "0");
    expect(actualCheckpoints[1].id, "1");
    expect(actualCheckpoints[2].id, "2");
    expect(actualCheckpoints[3].id, "3");
  });

  test('Test sortCheckpointsByPunchTimes when free order', () {
    const List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 0, 0, "control"),
      Checkpoint(1, "1", 0, 1, "control"),
      Checkpoint(2, "2", 1, 1, "control"),
      Checkpoint(3, "3", 2, 1, "control"),
      Checkpoint(4, "4", 2, 1, "finish"),
    ];
    const List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(4),
      PunchTime(2),
      PunchTime(1),
      PunchTime(5),
    ];

    final actualCheckpoints = PerformanceProcessor.sortCheckpointsByPunchTimes_(checkpoints, punchTimes);
    expect(actualCheckpoints.length, 5);
    expect(actualCheckpoints[0].id, "0");
    expect(actualCheckpoints[1].id, "3");
    expect(actualCheckpoints[2].id, "2");
    expect(actualCheckpoints[3].id, "1");
    expect(actualCheckpoints[4].id, "4");
  });

  test('Test calculateGlobalScores', () {
    List<AreaScore> legPerformanceScores = [
      AreaScore(
        controlExitReport: AnalysisReport(score: 0),
        handrailReport: AnalysisReport(score: 5),
        controlAttackReport: AnalysisReport(score: 10),
      ),
      AreaScore(
        controlExitReport: AnalysisReport(score: 10),
        handrailReport: AnalysisReport(score: 5),
        controlAttackReport: AnalysisReport(score: 00),
      ),
    ];

    final globalScores = PerformanceProcessor.calculateGlobalScores_(legPerformanceScores);
    expect(globalScores.controlExitReport.score, 5);
    expect(globalScores.handrailReport.score, 5);
    expect(globalScores.controlAttackReport.score, 5);
  });
}
