import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/vector.dart';

void main() {
  void expectArray(List<double> l1, List<double> l2) {
    expect(l1.length, l2.length);
    for (int i = 0; i < l1.length; i++) {
      expect(l1[i], l2[i]);
    }
  }

  test('Test determinant when collinear', () {
    Vector2 v1 = Vector2(-6, 10);
    Vector2 v2 = Vector2(9, -15);
    final double determinant = Vector2.determinant(v1, v2);
    expect(determinant, 0);
  });

  test('Test determinant when not collinear', () {
    Vector2 v1 = Vector2(4, 9);
    Vector2 v2 = Vector2(11, 23);
    final double determinant = Vector2.determinant(v1, v2);
    expect(determinant, -7);
  });

  test('Test dot product', () {
    Vector2 v1 = Vector2(7, 2);
    Vector2 v2 = Vector2(3, 6);
    final double determinant = v1.dot(v2);
    expect(determinant, 33);
  });

  test('Test computeAnglesBetweenSegments', () {
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(0, 1)), 0);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, 1), GeodesicPoint(0, 0), GeodesicPoint(0, -1)), 0);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(1, 0), GeodesicPoint(0, 0), GeodesicPoint(-1, 0)), 0);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(-1, 0), GeodesicPoint(0, 0), GeodesicPoint(1, 0)), 0);

    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(1, 0)), 90);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(-1, 0)), -90);

    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(1, 1)), 45);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(-1, 1)), -45);

    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(1, -1)), 135);
    expect(Vector2.computeAngleBetween2SegmentsInDegree_(GeodesicPoint(0, -1), GeodesicPoint(0, 0), GeodesicPoint(-1, -1)), -135);
  });

  test('Test computeAnglesBetweenSegmentsInDeg', () {
    List<GeodesicPoint> segments = [
      GeodesicPoint(0, 0),
      GeodesicPoint(1, 0),
      GeodesicPoint(1, 1),
      GeodesicPoint(2, 0),
    ];

    List<double> expected = [
      -90,
      135,
    ];
    final actual = Vector2.computeAnglesBetweenSegmentsInDegree(segments);
    expectArray(actual, expected);
  });
}
