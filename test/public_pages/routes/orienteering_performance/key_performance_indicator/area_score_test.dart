import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/analysis_report.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/area_score.dart';

void main() {
  test('Creation of empty AreaScore', () {
    var score = AreaScore(
      controlExitReport: AnalysisReport(),
      handrailReport: AnalysisReport(),
      controlAttackReport: AnalysisReport(),
      control1: Checkpoint(1, "1", 1, 1, "Control"),
      control2: Checkpoint(2, "2", 2, 2, "Control"),
    );
    expect(score.legName(), "1 - 2\n(1) - (2)");
  });

  test('Test computeAverage', () {
    var score = AreaScore(
      controlExitReport: AnalysisReport(score: 5),
      handrailReport: AnalysisReport(score: 0),
      controlAttackReport: AnalysisReport(score: 10),
    );
    expect(score.computeAverage(), 5);
  });
}
