import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/orienteering_performance/key_performance_indicator/keypoint_indicator_processor.dart';

void main() {
  void expectArray(List<double> l1, List<double> l2) {
    expect(l1.length, l2.length);
    for (int i = 0; i < l1.length; i++) {
      expect(l1[i], l2[i]);
    }
  }

  test('Test calculateStopDurationInMs', () {
    List<double> speedsInKmh = [0, 1, 2, 3];
    List<GeodesicPoint> points = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(1, 1, 1000, 10),
      GeodesicPoint(2, 2, 2000, 30),
      GeodesicPoint(3, 3, 3000, 40),
    ];
    final actual = KeypointIndicatorProcessor.calculateStopDurationInMs_(speedsInKmh, points, 2);
    expect(actual, 3000);
  });

  test('Test calculateAccelerationsInKmh2FromFiniteDifferences', () {
    List<double> speedsInKmh = [0, 1, 2, 0, 3, 2, 1];
    List<double> expected = [1, 1, -2, 3, -1, -1];
    List<double> actual = KeypointIndicatorProcessor.calculateAccelerationsInKmh2FromFiniteDifferences_(speedsInKmh);
    expectArray(actual, expected);
  });

  test('Test calculateSpeedVariationScore when score = 10', () {
    List<double> accelerationInKmh2 = [1, 1, 0, 1, 0, 0];
    Map<String, dynamic> log = {};
    double actual = KeypointIndicatorProcessor.calculateSpeedVariationScore_(accelerationInKmh2, 10, log);
    expect(actual, 10);
  });

  test('Test calculateSpeedVariationScore when empty', () {
    List<double> accelerationInKmh2 = [];
    Map<String, dynamic> log = {};
    double actual = KeypointIndicatorProcessor.calculateSpeedVariationScore_(accelerationInKmh2, 10, log);
    expect(actual, 10);
  });

  test('Test calculateSpeedVariationScore when score = 0', () {
    Map<String, dynamic> log = {};
    List<double> accelerationInKmh2 = [1, -2, -2, -2, 2, 0];
    double actual = KeypointIndicatorProcessor.calculateSpeedVariationScore_(accelerationInKmh2, 2, log);
    expect(actual, 0);
  });

  test('Test calculateSpeedVariationScore when score = 8.7', () {
    List<double> accelerationInKmh2 = [1, 1, -3, 3, 2, -2];
    Map<String, dynamic> log = {};
    double actual = KeypointIndicatorProcessor.calculateSpeedVariationScore_(accelerationInKmh2, 10, log);
    expect(actual, closeTo(6.3, 0.1));
  });

  test('Test handrail detectUTurn when loop', () {
    List<GeodesicPoint> decimatedRoute = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(-0.0001, 0.0001, 10000),
      GeodesicPoint(0, 0.0001, 20000),
      GeodesicPoint(-0.0001, 0, 30000),
    ];
    bool actual = KeypointIndicatorProcessor.detectUTurn_(decimatedRoute, 15, 4);
    expect(actual, true);
  });

  test('Test handrail detectUTurn when uTurn', () {
    List<GeodesicPoint> decimatedRoute = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0, 0.0001, 10000),
      GeodesicPoint(-0.0001, 0.0002, 20000),
      GeodesicPoint(-0.0001, 0, 30000),
    ];
    bool actual = KeypointIndicatorProcessor.detectUTurn_(decimatedRoute, 20, 10);
    expect(actual, true);
  });

  test('Test handrail detectUTurn when false', () {
    List<GeodesicPoint> decimatedRoute = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0, 0.0001, 10000),
      GeodesicPoint(-0.0001, 0.0001, 20000),
      GeodesicPoint(-0.0001, 0, 30000),
    ];
    double toleranceInM = 5;
    bool actual = KeypointIndicatorProcessor.detectUTurn_(decimatedRoute, toleranceInM, 4);
    expect(actual, false);
  });

  test('Test handrail detectUTurn when real case with no turn', () {
    List<GeodesicPoint> decimatedRoute = [
      GeodesicPoint(0.286833, 48.412363, 7788000),
      GeodesicPoint(0.286878, 48.412594, 7806000),
      GeodesicPoint(0.28695, 48.412616, 7815000),
      GeodesicPoint(0.286983, 48.41264, 7822000),
      GeodesicPoint(0.287187, 48.413112, 7868000),
      GeodesicPoint(0.287161, 48.413226, 7887000),
      GeodesicPoint(0.287233, 48.413362, 7905000),
      GeodesicPoint(0.287215, 48.413477, 7922000),
    ];
    bool actual = KeypointIndicatorProcessor.detectUTurn_(decimatedRoute, 10, 15);
    expect(actual, false);
  });

  test('Test handrail detectUTurn when real case with turn', () {
    List<GeodesicPoint> decimatedRoute = [
      GeodesicPoint(0.286833, 48.412363, 7788000),
      GeodesicPoint(49.043973, -0.426701, 1940007),
      GeodesicPoint(49.043992, -0.426662, 1941383),
      GeodesicPoint(49.044004, -0.426609, 1943059),
      GeodesicPoint(49.044012, -0.426545, 1944820),
      GeodesicPoint(49.044026, -0.426494, 1946456),
      GeodesicPoint(49.044046, -0.426439, 1948006),
      GeodesicPoint(49.044049, -0.426375, 1950039),
      GeodesicPoint(49.04405, -0.426341, 1951718),
      GeodesicPoint(49.044044, -0.426296, 1953520),
      GeodesicPoint(49.044055, -0.426247, 1955826),
      GeodesicPoint(49.044055, -0.426219, 1957477),
      GeodesicPoint(49.044073, -0.426184, 1959341),
      GeodesicPoint(49.044092, -0.426166, 1961075),
      GeodesicPoint(49.044105, -0.426139, 1963099),
      GeodesicPoint(49.04411, -0.426106, 1964824),
      GeodesicPoint(49.044111, -0.426061, 1967941),
      GeodesicPoint(49.04412, -0.426056, 1972005),
      GeodesicPoint(49.04413, -0.426038, 1973359),
      GeodesicPoint(49.044146, -0.426005, 1975297),
      GeodesicPoint(49.04416, -0.425974, 1977206),
      GeodesicPoint(49.044179, -0.425945, 1978813),
      GeodesicPoint(49.044194, -0.425917, 1981016),
      GeodesicPoint(49.044214, -0.425899, 1982763),
      GeodesicPoint(49.044243, -0.425866, 1985482),
      GeodesicPoint(49.044261, -0.425848, 1987137),
      GeodesicPoint(49.044295, -0.425819, 1990007),
      GeodesicPoint(49.044305, -0.425835, 1992015),
      GeodesicPoint(49.044295, -0.425838, 1996006),
      GeodesicPoint(49.044293, -0.425823, 2003615),
      GeodesicPoint(49.044296, -0.425804, 2006006),
      GeodesicPoint(49.044308, -0.425804, 2012007),
      GeodesicPoint(49.044322, -0.425838, 2018006),
      GeodesicPoint(49.044324, -0.425878, 2020006),
      GeodesicPoint(49.044322, -0.425893, 2022006),
      GeodesicPoint(49.044314, -0.425935, 2024036),
      GeodesicPoint(49.044305, -0.425977, 2026005),
      GeodesicPoint(49.0443, -0.426002, 2027994),
      GeodesicPoint(49.044293, -0.426035, 2029620),
      GeodesicPoint(49.044307, -0.426087, 2031234),
      GeodesicPoint(49.044317, -0.426151, 2032821),
      GeodesicPoint(49.044333, -0.426216, 2034466),
      GeodesicPoint(49.044346, -0.426262, 2036006),
      GeodesicPoint(49.04435, -0.426308, 2037374),
      GeodesicPoint(49.044353, -0.426359, 2039036),
      GeodesicPoint(49.044356, -0.426376, 2042023),
      GeodesicPoint(49.044334, -0.426422, 2043526),
      GeodesicPoint(49.044343, -0.426442, 2045520),
      GeodesicPoint(49.044358, -0.426447, 2046882),
      GeodesicPoint(49.044375, -0.426484, 2048974),
      GeodesicPoint(49.044386, -0.426508, 2051231),
      GeodesicPoint(49.044394, -0.426546, 2052901),
      GeodesicPoint(49.0444, -0.426569, 2054476),
      GeodesicPoint(49.044411, -0.426592, 2056016),
      GeodesicPoint(49.044418, -0.426625, 2058049),
      GeodesicPoint(49.044425, -0.426651, 2059676),
      GeodesicPoint(49.044436, -0.426676, 2061341),
      GeodesicPoint(49.044451, -0.426698, 2063018),
      GeodesicPoint(49.044459, -0.426721, 2064579),
      GeodesicPoint(49.044458, -0.426749, 2066006),
      GeodesicPoint(49.044451, -0.426782, 2067700),
      GeodesicPoint(49.044451, -0.426814, 2070008),
      GeodesicPoint(49.044461, -0.426827, 2072040),
      GeodesicPoint(49.04449, -0.426803, 2073802),
      GeodesicPoint(49.044516, -0.426779, 2075344),
      GeodesicPoint(49.044541, -0.426757, 2076898),
      GeodesicPoint(49.04458, -0.426736, 2079121),
      GeodesicPoint(49.044603, -0.426727, 2080875),
      GeodesicPoint(49.044613, -0.426727, 2082699),
      GeodesicPoint(49.044629, -0.426729, 2084800),
      GeodesicPoint(49.044644, -0.426731, 2086495),
      GeodesicPoint(49.044659, -0.426727, 2088007),
      GeodesicPoint(49.044678, -0.426724, 2090006),
      GeodesicPoint(49.044695, -0.426718, 2091617),
      GeodesicPoint(49.044707, -0.426693, 2094005),
      GeodesicPoint(49.044694, -0.42669, 2096676),
      GeodesicPoint(49.044678, -0.426703, 2112006),
      GeodesicPoint(49.044662, -0.426709, 2113642),
      GeodesicPoint(49.044635, -0.426736, 2116007),
      GeodesicPoint(49.044622, -0.426756, 2117378),
      GeodesicPoint(49.044608, -0.426757, 2118934),
      GeodesicPoint(49.044591, -0.426764, 2120997),
      GeodesicPoint(49.044566, -0.426781, 2124007),
      GeodesicPoint(49.044558, -0.426795, 2125541),
      GeodesicPoint(49.044536, -0.426782, 2126932),
      GeodesicPoint(49.044513, -0.426774, 2129019),
      GeodesicPoint(49.044487, -0.426762, 2131217),
      GeodesicPoint(49.044464, -0.426736, 2133473),
      GeodesicPoint(49.044437, -0.426738, 2136021),
      GeodesicPoint(49.044418, -0.426762, 2138006),
      GeodesicPoint(49.044401, -0.426773, 2140007),
      GeodesicPoint(49.044387, -0.426786, 2141898),
      GeodesicPoint(49.044368, -0.426815, 2143800),
      GeodesicPoint(49.044346, -0.426842, 2145860),
      GeodesicPoint(49.04431771701631, -0.4268611594405594, 2147817),
    ];
    bool actual = KeypointIndicatorProcessor.detectUTurn_(decimatedRoute, 10, 15);
    expect(actual, true);
  });
}
