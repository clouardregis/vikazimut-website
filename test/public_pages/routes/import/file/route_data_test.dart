import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/public_pages/model/checkpoint.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';
import 'package:vikazimut_website/public_pages/routes/import/file/track_file_format.dart';
import 'package:vikazimut_website/public_pages/routes/import/gpx_exception.dart';
import 'package:vikazimut_website/public_pages/routes/import/route_data.dart';
import 'package:vikazimut_website/public_pages/routes/route_list_page_presenter.dart';

void main() {
  test('Test check data when waypoints list is empty', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    int courseId = 1;
    String nickname = "    jules"; // With leading blank
    int format = 0;
    LatLonBox bounds = const LatLonBox(north: 50.0, south: 48.0, east: 1, west: -1, rotation: 0);
    CourseGeoreference course = CourseGeoreference(
      "example",
      bounds,
      "",
      49.0,
      0.0,
      Discipline.URBANO,
      null,
      0,
    );
    CourseData courseData = CourseData(course, []);

    TrackFileFormat.fromExtension("gpx");
    expect(() => RouteData.checkDataAndCreateRoute(
      courseId: courseId,
      nickname: nickname,
      format: format,
      waypoints: [],
      courseData: courseData,
    ), throwsA(isA<GpxException>()));
  });

  test('Test check data when outside the map', () {
    TestWidgetsFlutterBinding.ensureInitialized();

    int courseId = 1;
    String nickname = "    jules"; // With leading blank
    int format = 0;
    LatLonBox bounds = const LatLonBox(north: -50.0, south: -48.0, east: 1, west: -1, rotation: 0);

    CourseGeoreference course = CourseGeoreference(
      "example",
      bounds,
      "",
      49.0,
      0.0,
      Discipline.URBANO,
      null,
      0,
    );
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.285662, -0.538788, 0, 74.4),
      GeodesicPoint(49.285668, -0.538791, 2000, 74.0),
      GeodesicPoint(49.285669, -0.538804, 4000, 72.9),
      GeodesicPoint(49.285670, -0.538820, 6000, 72.4),
    ];
    CourseData courseData = CourseData(course, [
      const Checkpoint(0, "0", 0, 0, "START"),
      const Checkpoint(1, "0", 0, 0, "END"),
    ]);

    TrackFileFormat.fromExtension("gpx");
    expect(()=> RouteData.checkDataAndCreateRoute(
      courseId: courseId,
      nickname: nickname,
      format: format,
      waypoints: waypoints,
      courseData: courseData,
    ), throwsA(isA<GpxException>()));
  });

  test('Test check data when nominal', () {
    TestWidgetsFlutterBinding.ensureInitialized();

    int courseId = 1;
    String nickname = "    jules"; // With leading blank
    int format = 0;
    LatLonBox bounds = const LatLonBox(north: 50.0, south: 48.0, east: 1, west: -1, rotation: 0);

    CourseGeoreference course = CourseGeoreference(
      "example",
      bounds,
      "",
      49.0,
      0.0,
      Discipline.URBANO,
      null,
      0,
    );
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.285662, -0.538788, 0, 74.4),
      GeodesicPoint(49.285668, -0.538791, 2000, 74.0),
      GeodesicPoint(49.285669, -0.538804, 4000, 72.9),
      GeodesicPoint(49.285670, -0.538820, 6000, 72.4),
    ];
    CourseData courseData = CourseData(course, [
      const Checkpoint(0, "0", 0, 0, "START"),
      const Checkpoint(1, "0", 0, 0, "END"),
    ]);

    TrackFileFormat.fromExtension("gpx");
    RouteData contents = RouteData.checkDataAndCreateRoute(
      courseId: courseId,
      nickname: nickname,
      format: format,
      waypoints: waypoints,
      courseData: courseData,
    );
    expect(contents.nickname, nickname.trim());
    expect(contents.courseId, 1);
    expect(contents.gpsTrace, "49.285662,-0.538788,0,74.4;49.285668,-0.538791,2000,74.0;49.285669,-0.538804,4000,72.9;49.285670,-0.538820,6000,72.4;");
    expect(contents.punchTimes, [0, 6000]);
  });

  test('Test extract punchTimes nominal case', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1, 9.8),
      GeodesicPoint(49.328519, -0.389189, 2, 9.8),
      GeodesicPoint(49.328519, -10.389189, 3, 9.8),
      GeodesicPoint(49.329825, -0.387567, 4, 9.8),
      GeodesicPoint(49.329825, -10.387567, 5, 9.8),
      GeodesicPoint(49.331249, -0.389705, 6, 9.7),
    ];
    List<Checkpoint> checkpoints = [
      const Checkpoint(1, "1", 49.328493, -0.389201, "Control"),
      const Checkpoint(2, "2", 49.329825, -0.387567, "Control"),
      const Checkpoint(3, "3", 49.331249, -0.389705, "Control"),
    ];
    List<int> punchTimes = RouteData.extractPunchTimes_(waypoints, checkpoints, ValidationOrder.PRESET_ORDER.index);
    expect(punchTimes.length, 3);
    expect(punchTimes, [1, 4, 6]);
  });

  test('Test extract removeTrackPointsBeforeStartAndAfterEnd_', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1, 9.8),
      GeodesicPoint(49.328519, -0.389189, 2, 9.8),
      GeodesicPoint(49.328519, -10.389189, 3, 9.8),
      GeodesicPoint(49.329825, -0.387567, 4, 9.8),
      GeodesicPoint(49.329825, -10.387567, 5, 9.8),
      GeodesicPoint(49.331249, -0.389705, 6, 9.7),
    ];
    List<int> checkpoints = [
      3,
      4,
      5,
    ];
    List<GeodesicPoint> expected = [
      GeodesicPoint(49.328519, -10.389189, 3, 9.8),
      GeodesicPoint(49.329825, -0.387567, 4, 9.8),
      GeodesicPoint(49.329825, -10.387567, 5, 9.8),
    ];
    List<GeodesicPoint> actual = RouteData.removeTrackPointsBeforeStartAndAfterEnd_(waypoints, checkpoints);
    for (int i = 0; i < expected.length; i++) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
    }
  });

  test('Test checkTrackIsInMapBound nominal case', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328515, -0.389206, 82000, 9.8),
      GeodesicPoint(49.328519, -0.389189, 143000, 9.8),
      GeodesicPoint(49.328529, -0.389173, 244000, 9.8),
      GeodesicPoint(49.328543, -0.389156, 8025000, 9.7),
    ];

    LatLonBox bounds = const LatLonBox(north: 49.332469380, south: 49.324925520, east: -0.380839692, west: -0.397163799, rotation: 2.464828175);
    expect(RouteData.checkTrackIsInMapBound_(waypoints, bounds), true);
  });

  test('Test checkTrackIsInMapBound if is not in north bound', () {
    LatLonBox bounds = const LatLonBox(north: 49.332469380, south: 49.324925520, east: -0.380839692, west: -0.397163799, rotation: 2.464828175);
    List<GeodesicPoint> waypoints = [GeodesicPoint(49.348515, -0.389206, 82000, 9.8)];
    expect(RouteData.checkTrackIsInMapBound_(waypoints, bounds), false);
  });

  test('Test checkTrackIsInMapBound if is not in south bound', () {
    LatLonBox bounds = const LatLonBox(north: 49.332469380, south: 49.324925520, east: -0.380839692, west: -0.397163799, rotation: 2.464828175);
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328515, -0.399206, 88000, 9.8),
    ];
    expect(RouteData.checkTrackIsInMapBound_(waypoints, bounds), false);
  });

  test('Test checkTrackIsInMapBound if is not in east bound', () {
    LatLonBox bounds = const LatLonBox(north: 49.332469380, south: 49.324925520, east: -0.380839692, west: -0.397163799, rotation: 2.464828175);
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.318515, -0.389206, 88000, 9.8),
    ];
    expect(RouteData.checkTrackIsInMapBound_(waypoints, bounds), false);
  });

  test('Test checkTrackIsInMapBound if is not in west bound', () {
    LatLonBox bounds = const LatLonBox(north: 49.332469380, south: 49.324925520, east: -0.380839692, west: -0.397163799, rotation: 2.464828175);
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328515, -0.379206, 82000, 9.8),
    ];
    expect(RouteData.checkTrackIsInMapBound_(waypoints, bounds), false);
  });

  test('Test changeAbsoluteTimeToRelativeTime', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1000, 9.8),
      GeodesicPoint(49.328519, -0.389189, 2000, 9.8),
      GeodesicPoint(49.328519, -10.389189, 4000, 9.8),
      GeodesicPoint(49.329825, -0.387567, 4050, 9.8),
      GeodesicPoint(49.329825, -10.387567, 5000, 9.8),
      GeodesicPoint(49.331249, -0.389705, 6000, 9.7),
    ];
    List<int> checkpoints = [1000, 2000, 3000, 4000, 5000];
    waypoints = RouteData.changeAbsoluteTimeToRelativeTime_(waypoints, checkpoints);
    expect(waypoints[0].timestampInMillisecond, 0);
    expect(waypoints[1].timestampInMillisecond, 1000);
    expect(checkpoints[0], 0);
    expect(checkpoints[1], 1000);
    expect(checkpoints[4], 4000);
  });

  test('Test getWaypointsAsString', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1000, 9.8),
      GeodesicPoint(49.328519, -0.389189, 2000, 9.8),
      GeodesicPoint(49.328519, -10.389189, 4000, 9.8),
      GeodesicPoint(49.329825, -0.387567, 4050, 9.8),
      GeodesicPoint(49.329825, -10.387567, 5000, 9.8),
      GeodesicPoint(49.331249, -0.389705, 6000, 9.7),
    ];
    var actual = RouteData.waypointsToString_(waypoints);
    expect(actual, '49.328493,-0.389201,1000,9.8;49.328519,-0.389189,2000,9.8;49.328519,-10.389189,4000,9.8;49.329825,-0.387567,4050,9.8;49.329825,-10.387567,5000,9.8;49.331249,-0.389705,6000,9.7;');
  });

  test('Test refinePunchTimesPosition_', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1, 9.8),
      GeodesicPoint(49.328519, -0.389189, 2, 9.8),
      GeodesicPoint(49.328519, -10.389189, 3, 9.8),
      GeodesicPoint(49.329823, -0.387564, 4, 9.8),
      GeodesicPoint(49.329824, -0.387566, 5, 9.8),
      GeodesicPoint(49.329825, -0.387567, 6, 9.8),
      GeodesicPoint(49.329828, -0.38759, 7, 9.8),
      GeodesicPoint(49.329825, -10.387567, 8, 9.8),
      GeodesicPoint(49.331249, -0.389705, 9, 9.7),
    ];
    List<Checkpoint> checkpoints = [
      const Checkpoint(1, "1", 49.328493, -0.389201, "Control"),
      const Checkpoint(2, "2", 49.329825, -0.387567, "Control"),
      const Checkpoint(3, "3", 49.331249, -0.389705, "Control"),
    ];
    List<int> punchTimes = [1, 4, 9];
    List<int> actual = RouteData.refinePunchTimesPosition_(waypoints, checkpoints, punchTimes);
    expect(actual, [1, 6, 9]);
  });

  test('Test refinePunchTimesPosition_', () {
    String gpsTrace = "49.328493, -0.389201, 1, 9.8;49.328519, -0.389189, 2, 9.8;49.328519, -10.389189, 3, 9.8;49.329823, -0.387564, 4, 9.8;49.329824, -0.387566, 5, 9.8;49.329825, -0.387567, 6, 9.8;49.329828, -0.38759, 7, 9.8;49.329825, -10.387567, 8, 9.8;49.331249, -0.389705, 9, 9.7;";
    List<int> punchTimes = [1, 4, 9];
    RouteData toto = RouteData(nickname: "toto", format: 1, courseId: 11, gpsTrace: gpsTrace, punchTimes: punchTimes, totalTimeInMilliseconds: 22222);
    Map<String, dynamic> actual = toto.toJson();
    expect(actual, {
      'orienteer': 'toto',
      'format': '1',
      'totalTime': 22222,
      'courseId': 11,
      'controlPoints': [
        {'controlPoint': 0, 'punchTime': 1},
        {'controlPoint': 1, 'punchTime': 4},
        {'controlPoint': 2, 'punchTime': 9}
      ],
      'trace': '49.328493, -0.389201, 1, 9.8;49.328519, -0.389189, 2, 9.8;49.328519, -10.389189, 3, 9.8;49.329823, -0.387564, 4, 9.8;49.329824, -0.387566, 5, 9.8;49.329825, -0.387567, 6, 9.8;49.329828, -0.38759, 7, 9.8;49.329825, -10.387567, 8, 9.8;49.331249, -0.389705, 9, 9.7;',
      'imported': true
    });
  });
}
