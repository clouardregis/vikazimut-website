import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/import/strava/data/model_run_activity.dart';

void main() {
  test('Test repository stream', () {
    dynamic data = jsonDecode("""{
"latlng": {"data": [[49.281253, -0.702113], [49.281217, -0.702129]], "series_type": "distance", "original_size": 1480, "resolution": "high"},
"distance": {"data": [0, 4.1], "series_type": "distance", "original_size": 1480, "resolution": "high"}, 
"altitude": {"data": [44.2, 44.3], "series_type": "distance", "original_size": 1480, "resolution": "high"},
"time": {"data": [0, 1], "series_type": "distance", "original_size": 1480, "resolution": "high"}}""");
    var runActivity = RunActivity.fromJson(Map<String, dynamic>.from(data));
    expect(runActivity.positions![0], [49.281253, -0.702113]);
    expect(runActivity.times?[0], 0);
    expect(runActivity.altitudes, [44.2, 44.3]);
  });
}
