import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/import/strava/data/client.dart';

void main() {
  test('Test buildUri when no query parameters', () {
    String buildUri = NetworkingHelper.buildUri("http://vikazim.fr", null);
    expect(buildUri, "http://vikazim.fr");
  });

  test('Test buildUri when query parameters', () {
    String buildUri = NetworkingHelper.buildUri("http://vikazim.fr", {"page": 1, "per_page": 2});
    expect(buildUri, "http://vikazim.fr?page=1&per_page=2");
  });
}
