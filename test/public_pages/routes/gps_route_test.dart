import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

void main() {
  test('Test convertMMsToKmh_ cas 1 km/h', () {
    double actual = GpsRoute.computeVelocityInKmH_(1000, 60 * 60 * 1000);
    expect(actual, 1);
  });

  test('Test convertMMsToKmh_ cas 2 km/h', () {
    double actual = GpsRoute.computeVelocityInKmH_(1000, 30 * 60 * 1000);
    expect(actual, 2);
  });

  test('Test convertMMsToKmh_ cas 1 km/h bis', () {
    double actual = GpsRoute.computeVelocityInKmH_(500, 30 * 60 * 1000);
    expect(actual, 1);
  });

  test('Test  getRouteSpeedsAsColor_ failure cas 2', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(49.278524, -0.425337, 02005, 90.5),
      GeodesicPoint(49.278511, -0.425321, 03982, 89.9),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
    ];
    List<double> actual = GpsRoute.filterSpeeds_(route, 5);
    expect(actual, [0.0, 3.379510568145884, 1.264466523085825]);
  });

  test('Test  getRouteSpeedsAsColor_ failure cas 3', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(49.278524, -0.425337, 02005, 90.5),
      GeodesicPoint(49.278511, -0.425321, 03982, 89.9),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
    ];
    List<double> actual = GpsRoute.filterSpeeds_(route, 5);
    expect(actual, [0.0, 3.379510568145884, 1.264466523085825, 0.0, 0.0]);
  });

  test('Test getRouteSpeedsAsColor with constant speeds', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0.0, 10000),
      GeodesicPoint(0, 0.1, 20000),
      GeodesicPoint(0, 0.0, 30000),
      GeodesicPoint(0, 0.1, 40000),
      GeodesicPoint(0, 0.0, 50000),
      GeodesicPoint(0, 0.1, 60000),
      GeodesicPoint(0, 0.0, 70000),
      GeodesicPoint(0, 0.1, 80000),
      GeodesicPoint(0, 0.0, 90000),
      GeodesicPoint(0, 0.1, 100000),
    ];
    List<double> actual = GpsRoute.filterSpeeds_(route, 2);
    const double distance = 11131.94;
    const int time = 10;
    const double expected = distance * 3.6 / time;
    for (int i = 1; i < actual.length; i++) {
      expect(actual[i], moreOrLessEquals(expected, epsilon: 0.01));
    }
  });

  test('Test getRouteSpeedsAsColor with linear ascendant speeds', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0.0, 0),
      GeodesicPoint(0, 0.1, 1000),
      GeodesicPoint(0, 0.0, 3000),
      GeodesicPoint(0, 0.1, 6000),
      GeodesicPoint(0, 0.0, 10000),
      GeodesicPoint(0, 0.1, 15000), //
      GeodesicPoint(0, 0.0, 20000),
      GeodesicPoint(0, 0.1, 25000),
      GeodesicPoint(0, 0.0, 29000),
      GeodesicPoint(0, 0.1, 32000),
      GeodesicPoint(0, 0.0, 33000),
    ];
    //     const double distance = 11131.94;
    var expected = [
      0.0,
      40075.016685578485,
      20037.508342789242,
      13358.33889519283,
      10018.754171394623,
      8587.503575481105,
      8015.003337115698, // speed: 11131.94 × 3 × 3.6 × 1000 ÷ 3 × 5000
      8587.503575481105,
      10018.754171394623,
      13358.33889519283,
      40075.016685578485
    ];
    List<double> actual = GpsRoute.filterSpeeds_(route, 1);
    expect(actual, expected);
  });

  test('Test getRouteSpeedsAsColor with 6 gps points', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0.0, 0),
      GeodesicPoint(0, 0.1, 1000),
      GeodesicPoint(0, 0.0, 3000),
      GeodesicPoint(0, 0.1, 6000),
      GeodesicPoint(0, 0.0, 10000),
      GeodesicPoint(0, 0.1, 15000),
    ];
    List<double> actual = GpsRoute.filterSpeeds_(route, 2);
    expect(actual.length, route.length);
  });

  test('Test isPresetFormat when true', () {
    var actual = 0.isPresetOrder;
    expect(actual, true);
  });

  test('Test isPresetFormat when false', () {
    var actual = 1.isPresetOrder;
    expect(actual, false);
  });

  test('Test isFreeFormat when true', () {
    var actual = 1.isFreeOrder;
    expect(actual, true);
  });

  test('Test isFreeFormat when false', () {
    var actual = 0.isFreeOrder;
    expect(actual, false);
  });

  test('Test fromInt of CourseFormat when regular preset', () {
    var actual = ValidationOrder.toEnum(0);
    expect(actual, ValidationOrder.PRESET_ORDER);
  });

  test('Test fromInt of CourseFormat when regular free', () {
    var actual = ValidationOrder.toEnum(1);
    expect(actual, ValidationOrder.FREE_ORDER);
  });

  test('Test fromInt of CourseFormat when unsupervised preset order', () {
    var actual = ValidationOrder.toEnum(2);
    expect(actual, ValidationOrder.UNSUPERVISED_PRESET_ORDER);
  });

  test('Test fromInt of CourseFormat when outlier', () {
    var actual = ValidationOrder.toEnum(400);
    expect(actual, ValidationOrder.PRESET_ORDER);
  });

  test('Test isFreeOrder when FREE', () {
    var format = ValidationOrder.FREE_ORDER;
    expect(format.isFreeOrder, true);
  });

  test('Test isFreeOrder when PRESET', () {
    var format = ValidationOrder.PRESET_ORDER;
    expect(format.isFreeOrder, false);
  });

  test('Test isPresetOrder when PRESET', () {
    var format = ValidationOrder.PRESET_ORDER;
    expect(format.isPresetOrder, true);
  });

  test('Test isPresetOrder when PRESET', () {
    var format = ValidationOrder.UNSUPERVISED_PRESET_ORDER;
    expect(format.isPresetOrder, true);
  });

  test('Test isPresetOrder when FREE', () {
    var format = ValidationOrder.FREE_ORDER;
    expect(format.isPresetOrder, false);
  });

  test('Test validation order equals 1', () {
    var format = ValidationOrder.FREE_ORDER;
    expect(format.equals(ValidationOrder.FREE_ORDER), true);
  });

  test('Test validation order equals 2', () {
    var format = ValidationOrder.PRESET_ORDER;
    expect(format.equals(ValidationOrder.PRESET_ORDER), true);
  });

  test('Test validation order equals 3', () {
    var format = ValidationOrder.UNSUPERVISED_PRESET_ORDER;
    expect(format.equals(ValidationOrder.UNSUPERVISED_PRESET_ORDER), true);
  });

  test('Test validation order equals 4', () {
    var format = ValidationOrder.UNSUPERVISED_PRESET_ORDER;
    expect(format.equals(ValidationOrder.PRESET_ORDER), true);
  });

  test('Test validation order equals 5', () {
    var format = ValidationOrder.PRESET_ORDER;
    expect(format.equals(ValidationOrder.UNSUPERVISED_PRESET_ORDER), true);
  });

  test('Test validation order equals 6', () {
    var format = ValidationOrder.FREE_ORDER;
    expect(format.equals(ValidationOrder.UNSUPERVISED_PRESET_ORDER), false);
  });

  test('Test validation order equals 7', () {
    var format = ValidationOrder.FREE_ORDER;
    expect(format.equals(ValidationOrder.PRESET_ORDER), false);
  });

  test('Test validation order equals 8', () {
    var format = ValidationOrder.PRESET_ORDER;
    expect(format.equals(ValidationOrder.FREE_ORDER), false);
  });

  test('Test validation order equals 9', () {
    var format = ValidationOrder.UNSUPERVISED_PRESET_ORDER;
    expect(format.equals(ValidationOrder.FREE_ORDER), false);
  });
}
