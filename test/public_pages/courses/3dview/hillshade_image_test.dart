import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/hillshade_image.dart';

void main() {
  test('Test only 1 pixel and flat image', () {
    List<double> heightmap = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    List<int> x = HillshadeImage.generateHillshade_(45, 315, heightmap, 3, 3, 5);
    expect(x[1 * 3 + 1], 180);
  });

  test('Test only 1 pixel and relief image', () {
    List<double> heightpap = [2450, 2461, 2483, 2452, 2461, 2483, 2447, 2455, 2477];
    List<int> x = HillshadeImage.generateHillshade_(45, 315, heightpap, 3, 3, 5);
    expect(x[1 * 3 + 1], 154);
  });

  test('Test brighten image when no effect', () {
    List<int> heightMap = [255, 127, 0];
    List<int> expected = [255, 127, 0];
    List<int> actual = HillshadeImage.brighten_(heightMap);
    expect(actual, expected);
  });

  test('Test brighten image when effect', () {
    List<int> heightMap = [127, 10, 0];
    List<int> expected = [255, 138, 128];
    List<int> actual = HillshadeImage.brighten_(heightMap);
    expect(actual, expected);
  });

  test('Test combineMultipleHillshades', () {
    List<List<int>> hillshades = [
      [1, 2, 3],
      [4, 5, 6]
    ];
    List<int> expected = [2, 3, 4];
    List<int> actual = HillshadeImage.combineMultipleHillshades_(hillshades);
    expect(actual, expected);
  });
}
