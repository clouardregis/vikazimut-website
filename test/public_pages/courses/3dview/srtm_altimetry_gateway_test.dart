import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/srtm_altimetry_gateway.dart';

void main() {
  test('Test convertLatitudeToLineIndex 1', () {
    int row = SrtmAltimetryGateway.convertLatitudeToMapRowIndex(49).toInt();
    expect(row, 3600);
  });

  test('Test convertLatitudeToLineIndex 2', () {
    int row = SrtmAltimetryGateway.convertLatitudeToMapRowIndex(49.5).toInt();
    expect(row, 1799);
  });

  test('Test convertLatitudeToLineIndex 3', () {
    int row = SrtmAltimetryGateway.convertLatitudeToMapRowIndex(49.99999).toInt();
    expect(row, 0);
  });

  test('Test convertLongitudeToLineIndex 1', () {
    int col = SrtmAltimetryGateway.convertLongitudeToMapColumnIndex(0).toInt();
    expect(col, 0);
  });

  test('Test convertLongitudeToLineIndex 3', () {
    int col = SrtmAltimetryGateway.convertLongitudeToMapColumnIndex(0.5).toInt();
    expect(col, 1800);
  });

  test('Test convertLongitudeToLineIndex 2', () {
    int col = SrtmAltimetryGateway.convertLongitudeToMapColumnIndex(0.999999).toInt();
    expect(col, 3600);
  });

  test('Test getTileName 1', () {
    double longitude = 0.5;
    double latitude = 49.5;
    String name = SrtmAltimetryGateway.getTileName(longitude: longitude, latitude: latitude);
    expect(name, "N49E000");
  });

  test('Test getTileName 2', () {
    double longitude = 0.0;
    double latitude = 49.0;
    String name = SrtmAltimetryGateway.getTileName(longitude: longitude, latitude: latitude);
    expect(name, "N49E000");
  });

  test('Test getTileName 3', () {
    double longitude = -0.5;
    double latitude = 48.99;
    String name = SrtmAltimetryGateway.getTileName(longitude: longitude, latitude: latitude);
    expect(name, "N48W001");
  });

  test('Test getTileName 4', () {
    double longitude = -0.5;
    double latitude = -48.99;
    String name = SrtmAltimetryGateway.getTileName(longitude: longitude, latitude: latitude);
    expect(name, "S49W001");
  });

  test('Test getAllTileNames 1', () {
    LatLonBox region = const LatLonBox(north: 49.5, west: -0.2, south: 49, east: -0.2, rotation: 0);
    List<String> names = SrtmAltimetryGateway.getAllTileNamesCoveringRegion(region);
    expect(names.length, 1);
    expect(names[0], "N49W001");
  });

  test('Test getAllTileNames 2', () {
    LatLonBox region = const LatLonBox(north: -49.5, west: -0.2, south: -49.9, east: -0.1, rotation: 0);
    List<String> names = SrtmAltimetryGateway.getAllTileNamesCoveringRegion(region);
    expect(names.length, 1);
    expect(names[0], "S50W001");
  });

  test('Test getAllTileNames 3', () {
    LatLonBox region = const LatLonBox(north: 49.5, west: -0.2, south: 48.9, east: -0.2, rotation: 0);
    List<String> names = SrtmAltimetryGateway.getAllTileNamesCoveringRegion(region);
    expect(names.length, 2);
    expect(names[0], "N48W001");
    expect(names[1], "N49W001");
  });

  test('Test getAllTileNames 4', () {
    LatLonBox region = const LatLonBox(north: 49.5, west: -0.1, south: 49.4, east: 0.1, rotation: 0);
    List<String> names = SrtmAltimetryGateway.getAllTileNamesCoveringRegion(region);
    expect(names.length, 2);
    expect(names[0], "N49W001");
    expect(names[1], "N49E000");
  });

  test('Test getAllTileNames 5', () {
    LatLonBox region = const LatLonBox(north: 49.5, west: -1.02, south: 49.4, east: 1.02, rotation: 0);
    List<String> names = SrtmAltimetryGateway.getAllTileNamesCoveringRegion(region);
    expect(names.length, 4);
    expect(names[0], "N49W002");
    expect(names[1], "N49W001");
    expect(names[2], "N49E000");
    expect(names[3], "N49E001");
  });

  test('Test conversion to Int16 case 1', () {
    Uint8List data = Uint8List.fromList([0, 1]);
    ByteData byteData = ByteData.view(data.buffer);
    double actual = SrtmAltimetryGateway.toInt16(byteData, 0, 0, 1);
    expect(actual, 1);
  });
  test('Test conversion to Int16 case -1', () {
    Uint8List data = Uint8List.fromList([255, 255]);
    ByteData byteData = ByteData.view(data.buffer);
    double actual = SrtmAltimetryGateway.toInt16(byteData, 0, 0, 1);
    expect(actual, -1);
  });
  test('Test conversion to Int16 case max int', () {
    Uint8List data = Uint8List.fromList([0, 255]);
    ByteData byteData = ByteData.view(data.buffer);
    double actual = SrtmAltimetryGateway.toInt16(byteData, 0, 0, 1);
    expect(actual, 255);
  });
  test('Test conversion to Int16 case max uint', () {
    Uint8List data = Uint8List.fromList([128, 0]);
    ByteData byteData = ByteData.view(data.buffer);
    double actual = SrtmAltimetryGateway.toInt16(byteData, 0, 0, 1);
    expect(actual, -32768);
  });
}
