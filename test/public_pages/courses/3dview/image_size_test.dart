import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/image_size.dart';

void main() {
  test('Test resize with same ratio 1', () {
    ImageSize size  = ImageSize.calculateAspectRatio(100, 200, 50);
    expect(size.width, 25);
    expect(size.height, 50);
  });

  test('Test resize with same ratio 2', () {
    ImageSize size  = ImageSize.calculateAspectRatio(200, 100, 50);
    expect(size.width, 50);
    expect(size.height, 25);
  });

  test('Test resize with same ratio 3', () {
    ImageSize size  = ImageSize.calculateAspectRatio(100, 50, 150);
    expect(size.width, 100);
    expect(size.height, 50);
  });

}
