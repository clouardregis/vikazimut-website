import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/course_3d_model.dart';
import 'package:vikazimut_website/public_pages/routes/3d_view/image_size.dart';

void main() {
  test('Test filter with no attribute', () {
    String actual = Course3dModel.replaceAttributeByValueInWebGLTemplate_("toto", {});
    expect(actual, "toto");
  });

  test('Test filter with one attribute', () {
    String actual = Course3dModel.replaceAttributeByValueInWebGLTemplate_("var attribute = {% attribute %};", {"attribute": "value"});
    expect(actual, "var attribute = value;");
  });

  test('Test filter with two attributes', () {
    String actual = Course3dModel.replaceAttributeByValueInWebGLTemplate_("var attribute1 = {% attribute_1 %};\nvar attribute2 = {% attribute_2 %};", {"attribute_1": "value1", "attribute_2": "value2"});
    expect(actual, "var attribute1 = value1;\nvar attribute2 = value2;");
  });

  test('Test filter with bad attribute', () {
    String actual = Course3dModel.replaceAttributeByValueInWebGLTemplate_("var attribute = {% attribute %};", {"no_attribute": "value"});
    expect(actual, "var attribute = {% attribute %};");
  });

  test('Test computeTerrainWidthInMeter', () {
    LatLonBox bounds = const LatLonBox(north: 1, east: 1, south: -1, west: -1, rotation: 0);
    int actual = Course3dModel.computeTerrainWidthInMeter(bounds);
    expect(actual, 222605);
  });

  test('Test computeResolutionInMeterPerPixel', () {
    LatLonBox bounds = const LatLonBox(north: 1, east: 1, south: -1, west: -1, rotation: 0);
    ImageSize size = const ImageSize(100, 200);
    double actual = Course3dModel.computeResolutionInMeterPerPixel(bounds, size);
    expect(actual, 222605 / 100);
  });
}
