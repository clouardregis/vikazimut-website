import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/courses/worldmap/location_as_dot.dart';
import 'package:vikazimut_website/public_pages/courses/worldmap/worldmap_page_presenter.dart';
import 'package:vikazimut_website/public_pages/model/course.dart';

void main() {
  test('Test getSelectCourseIndex from list when name is empty', () {
    List<LocationAsDot> dots = [
      const LocationAsDot(0, 0, ["foo1"]),
    ];
    int? actual = WorldMapPagePresenter.getSelectCourseIndex_("", dots);
    expect(actual, -1);
  });

  test('Test getSelectCourseIndex from list when found', () {
    List<LocationAsDot> dots = [
      const LocationAsDot(0, 0, ["foo1"]),
      const LocationAsDot(0, 0, ["foo2"]),
      const LocationAsDot(0, 0, ["foo3", "foo4"]),
    ];
    int? actual = WorldMapPagePresenter.getSelectCourseIndex_('foo3', dots);
    expect(actual, 2);
  });

  test('Test getSelectCourseIndex when not found', () {
    List<LocationAsDot> dots = [
      const LocationAsDot(0, 0, ["foo1"]),
      const LocationAsDot(0, 0, ["foo2"]),
      const LocationAsDot(0, 0, ["foo3", "foo4"]),
    ];
    int? actual = WorldMapPagePresenter.getSelectCourseIndex_('foo5', dots);
    expect(actual, -1);
  });

  test('Test getCourseFromName when nominal', () {
    List<Course> courses = [
      const Course(id: 0, name: "name1", latitude: 0, longitude: 0),
      const Course(id: 1, name: "name2", latitude: 0, longitude: 0),
      const Course(id: 2, name: "name3", latitude: 0, longitude: 0),
      const Course(id: 3, name: "name4", latitude: 0, longitude: 0),
    ];
    Course? actual = WorldMapPagePresenter.getCourseFromName(courses, "name2");
    expect(actual?.id, 1);
  });

  test('Test getCourseFromName when no name', () {
    List<Course> courses = [
      const Course(id: 0, name: "name1", latitude: 0, longitude: 0),
      const Course(id: 1, name: "name2", latitude: 0, longitude: 0),
      const Course(id: 2, name: "name3", latitude: 0, longitude: 0),
      const Course(id: 3, name: "name4", latitude: 0, longitude: 0),
    ];
    Course? actual = WorldMapPagePresenter.getCourseFromName(courses, "unknown");
    expect(actual?.id, null);
  });
}
