import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/courses/nearby_courses/nearby_course_model.dart';

void main() {
  test('Creation of NearByCourses From Json File 1', () {
    Map<String, dynamic> json = {"id": 1748, "name": "Grimbosq SPE CO Orange", "tracks": 0, "distance": 1};

    NearbyCourseModel mapProxy = NearbyCourseModel.fromJson(json);
    expect(mapProxy.id, 1748);
    expect(mapProxy.name, "Grimbosq SPE CO Orange");
    expect(mapProxy.tracks, 0);
    expect(mapProxy.club, null);
    expect(mapProxy.distance, 1);
    expect(mapProxy.clubUrl, null);
    expect(mapProxy.touristic, false);
    expect(mapProxy.printable, false);
  });

  test('Creation of NearByCourses From Json File 2', () {
    Map<String, dynamic> json = {"id": 1748, "name": "Grimbosq SPE CO Orange", "tracks": 10, "club": "STAPS CAEN", "distance": 2, "touristic":1,  "cluburl":"https://vikazim.fr", "printable":true};

    NearbyCourseModel mapProxy = NearbyCourseModel.fromJson(json);
    expect(mapProxy.id, 1748);
    expect(mapProxy.name, "Grimbosq SPE CO Orange");
    expect(mapProxy.tracks, 10);
    expect(mapProxy.club, "STAPS CAEN");
    expect(mapProxy.distance, 2);
    expect(mapProxy.clubUrl, 'https://vikazim.fr');
    expect(mapProxy.touristic, true);
    expect(mapProxy.printable, true);
  });
}
