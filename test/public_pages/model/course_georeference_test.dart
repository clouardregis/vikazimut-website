import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/discipline.dart';
import 'package:vikazimut_website/public_pages/model/course_georeference.dart';

void main() {
  test('test CourseGeoreference.fromJson', () {
    var json = {
      "name": "Grimbosq 110523 CAPEPS",
      "kml": """<?xml version="1.0" encoding="UTF-8"?>
<!-- Generator: OCAD 2020.6.5.4046 -->
<kml xmlns="http://www.opengis.net/kml/2.2">
<Folder>
	<name/>
	<GroundOverlay>
		<name>tile_0_0.jpg</name>
		<drawOrder>75</drawOrder>
		<Icon>
			<href>files/tile_0_0.jpg</href>
			<viewBoundScale>0.75</viewBoundScale>
		</Icon>
	  <color>FFffffff</color>
		<LatLonBox>
			<north>49.047497558</north>
			<south>49.033130753</south>
			<east>-0.415154864</east>
			<west>-0.448060913</west>
			<rotation>2.501593419</rotation>
		</LatLonBox>
	</GroundOverlay>
</Folder>
</kml>""",
      "image": "4315",
      "latitude": "49.040639",
      "longitude": "-0.446552",
      "discipline": 1,
      "hiddenLevel": 1,
    };

    var actual = CourseGeoreference.fromJson(json);
    expect(actual.name, "Grimbosq 110523 CAPEPS");
    expect(actual.bounds.north, 49.047497558);
    expect(actual.bounds.south, 49.033130753);
    expect(actual.bounds.east, -0.415154864);
    expect(actual.bounds.west, -0.448060913);
    expect(actual.startLatitude, 49.040639);
    expect(actual.startLongitude, -0.446552);
    expect(actual.discipline, Discipline.FORESTO);
    expect(actual.imageId, "4315");
    expect(actual.hiddenLevel, 1);
  });

  test('test parseKml', () {
    String kml = """<?xml version="1.0" encoding="UTF-8"?>
<!-- Generator: OCAD 2020.6.5.4046 -->
<kml xmlns="http://www.opengis.net/kml/2.2">
<Folder>
	<name/>
	<GroundOverlay>
		<name>tile_0_0.jpg</name>
		<drawOrder>75</drawOrder>
		<Icon>
			<href>files/tile_0_0.jpg</href>
			<viewBoundScale>0.75</viewBoundScale>
		</Icon>
	  <color>FFffffff</color>
		<LatLonBox>
			<north>49.047497558</north>
			<south>49.033130753</south>
			<east>-0.415154864</east>
			<west>-0.448060913</west>
			<rotation>2.501593419</rotation>
		</LatLonBox>
	</GroundOverlay>
</Folder>
</kml>""";

    var actual = CourseGeoreference.parseKml(kml);
    expect(actual.north, 49.047497558);
    expect(actual.south, 49.033130753);
    expect(actual.east, -0.415154864);
    expect(actual.west, -0.448060913);
    expect(actual.rotation, 2.501593419);
  });
}
