import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/leg.dart';
import 'package:vikazimut_website/public_pages/model/leg_helper.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';

void main() {
  void expectArray(List<GeodesicPoint> l1, List<GeodesicPoint> l2) {
    expect(l1.length, l2.length);
    for (int i = 0; i < l1.length; i++) {
      expect(l1[i].timestampInMillisecond, l2[i].timestampInMillisecond);
    }
  }

  GeodesicPoint g(int time) {
    return GeodesicPoint(0, 0, time);
  }

  test('Test splitRouteIntoLegs when complete', () {
    List<GeodesicPoint> route = [g(0), g(1), g(2), g(3), g(10), g(12), g(15), g(17), g(20), g(21), g(22), g(30)];
    List<GeodesicPoint> expected1 = [g(0), g(1), g(2), g(3), g(10)];
    List<GeodesicPoint> expected2 = [g(10), g(12), g(15), g(17), g(20)];
    List<GeodesicPoint> expected3 = [g(20), g(21), g(22), g(30)];
    List<Leg> legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 10), Leg(2, punchTime: 20), Leg(3, punchTime: 30)];
    LegHelper.splitRouteIntoLegs(route, legs);
    expectArray(legs[1].waypoints!, expected1);
    expectArray(legs[2].waypoints!, expected2);
    expectArray(legs[3].waypoints!, expected3);
  });

  test('Test splitRouteIntoLegs when MP free order', () {
    List<GeodesicPoint> route = [g(0), g(1), g(2), g(3), g(10), g(12), g(15), g(17), g(20), g(21), g(22), g(30)];
    List<GeodesicPoint> expected1 = [g(0), g(1), g(2), g(3), g(10)];
    List<GeodesicPoint> expected2 = [g(10), g(12), g(15), g(17), g(20), g(21), g(22), g(30)];
    List<Leg> legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 10), Leg(2, punchTime: 30), Leg(3, punchTime: -1), Leg(4, punchTime: -1)];
    LegHelper.splitRouteIntoLegs(route, legs);
    expectArray(legs[1].waypoints!, expected1);
    expectArray(legs[2].waypoints!, expected2);
  });

  test('Test addGpsPointAtPunchTimes case nominal', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(-1, 1, 10),
      GeodesicPoint(-2, 2, 20),
      GeodesicPoint(-3, 3, 30),
      GeodesicPoint(-4, 4, 40),
      GeodesicPoint(-5, 5, 50),
      GeodesicPoint(-6, 6, 60),
      GeodesicPoint(-7, 7, 70),
      GeodesicPoint(-8, 8, 80),
      GeodesicPoint(-9, 9, 90),
      GeodesicPoint(-10, 10, 100),
      GeodesicPoint(-11, 11, 110),
      GeodesicPoint(-12, 12, 120),
    ];
    List<Leg> legs = [Leg(0, punchTime: 0), Leg(1, punchTime: 20), Leg(2, punchTime: 35), Leg(3, punchTime: 52), Leg(4, punchTime: 120)];
    final List<GeodesicPoint> actual = LegHelper.addGpsPointAtPunchTimes_(route, legs);
    expect(actual.length, route.length + 2);
    expect(actual[3].timestampInMillisecond, 30);
    expect(actual[4].timestampInMillisecond, 35);
    expect(actual[5].timestampInMillisecond, 40);
    expect(actual[6].timestampInMillisecond, 50);
    expect(actual[7].timestampInMillisecond, 52);
    expect(actual[8].timestampInMillisecond, 60);
  });

  test('Test addGpsPointAtPunchTimes case uncompleted track', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(-1, 1, 10),
      GeodesicPoint(-2, 2, 20),
      GeodesicPoint(-3, 3, 30),
      GeodesicPoint(-4, 4, 40),
      GeodesicPoint(-5, 5, 50),
      GeodesicPoint(-6, 6, 60),
      GeodesicPoint(-7, 7, 70),
      GeodesicPoint(-8, 8, 80),
      GeodesicPoint(-9, 9, 90),
      GeodesicPoint(-10, 10, 100),
      GeodesicPoint(-11, 11, 110),
      GeodesicPoint(-12, 12, 120),
    ];
    List<Leg> legs = [Leg(0, punchTime: 50), Leg(1, punchTime: 120), Leg(2, punchTime: -1), Leg(3, punchTime: -1), Leg(4, punchTime: -1), Leg(5, punchTime: -1)];
    final List<GeodesicPoint> actual = LegHelper.addGpsPointAtPunchTimes_(route, legs);
    expect(actual.length, route.length);
  });

  test('Sort legs when nominal', () {
    Leg leg0 = Leg(0)..punchTime = 0;
    Leg leg1 = Leg(1)..punchTime = 1000;
    Leg leg2 = Leg(2)..punchTime = 2000;
    Leg leg3 = Leg(3)..punchTime = 3000;
    List<Leg> legs = [leg0, leg1, leg2, leg3];
    LegHelper.sortLegsByPunchTimes(legs);
    expect(legs[0].index, leg0.index);
    expect(legs[1].index, leg1.index);
    expect(legs[2].index, leg2.index);
    expect(legs[3].index, leg3.index);
  });

  test('Sort legs when 1 missing punch time', () {
    Leg leg0 = Leg(0)..punchTime = 0;
    Leg leg1 = Leg(1)..punchTime = 1500;
    Leg leg2 = Leg(2)..punchTime = -1;
    Leg leg3 = Leg(3)..punchTime = 1000;
    Leg leg4 = Leg(4)..punchTime = 2000;
    List<Leg> legs = [leg0, leg1, leg2, leg3, leg4];
    LegHelper.sortLegsByPunchTimes(legs);
    expect(legs[0].index, leg0.index);
    expect(legs[1].index, leg3.index);
    expect(legs[2].index, leg1.index);
    expect(legs[3].index, leg4.index);
    expect(legs[4].index, leg2.index);
  });

  test('Sort legs when 2 intermediate points are missing', () {
    Leg leg0 = Leg(0)..punchTime = 0;
    Leg leg1 = Leg(1)..punchTime = 1500;
    Leg leg2 = Leg(2)..punchTime = -1;
    Leg leg3 = Leg(3)..punchTime = 1000;
    Leg leg4 = Leg(4)..punchTime = -1;
    Leg leg5 = Leg(5)..punchTime = 2000;
    List<Leg> legs = [leg0, leg1, leg2, leg3, leg4, leg5];
    LegHelper.sortLegsByPunchTimes(legs);
    expect(legs[0].index, leg0.index);
    expect(legs[1].index, leg3.index);
    expect(legs[2].index, leg1.index);
    expect(legs[3].index, leg5.index);
    expect(legs[4].index, leg2.index);
    expect(legs[5].index, leg4.index);
  });

  test('Sort legs when no end', () {
    Leg leg0 = Leg(0)..punchTime = 0;
    Leg leg1 = Leg(1)..punchTime = -1;
    Leg leg2 = Leg(2)..punchTime = 1000;
    Leg leg3 = Leg(3)..punchTime = 100;
    Leg leg4 = Leg(3)..punchTime = -1;
    List<Leg> legs = [leg0, leg1, leg2, leg3, leg4];
    LegHelper.sortLegsByPunchTimes(legs);
    expect(legs[0].index, leg0.index);
    expect(legs[1].index, leg3.index);
    expect(legs[2].index, leg2.index);
    expect(legs[3].index, leg1.index);
    expect(legs[4].index, leg4.index);
  });

  test('Sort legs when nominal', () {
    Leg leg0 = Leg(3);
    Leg leg1 = Leg(3);
    Leg leg2 = Leg(1);
    Leg leg3 = Leg(2);
    List<Leg> legs = [leg0, leg1, leg2, leg3];
    LegHelper.sortLegsByControlIndex(legs);
    expect(legs[0], leg2);
    expect(legs[1], leg3);
    expect(legs[2], leg0);
    expect(legs[3], leg1);
  });

  test('Build legs from punch times', () {
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(1000), PunchTime(2000)];
    var actual = LegHelper.buildLegsFromPunchTimes(3, punchTimes);
    expect(actual.length, 3);
    expect(actual[1].index, 1);
    expect(actual[1].punchTime, 1000);
    expect(actual[2].index, 2);
    expect(actual[2].punchTime, 2000);
  });
}
