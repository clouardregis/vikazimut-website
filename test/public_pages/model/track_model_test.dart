import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/punch_time.dart';
import 'package:vikazimut_website/public_pages/model/track_model.dart';
import 'package:vikazimut_website/public_pages/routes/gps_route.dart';

void main() {
  test('Test sort punch times simple', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(2),
      const PunchTime(1),
      const PunchTime(5),
    ];
    List<int> actual = TrackModel.sortCheckpointsByPunchTimes(punchTimes);
    List<int> expected = [0, 2, 1, 3];
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i], expected[i]);
    }
  });

  test('Test sort punch times with unknowns', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(-1),
      const PunchTime(-1),
      const PunchTime(2),
      const PunchTime(10),
    ];
    List<int> actual = TrackModel.sortCheckpointsByPunchTimes(punchTimes);
    List<int> expected = [0, 3, 1, 2, 4];
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i], expected[i]);
    }
  });

  test('Test sort punch times', () {
    List<PunchTime> punchTimes = [
      const PunchTime(0),
      const PunchTime(1),
      const PunchTime(-1),
      const PunchTime(-1),
      const PunchTime(3),
      const PunchTime(4),
      const PunchTime(-1),
      const PunchTime(2),
      const PunchTime(-1),
      const PunchTime(6),
      const PunchTime(10),
    ];
    List<int> actual = TrackModel.sortCheckpointsByPunchTimes(punchTimes);
    List<int> expected = [0, 1, 7, 4, 5, 9, 2, 3, 6, 8, 10];
    expect(actual, expected);
  });

  test('Test TrackModel.fromJson case free order', () {
    var json = [
      15405,
      "John",
      2339476,
      [
        {"controlPoint": 0, "punchTime": 1},
        {"controlPoint": 1, "punchTime": 115766},
        {"controlPoint": 2, "punchTime": 414638, "forced": true},
        {"controlPoint": 3, "punchTime": 548482},
        {"controlPoint": 4, "punchTime": 785489, "forced": true},
        {"controlPoint": 5, "punchTime": 872370, "forced": true},
        {"controlPoint": 6, "punchTime": 1008548, "forced": true},
        {"controlPoint": 7, "punchTime": 1197495, "forced": true},
        {"controlPoint": 8, "punchTime": 1404590, "forced": true},
        {"controlPoint": 9, "punchTime": 1720611, "forced": true},
        {"controlPoint": 10, "punchTime": 2016996, "forced": true},
        {"controlPoint": 11, "punchTime": 2314820, "forced": true},
        {"controlPoint": 12, "punchTime": 2339474}
      ],
      1,
      false,
      true,
      120,
      2,
    ];
    var actual = TrackModel.fromJson(json);
    expect(actual.id, 15405);
    expect(actual.nickname, "John");
    expect(actual.totalTimeInMilliseconds, 2339476);
    expect(actual.imported, false);
    expect(actual.courseFormat, ValidationOrder.FREE_ORDER);
    expect(actual.punchTimes.length, 13);
    expect(actual.score, 120);
    expect(actual.runCount, 2);
    expect(actual.isCheating, true);
  });

  test('Test TrackModel.fromJson case unsupervised preset order', () {
    var json = [
      15405,
      "John",
      2339476,
      [
        {"controlPoint": 0, "punchTime": 1},
        {"controlPoint": 1, "punchTime": 115766},
        {"controlPoint": 2, "punchTime": 414638, "forced": true},
        {"controlPoint": 3, "punchTime": 548482},
        {"controlPoint": 4, "punchTime": 785489, "forced": true},
        {"controlPoint": 5, "punchTime": 872370, "forced": true},
        {"controlPoint": 6, "punchTime": 1008548, "forced": true},
        {"controlPoint": 7, "punchTime": 1197495, "forced": true},
        {"controlPoint": 8, "punchTime": 1404590, "forced": true},
        {"controlPoint": 9, "punchTime": 1720611, "forced": true},
        {"controlPoint": 10, "punchTime": 2016996, "forced": true},
        {"controlPoint": 11, "punchTime": 2314820, "forced": true},
        {"controlPoint": 12, "punchTime": 2339474}
      ],
      2,
      false,
      true,
      120,
      2,
    ];
    var actual = TrackModel.fromJson(json);
    expect(actual.id, 15405);
    expect(actual.nickname, "John");
    expect(actual.totalTimeInMilliseconds, 2339476);
    expect(actual.imported, false);
    expect(actual.courseFormat, ValidationOrder.UNSUPERVISED_PRESET_ORDER);
    expect(actual.punchTimes.length, 13);
    expect(actual.score, 120);
    expect(actual.runCount, 2);
    expect(actual.isCheating, true);
  });

  test('Test countMissingPunches when empty', () {
    List<PunchTime> punchTimes = [];
    var actual = TrackModel.countMissingPunches_(punchTimes);
    expect(actual, 0);
  });

  test('Test countMissingPunches when all punched', () {
    List<PunchTime> punchTimes = [
      const PunchTime(1),
      const PunchTime(2),
    ];
    var actual = TrackModel.countMissingPunches_(punchTimes);
    expect(actual, 0);
  });

  test('Test countMissingPunches when missing punches', () {
    List<PunchTime> punchTimes = [
      const PunchTime(1),
      const PunchTime(-1),
      const PunchTime(2),
      const PunchTime(-1),
    ];
    var actual = TrackModel.countMissingPunches_(punchTimes);
    expect(actual, 2);
  });

  test('Test countForcedCheckpoints_', () {
    List<PunchTime> punchTimes = [];
    var actual = TrackModel.countForcedCheckpoints_(punchTimes);
    expect(actual, 0);
  });

  test('Test countForcedCheckpoints_', () {
    List<PunchTime> punchTimes = [
      const PunchTime(1, forced: false),
      const PunchTime(2, forced: true),
      const PunchTime(3, forced: false),
    ];
    var actual = TrackModel.countForcedCheckpoints_(punchTimes);
    expect(actual, 1);
  });
}
