import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

void main() {
  test('test distanceBetweenInMeter 1', () {
    var actual = GeodesicPoint.distanceBetweenInMeter(0, 0, 1, 0);
    expect(actual, moreOrLessEquals(111319.4, epsilon: 0.1));
  });

  test('test distanceBetweenInMeter 2', () {
    var actual = GeodesicPoint.distanceBetweenInMeter(0, 0, 0, 1);
    expect(actual, moreOrLessEquals(111319.4, epsilon: 0.1));
  });
}
