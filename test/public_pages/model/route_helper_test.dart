import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';
import 'package:vikazimut_website/public_pages/model/route_helper.dart';

void main() {
  test('Test findFirstIndexWhereDistanceExceedsThresholdFromStart_ ', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.0, 1),
      GeodesicPoint(0.00002, 0.0, 2),
      GeodesicPoint(0.0002, 0.0, 3),
      GeodesicPoint(0.00021, 0.0, 4),
      GeodesicPoint(0.00022, 0.0, 5),
      GeodesicPoint(0.000019, 0.0, 6),
      GeodesicPoint(0.000018, 0.0, 7),
      GeodesicPoint(0.0002, 0.0, 8), // not ok
      GeodesicPoint(0.00021, 0.0, 9), // not ok
    ];
    GeodesicPoint checkpoint = GeodesicPoint(0, 0);
    int actual = RouteHelper.findIndexOfLastPointInsideRadius_(route, checkpoint, 10);
    expect(actual, 8);
  });

  test('Test findFirstIndexWhereDistanceExceedsThresholdFromEnd_ ', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0), // not ok
      GeodesicPoint(0.001, 0.0, 1), // not ok
      GeodesicPoint(0.00201, 0.0, 2), // ok
      GeodesicPoint(0.002, 0.0, 3), // ok
    ];
    GeodesicPoint checkpoint = GeodesicPoint(0.002, 0);
    int actual = RouteHelper.findIndexOfFirstPointInsideRadius_(route, checkpoint, 10);
    expect(actual, 1);
  });

  test('Test splitTrackIntoAreas when too few points for area 2', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.0, 1),
      GeodesicPoint(0.00001, 0.0, 2),
      GeodesicPoint(0.00001, 0.0, 3),
      GeodesicPoint(0.00004, 0.0, 4),
      GeodesicPoint(0.10001, 0.0, 5),
      GeodesicPoint(0.20001, 0.0, 9),
      GeodesicPoint(0.20002, 0.0, 10),
      GeodesicPoint(0.20003, 0.0, 11),
      GeodesicPoint(0.20004, 0.0, 12),
    ];
    GeodesicPoint startCheckpoint = GeodesicPoint(0, 0);
    GeodesicPoint endCheckpoint = GeodesicPoint(0.20004, 0.0);
    List<List<GeodesicPoint>> actual = RouteHelper.splitTrackInto3Areas(route, startCheckpoint, endCheckpoint, 10);
    expect(actual[0].length, 5);
    expect(actual[1].length, 1);
    expect(actual[2].length, 4);
  });

  test('Test splitTrackIntoAreas when too few points for area 3', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.0, 1),
      GeodesicPoint(0.00004, 0.0, 2),
      GeodesicPoint(0.00004, 0.0, 3),
      GeodesicPoint(0.00004, 0.0, 4),
      GeodesicPoint(0.10001, 0.0, 5),
      GeodesicPoint(0.10002, 0.0, 6),
      GeodesicPoint(0.10003, 0.0, 7),
      GeodesicPoint(0.20004, 0.0, 12),
    ];
    GeodesicPoint startCheckpoint = GeodesicPoint(0, 0);
    GeodesicPoint endCheckpoint = GeodesicPoint(0.20004, 0.0);

    List<List<GeodesicPoint>> actual = RouteHelper.splitTrackInto3Areas(route, startCheckpoint, endCheckpoint, 10);
    expect(actual[0].length, 5);
    expect(actual[1].length, 3);
    expect(actual[2].length, 1);
  });

  test('Test splitTrackIntoAreas when not enough points', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.0, 1),
      GeodesicPoint(0.00002, 0.0, 4),
      GeodesicPoint(0.00003, 0.0, 5),
    ];
    GeodesicPoint startCheckpoint = GeodesicPoint(0, 0);
    GeodesicPoint endCheckpoint = GeodesicPoint(0.00003, 0.0);

    List<List<GeodesicPoint>> actual = RouteHelper.splitTrackInto3Areas(
      route,
      startCheckpoint,
      endCheckpoint,
      50,
    );
    expect(actual[0].length, 0);
    expect(actual[1].length, 1);
    expect(actual[2].length, 3);
  });

  test('Test splitTrackInto3Phases when nominal', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(0.00001, 0.0, 1),
      GeodesicPoint(0.00002, 0.0, 2),
      GeodesicPoint(0.00003, 0.0, 3),
      GeodesicPoint(0.00004, 0.0, 4),
      GeodesicPoint(0.10001, 0.0, 5),
      GeodesicPoint(0.10002, 0.0, 6),
      GeodesicPoint(0.10003, 0.0, 7),
      GeodesicPoint(0.10004, 0.0, 8),
      GeodesicPoint(0.20001, 0.0, 9),
      GeodesicPoint(0.20002, 0.0, 10),
      GeodesicPoint(0.20003, 0.0, 11),
      GeodesicPoint(0.20004, 0.0, 12),
    ];
    GeodesicPoint startCheckpoint = GeodesicPoint(0, 0);
    GeodesicPoint endCheckpoint = GeodesicPoint(0.20004, 0.0);

    List<List<GeodesicPoint>> actual = RouteHelper.splitTrackInto3Areas(route, startCheckpoint, endCheckpoint, 10);
    expect(actual[0].length, 5);
    expect(actual[0][0].timestampInMillisecond, 0);
    expect(actual[0][4].timestampInMillisecond, 4);

    expect(actual[1].length, 4);
    expect(actual[1][0].timestampInMillisecond, 5);
    expect(actual[1][3].timestampInMillisecond, 8);

    expect(actual[2].length, 4);
    expect(actual[2][0].timestampInMillisecond, 9);
    expect(actual[2][3].timestampInMillisecond, 12);
  });

  test('Test createPointAtGivenTime case nominal 1', () {
    GeodesicPoint point1 = GeodesicPoint(0, 0, 500, 0);
    GeodesicPoint point2 = GeodesicPoint(1, -1, 1500, 10);
    int timeInMs = 1000;
    final GeodesicPoint actual = RouteHelper.createPointAtGivenTime(point1, point2, timeInMs);
    expect(actual.latitude, 0.5);
    expect(actual.longitude, -0.5);
    expect(actual.altitude, 5);
    expect(actual.timestampInMillisecond, 1000);
  });

  test('Test createPointAtGivenTime case nominal 2', () {
    GeodesicPoint point1 = GeodesicPoint(0, 0, 200, 0);
    GeodesicPoint point2 = GeodesicPoint(1, -1, 1200, 10);
    int timeInMs = 450;
    final GeodesicPoint actual = RouteHelper.createPointAtGivenTime(point1, point2, timeInMs);
    expect(actual.latitude, 0.25);
    expect(actual.longitude, -0.25);
    expect(actual.altitude, 2.5);
    expect(actual.timestampInMillisecond, 450);
  });

  test('Test computeAverageDistanceAndTime', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(-1, 1, 1000, 10),
      GeodesicPoint(-2, 2, 2000, 10),
    ];
    final (distance, time) = RouteHelper.computeAverageDistanceAndTime_(route);
    expect(distance, moreOrLessEquals(314827, epsilon: 0.1));
    expect(time, 2000);
  });

  test('Test computeAverageSpeedEffortInKmh when empty', () {
    List<GeodesicPoint> route = [];
    final double actual = RouteHelper.computeAverageSpeedEffortInKmh(route);
    expect(actual, 0);
  });

  test('Test computeAverageSpeedEffortInKmh', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(-0.0001, 0.0001, 2000), // 30.68 km/h during 2 s
      GeodesicPoint(-0.0001, 0.0002, 3000), // 34.20 km/h during 1 s
      GeodesicPoint(-0.0002, 0.0002, 4000), // 40.07 km/h during 1 s
    ];
    double expected = (30.68 * 2 + 34.20 + 40.07) / 4;
    final double actual = RouteHelper.computeAverageSpeedEffortInKmh(route);
    expect(actual, closeTo(expected, 0.1));
  });

  test('Test computeInstantaneousSpeedsInMs', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 0.001, 1000, 100),
      GeodesicPoint(0.001, 0.001, 2000, 100),
    ];
    List<double> actual = RouteHelper.computeInstantaneousEffortSpeedsInKmh(route);
    expect(actual, [400.7501668557848, 400.7501668557848]);
  });

  test('Test filterSpeeds when empty', () {
    List<double> actual = RouteHelper.filterSpeeds_([], []);
    expect(actual.isEmpty, true);
  });

  test('Test filterSpeeds when constant', () {
    List<double> speeds = [10, 10, 10, 10, 10];
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 1000),
      GeodesicPoint(0, 0, 2000),
      GeodesicPoint(0, 0, 3000),
      GeodesicPoint(0, 0, 4000),
      GeodesicPoint(0, 0, 5000),
      GeodesicPoint(0, 0, 6000),
    ];
    List<double> actual = RouteHelper.filterSpeeds_(speeds, route);
    expect(actual.length, speeds.length);
    expect(actual[3], 10);
  });

  test('Test filterSpeeds peak', () {
    List<double> speeds = [10, 20, 30, 20, 10];
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 1000),
      GeodesicPoint(0, 0, 2000),
      GeodesicPoint(0, 0, 3000),
      GeodesicPoint(0, 0, 4000),
      GeodesicPoint(0, 0, 5000),
      GeodesicPoint(0, 0, 6000),
    ];
    List<double> actual = RouteHelper.filterSpeeds_(speeds, route);
    expect(actual.length, speeds.length);
    expect(actual[0], moreOrLessEquals(13.3, epsilon: 0.1));
    expect(actual[4], moreOrLessEquals(13.3, epsilon: 0.1));
    expect(actual[1], moreOrLessEquals(20, epsilon: 0.1));
    expect(actual[3], moreOrLessEquals(20, epsilon: 0.1));
    expect(actual[2], moreOrLessEquals(23.3, epsilon: 0.1));
  });
}
