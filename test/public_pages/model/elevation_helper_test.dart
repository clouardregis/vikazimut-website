import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/elevation_helper.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

void main() {
  test('Mean filtering when flat', () {
    final List<double> altitudes = [0, 0, 0, 102, 102, 102, 102, 102, 102, 102, 102];
    List<GeodesicPoint> waypoints = _buildWaypointFromAltitudes(altitudes);
    ElevationHelper.filterWithLowPassToRemoveSmallHolesOrHills_(waypoints, 2);
    final List<double> expected = [0, 20.4, 40.8, 61.2, 81.6, 102, 102, 102, 102, 102, 102];
    for (int i = 0; i < altitudes.length; i++) {
      expect(waypoints[i].altitude, moreOrLessEquals(expected[i], epsilon: 0.01));
    }
  });

  test('Mean filtering when nominal', () {
    final List<double> altitudes = [102, 108, 109, 108, 106, 108, 110, 110];
    List<GeodesicPoint> waypoints = _buildWaypointFromAltitudes(altitudes);
    ElevationHelper.filterWithLowPassToRemoveSmallHolesOrHills_(waypoints, 2);
    final List<double> expected = [104.6, 105.8, 106.6, 107.8, 108.2, 108.4, 108.8, 109.6];
    for (var i = 0; i < waypoints.length; i++) {
      expect(waypoints[i].altitude, moreOrLessEquals(expected[i], epsilon: 0.01));
    }
  });

  test('Gaussian filtering when flat', () {
    final List<double> altitudes = [102, 102, 102, 102, 102, 102, 102, 102];
    List<GeodesicPoint> waypoints = _buildWaypointFromAltitudes(altitudes);
    ElevationHelper.gaussianFilteringToFilterElevationCurve_(waypoints, 1);
    final List<double> expected = [102, 102, 102, 102, 102, 102, 102, 102];
    for (var i = 0; i < waypoints.length; i++) {
      expect(waypoints[i].altitude, expected[i]);
    }
  });

  test('Gaussian filtering when nominal', () {
    final List<double> altitudes = [102, 108, 109, 108, 106, 108, 110, 110];
    List<GeodesicPoint> waypoints = _buildWaypointFromAltitudes(altitudes);
    ElevationHelper.gaussianFilteringToFilterElevationCurve_(waypoints, 2);
    final List<double> expected = [103.21, 106.99, 108.55, 107.81, 106.82, 108.01, 109.58, 109.99];
    for (var i = 0; i < waypoints.length; i++) {
      expect(waypoints[i].altitude, moreOrLessEquals(expected[i], epsilon: 0.1));
    }
  });

  test('Gaussian filtering when nominal 2', () {
    final List<double> altitudes = [0, 1, 2, 3, 4, 5, 6];
    List<GeodesicPoint> waypoints = _buildWaypointFromAltitudes(altitudes);
    ElevationHelper.gaussianFilteringToFilterElevationCurve_(waypoints, 3);
    final List<double> expected = [0.36, 1.06, 2.0, 2.99, 3.99, 4.93, 5.63];
    for (var i = 0; i < waypoints.length; i++) {
      expect(waypoints[i].altitude, moreOrLessEquals(expected[i], epsilon: 0.01));
    }
  });

  test('Compute gaussian filter', () {
    int halfSize = 3;
    List<double> actualFilter = List<double>.filled(7, 0, growable: false);
    List<double> expectedFilter = [0.55, 6.76, 30.32, 50, 30.32, 6.76, 0.55];
    double gaussianSum = ElevationHelper.computeGaussianFilter_(halfSize, actualFilter);
    expect(gaussianSum, moreOrLessEquals(125.29, epsilon: 0.01));
    for (var i = 0; i < actualFilter.length; i++) {
      expect(actualFilter[i], moreOrLessEquals(expectedFilter[i], epsilon: 0.1));
    }
  });

  test('Test compute elevation gain in preset order nominal when no elevations', () {
    final List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(1, 1, 1, ElevationHelper.NO_ELEVATION), // leg 1
      GeodesicPoint(2, 2, 2, ElevationHelper.NO_ELEVATION), // Leg 2
      GeodesicPoint(3, 3, 3, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(4, 4, 4, ElevationHelper.NO_ELEVATION), // leg3
      GeodesicPoint(5, 5, 5, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(6, 6, 6, ElevationHelper.NO_ELEVATION) // Leg 3
    ];
    double actual = ElevationHelper.calculateCumulativeElevationGain(waypoints);
    expect(true, actual < 0);
  });

  test('Test compute elevation gain nominal', () {
    final List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0, 0),
      GeodesicPoint(1, 1, 1, 1),
      GeodesicPoint(2, 2, 2, 2),
      GeodesicPoint(3, 3, 3, 3),
      GeodesicPoint(4, 4, 4, 4),
      GeodesicPoint(5, 5, 5, 5),
      GeodesicPoint(6, 6, 6, 6),
      GeodesicPoint(6, 6, 6, 7),
      GeodesicPoint(6, 6, 6, 8),
    ];
    double actual = ElevationHelper.calculateCumulativeElevationGain(waypoints);
    expect(actual, 8);
  });

  test('Test compute elevation gain when null', () {
    final List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0, 8),
      GeodesicPoint(1, 1, 1, 7),
      GeodesicPoint(2, 2, 2, 6),
      GeodesicPoint(3, 3, 3, 5),
      GeodesicPoint(4, 4, 4, 4),
      GeodesicPoint(5, 5, 5, 3),
      GeodesicPoint(6, 6, 6, 2),
      GeodesicPoint(7, 6, 6, 1),
      GeodesicPoint(8, 6, 6, 0),
    ];
    double actual = ElevationHelper.calculateCumulativeElevationGain(waypoints);
    expect(actual, 0);
  });
}

_buildWaypointFromAltitudes(List<double> altitudes) {
  return List<GeodesicPoint>.generate(altitudes.length, (index) => GeodesicPoint(0, 0, 0, altitudes[index]));
}
