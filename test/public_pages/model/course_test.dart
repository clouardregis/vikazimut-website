import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/model/course.dart';

void main() {
  test('Test Course.fromJson', () {
    var json = {"id": 546, "name": "HATE LARCAY VIOLET 8KM", "latitude": "47.337373", "longitude": "0.754034", "length": 7980};
    var actual = Course.fromJson(json);
    expect(actual.latitude, 47.337373);
    expect(actual.longitude, 0.754034);
    expect(actual.name, "HATE LARCAY VIOLET 8KM");
    expect(actual.id, 546);
  });
}
