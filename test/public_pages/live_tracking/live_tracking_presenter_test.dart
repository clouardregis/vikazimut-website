import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/public_pages/live_tracking/live_tracking_presenter.dart';
import 'package:vikazimut_website/public_pages/live_tracking/orienteer_item.dart';
import 'package:vikazimut_website/public_pages/live_tracking/settings_view.dart';
import 'package:vikazimut_website/public_pages/model/geodesic_point.dart';

void main() {
  test('Test getNewOrienteers', () {
    LiveTrackingPresenter presenter = LiveTrackingCreatorPresenter(0);
    List<OrienteerItem> orienteerList = [
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 3, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];

    List<OrienteerItem> newOrienteerList = [
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 4, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 5, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];
    var newOrienteers = presenter.getNewOrUpdateOrienteers_(newOrienteerList, orienteerList);
    expect(newOrienteers.length, 2);
    expect(newOrienteers[0].id, 4);
    expect(newOrienteers[1].id, 5);
  });

  test('Test getFinishers', () {
    LiveTrackingPresenter presenter = LiveTrackingCreatorPresenter(0);
    List<OrienteerItem> orienteerList = [
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 3, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];

    List<OrienteerItem> newOrienteerList = [
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 4, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];
    var newOrienteers = presenter.getFinishers_(newOrienteerList, orienteerList);
    expect(newOrienteers.length, 1);
    expect(newOrienteers[0].id, 3);
  });

  test('Test remove finishers', () {
    LiveTrackingPresenter presenter = LiveTrackingCreatorPresenter(0);
    presenter.orienteers.addAll([
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 3, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ]);

    List<OrienteerItem> finishers = [
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];
    presenter.removeFinishers_(finishers);
    expect(presenter.orienteers.length, 2);
    expect(presenter.orienteers[0].id, 0);
    expect(presenter.orienteers[1].id, 3);
  });

  test('Test add new orienteers', () {
    LiveTrackingPresenter presenter = LiveTrackingCreatorPresenter(0);
    presenter.orienteers.addAll([
      OrienteerItem(id: 0, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 1, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 2, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 3, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ]);

    List<OrienteerItem> beginners = [
      OrienteerItem(id: 4, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 5, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ];
    presenter.addNewOrienteers_(beginners);
    expect(presenter.orienteers.length, 6);
    expect(presenter.orienteers[0].id, 0);
    expect(presenter.orienteers[3].id, 3);
    expect(presenter.orienteers[5].id, 5);
  });

  test('Test dismiss orienteer', () {
    LiveTrackingCreatorPresenter presenter = LiveTrackingCreatorPresenter(0);
    presenter.orienteers.addAll([
      OrienteerItem(id: 10, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 11, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 12, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
      OrienteerItem(id: 13, currentPositions: [GeodesicPoint(0, 0)], nickname: '', startDateInMilliseconds: 0),
    ]);

    presenter.dismissOrienteer_(2);
    expect(presenter.orienteers.length, 4);
    expect(presenter.orienteers[2].id, 13);
    expect(presenter.orienteers[3].id, 12);
    expect(presenter.orienteers[3].hasFinished, true);
  });

  test('Test sort orienteers', () {
    LiveTrackingCreatorPresenter presenter = LiveTrackingCreatorPresenter(0);
    List<OrienteerItem> orienteers = [
      OrienteerItem(
        currentPositions: [],
        id: 1,
        nickname: "foo1",
        startDateInMilliseconds: 0,
        score: 2,
      ),
      OrienteerItem(
        currentPositions: [],
        id: 2,
        nickname: "foo2",
        startDateInMilliseconds: 0,
        score: 3,
      ),
      OrienteerItem(
        currentPositions: [],
        id: 3,
        nickname: "foo3",
        startDateInMilliseconds: 0,
        score: 1,
      )
    ];
    orienteers[0].totalTimeInMilliseconds = 2;
    orienteers[1].totalTimeInMilliseconds = 1;
    orienteers[2].totalTimeInMilliseconds = 3;
    presenter.liveTrackingPreferences.sortCriteria = SortCriteria.time;
    presenter.sortOrienteers(orienteers);
    expect(orienteers[0].id, 3);
    expect(orienteers[1].id, 1);
    expect(orienteers[2].id, 2);
    presenter.liveTrackingPreferences.sortCriteria = SortCriteria.score;
    presenter.sortOrienteers(orienteers);
    expect(orienteers[0].id, 2);
    expect(orienteers[1].id, 1);
    expect(orienteers[2].id, 3);
  });
}
