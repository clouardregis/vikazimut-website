import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/constants.dart';

void main() {
  test('test server address', () {
    expect(SERVER, "https://vikazimut.vikazim.fr");
  });
}
