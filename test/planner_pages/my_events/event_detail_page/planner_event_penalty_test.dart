import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_events/event_detail_page/planner_event_penalty.dart';

void main() {
  test('test penalty.toJson', () {
    var penalty = Penalty();
    penalty.eventCourseId = 1;
    penalty.participantId = 2;
    penalty.missingPunchPenalty = 3;
    penalty.overtimePenalty = 4;
    var result = penalty.toJson();
    expect(result.toString(), '{"event_course_id":1,"participant_id":2,"missing_punch_penalty":3,"overtime_penalty":4}');
  });
}
