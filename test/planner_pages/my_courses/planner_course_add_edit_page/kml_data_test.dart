import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/common/lat_lon_box.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/iof_xml_data.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/kml_data.dart';

void main() {
  test('Test to xml', () {
    var kmlData = KmlData(georeferencing: const LatLonBox(north: 1, south: 2, east: 3, west: 4, rotation: 5), courseName: "", imageFilename: "");
    var expected = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Folder>
	<name></name>
	<GroundOverlay>
		<name></name>
		<Icon>
			<href>files/tile_0_0.jpg</href>
		</Icon>
		<LatLonBox>
			<north>1.0</north>
			<south>2.0</south>
			<east>3.0</east>
			<west>4.0</west>
			<rotation>5.0</rotation>
		</LatLonBox>
	</GroundOverlay>
</Folder>
</kml>""";
    expect(kmlData.toXml(), IofXmlData.minifyXmlContent_(expected));
  });
}
