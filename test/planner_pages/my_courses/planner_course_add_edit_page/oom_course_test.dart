import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut_website/planner_pages/my_courses/planner_course_add_edit_page/oom_course.dart';
import 'package:xml/xml.dart';

void main() {
  test('Test convert OOM XML to iof xml', () {
    String xmlContents = """<?xml version="1.0" encoding="UTF-8"?>
<CourseDate xmlns="http://www.orienteering.org/datastandard/3.0" iofVersion="3.0" creator="OpenOrienteering Mapper 0.9.5" createTime="2023-06-24T12:24:13">
    <Event>
        <Name>Test OOM</Name>
    </Event>
    <RaceCourseData>
        <Control>
            <Id>S1</Id>
            <CourseControl lng="-0.4272015" lat="49.2810387"/>
        </Control>
        <Control>
            <Id>31</Id>
            <CourseControl lng="-0.4260013" lat="49.2792446"/>
        </Control>
        <Control>
            <Id>32</Id>
            <CourseControl lng="-0.4243591" lat="49.2793005"/>
        </Control>
        <Control>
            <Id>33</Id>
            <CourseControl lng="-0.4225964" lat="49.2802065"/>
        </Control>
        <Control>
            <Id>F1</Id>
            <CourseControl lng="-0.4260701" lat="49.281288"/>
        </Control>
        <Course>
            <Name>Unnamed course</Name>
            <CourseControl type="Start">
                <Control>S1</Control>
            </CourseControl>
            <CourseControl type="Control">
                <Control>31</Control>
            </CourseControl>
            <CourseControl type="Control">
                <Control>32</Control>
            </CourseControl>
            <CourseControl type="Control">
                <Control>33</Control>
            </CourseControl>
            <CourseControl type="Finish">
                <Control>F1</Control>
            </CourseControl>
        </Course>
    </RaceCourseData>
</CourseDate>
""";
    final result = OOMXmlCourse.convertToIofSchema(xmlContents);
    final XmlDocument document = XmlDocument.parse(result);
    final courseData = document.getElement("CourseData");
    expect(courseData, isNotNull);

    final position = document.findAllElements("Position");
    expect(position.length, isNonZero);

    final map = document.findAllElements("Map");
    expect(map.length, 1);
  });
}
