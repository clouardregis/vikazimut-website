# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/index.html),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.15.0] 2025-20-02
Added ski-o
Added upload KMZ in course modification
Revised time table in accordance with the app

## [2.14.10] 2025-21-02
Revised all route analysis tabs

## [2.13.7] 2025-21-01
Revised live tracking
Updated Flutter version

## [2.13.2] 2024-16-12
Added facility to hide tracks by the planner

## [2.13.1] 2024-12-12
Revised live tracking

## [2.13.0] 2024-11-17
Added physical performance tab

## [2.12.5] 2024-11-04
Added Portugues translation

## [2.12.0] 2023-07-25

Added performance analysis

## [2.11.0] 2023-07-25

Revised mobile version
Added cumulative time in time page
Revised animation with new leg selector for free order mode.
Revised event UI
Added route analysis
Added new facilities for adding participants in events

## [2.10.0] 2023-07-25

Added facility to register multiple participants from text file
Revised profile

## [2.7.0] 2023-12-26

- Updated IGN gateway for 3D view rendering

## [2.7.0] 2023-12-26

- Added live tracking

## [2.6.9] 2023-10-02

- Add OpenStreetMap underneath animation
- Refactored completely the code

## [2.6.0] 2023-07-31

- Fixed bug on create course
- Refactored completely the code

## [2.5.2] 2023-06-14

- Added 3D view

## [2.0.0] 2023-01-21

- New site website completely rewritten in Flutter

## [1.0.6+32] 2022-08-27

- Added Portuguese translation